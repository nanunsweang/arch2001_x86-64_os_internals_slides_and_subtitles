1
00:00:00,000 --> 00:00:03,840
well what did it look like for me the

2
00:00:01,520 --> 00:00:05,839
present bit was set the read/write bit

3
00:00:03,840 --> 00:00:08,400
was set so it was writable the user/

4
00:00:05,839 --> 00:00:12,160
supervisor was 0 so it was kernel

5
00:00:08,400 --> 00:00:15,200
XD bit was set so it was non-executable

6
00:00:12,160 --> 00:00:18,640
dirty and global and with that

7
00:00:15,200 --> 00:00:18,960
congratulations you did it I'm proud of

8
00:00:18,640 --> 00:00:21,039
you

9
00:00:18,960 --> 00:00:23,119
apocalypse is proud of you you did it

10
00:00:21,039 --> 00:00:24,720
you made it through this dry and barren

11
00:00:23,119 --> 00:00:27,039
desert you proved yourself

12
00:00:24,720 --> 00:00:28,000
one of the fit you're wiser you're

13
00:00:27,039 --> 00:00:30,640
stronger

14
00:00:28,000 --> 00:00:32,640
is this class secretly just a mechanism

15
00:00:30,640 --> 00:00:33,600
for weeding out people and finding out

16
00:00:32,640 --> 00:00:35,840
who are weak

17
00:00:33,600 --> 00:00:37,600
maybe I don't know I'm not saying I

18
00:00:35,840 --> 00:00:39,120
won't tell if you don't

19
00:00:37,600 --> 00:00:41,440
so what did you learn in this section

20
00:00:39,120 --> 00:00:43,440
well you dug into all of the entries

21
00:00:41,440 --> 00:00:46,399
and you understood exactly what is

22
00:00:43,440 --> 00:00:48,800
inside these oh so interesting

23
00:00:46,399 --> 00:00:50,840
page tables which are processed by the

24
00:00:48,800 --> 00:00:53,840
memory management unit

25
00:00:50,840 --> 00:00:53,840
congratulations

