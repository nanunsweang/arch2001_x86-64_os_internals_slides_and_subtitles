1
00:00:00,160 --> 00:00:03,120
In this section we're going to talk

2
00:00:01,439 --> 00:00:05,520
about something called canonical

3
00:00:03,120 --> 00:00:06,080
addresses and it has to do with the idea

4
00:00:05,520 --> 00:00:08,240
that

5
00:00:06,080 --> 00:00:10,960
although we ostensibly have 64-bit

6
00:00:08,240 --> 00:00:11,679
systems in reality we can't use all the

7
00:00:10,960 --> 00:00:13,200
bits

8
00:00:11,679 --> 00:00:15,120
now there's different ways that the bit

9
00:00:13,200 --> 00:00:16,400
space could be divided up

10
00:00:15,120 --> 00:00:18,640
but I just wanted to show you this

11
00:00:16,400 --> 00:00:19,680
picture up front so that when I explain

12
00:00:18,640 --> 00:00:20,960
the next sections

13
00:00:19,680 --> 00:00:23,600
you have something in your head to

14
00:00:20,960 --> 00:00:25,840
visualize it and the basic point is

15
00:00:23,600 --> 00:00:27,840
the address space the 64-bit linear

16
00:00:25,840 --> 00:00:29,599
address space is broken up such that the

17
00:00:27,840 --> 00:00:32,000
uppermost range is valid

18
00:00:29,599 --> 00:00:34,239
and the lower most range is valid and

19
00:00:32,000 --> 00:00:36,960
the middle bits for some number of bits

20
00:00:34,239 --> 00:00:39,040
is going to be invalid so as I mentioned

21
00:00:36,960 --> 00:00:41,280
on today's processors we don't actually

22
00:00:39,040 --> 00:00:43,680
have the capability to access either a

23
00:00:41,280 --> 00:00:45,200
64-bit linear address space or 64-bit

24
00:00:43,680 --> 00:00:47,039
physical address space

25
00:00:45,200 --> 00:00:49,039
only if you have the absolute newest

26
00:00:47,039 --> 00:00:49,840
hardware would you have the capability

27
00:00:49,039 --> 00:00:53,680
to access

28
00:00:49,840 --> 00:00:55,920
57-bit linear and 52-bit physical

29
00:00:53,680 --> 00:00:56,879
so the canonical addresses are ones in

30
00:00:55,920 --> 00:01:00,559
which the upper

31
00:00:56,879 --> 00:01:02,800
N unused bits are always 0 or 1

32
00:01:00,559 --> 00:01:05,119
so if you had a new processor and you

33
00:01:02,800 --> 00:01:07,280
supported 57 bits and your operating

34
00:01:05,119 --> 00:01:09,520
system used it which as I said

35
00:01:07,280 --> 00:01:11,040
I don't know anyone who does right now

36
00:01:09,520 --> 00:01:14,080
then the bits beyond

37
00:01:11,040 --> 00:01:15,040
57 would all have to always be 0 or

38
00:01:14,080 --> 00:01:17,520
always be 1

39
00:01:15,040 --> 00:01:19,119
and the processor would just sign extend

40
00:01:17,520 --> 00:01:23,360
the 57th bit

41
00:01:19,119 --> 00:01:25,680
or 56th bit if you're a zero indexing

42
00:01:23,360 --> 00:01:28,159
to make it so that that 1 or that 0

43
00:01:25,680 --> 00:01:31,040
is set for all of the uppermost bits

44
00:01:28,159 --> 00:01:33,439
so from intel's 5 level paging paper

45
00:01:31,040 --> 00:01:36,640
it says a 48-bit canonical address

46
00:01:33,439 --> 00:01:39,920
is canonical if bits 47 so that's

47
00:01:36,640 --> 00:01:41,600
zero indexed 47 through 63 are all

48
00:01:39,920 --> 00:01:42,640
identical they all have to be 0 they

49
00:01:41,600 --> 00:01:45,840
all have to be 1

50
00:01:42,640 --> 00:01:48,079
same thing with 57 bit bits 56 through

51
00:01:45,840 --> 00:01:51,119
63 have to be all identical

52
00:01:48,079 --> 00:01:53,040
so in terms of the 48-bit linear address

53
00:01:51,119 --> 00:01:56,320
space which is used for

54
00:01:53,040 --> 00:01:58,719
4 level paging that would mean that

55
00:01:56,320 --> 00:02:00,320
the bits from the addresses from the

56
00:01:58,719 --> 00:02:03,280
linear address space from 0

57
00:02:00,320 --> 00:02:03,759
all the way up to zero zero zero zero

58
00:02:03,280 --> 00:02:07,680
seven

59
00:02:03,759 --> 00:02:08,000
ffff are all part of the lower range and

60
00:02:07,680 --> 00:02:11,680
so

61
00:02:08,000 --> 00:02:14,800
this 7 is one in which bit

62
00:02:11,680 --> 00:02:17,120
index 46 is set to

63
00:02:14,800 --> 00:02:18,000
0 and then all the rest of the bits

64
00:02:17,120 --> 00:02:21,360
have to be

65
00:02:18,000 --> 00:02:24,480
set to 0 whereas this high range

66
00:02:21,360 --> 00:02:27,920
is one in bit index 47

67
00:02:24,480 --> 00:02:29,440
sorry 46 is set to 1 and then all of

68
00:02:27,920 --> 00:02:31,760
the upper bits have to be 1

69
00:02:29,440 --> 00:02:34,000
so this is what gives us our red green

70
00:02:31,760 --> 00:02:36,000
chart here in which there are two ranges

71
00:02:34,000 --> 00:02:38,400
that are canonical addresses

72
00:02:36,000 --> 00:02:40,480
the lower range and the upper range the

73
00:02:38,400 --> 00:02:42,959
lower range or the ones starting at 0

74
00:02:40,480 --> 00:02:44,480
all the way up to the point where the

75
00:02:42,959 --> 00:02:47,519
max of everything is ones

76
00:02:44,480 --> 00:02:49,280
except for bit index 46

77
00:02:47,519 --> 00:02:51,840
and the upper range is one where

78
00:02:49,280 --> 00:02:55,120
everything is zeros all the way up to

79
00:02:51,840 --> 00:02:56,400
index 46 is 1 and these upper things

80
00:02:55,120 --> 00:02:58,640
are extended to 1

81
00:02:56,400 --> 00:02:59,840
and then all the way up to all ones so

82
00:02:58,640 --> 00:03:02,480
by the same token

83
00:02:59,840 --> 00:03:03,760
a 57-bit canonical address would just be

84
00:03:02,480 --> 00:03:06,800
one in which

85
00:03:03,760 --> 00:03:08,640
bits 56 to 63 must all be the same and

86
00:03:06,800 --> 00:03:10,159
so that looks like this you can see it's

87
00:03:08,640 --> 00:03:12,560
obviously you know before it was

88
00:03:10,159 --> 00:03:14,000
zero zero zero zero so there were 4

89
00:03:12,560 --> 00:03:16,959
zeros followed by 7

90
00:03:14,000 --> 00:03:18,159
f's and here it's zero zero and then all

91
00:03:16,959 --> 00:03:21,599
f's

92
00:03:18,159 --> 00:03:24,400
and the upper range starts at ff

93
00:03:21,599 --> 00:03:25,120
zero zero zero all the way up to all f's

94
00:03:24,400 --> 00:03:27,760
so here

95
00:03:25,120 --> 00:03:28,720
it's using up more of the 64-bit address

96
00:03:27,760 --> 00:03:31,120
space so this

97
00:03:28,720 --> 00:03:32,000
range is wider this range is wider but

98
00:03:31,120 --> 00:03:34,159
there are still

99
00:03:32,000 --> 00:03:35,519
some range of addresses in the middle

100
00:03:34,159 --> 00:03:37,120
that are not valid

101
00:03:35,519 --> 00:03:39,599
and for which the processor will throw

102
00:03:37,120 --> 00:03:41,680
an error specifically it will throw a

103
00:03:39,599 --> 00:03:43,440
general protection fault

104
00:03:41,680 --> 00:03:44,640
so it's interesting that actually

105
00:03:43,440 --> 00:03:45,840
there's been a few types of

106
00:03:44,640 --> 00:03:47,360
vulnerabilities that have been

107
00:03:45,840 --> 00:03:50,159
discovered over the years

108
00:03:47,360 --> 00:03:52,239
due to the behavior of the system when

109
00:03:50,159 --> 00:03:54,959
general protection faults are thrown

110
00:03:52,239 --> 00:03:55,920
due to accesses using non-canonical

111
00:03:54,959 --> 00:03:58,959
addresses

112
00:03:55,920 --> 00:04:00,480
so I'll put links on the website to the

113
00:03:58,959 --> 00:04:02,799
details and write-ups about these

114
00:04:00,480 --> 00:04:04,319
various CVEs and they really just had to

115
00:04:02,799 --> 00:04:05,680
do with the fact that the

116
00:04:04,319 --> 00:04:07,680
you know if you go look at the pseudo

117
00:04:05,680 --> 00:04:09,280
code for a given assembly instruction if

118
00:04:07,680 --> 00:04:10,319
you looked at sysret, if you looked at

119
00:04:09,280 --> 00:04:12,080
pop ss

120
00:04:10,319 --> 00:04:13,760
they had certain behaviors and certain

121
00:04:12,080 --> 00:04:15,040
erroring out cases for general

122
00:04:13,760 --> 00:04:17,519
protection faults

123
00:04:15,040 --> 00:04:19,280
and particular ways that an attacker

124
00:04:17,519 --> 00:04:21,440
could take advantage of this by setting

125
00:04:19,280 --> 00:04:24,160
addresses as non-canonical

126
00:04:21,440 --> 00:04:25,759
would lead to exception handling where

127
00:04:24,160 --> 00:04:27,199
attacker-controlled values were being

128
00:04:25,759 --> 00:04:28,880
used for some registers

129
00:04:27,199 --> 00:04:30,560
and so that was the general protection

130
00:04:28,880 --> 00:04:32,960
fault as mentioned before

131
00:04:30,560 --> 00:04:35,360
in the interrupt section it's interrupt

132
00:04:32,960 --> 00:04:37,280
13.

133
00:04:35,360 --> 00:04:38,960
it's a general sort of catch-all that's

134
00:04:37,280 --> 00:04:40,720
used for all sorts of different types of

135
00:04:38,960 --> 00:04:42,240
errors when assembly instructions are

136
00:04:40,720 --> 00:04:44,000
used in weird ways you'll see it all

137
00:04:42,240 --> 00:04:46,560
over the place if you go look at the

138
00:04:44,000 --> 00:04:48,080
details of assembly instructions but

139
00:04:46,560 --> 00:04:50,479
here is the thing that I really want you

140
00:04:48,080 --> 00:04:52,400
to take away from canonical addresses

141
00:04:50,479 --> 00:04:54,160
it is the fact that most operating

142
00:04:52,400 --> 00:04:57,440
systems place the kernel

143
00:04:54,160 --> 00:05:00,400
in the high range and place userspace

144
00:04:57,440 --> 00:05:00,720
in the low range consequently if you

145
00:05:00,400 --> 00:05:03,360
just

146
00:05:00,720 --> 00:05:04,160
look at the top bits of an address if

147
00:05:03,360 --> 00:05:06,639
it's f

148
00:05:04,160 --> 00:05:08,400
you can generally say that is some sort

149
00:05:06,639 --> 00:05:09,919
of kernel address that is some sort of

150
00:05:08,400 --> 00:05:11,680
kernel point or some sort of kernel

151
00:05:09,919 --> 00:05:14,479
function pointer and if the

152
00:05:11,680 --> 00:05:16,560
top bits are all zeros then generally

153
00:05:14,479 --> 00:05:19,600
speaking for most operating systems that

154
00:05:16,560 --> 00:05:22,000
is expected to be a user space address

155
00:05:19,600 --> 00:05:23,919
so this can be useful when for instance

156
00:05:22,000 --> 00:05:24,720
an attacker is trying to compromise the

157
00:05:23,919 --> 00:05:26,320
kernel

158
00:05:24,720 --> 00:05:28,800
if they're trying to look for memory

159
00:05:26,320 --> 00:05:30,639
disclosure vulnerabilities in order to

160
00:05:28,800 --> 00:05:32,800
you know leak information about kernel

161
00:05:30,639 --> 00:05:34,080
address space layout randomization

162
00:05:32,800 --> 00:05:36,240
or if they're trying to find you know

163
00:05:34,080 --> 00:05:38,800
function pointers to overwrite

164
00:05:36,240 --> 00:05:39,600
knowledge of the fact that addresses in

165
00:05:38,800 --> 00:05:42,240
kernel

166
00:05:39,600 --> 00:05:42,639
tend to start with f's is a nice thing

167
00:05:42,240 --> 00:05:46,560
to

168
00:05:42,639 --> 00:05:46,560
give the attacker information

