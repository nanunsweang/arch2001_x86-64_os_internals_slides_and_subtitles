1
00:00:00,080 --> 00:00:03,600
all right how did I interpret my entry

2
00:00:02,000 --> 00:00:04,560
and how should you have interpreted

3
00:00:03,600 --> 00:00:07,759
yours

4
00:00:04,560 --> 00:00:10,559
well present bit is equal to 1 on mine

5
00:00:07,759 --> 00:00:11,040
you can see that right there XD is equal

6
00:00:10,559 --> 00:00:12,480
to 1

7
00:00:11,040 --> 00:00:14,559
finally we see something with the most

8
00:00:12,480 --> 00:00:17,680
significant bit set

9
00:00:14,559 --> 00:00:19,840
read/write is 0 so this is read-only

10
00:00:17,680 --> 00:00:22,400
you can see an R here instead of the D

11
00:00:19,840 --> 00:00:26,000
uh W rather we've seen before

12
00:00:22,400 --> 00:00:28,320
U/S it is again kernel K

13
00:00:26,000 --> 00:00:30,080
and PS is equal to 1 in this

14
00:00:28,320 --> 00:00:33,200
particular thing so bit 7

15
00:00:30,080 --> 00:00:35,920
you know index in tutudu 0xa

16
00:00:33,200 --> 00:00:37,200
is 1010 so bit 7 is

17
00:00:35,920 --> 00:00:40,000
equal to 1

18
00:00:37,200 --> 00:00:42,079
so PS is 1 large page this means it's

19
00:00:40,000 --> 00:00:43,280
a 2 megabyte page to answer this

20
00:00:42,079 --> 00:00:45,039
question right here

21
00:00:43,280 --> 00:00:47,280
so if it's 2 megabyte page we are

22
00:00:45,039 --> 00:00:48,000
interpreting it according to this data

23
00:00:47,280 --> 00:00:50,399
structure

24
00:00:48,000 --> 00:00:51,600
and that means there is a G for global

25
00:00:50,399 --> 00:00:54,719
present there is a D

26
00:00:51,600 --> 00:00:58,160
for dirty or not so is G

27
00:00:54,719 --> 00:00:58,800
set yes G is set this is a global page

28
00:00:58,160 --> 00:01:02,239
so it's

29
00:00:58,800 --> 00:01:05,360
set to stick in the TLB cache

30
00:01:02,239 --> 00:01:08,560
between different CR3 changes and

31
00:01:05,360 --> 00:01:09,920
D is 0 to indicate this is not dirty

32
00:01:08,560 --> 00:01:11,840
now as you are looking at these most

33
00:01:09,920 --> 00:01:12,159
significant bits in order to see that

34
00:01:11,840 --> 00:01:14,640
the

35
00:01:12,159 --> 00:01:15,200
XD bit is set you may have noticed that

36
00:01:14,640 --> 00:01:18,320
this

37
00:01:15,200 --> 00:01:21,680
particular value 8 1

38
00:01:18,320 --> 00:01:23,600
1010 for 0xa would mean that

39
00:01:21,680 --> 00:01:25,119
some other bits would actually get set

40
00:01:23,600 --> 00:01:26,159
that we haven't actually talked about in

41
00:01:25,119 --> 00:01:28,479
this class

42
00:01:26,159 --> 00:01:29,840
now ignored we don't care about because

43
00:01:28,479 --> 00:01:31,680
those are ignored

44
00:01:29,840 --> 00:01:33,680
and the operating system can safely use

45
00:01:31,680 --> 00:01:34,720
those bits for whatever it wants to use

46
00:01:33,680 --> 00:01:38,200
them for

47
00:01:34,720 --> 00:01:41,759
and even if someone was using 56

48
00:01:38,200 --> 00:01:43,439
57-bit linear addresses uh the maximum

49
00:01:41,759 --> 00:01:44,479
right now for physical addresses is

50
00:01:43,439 --> 00:01:46,720
still 52

51
00:01:44,479 --> 00:01:48,479
so intel would have to expand the

52
00:01:46,720 --> 00:01:49,759
physical address space before using

53
00:01:48,479 --> 00:01:51,040
those bits would be unsafe for an

54
00:01:49,759 --> 00:01:54,320
operating system

55
00:01:51,040 --> 00:01:56,159
but this bit in particular the prot key

56
00:01:54,320 --> 00:01:58,640
is what's interesting so that is a

57
00:01:56,159 --> 00:02:00,000
mechanism called process context ids

58
00:01:58,640 --> 00:02:02,640
PCIDs

59
00:02:00,000 --> 00:02:04,399
and microsoft uses this as part of the

60
00:02:02,640 --> 00:02:06,159
mitigation for the meltdown

61
00:02:04,399 --> 00:02:08,160
vulnerability which was a

62
00:02:06,159 --> 00:02:10,319
thing to do with the speculative

63
00:02:08,160 --> 00:02:12,640
execution which would allow for

64
00:02:10,319 --> 00:02:14,400
disclosing memory in kernelspace from

65
00:02:12,640 --> 00:02:14,720
userspace that shouldn't have privilege

66
00:02:14,400 --> 00:02:17,760
to

67
00:02:14,720 --> 00:02:19,840
see it so I haven't decided yet whether

68
00:02:17,760 --> 00:02:22,480
or not we're going to cover PCIDs as

69
00:02:19,840 --> 00:02:23,680
extra optional material in this class or

70
00:02:22,480 --> 00:02:26,080
leave it until later

71
00:02:23,680 --> 00:02:28,640
in the context of windows internals

72
00:02:26,080 --> 00:02:28,640
class

