1
00:00:00,080 --> 00:00:04,080
all right the end is in sight only one

2
00:00:01,520 --> 00:00:04,960
more page table to go page table entries

3
00:00:04,080 --> 00:00:08,880
what are they

4
00:00:04,960 --> 00:00:10,800
enhance what's inside the PTE

5
00:00:08,880 --> 00:00:12,160
well there's no more PS bit to care

6
00:00:10,800 --> 00:00:14,240
about because you're so far

7
00:00:12,160 --> 00:00:16,000
down the page table walk that the only

8
00:00:14,240 --> 00:00:17,920
thing you could possibly be pointing at

9
00:00:16,000 --> 00:00:19,680
is a 4 kilobyte page

10
00:00:17,920 --> 00:00:22,080
so the physical address of a 4

11
00:00:19,680 --> 00:00:22,560
kilobyte page is inside of here it must

12
00:00:22,080 --> 00:00:25,599
be

13
00:00:22,560 --> 00:00:27,359
page aligned 0x000 is the

14
00:00:25,599 --> 00:00:29,439
last bits of the address

15
00:00:27,359 --> 00:00:30,400
all of the entries that we know and love

16
00:00:29,439 --> 00:00:32,559
the present bit

17
00:00:30,400 --> 00:00:33,760
the read/write bit the user/supervisor

18
00:00:32,559 --> 00:00:36,160
bit dirty global

19
00:00:33,760 --> 00:00:38,160
and execute disable are all here and

20
00:00:36,160 --> 00:00:39,120
they all behave exactly the same as they

21
00:00:38,160 --> 00:00:41,040
did before

22
00:00:39,120 --> 00:00:42,559
this is getting easy this is getting

23
00:00:41,040 --> 00:00:45,440
boring you know this

24
00:00:42,559 --> 00:00:45,440
at this point

