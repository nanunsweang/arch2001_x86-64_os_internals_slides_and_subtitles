1
00:00:00,320 --> 00:00:04,160
okay so we've seen that the CR3 gives us

2
00:00:02,879 --> 00:00:06,720
the physical address

3
00:00:04,160 --> 00:00:08,559
of the page map level 4 and we know

4
00:00:06,720 --> 00:00:10,639
from the previous table section that for

5
00:00:08,559 --> 00:00:12,240
any given linear address the top 9

6
00:00:10,639 --> 00:00:14,719
bits are going to be used to

7
00:00:12,240 --> 00:00:16,160
index into the page map level 4 to

8
00:00:14,719 --> 00:00:21,199
find the page map level 4

9
00:00:16,160 --> 00:00:21,199
entry so let's go ahead and enhance

10
00:00:22,400 --> 00:00:28,240
what's inside the page map level 4 entry

11
00:00:25,599 --> 00:00:29,039
well it could look like two things the

12
00:00:28,240 --> 00:00:31,279
first

13
00:00:29,039 --> 00:00:32,559
and most important bit here is the

14
00:00:31,279 --> 00:00:35,600
present bit

15
00:00:32,559 --> 00:00:37,440
if the present bit is equal to 0 then

16
00:00:35,600 --> 00:00:39,920
it'll all these other bits are just

17
00:00:37,440 --> 00:00:41,840
ignored and any attempt to

18
00:00:39,920 --> 00:00:43,680
translate a linear to virtual address

19
00:00:41,840 --> 00:00:44,879
that hits one of these entries like this

20
00:00:43,680 --> 00:00:47,280
with present 0

21
00:00:44,879 --> 00:00:48,559
will cause a page fault and interrupt

22
00:00:47,280 --> 00:00:50,480
14.

23
00:00:48,559 --> 00:00:52,879
and it might be recoverable it might not

24
00:00:50,480 --> 00:00:55,600
be we'll talk about that later

25
00:00:52,879 --> 00:00:57,360
and so only if present is equal to 1

26
00:00:55,600 --> 00:00:58,879
or any of the rest of these bits

27
00:00:57,360 --> 00:01:01,680
actually interpreted by the

28
00:00:58,879 --> 00:01:03,520
memory management unit so in general you

29
00:01:01,680 --> 00:01:05,680
would expect that in the page map

30
00:01:03,520 --> 00:01:07,360
level 4 an operating system would

31
00:01:05,680 --> 00:01:09,360
basically just zero-initialize

32
00:01:07,360 --> 00:01:11,280
everything and then it would only fill

33
00:01:09,360 --> 00:01:13,920
in the entries that correspond

34
00:01:11,280 --> 00:01:15,200
to virtual to physical mappings that it

35
00:01:13,920 --> 00:01:17,040
wants to actually make

36
00:01:15,200 --> 00:01:19,200
everything else should be default

37
00:01:17,040 --> 00:01:22,320
invalid and cause some sort of error

38
00:01:19,200 --> 00:01:25,439
when someone tries to access it

39
00:01:22,320 --> 00:01:28,560
alright so bit 1 here

40
00:01:25,439 --> 00:01:31,119
is the read/write flag and

41
00:01:28,560 --> 00:01:33,759
I have highlighted or I've bolded the

42
00:01:31,119 --> 00:01:36,799
write here to indicate that if

43
00:01:33,759 --> 00:01:37,680
bit 1 is equal to 1 it means that writes

44
00:01:36,799 --> 00:01:39,200
are allowed

45
00:01:37,680 --> 00:01:41,040
I highlight that because it's not

46
00:01:39,200 --> 00:01:42,640
exactly intuitive you know whether or

47
00:01:41,040 --> 00:01:45,840
not the the 1 would mean

48
00:01:42,640 --> 00:01:47,520
you know it's reads only or read/write

49
00:01:45,840 --> 00:01:49,920
could be either way and it's actually

50
00:01:47,520 --> 00:01:52,159
this next bit the user/supervisor

51
00:01:49,920 --> 00:01:54,000
it's when it's 1 it means it's user

52
00:01:52,159 --> 00:01:56,240
space instead of supervisor kernel

53
00:01:54,000 --> 00:01:57,920
space so anyways just to try to make it

54
00:01:56,240 --> 00:01:59,119
a little bit more memorable for you

55
00:01:57,920 --> 00:02:00,960
later on so you don't have to

56
00:01:59,119 --> 00:02:02,399
keep re-looking up the definition

57
00:02:00,960 --> 00:02:03,040
throughout the slides it should have

58
00:02:02,399 --> 00:02:06,479
bolding

59
00:02:03,040 --> 00:02:10,800
to when I say the read/write flag 1

60
00:02:06,479 --> 00:02:13,840
means writable 1 means userspace here

61
00:02:10,800 --> 00:02:14,800
so any attempts to write to a read/write

62
00:02:13,840 --> 00:02:16,879
equals 0

63
00:02:14,800 --> 00:02:18,640
therefore if it's 0 it is not

64
00:02:16,879 --> 00:02:20,640
writable it's read-only

65
00:02:18,640 --> 00:02:21,760
any attempt to do that would again yield

66
00:02:20,640 --> 00:02:24,239
a page fault

67
00:02:21,760 --> 00:02:25,840
interrupt error that the operating

68
00:02:24,239 --> 00:02:29,200
system would have to deal with

69
00:02:25,840 --> 00:02:32,640
user/supervisor flag if it's 0 then

70
00:02:29,200 --> 00:02:35,680
only the supervisor is able to access

71
00:02:32,640 --> 00:02:38,000
the particular page and if it's 1

72
00:02:35,680 --> 00:02:38,879
then user and supervisor are allowed to

73
00:02:38,000 --> 00:02:41,519
access so

74
00:02:38,879 --> 00:02:43,760
1 means user can access it otherwise

75
00:02:41,519 --> 00:02:46,319
it's a supervisor only thing

76
00:02:43,760 --> 00:02:48,080
again attempts to access it with you

77
00:02:46,319 --> 00:02:51,040
know if you're in userspace

78
00:02:48,080 --> 00:02:52,160
and the bit is 0 which would mean

79
00:02:51,040 --> 00:02:54,720
that you know

80
00:02:52,160 --> 00:02:57,599
it is not userspace then that would

81
00:02:54,720 --> 00:03:00,159
cause a page fault

82
00:02:57,599 --> 00:03:02,319
there's also the execute disable bit up

83
00:03:00,159 --> 00:03:04,319
here at bit 63

84
00:03:02,319 --> 00:03:06,480
this was actually added to 32-bit

85
00:03:04,319 --> 00:03:09,040
systems up at bit 31

86
00:03:06,480 --> 00:03:10,000
uh before it was added and added as part

87
00:03:09,040 --> 00:03:13,680
of the

88
00:03:10,000 --> 00:03:16,319
64-bit extensions so if this is 1

89
00:03:13,680 --> 00:03:18,720
and the MMU is walking through this page

90
00:03:16,319 --> 00:03:19,920
table on its way to fetch some assembly

91
00:03:18,720 --> 00:03:22,080
instructions

92
00:03:19,920 --> 00:03:23,760
then it will say no this instruction

93
00:03:22,080 --> 00:03:24,080
fetch cannot occur this is supposed to

94
00:03:23,760 --> 00:03:27,760
be

95
00:03:24,080 --> 00:03:29,760
a non-executable and execute disabled

96
00:03:27,760 --> 00:03:31,360
area of memory and therefore it'll throw

97
00:03:29,760 --> 00:03:33,120
up a page fault again

98
00:03:31,360 --> 00:03:34,799
so this was originally called the no

99
00:03:33,120 --> 00:03:37,360
execute bit on the AMD

100
00:03:34,799 --> 00:03:38,159
systems when the extension was added so

101
00:03:37,360 --> 00:03:40,480
you may

102
00:03:38,159 --> 00:03:42,799
frequently see this bit referred to as

103
00:03:40,480 --> 00:03:46,799
NX instead of XD

104
00:03:42,799 --> 00:03:48,720
so if you took this execute disable bit

105
00:03:46,799 --> 00:03:50,799
and you spread it around on your various

106
00:03:48,720 --> 00:03:52,640
page tables for the shared memory area

107
00:03:50,799 --> 00:03:55,840
that we were talking about before

108
00:03:52,640 --> 00:03:57,599
ultimately it is the first bit that is

109
00:03:55,840 --> 00:03:58,720
encountered by the MMU as it's walking

110
00:03:57,599 --> 00:04:00,400
the page tables

111
00:03:58,720 --> 00:04:02,239
which takes the precedent and which

112
00:04:00,400 --> 00:04:05,280
effectively influences

113
00:04:02,239 --> 00:04:07,200
all the downstream uh permissions put

114
00:04:05,280 --> 00:04:09,120
another way it's ultimately the most

115
00:04:07,200 --> 00:04:09,840
restrictive permissions which take

116
00:04:09,120 --> 00:04:12,560
effect

117
00:04:09,840 --> 00:04:12,959
so if the very top level page map level

118
00:04:12,560 --> 00:04:15,680
4

119
00:04:12,959 --> 00:04:17,600
entry had the execute disable bit then

120
00:04:15,680 --> 00:04:18,880
everything else in that virtual memory

121
00:04:17,600 --> 00:04:22,320
address space would be

122
00:04:18,880 --> 00:04:22,960
execute disabled if on the other hand it

123
00:04:22,320 --> 00:04:24,240
was 0

124
00:04:22,960 --> 00:04:26,240
then you know you could keep walking

125
00:04:24,240 --> 00:04:28,400
down okay that's 0 that's 0 that's

126
00:04:26,240 --> 00:04:30,400
0 okay this should be executable

127
00:04:28,400 --> 00:04:32,160
0, 0, 0, 0 okay that should be

128
00:04:30,400 --> 00:04:35,120
executable so there's a 0, 0, 0, 1

129
00:04:32,160 --> 00:04:36,320
okay no that's not executable right

130
00:04:35,120 --> 00:04:38,320
so basically

131
00:04:36,320 --> 00:04:40,240
the most restrictive thing all the way

132
00:04:38,320 --> 00:04:41,600
up at the highest level that affects all

133
00:04:40,240 --> 00:04:43,440
the downstream stuff

134
00:04:41,600 --> 00:04:45,199
so if you ultimately want these things

135
00:04:43,440 --> 00:04:47,040
to be executable it should be

136
00:04:45,199 --> 00:04:48,960
sort of the the last levels of

137
00:04:47,040 --> 00:04:49,919
translation which are ultimately set as

138
00:04:48,960 --> 00:04:52,479
non-executable

139
00:04:49,919 --> 00:04:53,280
okay so within this page map level 4

140
00:04:52,479 --> 00:04:56,320
entry

141
00:04:53,280 --> 00:04:58,400
bits M-1 to 12 are the

142
00:04:56,320 --> 00:05:00,240
physical address where the page

143
00:04:58,400 --> 00:05:02,720
directory pointer table can be found by

144
00:05:00,240 --> 00:05:04,160
the MMU again just like with CR3

145
00:05:02,720 --> 00:05:07,120
this is a physical address it's not a

146
00:05:04,160 --> 00:05:07,440
virtual address you can't easily access

147
00:05:07,120 --> 00:05:09,280
it

148
00:05:07,440 --> 00:05:10,639
sort of an operating system generally

149
00:05:09,280 --> 00:05:13,199
sets up these tables

150
00:05:10,639 --> 00:05:15,039
and ultimately has their own little ways

151
00:05:13,199 --> 00:05:17,280
and workarounds in order to

152
00:05:15,039 --> 00:05:18,880
you know write to these entries but this

153
00:05:17,280 --> 00:05:20,320
is a physical address

154
00:05:18,880 --> 00:05:22,160
it's not something you will be able to

155
00:05:20,320 --> 00:05:25,440
trivially view within

156
00:05:22,160 --> 00:05:27,440
the within the debugger for instance

157
00:05:25,440 --> 00:05:29,120
and again just like everything else here

158
00:05:27,440 --> 00:05:31,600
because it's using these top

159
00:05:29,120 --> 00:05:32,960
bits and leaving these bottom 12 bits

160
00:05:31,600 --> 00:05:34,960
for use of flags

161
00:05:32,960 --> 00:05:36,560
the expectation is that this page

162
00:05:34,960 --> 00:05:39,280
directory pointer table

163
00:05:36,560 --> 00:05:42,160
will be aligned on a 0x1000 page

164
00:05:39,280 --> 00:05:42,160
aligned boundary

