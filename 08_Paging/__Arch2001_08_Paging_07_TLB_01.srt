1
00:00:00,080 --> 00:00:04,080
now if this question hasn't occurred to

2
00:00:02,000 --> 00:00:06,640
you yet it would have eventually

3
00:00:04,080 --> 00:00:08,400
basically why do we want to use large

4
00:00:06,640 --> 00:00:10,719
pages that are 2 megabytes 4

5
00:00:08,400 --> 00:00:12,559
megabytes or 1 gigabyte big

6
00:00:10,719 --> 00:00:15,040
instead of just everything always being

7
00:00:12,559 --> 00:00:17,199
a 4 kilobyte page

8
00:00:15,040 --> 00:00:19,199
well from this information in the

9
00:00:17,199 --> 00:00:21,439
november 2008 edition

10
00:00:19,199 --> 00:00:23,840
of the intel manual which doesn't seem

11
00:00:21,439 --> 00:00:25,039
to be present in the 2020 version

12
00:00:23,840 --> 00:00:27,439
anymore

13
00:00:25,039 --> 00:00:29,599
it says that a typical example of mixing

14
00:00:27,439 --> 00:00:31,679
4 kilobyte and 4 megabyte pages

15
00:00:29,599 --> 00:00:33,360
is to place the operating system or

16
00:00:31,679 --> 00:00:36,719
executives kernel

17
00:00:33,360 --> 00:00:38,320
into a large page to reduce TLB misses

18
00:00:36,719 --> 00:00:39,520
and thus improve overall system

19
00:00:38,320 --> 00:00:42,160
performance

20
00:00:39,520 --> 00:00:43,040
processor maintains 4 megabyte page

21
00:00:42,160 --> 00:00:45,600
entries and

22
00:00:43,040 --> 00:00:46,960
4 kilobyte page entries in separate

23
00:00:45,600 --> 00:00:49,520
TLBs

24
00:00:46,960 --> 00:00:50,239
so placing code that is often used such

25
00:00:49,520 --> 00:00:53,120
as the kernel

26
00:00:50,239 --> 00:00:55,120
in a larger page frees up 4 kilobyte

27
00:00:53,120 --> 00:00:56,320
page TLB entries for application

28
00:00:55,120 --> 00:00:58,879
programs and tasks

29
00:00:56,320 --> 00:00:59,520
so the basic answer is using larger

30
00:00:58,879 --> 00:01:02,160
pages

31
00:00:59,520 --> 00:01:04,239
improves performance especially because

32
00:01:02,160 --> 00:01:06,400
you might have a big blob of memory

33
00:01:04,239 --> 00:01:08,080
such as an operating systems kernel

34
00:01:06,400 --> 00:01:10,479
that's going to be always the same it's

35
00:01:08,080 --> 00:01:12,479
not self-modifying or anything like that

36
00:01:10,479 --> 00:01:14,080
and therefore by putting it in a cache

37
00:01:12,479 --> 00:01:15,040
so that the virtual to physical

38
00:01:14,080 --> 00:01:17,600
translation

39
00:01:15,040 --> 00:01:19,200
is easily looked up every time you

40
00:01:17,600 --> 00:01:21,600
transition from userspace to kernel

41
00:01:19,200 --> 00:01:23,840
space such as while doing a system call

42
00:01:21,600 --> 00:01:25,759
this will mean that you don't have to go

43
00:01:23,840 --> 00:01:27,520
out and walk the page tables in memory

44
00:01:25,759 --> 00:01:29,680
so it'll be significantly faster so

45
00:01:27,520 --> 00:01:32,400
let's go ahead and look at what the TLB

46
00:01:29,680 --> 00:01:34,400
is and why this improves performance

47
00:01:32,400 --> 00:01:35,840
so back to this wikipedia page that we

48
00:01:34,400 --> 00:01:37,920
had mentioned briefly before

49
00:01:35,840 --> 00:01:40,079
we said that virtual addresses come

50
00:01:37,920 --> 00:01:42,720
nominally from the CPU

51
00:01:40,079 --> 00:01:44,560
and the MMU is responsible for walking

52
00:01:42,720 --> 00:01:45,439
page tables and translating virtual

53
00:01:44,560 --> 00:01:47,439
addresses

54
00:01:45,439 --> 00:01:48,720
into physical addresses that it puts out

55
00:01:47,439 --> 00:01:50,880
to RAM but

56
00:01:48,720 --> 00:01:52,799
there is this side lookup with the TLB

57
00:01:50,880 --> 00:01:55,360
the translation look aside buffer

58
00:01:52,799 --> 00:01:57,360
which is a cache of virtual to physical

59
00:01:55,360 --> 00:02:00,640
translations

60
00:01:57,360 --> 00:02:02,240
so basically the point is every you know

61
00:02:00,640 --> 00:02:04,399
remember the memory hierarchy

62
00:02:02,240 --> 00:02:07,200
attempts to go out to RAM are more

63
00:02:04,399 --> 00:02:08,959
costly than just using local caches or

64
00:02:07,200 --> 00:02:11,440
using local registers

65
00:02:08,959 --> 00:02:12,400
and so because a page table itself is

66
00:02:11,440 --> 00:02:14,319
out in RAM

67
00:02:12,400 --> 00:02:15,760
if there's some address in physical

68
00:02:14,319 --> 00:02:17,200
memory like a kernel you're going to

69
00:02:15,760 --> 00:02:19,040
access very frequently

70
00:02:17,200 --> 00:02:20,879
you want to not have to walk a page

71
00:02:19,040 --> 00:02:23,040
table just to find where the kernel is

72
00:02:20,879 --> 00:02:26,080
in physical RAM so basically the TLB

73
00:02:23,040 --> 00:02:28,080
improves performance because the MMU can

74
00:02:26,080 --> 00:02:30,000
just consult the tables and they can try

75
00:02:28,080 --> 00:02:31,599
different sizes and it can say

76
00:02:30,000 --> 00:02:33,360
okay for this virtual address does it

77
00:02:31,599 --> 00:02:35,760
exist in the 4 kilobyte

78
00:02:33,360 --> 00:02:37,360
TLB no does it exist in the 4

79
00:02:35,760 --> 00:02:39,200
megabyte TLB yes

80
00:02:37,360 --> 00:02:41,519
okay now that I see it exists in the

81
00:02:39,200 --> 00:02:43,440
4 kilo 4 megabyte TLB

82
00:02:41,519 --> 00:02:45,519
then I can just take the bottom 22 bits

83
00:02:43,440 --> 00:02:47,120
of the address and OR it on to whatever

84
00:02:45,519 --> 00:02:48,160
translation gets looked up from the

85
00:02:47,120 --> 00:02:50,560
cache

86
00:02:48,160 --> 00:02:52,720
and so essentially different TLB sizes

87
00:02:50,560 --> 00:02:54,239
can be used in order to so 2 and

88
00:02:52,720 --> 00:02:56,400
4 megabyte are never actually used at

89
00:02:54,239 --> 00:02:58,319
the same time uh it's 2 megabytes for

90
00:02:56,400 --> 00:03:00,159
the 64-bit stuff we care about

91
00:02:58,319 --> 00:03:01,680
care about it was 4 megabyte back in

92
00:03:00,159 --> 00:03:03,360
the old 32

93
00:03:01,680 --> 00:03:04,959
bit physical address extensions that we

94
00:03:03,360 --> 00:03:06,720
didn't really cover in this class

95
00:03:04,959 --> 00:03:08,480
so for all intents and purposes there's

96
00:03:06,720 --> 00:03:11,120
just kind of you know 3

97
00:03:08,480 --> 00:03:12,159
possible caches that have to be looked

98
00:03:11,120 --> 00:03:15,200
up in

99
00:03:12,159 --> 00:03:16,959
and so once the MMU determines that it

100
00:03:15,200 --> 00:03:19,599
does exist in the cache once it gets a

101
00:03:16,959 --> 00:03:20,879
hit on a cache for the virtual address

102
00:03:19,599 --> 00:03:22,879
then it can just say okay I'm going to

103
00:03:20,879 --> 00:03:24,720
take the bottom 12 bits the bottom 22

104
00:03:22,879 --> 00:03:27,280
bits the bottom 30 bits

105
00:03:24,720 --> 00:03:28,319
and just OR those together with the

106
00:03:27,280 --> 00:03:30,000
particular

107
00:03:28,319 --> 00:03:31,840
physical address that it got out of the

108
00:03:30,000 --> 00:03:34,239
cache so in some sense you can think of

109
00:03:31,840 --> 00:03:35,040
the TLB trying to achieve the same goal

110
00:03:34,239 --> 00:03:37,680
that we saw

111
00:03:35,040 --> 00:03:39,440
back in the segmentation registers where

112
00:03:37,680 --> 00:03:40,799
they had a hidden portion and the whole

113
00:03:39,440 --> 00:03:43,120
point was to say

114
00:03:40,799 --> 00:03:45,360
we don't want to look up things from

115
00:03:43,120 --> 00:03:47,680
tables like the GDT all the time

116
00:03:45,360 --> 00:03:49,200
just here the TLB is saying we don't

117
00:03:47,680 --> 00:03:50,640
want to look up things from the page

118
00:03:49,200 --> 00:03:52,640
tables all the time

119
00:03:50,640 --> 00:03:54,560
but whereas the segmentation was

120
00:03:52,640 --> 00:03:56,080
relatively simple because

121
00:03:54,560 --> 00:03:57,439
there was not all sorts of different

122
00:03:56,080 --> 00:03:58,400
types of translations it's just

123
00:03:57,439 --> 00:04:00,959
something that's

124
00:03:58,400 --> 00:04:02,400
sort of set up once page tables are much

125
00:04:00,959 --> 00:04:04,159
more complicated they're getting swapped

126
00:04:02,400 --> 00:04:06,080
around between different processes

127
00:04:04,159 --> 00:04:08,159
all the time so they behave more like

128
00:04:06,080 --> 00:04:10,480
real caches and that they have

129
00:04:08,159 --> 00:04:11,599
you know notions of associativity if

130
00:04:10,480 --> 00:04:13,200
you've ever you know

131
00:04:11,599 --> 00:04:15,599
studied real caches and if you don't it

132
00:04:13,200 --> 00:04:16,880
doesn't matter all that matters is this

133
00:04:15,599 --> 00:04:19,280
simple sort of picture

134
00:04:16,880 --> 00:04:20,160
that I've given to give you a notion of

135
00:04:19,280 --> 00:04:22,079
what it means

136
00:04:20,160 --> 00:04:23,840
so if you had say a 32-bit linear

137
00:04:22,079 --> 00:04:27,040
address and the address was

138
00:04:23,840 --> 00:04:29,600
4f007700

139
00:04:27,040 --> 00:04:31,680
then it would basically take the top

140
00:04:29,600 --> 00:04:34,320
part the 4f007

141
00:04:31,680 --> 00:04:36,560
it would try to find it in a TLB could

142
00:04:34,320 --> 00:04:37,680
be the 4 kilobyte TLB could be the 4

143
00:04:36,560 --> 00:04:40,720
megabyte TLB

144
00:04:37,680 --> 00:04:41,600
and if it gets a hit in the TLB then it

145
00:04:40,720 --> 00:04:43,919
says okay

146
00:04:41,600 --> 00:04:45,280
this virtual address corresponds to this

147
00:04:43,919 --> 00:04:47,040
physical address

148
00:04:45,280 --> 00:04:48,800
and then it's just going to OR that

149
00:04:47,040 --> 00:04:52,160
together with this 700

150
00:04:48,800 --> 00:04:56,280
and so virtual address 4f007

151
00:04:52,160 --> 00:04:59,360
700 turns into 2bad

152
00:04:56,280 --> 00:05:02,720
1700 because the base of the thing

153
00:04:59,360 --> 00:05:03,280
is 1000 and then adding in the 700

154
00:05:02,720 --> 00:05:04,880
offset

155
00:05:03,280 --> 00:05:07,680
gives you the location that it's trying

156
00:05:04,880 --> 00:05:08,320
to access so basically a TLB is a very

157
00:05:07,680 --> 00:05:11,120
simple

158
00:05:08,320 --> 00:05:11,680
virtual page number to physical frame

159
00:05:11,120 --> 00:05:13,919
number

160
00:05:11,680 --> 00:05:15,360
lookup table it's more complicated but

161
00:05:13,919 --> 00:05:16,880
that's that's all you really have to

162
00:05:15,360 --> 00:05:19,680
think of it as

163
00:05:16,880 --> 00:05:20,400
now one of the behaviors of the TLB is

164
00:05:19,680 --> 00:05:23,440
that whenever

165
00:05:20,400 --> 00:05:25,440
CR3 is set to a new value which as a

166
00:05:23,440 --> 00:05:26,720
reminder only ring 0 can move a value

167
00:05:25,440 --> 00:05:29,120
into CR3

168
00:05:26,720 --> 00:05:31,199
whenever that happens all TLB entries

169
00:05:29,120 --> 00:05:34,000
which are not marked as global

170
00:05:31,199 --> 00:05:36,080
are flushed so we had just very briefly

171
00:05:34,000 --> 00:05:37,520
hit on the global flag and we just said

172
00:05:36,080 --> 00:05:38,000
ah that's going to matter later on for

173
00:05:37,520 --> 00:05:40,000
caching

174
00:05:38,000 --> 00:05:41,039
and stuff and here is where it finally

175
00:05:40,000 --> 00:05:44,240
matters

176
00:05:41,039 --> 00:05:46,800
so basically in these entries

177
00:05:44,240 --> 00:05:47,280
if they had the global bit set what it

178
00:05:46,800 --> 00:05:49,680
means

179
00:05:47,280 --> 00:05:51,039
is they essentially will survive a

180
00:05:49,680 --> 00:05:53,720
context switch

181
00:05:51,039 --> 00:05:54,960
it means when you tra when you swap from

182
00:05:53,720 --> 00:05:58,160
notepad.exe

183
00:05:54,960 --> 00:06:00,400
to you know svchost.exe

184
00:05:58,160 --> 00:06:01,840
if some particular area of memory was

185
00:06:00,400 --> 00:06:03,919
marked as global

186
00:06:01,840 --> 00:06:04,960
that translation of virtual to physical

187
00:06:03,919 --> 00:06:07,600
memory will

188
00:06:04,960 --> 00:06:09,120
remain the same in the TLB and therefore

189
00:06:07,600 --> 00:06:10,560
any attempts to look up

190
00:06:09,120 --> 00:06:12,160
memory at that particular virtual

191
00:06:10,560 --> 00:06:12,880
address will go to the same physical

192
00:06:12,160 --> 00:06:14,560
address

193
00:06:12,880 --> 00:06:16,880
even though you've just actually swapped

194
00:06:14,560 --> 00:06:20,080
processes swapped page tables

195
00:06:16,880 --> 00:06:22,080
and everything else so if that if if

196
00:06:20,080 --> 00:06:24,160
page table entries or virtual to

197
00:06:22,080 --> 00:06:26,960
physical translations survive

198
00:06:24,160 --> 00:06:29,360
in the TLB between context switches you

199
00:06:26,960 --> 00:06:30,960
need some way to actually kick them out

200
00:06:29,360 --> 00:06:32,720
and the way that they get kicked out is

201
00:06:30,960 --> 00:06:35,520
with a new assembly instruction

202
00:06:32,720 --> 00:06:37,039
the invalidate page instruction so

203
00:06:35,520 --> 00:06:37,919
basically you could have some global

204
00:06:37,039 --> 00:06:40,160
area of memory

205
00:06:37,919 --> 00:06:41,199
the most likely case would be something

206
00:06:40,160 --> 00:06:43,280
like the

207
00:06:41,199 --> 00:06:44,240
2 megabyte portion for the kernel

208
00:06:43,280 --> 00:06:46,560
because

209
00:06:44,240 --> 00:06:48,639
the operating system would like the you

210
00:06:46,560 --> 00:06:50,240
know kernel virtual to physical

211
00:06:48,639 --> 00:06:52,000
address translation to be exactly the

212
00:06:50,240 --> 00:06:54,800
same between all the different processes

213
00:06:52,000 --> 00:06:56,160
because they're going to because they're

214
00:06:54,800 --> 00:06:57,919
all going to see you know the same sort

215
00:06:56,160 --> 00:06:58,560
of entry points when they have system

216
00:06:57,919 --> 00:07:01,199
calls

217
00:06:58,560 --> 00:07:03,039
and so that's just an optimization thing

218
00:07:01,199 --> 00:07:04,400
albeit an optimization thing that's had

219
00:07:03,039 --> 00:07:05,919
to be changed a little bit because of

220
00:07:04,400 --> 00:07:08,240
meltdown mitigations

221
00:07:05,919 --> 00:07:10,160
but notionally you can imagine that you

222
00:07:08,240 --> 00:07:11,520
know the kernel is just sitting around

223
00:07:10,160 --> 00:07:13,840
at the same virtual to physical

224
00:07:11,520 --> 00:07:15,759
translation between different processes

225
00:07:13,840 --> 00:07:17,680
so what happens if you want to you know

226
00:07:15,759 --> 00:07:20,240
force and kick out that particular

227
00:07:17,680 --> 00:07:22,080
kernel virtual to physical translation

228
00:07:20,240 --> 00:07:23,280
the kernel itself would have to use the

229
00:07:22,080 --> 00:07:25,440
invalidate page

230
00:07:23,280 --> 00:07:26,479
instruction to force something out of

231
00:07:25,440 --> 00:07:29,199
the TLB

232
00:07:26,479 --> 00:07:31,120
again as alluded to in the quote from

233
00:07:29,199 --> 00:07:33,120
the 2008 manuals there are multiple

234
00:07:31,120 --> 00:07:36,000
types of TLBs for different

235
00:07:33,120 --> 00:07:37,039
sizes there's also different types for

236
00:07:36,000 --> 00:07:40,080
data access

237
00:07:37,039 --> 00:07:41,039
versus instruction access so data TLBs

238
00:07:40,080 --> 00:07:44,400
DTLBs

239
00:07:41,039 --> 00:07:46,560
and instruction TLBs ITLBs so

240
00:07:44,400 --> 00:07:47,759
basically for data TLBs if you look at

241
00:07:46,560 --> 00:07:49,360
the latest

242
00:07:47,759 --> 00:07:51,039
chips you would see that it could have

243
00:07:49,360 --> 00:07:51,919
you know up to 6 different types of

244
00:07:51,039 --> 00:07:53,680
TLBs

245
00:07:51,919 --> 00:07:54,960
you could have data TLBs for 4

246
00:07:53,680 --> 00:07:56,800
kilobyte access

247
00:07:54,960 --> 00:07:58,160
2 or 4 megabyte access and 1

248
00:07:56,800 --> 00:08:00,479
gigabyte access

249
00:07:58,160 --> 00:08:01,440
you could have instruction TLBs for 4

250
00:08:00,479 --> 00:08:04,000
kilobyte or

251
00:08:01,440 --> 00:08:05,919
2 or 4 megabyte don't seem to have

252
00:08:04,000 --> 00:08:07,759
any TLBs currently defined in the

253
00:08:05,919 --> 00:08:08,800
manuals for the notion of 1 gigabyte

254
00:08:07,759 --> 00:08:11,039
pages so

255
00:08:08,800 --> 00:08:12,720
that means that basically intel is

256
00:08:11,039 --> 00:08:15,599
assuming that no one's trying to map

257
00:08:12,720 --> 00:08:17,120
1 gigabyte of a blob into memory that

258
00:08:15,599 --> 00:08:19,199
always just sticks around which is

259
00:08:17,120 --> 00:08:21,520
accurate as of today and then finally

260
00:08:19,199 --> 00:08:24,560
there's the notion of shared TLB

261
00:08:21,520 --> 00:08:26,560
or level 2 TLB which can actually be

262
00:08:24,560 --> 00:08:27,759
shared between the instruction and data

263
00:08:26,560 --> 00:08:29,520
TLBs so

264
00:08:27,759 --> 00:08:31,199
different intel chips actually have

265
00:08:29,520 --> 00:08:32,880
different caches different number of

266
00:08:31,199 --> 00:08:34,080
entries and everything else and so you

267
00:08:32,880 --> 00:08:35,760
actually have to look up

268
00:08:34,080 --> 00:08:37,200
what's supported on a particular piece

269
00:08:35,760 --> 00:08:39,120
of hardware so

270
00:08:37,200 --> 00:08:43,039
for instance right now I'm doing this

271
00:08:39,120 --> 00:08:46,399
class on an intel i9-9980HK

272
00:08:43,039 --> 00:08:48,959
from my last generation of x86-based

273
00:08:46,399 --> 00:08:51,519
macbook pros so when I looked up the

274
00:08:48,959 --> 00:08:52,000
information with cpuid I found that it

275
00:08:51,519 --> 00:08:56,320
says

276
00:08:52,000 --> 00:08:57,440
it has 64 4 kilobyte entries in the data

277
00:08:56,320 --> 00:09:01,279
TLB

278
00:08:57,440 --> 00:09:04,080
and 32 2/4 megabyte entries

279
00:09:01,279 --> 00:09:05,120
and 4 1 gigabyte entries it also

280
00:09:04,080 --> 00:09:08,880
has 64

281
00:09:05,120 --> 00:09:12,399
4 kilobyte ITLB entries and 8

282
00:09:08,880 --> 00:09:14,000
2/4 ITLB entries but what was

283
00:09:12,399 --> 00:09:16,080
interesting was when I looked at this

284
00:09:14,000 --> 00:09:16,640
exact slide from my previous slides on

285
00:09:16,080 --> 00:09:20,240
my much

286
00:09:16,640 --> 00:09:22,959
older macbook pro it said it had 256 4

287
00:09:20,240 --> 00:09:25,040
kilobyte entries and 128 4 kilobyte

288
00:09:22,959 --> 00:09:27,200
entries for D and I TLBs

289
00:09:25,040 --> 00:09:28,240
so what gives like I was you know very

290
00:09:27,200 --> 00:09:31,519
sad I said

291
00:09:28,240 --> 00:09:33,760
why is my hardware going backwards

292
00:09:31,519 --> 00:09:36,000
well the answer is because my hardware

293
00:09:33,760 --> 00:09:41,279
is actually adding the shared TLB

294
00:09:36,000 --> 00:09:43,440
that level 2 TLB which gives me 1536

295
00:09:41,279 --> 00:09:46,080
4/2 megabyte entries and

296
00:09:43,440 --> 00:09:48,880
16 1 gigabyte entries so

297
00:09:46,080 --> 00:09:49,360
all right well I have slightly less you

298
00:09:48,880 --> 00:09:52,160
know

299
00:09:49,360 --> 00:09:53,200
well because it's a shared TLB it can be

300
00:09:52,160 --> 00:09:56,399
shared between D

301
00:09:53,200 --> 00:09:57,920
and I so really I have way more DTLBs

302
00:09:56,399 --> 00:09:59,839
and ITLBs than

303
00:09:57,920 --> 00:10:01,680
I would previously have so it's all good

304
00:09:59,839 --> 00:10:04,720
and that brings us to our mini lab I

305
00:10:01,680 --> 00:10:07,600
want you to go edit your cpuid

306
00:10:04,720 --> 00:10:09,120
and set the eax equal to 2 and then

307
00:10:07,600 --> 00:10:12,079
print out all of the eax,

308
00:10:09,120 --> 00:10:13,519
ebx, ecx, edx values and go into the

309
00:10:12,079 --> 00:10:16,959
manual look at table

310
00:10:13,519 --> 00:10:19,920
3-12 in the cpuid instruction page

311
00:10:16,959 --> 00:10:20,560
and translate what your entries are

312
00:10:19,920 --> 00:10:21,920
saying

313
00:10:20,560 --> 00:10:23,920
basically it's going to be a bunch of

314
00:10:21,920 --> 00:10:25,839
bytes and each of those bytes is telling

315
00:10:23,920 --> 00:10:27,200
you something about what you support in

316
00:10:25,839 --> 00:10:30,959
terms of TLBs

317
00:10:27,200 --> 00:10:30,959
and also other caching types

