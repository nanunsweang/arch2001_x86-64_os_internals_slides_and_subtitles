1
00:00:00,04 --> 00:00:01,59
So what do I want you to do?

2
00:00:01,59 --> 00:00:03,77
I want you to go into your debugger now.

3
00:00:03,77 --> 00:00:06,32
We just saw for the kernel addresses that we've been

4
00:00:06,32 --> 00:00:07,02
looking at

5
00:00:07,03 --> 00:00:09,8
It turns out that they're using 2 megabyte pages

6
00:00:09,81 --> 00:00:12,29
So we need to go pick some other address that

7
00:00:12,29 --> 00:00:15,17
is more likely to be using a 4 kilobyte page

8
00:00:15,18 --> 00:00:16,16
So to do that,

9
00:00:16,17 --> 00:00:18,66
I want you to go back and do your ms_gdt

10
00:00:18,66 --> 00:00:21,78
and then you need to take note of which

11
00:00:21,78 --> 00:00:24,85
particular processor it stopped you on when you just broke

12
00:00:24,85 --> 00:00:27,42
into the bugger and you want to go to that

13
00:00:27,42 --> 00:00:31,46
processor core and then go down core 0

14
00:00:31,46 --> 00:00:34,0
So it could be 1, 2, 3 Whatever it depends on how

15
00:00:34,0 --> 00:00:37,91
many processors virtual processors you have in your VM

16
00:00:37,92 --> 00:00:41,22
So whatever you broke in on use from that entry

17
00:00:41,23 --> 00:00:46,36
the TSS64 and use the virtual address of that

18
00:00:46,64 --> 00:00:49,74
Then run pte on that virtual address and analyze the

19
00:00:49,74 --> 00:00:50,4
PTE,

20
00:00:50,4 --> 00:00:53,26
which again is correctly identified as a PTE,

21
00:00:53,84 --> 00:00:57,02
analyze the entry and see which of the bits

22
00:00:57,02 --> 00:00:57,68
P,

23
00:00:57,68 --> 00:00:59,36
R/W, U/S, G,

24
00:00:59,44 --> 00:01:01,23
D, XD are set,

25
00:01:01,24 --> 00:01:03,26
and also for your own edification

26
00:01:03,26 --> 00:01:06,23
What is the physical address where it's going to be

27
00:01:06,23 --> 00:01:08,56
pointing at the 4 kilobyte page?

