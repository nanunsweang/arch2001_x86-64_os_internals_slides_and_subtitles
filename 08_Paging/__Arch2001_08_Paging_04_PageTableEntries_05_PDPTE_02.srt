1
00:00:00,000 --> 00:00:03,199
all right so now you're going to return

2
00:00:02,000 --> 00:00:05,600
to the pte

3
00:00:03,199 --> 00:00:07,359
output that you used in the previous

4
00:00:05,600 --> 00:00:10,080
page table walkabout

5
00:00:07,359 --> 00:00:11,920
and I want you to go look at this entry

6
00:00:10,080 --> 00:00:14,719
right here

7
00:00:11,920 --> 00:00:17,840
window WinDbg calls it the PPE and

8
00:00:14,719 --> 00:00:19,600
what it is is a PDPTE

9
00:00:17,840 --> 00:00:22,080
and so I want you to go interpret the

10
00:00:19,600 --> 00:00:25,119
values of the PDPTE

11
00:00:22,080 --> 00:00:26,240
as according to the tables that we saw

12
00:00:25,119 --> 00:00:28,400
in the previous slides

13
00:00:26,240 --> 00:00:30,560
so which of the present bit read/write

14
00:00:28,400 --> 00:00:31,119
bit user/supervisor bit global bit dirty

15
00:00:30,560 --> 00:00:34,640
bit

16
00:00:31,119 --> 00:00:36,480
PS bit and it should say XD bit are set

17
00:00:34,640 --> 00:00:37,680
in your particular entry that you're

18
00:00:36,480 --> 00:00:40,320
seeing right now

19
00:00:37,680 --> 00:00:42,480
also does your particular entry point at

20
00:00:40,320 --> 00:00:44,079
a 1 gigabyte page or does it point to

21
00:00:42,480 --> 00:00:47,039
a page directory

22
00:00:44,079 --> 00:00:48,960
and if regardless of which one it does

23
00:00:47,039 --> 00:00:50,719
what is the physical address of that so

24
00:00:48,960 --> 00:00:54,399
again you know the actual questions will

25
00:00:50,719 --> 00:00:54,399
be on the website not here

