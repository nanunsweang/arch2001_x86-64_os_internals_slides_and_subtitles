1
00:00:00,04 --> 00:00:00,55
Okay

2
00:00:00,55 --> 00:00:02,3
So by way of example,

3
00:00:02,3 --> 00:00:06,36
what did my page directory pointer table entry look like

4
00:00:06,84 --> 00:00:11,11
Well you already kind of learned the code translation here

5
00:00:11,11 --> 00:00:14,85
So V valid equals present equals 1 E for executable

6
00:00:14,85 --> 00:00:16,56
means not executable is not set,

7
00:00:16,94 --> 00:00:19,22
but this could kind of mislead you once you get

8
00:00:19,22 --> 00:00:20,26
to this D bit

9
00:00:20,64 --> 00:00:22,51
So if you look at the PS bit,

10
00:00:22,51 --> 00:00:24,34
the PS bit is set to 0

11
00:00:24,34 --> 00:00:28,11
So this is the proper interpretation and in this interpretation

12
00:00:28,11 --> 00:00:30,44
there is no actual D bit

13
00:00:30,45 --> 00:00:33,37
What would be the D bit is treated as ignored,

14
00:00:33,37 --> 00:00:34,76
as far as the MMUs concern

15
00:00:35,24 --> 00:00:36,04
So it's ignored

16
00:00:36,04 --> 00:00:38,43
And so the operating system could use that bit for

17
00:00:38,43 --> 00:00:39,28
something else

18
00:00:39,32 --> 00:00:42,04
But for all intents for our purposes,

19
00:00:42,04 --> 00:00:44,65
we say that if it's not a 1 gigabyte page

20
00:00:44,66 --> 00:00:47,4
the D bit doesn't exist and so does it point at

21
00:00:47,4 --> 00:00:48,69
1 gigabyte or page directory,

22
00:00:48,69 --> 00:00:50,34
like I said PS 0

23
00:00:50,35 --> 00:00:52,86
Therefore mine points at a page directory

