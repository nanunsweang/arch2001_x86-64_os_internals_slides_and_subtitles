1
00:00:00,000 --> 00:00:06,720
now quick aside about the PML4Es

2
00:00:03,919 --> 00:00:08,080
we saw read/write we saw user/supervisor

3
00:00:06,720 --> 00:00:10,480
and we saw XD

4
00:00:08,080 --> 00:00:12,240
these are all access control mechanisms

5
00:00:10,480 --> 00:00:15,120
that are ultimately controlling what

6
00:00:12,240 --> 00:00:18,720
userspace or kernelspace can do

7
00:00:15,120 --> 00:00:19,039
we also back on CR0 saw the WP bit and

8
00:00:18,720 --> 00:00:21,359
we

9
00:00:19,039 --> 00:00:22,480
probably forgot about it but we said

10
00:00:21,359 --> 00:00:24,960
that WP

11
00:00:22,480 --> 00:00:25,599
stops ring 0 from writing to read-only

12
00:00:24,960 --> 00:00:28,960
pages

13
00:00:25,599 --> 00:00:31,439
so okay if a particular area of memory

14
00:00:28,960 --> 00:00:33,840
according to the page map level 4 is

15
00:00:31,439 --> 00:00:37,280
supposed to be read-only

16
00:00:33,840 --> 00:00:39,200
then if the write protect bit is set

17
00:00:37,280 --> 00:00:40,719
then the kernel can't write to that

18
00:00:39,200 --> 00:00:42,239
read-only memory but if the write

19
00:00:40,719 --> 00:00:43,840
protect bit is not set

20
00:00:42,239 --> 00:00:45,600
then the kernel can just go ahead and

21
00:00:43,840 --> 00:00:47,280
ignore those read-only

22
00:00:45,600 --> 00:00:48,800
access controls and just do whatever it

23
00:00:47,280 --> 00:00:50,800
wants

24
00:00:48,800 --> 00:00:52,640
well it turns out that the write protect

25
00:00:50,800 --> 00:00:55,199
bit also plays a role in

26
00:00:52,640 --> 00:00:57,440
SMEP and SMAP logic so we said we'd come

27
00:00:55,199 --> 00:00:58,960
back to that and cover them briefly

28
00:00:57,440 --> 00:01:00,960
before we showed you you know here's

29
00:00:58,960 --> 00:01:03,199
some control bits of interest

30
00:01:00,960 --> 00:01:04,479
and we said SMEP and SMAP are going to

31
00:01:03,199 --> 00:01:07,200
be covered more

32
00:01:04,479 --> 00:01:08,240
in detail in the exploits in a future

33
00:01:07,200 --> 00:01:11,119
exploits class

34
00:01:08,240 --> 00:01:12,880
to be determined which one but let's

35
00:01:11,119 --> 00:01:14,640
talk about what those are briefly just

36
00:01:12,880 --> 00:01:16,720
in the context of the access controls

37
00:01:14,640 --> 00:01:19,200
that we've already seen

38
00:01:16,720 --> 00:01:20,000
SMAP is supervisor mode access

39
00:01:19,200 --> 00:01:23,040
prevention

40
00:01:20,000 --> 00:01:25,920
and what it does is it says ring 0

41
00:01:23,040 --> 00:01:26,600
supervisor is not allowed to read and

42
00:01:25,920 --> 00:01:29,520
write

43
00:01:26,600 --> 00:01:33,040
non-supervisor pages so if you

44
00:01:29,520 --> 00:01:36,400
meaning userspace don't let the kernel

45
00:01:33,040 --> 00:01:38,960
write to userspace pages all right

46
00:01:36,400 --> 00:01:40,960
supervisor mode execution prevention is

47
00:01:38,960 --> 00:01:45,040
don't let ring 0 the supervisor

48
00:01:40,960 --> 00:01:47,920
execute non-supervisor userspace pages

49
00:01:45,040 --> 00:01:49,439
so why though right why I thought the

50
00:01:47,920 --> 00:01:51,280
supervisor I thought the kernel was

51
00:01:49,439 --> 00:01:54,399
supposed to be all powerful

52
00:01:51,280 --> 00:01:55,680
it's the top boss so why can't it do

53
00:01:54,399 --> 00:01:57,680
those things why would you ever want to

54
00:01:55,680 --> 00:01:59,680
stop it from doing those things

55
00:01:57,680 --> 00:02:02,159
and the answer is that a common

56
00:01:59,680 --> 00:02:04,640
technique for exploiting the kernel

57
00:02:02,159 --> 00:02:05,200
is that an attacker prepares data or

58
00:02:04,640 --> 00:02:07,119
code

59
00:02:05,200 --> 00:02:08,720
in userspace which they control and

60
00:02:07,119 --> 00:02:09,840
which is much easier for them to

61
00:02:08,720 --> 00:02:11,760
manipulate

62
00:02:09,840 --> 00:02:14,239
and then they trick the kernel through

63
00:02:11,760 --> 00:02:15,920
some sort of vulnerability or bug

64
00:02:14,239 --> 00:02:18,560
to basically read from

65
00:02:15,920 --> 00:02:20,239
attacker-controlled data in userspace

66
00:02:18,560 --> 00:02:22,319
and that you would want to prevent with

67
00:02:20,239 --> 00:02:24,560
this access prevention or

68
00:02:22,319 --> 00:02:26,239
execute some code in userspace so they

69
00:02:24,560 --> 00:02:28,319
literally just stick some code in user

70
00:02:26,239 --> 00:02:29,920
space and have a bug and get rip to

71
00:02:28,319 --> 00:02:31,920
point at the userspace code

72
00:02:29,920 --> 00:02:33,680
and boom they're now getting the kernel

73
00:02:31,920 --> 00:02:35,840
to execute arbitrary code

74
00:02:33,680 --> 00:02:37,440
and that is what you would use SMEP for

75
00:02:35,840 --> 00:02:39,599
execute prevention

76
00:02:37,440 --> 00:02:40,640
so these are basically both trying to

77
00:02:39,599 --> 00:02:42,879
harden a kernel

78
00:02:40,640 --> 00:02:45,120
against userspace trying to take it

79
00:02:42,879 --> 00:02:47,200
down through an exploit

80
00:02:45,120 --> 00:02:48,560
so a quick check you should go check

81
00:02:47,200 --> 00:02:52,400
whether CR0

82
00:02:48,560 --> 00:02:53,040
WP is set in your system and is SMEP and

83
00:02:52,400 --> 00:02:56,239
SMAP

84
00:02:53,040 --> 00:02:56,239
set in your system

