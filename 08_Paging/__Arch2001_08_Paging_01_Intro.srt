1
00:00:00,080 --> 00:00:04,880
it's time now to return to this diagram

2
00:00:02,320 --> 00:00:07,279
from before that we saw in segmentation

3
00:00:04,880 --> 00:00:09,120
we've seen how logical addresses are

4
00:00:07,279 --> 00:00:10,800
translated to linear addresses through

5
00:00:09,120 --> 00:00:12,799
things like the GDT
6
00:00:10,800 --> 00:00:14,080
and we've been treating it as if linear

7
00:00:12,799 --> 00:00:14,880
addresses were equal to physical

8
00:00:14,080 --> 00:00:16,320
addresses

9
00:00:14,880 --> 00:00:18,880
but now we can finally see virtual

10
00:00:16,320 --> 00:00:19,680
addresses so logical addresses again

11
00:00:18,880 --> 00:00:21,520
were those far

12
00:00:19,680 --> 00:00:23,600
pointers that had a segment selector and

13
00:00:21,520 --> 00:00:24,640
an offset linear addresses with some

14
00:00:23,600 --> 00:00:27,199
notional flat

15
00:00:24,640 --> 00:00:28,800
64-bit space for purposes of this class

16
00:00:27,199 --> 00:00:31,119
post segmentation

17
00:00:28,800 --> 00:00:33,200
and now virtual addresses are linear

18
00:00:31,119 --> 00:00:34,640
addresses that take paging into account

19
00:00:33,200 --> 00:00:35,520
and we'll learn all about paging in this

20
00:00:34,640 --> 00:00:37,200
section

21
00:00:35,520 --> 00:00:39,600
and finally virtual addresses will

22
00:00:37,200 --> 00:00:43,600
translate through to physical addresses

23
00:00:39,600 --> 00:00:46,160
which are some offset into physical RAM

24
00:00:43,600 --> 00:00:47,840
so this was the diagram we had before

25
00:00:46,160 --> 00:00:48,480
with logical addresses going through the

26
00:00:47,840 --> 00:00:50,000
GDT

27
00:00:48,480 --> 00:00:52,559
linear addresses going directly to

28
00:00:50,000 --> 00:00:53,600
physical now to remove the wool from

29
00:00:52,559 --> 00:00:56,719
your eyes

30
00:00:53,600 --> 00:00:58,079
and we have virtual addresses which are

31
00:00:56,719 --> 00:01:00,719
the linear addresses

32
00:00:58,079 --> 00:01:03,280
as analyzed through some sort of page

33
00:01:00,719 --> 00:01:05,360
table structure

34
00:01:03,280 --> 00:01:07,280
so the terminology for this section is

35
00:01:05,360 --> 00:01:09,760
called paging because

36
00:01:07,280 --> 00:01:12,159
chunks of memory are divided into fixed

37
00:01:09,760 --> 00:01:14,159
size pieces called pages

38
00:01:12,159 --> 00:01:15,920
now there's an analogy you can use to

39
00:01:14,159 --> 00:01:16,640
kind of think about how memory is

40
00:01:15,920 --> 00:01:18,159
organized

41
00:01:16,640 --> 00:01:20,400
I don't remember whether I got this out

42
00:01:18,159 --> 00:01:22,000
of some intel description or just some

43
00:01:20,400 --> 00:01:23,280
random person on the internet but I like

44
00:01:22,000 --> 00:01:26,560
this analogy

45
00:01:23,280 --> 00:01:28,640
so if you think of all information is

46
00:01:26,560 --> 00:01:30,079
organized into a library and that's

47
00:01:28,640 --> 00:01:32,000
somewhat daunting it's just

48
00:01:30,079 --> 00:01:33,200
all over the place so one of the first

49
00:01:32,000 --> 00:01:35,520
things you would do when you go to a

50
00:01:33,200 --> 00:01:37,200
library is use some sort of lookup table

51
00:01:35,520 --> 00:01:38,960
in order to find the particular book

52
00:01:37,200 --> 00:01:41,119
that has the information that you want

53
00:01:38,960 --> 00:01:42,560
from that book you might then select an

54
00:01:41,119 --> 00:01:44,399
individual page

55
00:01:42,560 --> 00:01:46,399
and on the individual page you might

56
00:01:44,399 --> 00:01:48,960
select a particular sentence

57
00:01:46,399 --> 00:01:51,040
or using the intel terminology you might

58
00:01:48,960 --> 00:01:53,520
go for a particular word

59
00:01:51,040 --> 00:01:54,320
remember the intel word was a 16-bit

60
00:01:53,520 --> 00:01:57,360
value a

61
00:01:54,320 --> 00:01:57,759
dword was a 32-bit value so word would be

62
00:01:57,360 --> 00:02:00,079
dark

63
00:01:57,759 --> 00:02:01,360
double word would be dark tower thank

64
00:02:00,079 --> 00:02:03,759
you sai

65
00:02:01,360 --> 00:02:05,200
so this analogy is basically just saying

66
00:02:03,759 --> 00:02:08,239
page is some

67
00:02:05,200 --> 00:02:10,160
blob of information but within the page

68
00:02:08,239 --> 00:02:13,200
are the actual individual data bytes

69
00:02:10,160 --> 00:02:15,360
that we're trying to access

70
00:02:13,200 --> 00:02:17,120
so when paging is enabled a linear

71
00:02:15,360 --> 00:02:19,120
address is the same thing as a virtual

72
00:02:17,120 --> 00:02:20,800
memory address or a virtual address so

73
00:02:19,120 --> 00:02:22,720
I'll just end up using those terms

74
00:02:20,800 --> 00:02:24,879
interchangeably very frequently

75
00:02:22,720 --> 00:02:26,239
and you'll also occasionally hear me use

76
00:02:24,879 --> 00:02:28,959
the term frame

77
00:02:26,239 --> 00:02:30,080
to refer to a page sized chunk of

78
00:02:28,959 --> 00:02:32,080
physical memory

79
00:02:30,080 --> 00:02:33,440
so if a page has some particular size

80
00:02:32,080 --> 00:02:35,680
like 4 kilobytes

81
00:02:33,440 --> 00:02:36,879
then a frame will just refer to a 4

82
00:02:35,680 --> 00:02:38,959
kilobyte chunk of

83
00:02:36,879 --> 00:02:40,239
physical memory also occasionally I'll

84
00:02:38,959 --> 00:02:41,519
refer to things like the memory

85
00:02:40,239 --> 00:02:43,920
management unit

86
00:02:41,519 --> 00:02:46,000
the MMU is basically going to be treated

87
00:02:43,920 --> 00:02:47,519
as the piece of hardware which is

88
00:02:46,000 --> 00:02:50,160
responsible for

89
00:02:47,519 --> 00:02:52,080
taking into account it utilizes these

90
00:02:50,160 --> 00:02:54,640
tables these things like the GDT

91
00:02:52,080 --> 00:02:55,920
LDT utilizes these page tables we're

92
00:02:54,640 --> 00:02:57,920
going to learn about next

93
00:02:55,920 --> 00:02:59,920
and it's hardware that walks these

94
00:02:57,920 --> 00:03:01,920
tables that software sets up

95
00:02:59,920 --> 00:03:04,080
in order to do this all logical to

96
00:03:01,920 --> 00:03:05,760
physical address translation

97
00:03:04,080 --> 00:03:07,680
also eventually we'll talk about the

98
00:03:05,760 --> 00:03:10,239
translation look aside buffer

99
00:03:07,680 --> 00:03:11,920
which is basically a cache of virtual to

100
00:03:10,239 --> 00:03:14,239
physical mappings

101
00:03:11,920 --> 00:03:15,680
so from the wikipedia page this is a

102
00:03:14,239 --> 00:03:17,440
good enough view of things

103
00:03:15,680 --> 00:03:19,599
you've got your CPU and it may be

104
00:03:17,440 --> 00:03:20,480
issuing requests for particular virtual

105
00:03:19,599 --> 00:03:22,720
addresses

106
00:03:20,480 --> 00:03:23,920
and then the MMU has to translate

107
00:03:22,720 --> 00:03:24,720
through the tables we're going to learn

108
00:03:23,920 --> 00:03:26,959
about next

109
00:03:24,720 --> 00:03:28,799
to find physical addresses sometimes it

110
00:03:26,959 --> 00:03:30,480
walks the tables but if it can get away

111
00:03:28,799 --> 00:03:32,720
with it it'll use the TLB

112
00:03:30,480 --> 00:03:35,040
translation look aside buffer has just a

113
00:03:32,720 --> 00:03:37,519
cache to say well this virtual address

114
00:03:35,040 --> 00:03:40,080
maps to that physical address now paging

115
00:03:37,519 --> 00:03:41,440
makes memory access virtual in sort of

116
00:03:40,080 --> 00:03:43,280
two senses

117
00:03:41,440 --> 00:03:45,040
the first sense of the word virtual

118
00:03:43,280 --> 00:03:46,879
memory is the notion

119
00:03:45,040 --> 00:03:48,799
that there's no longer a one-to-one

120
00:03:46,879 --> 00:03:49,680
correspondence between linear addresses

121
00:03:48,799 --> 00:03:51,760
and physical

122
00:03:49,680 --> 00:03:52,959
and that means you might have some high

123
00:03:51,760 --> 00:03:55,280
linear address

124
00:03:52,959 --> 00:03:57,360
and it may map to a low physical address

125
00:03:55,280 --> 00:03:59,920
or you may have low that maps to high or

126
00:03:57,360 --> 00:04:01,680
low that maps to nothing at all so

127
00:03:59,920 --> 00:04:03,280
the memory address has become virtual in

128
00:04:01,680 --> 00:04:04,879
the sense that you can't expect a

129
00:04:03,280 --> 00:04:06,640
one-to-one correspondence

130
00:04:04,879 --> 00:04:09,040
between some quote-unquote address you

131
00:04:06,640 --> 00:04:11,599
see and the actual address that goes out

132
00:04:09,040 --> 00:04:13,760
to the physical hardware buses to go

133
00:04:11,599 --> 00:04:15,519
index into RAM now the thing to remember

134
00:04:13,760 --> 00:04:17,759
is that for most of human history

135
00:04:15,519 --> 00:04:19,759
it was very cost-prohibitive to have 2

136
00:04:17,759 --> 00:04:22,479
to the 32 bytes of RAM

137
00:04:19,759 --> 00:04:23,520
only the most wealthy of rulers such as

138
00:04:22,479 --> 00:04:25,199
Gilgamesh

139
00:04:23,520 --> 00:04:27,120
could possibly afford to have 4

140
00:04:25,199 --> 00:04:29,120
gigabytes of RAM and that of course

141
00:04:27,120 --> 00:04:30,639
meant that virtual memory was for the

142
00:04:29,120 --> 00:04:33,840
most part translating

143
00:04:30,639 --> 00:04:34,479
a large 2 to the 32-bit space to a small

144
00:04:33,840 --> 00:04:35,840
chunk of

145
00:04:34,479 --> 00:04:37,759
physical memory whatever you could

146
00:04:35,840 --> 00:04:40,720
afford in your computer and now

147
00:04:37,759 --> 00:04:41,360
when paging is enabled on 64-bit systems

148
00:04:40,720 --> 00:04:44,000
we are

149
00:04:41,360 --> 00:04:45,840
in the area of time where it's cost

150
00:04:44,000 --> 00:04:47,120
prohibitive for people to have 2 to the

151
00:04:45,840 --> 00:04:49,680
64 bytes of

152
00:04:47,120 --> 00:04:50,800
RAM so necessarily it's always going to

153
00:04:49,680 --> 00:04:52,639
be the case that

154
00:04:50,800 --> 00:04:54,880
the linear address space the 64-bit

155
00:04:52,639 --> 00:04:56,880
address space is going to map from

156
00:04:54,880 --> 00:04:58,639
a large chunk of linear address space to

157
00:04:56,880 --> 00:05:01,759
a small chunk of physical memory

158
00:04:58,639 --> 00:05:03,680
no one has 2 to 64 bytes of RAM today

159
00:05:01,759 --> 00:05:05,440
the memory can also be virtual in a

160
00:05:03,680 --> 00:05:08,160
different sense of the word

161
00:05:05,440 --> 00:05:09,280
specifically it could be the case that

162
00:05:08,160 --> 00:05:11,759
memory that is not

163
00:05:09,280 --> 00:05:12,800
in active or frequent use at the moment

164
00:05:11,759 --> 00:05:15,600
is ultimately

165
00:05:12,800 --> 00:05:16,639
removed from RAM and transferred to some

166
00:05:15,600 --> 00:05:19,360
slower storage

167
00:05:16,639 --> 00:05:21,520
such as your hard drive or SSD that's

168
00:05:19,360 --> 00:05:23,440
called being the RAM is swapped out or

169
00:05:21,520 --> 00:05:26,479
it's paged out to disk

170
00:05:23,440 --> 00:05:28,639
and then if and when a process gets

171
00:05:26,479 --> 00:05:30,560
scheduled and that particular RAM gets

172
00:05:28,639 --> 00:05:31,440
accessed then the operating system on

173
00:05:30,560 --> 00:05:34,400
demand

174
00:05:31,440 --> 00:05:36,080
can go ahead and pull that memory in and

175
00:05:34,400 --> 00:05:38,160
put it back into RAM

176
00:05:36,080 --> 00:05:40,240
and so this definition is actually what

177
00:05:38,160 --> 00:05:42,720
we see intel use in the manual at some

178
00:05:40,240 --> 00:05:44,800
places

179
00:05:42,720 --> 00:05:46,720
and this is also something which I

180
00:05:44,800 --> 00:05:48,639
personally use just in my nerdy

181
00:05:46,720 --> 00:05:49,199
terminology frequently when I'm trying

182
00:05:48,639 --> 00:05:51,520
to

183
00:05:49,199 --> 00:05:53,440
you know stop and sit and activate my

184
00:05:51,520 --> 00:05:55,039
long-term storage memory for something

185
00:05:53,440 --> 00:05:57,280
that I haven't dealt with in a while

186
00:05:55,039 --> 00:05:58,479
I say you know hold on trying to page

187
00:05:57,280 --> 00:06:00,560
this back in right now

188
00:05:58,479 --> 00:06:02,240
I don't remember how this works so this

189
00:06:00,560 --> 00:06:03,759
sort of virtual memory is of course very

190
00:06:02,240 --> 00:06:04,800
good because it allows your operating

191
00:06:03,759 --> 00:06:06,800
system to pretend

192
00:06:04,800 --> 00:06:08,160
that it has more RAM that it actually

193
00:06:06,800 --> 00:06:11,120
physically does which

194
00:06:08,160 --> 00:06:12,960
was very important for a long time and

195
00:06:11,120 --> 00:06:14,960
the thing is it will of course come at a

196
00:06:12,960 --> 00:06:16,560
performance penalty cost if you remember

197
00:06:14,960 --> 00:06:18,880
the memory hierarchy it's

198
00:06:16,560 --> 00:06:20,160
you know orders of magnitude slower to

199
00:06:18,880 --> 00:06:22,560
try to go out to

200
00:06:20,160 --> 00:06:23,520
solid-state storage today than it is to

201
00:06:22,560 --> 00:06:26,000
just access RAM

202
00:06:23,520 --> 00:06:26,720
although there are sort of hybrid

203
00:06:26,000 --> 00:06:28,319
storage

204
00:06:26,720 --> 00:06:30,400
technologies coming along that are

205
00:06:28,319 --> 00:06:31,919
making that much smaller

206
00:06:30,400 --> 00:06:34,240
so the thing is of course that it's

207
00:06:31,919 --> 00:06:36,240
definitely worth it to

208
00:06:34,240 --> 00:06:37,840
pretend to have this sort of RAM rather

209
00:06:36,240 --> 00:06:39,680
than just start having the operating

210
00:06:37,840 --> 00:06:41,039
system throw it throw up its hands and

211
00:06:39,680 --> 00:06:42,639
say oh I don't know I don't have any

212
00:06:41,039 --> 00:06:43,759
more RAM for you sorry can't run any

213
00:06:42,639 --> 00:06:45,520
more programs

214
00:06:43,759 --> 00:06:46,960
sorry you know this program used up all

215
00:06:45,520 --> 00:06:49,759
the memory that other program

216
00:06:46,960 --> 00:06:51,440
has to crash now so that sort of virtual

217
00:06:49,759 --> 00:06:52,160
memory has always been extremely useful

218
00:06:51,440 --> 00:06:54,639
as well

219
00:06:52,160 --> 00:06:56,400
and that mechanism is ultimately enabled

220
00:06:54,639 --> 00:06:58,319
by this paging technology we'll talk

221
00:06:56,400 --> 00:07:00,080
about in a second

222
00:06:58,319 --> 00:07:02,400
all right so what did we learn about in

223
00:07:00,080 --> 00:07:04,240
this section just very trivially we

224
00:07:02,400 --> 00:07:06,080
got our reintroduction to this linear

225
00:07:04,240 --> 00:07:09,120
address space which for our purposes

226
00:07:06,080 --> 00:07:10,800
is just some big flat 64-bit space

227
00:07:09,120 --> 00:07:12,800
and we have this notion that there's

228
00:07:10,800 --> 00:07:15,919
pages as fixed-sized

229
00:07:12,800 --> 00:07:17,919
chunks of of memory fixed size chunks of

230
00:07:15,919 --> 00:07:20,080
virtual memory and frames are the fixed

231
00:07:17,919 --> 00:07:20,880
size chunks of physical memory that back

232
00:07:20,080 --> 00:07:22,880
it

233
00:07:20,880 --> 00:07:25,280
and so now in the next sections we're

234
00:07:22,880 --> 00:07:27,360
going to try to understand how all these

235
00:07:25,280 --> 00:07:30,160
tables work to get us from here

236
00:07:27,360 --> 00:07:30,160
to there

