1
00:00:00,000 --> 00:00:03,520
so now I want you to do a quick lab I

2
00:00:01,839 --> 00:00:04,240
want you to go ahead and go into

3
00:00:03,520 --> 00:00:07,359
WinDbg

4
00:00:04,240 --> 00:00:10,960
and read the CR3 value so just break in

5
00:00:07,359 --> 00:00:12,719
and read CR3 value based on that

6
00:00:10,960 --> 00:00:14,719
and based on the max physical address

7
00:00:12,719 --> 00:00:16,800
that you saw before and looking back at

8
00:00:14,719 --> 00:00:18,720
the interpretation of CR3 according to

9
00:00:16,800 --> 00:00:21,760
these slides in the manual

10
00:00:18,720 --> 00:00:24,800
what is the physical address where the

11
00:00:21,760 --> 00:00:25,359
page map level 4 is also to get at this

12
00:00:24,800 --> 00:00:27,519
notion

13
00:00:25,359 --> 00:00:28,800
that the pro that the operating system

14
00:00:27,519 --> 00:00:31,279
will be swapping around

15
00:00:28,800 --> 00:00:33,200
page tables for different processes go

16
00:00:31,279 --> 00:00:34,880
ahead and continue in debug and then

17
00:00:33,200 --> 00:00:37,440
pause and then continue and pause and

18
00:00:34,880 --> 00:00:39,840
continue and pause repeat it a few times

19
00:00:37,440 --> 00:00:41,280
and what you should see is that the CR3

20
00:00:39,840 --> 00:00:43,280
value is changing

21
00:00:41,280 --> 00:00:44,879
now one time I got myself into a state

22
00:00:43,280 --> 00:00:45,920
where it just really didn't seem to want

23
00:00:44,879 --> 00:00:48,079
to change but

24
00:00:45,920 --> 00:00:49,840
hopefully if you pause and stop a few

25
00:00:48,079 --> 00:00:52,960
times maybe start your vm

26
00:00:49,840 --> 00:00:54,800
go move around some processes you know

27
00:00:52,960 --> 00:00:55,680
just so that they take active focus and

28
00:00:54,800 --> 00:00:57,280
then

29
00:00:55,680 --> 00:00:59,600
break in again you'll see what I'm

30
00:00:57,280 --> 00:01:01,359
talking about here also each time you

31
00:00:59,600 --> 00:01:03,600
break if you'd like a little bit of

32
00:01:01,359 --> 00:01:05,439
more context about what windows thinks

33
00:01:03,600 --> 00:01:07,520
this particular process is that

34
00:01:05,439 --> 00:01:08,880
it's in the context right now you can

35
00:01:07,520 --> 00:01:11,920
run bang process

36
00:01:08,880 --> 00:01:12,560
-1 space 0 and that will

37
00:01:11,920 --> 00:01:14,320
show you

38
00:01:12,560 --> 00:01:17,200
what it thinks this particular process

39
00:01:14,320 --> 00:01:17,200
context is

