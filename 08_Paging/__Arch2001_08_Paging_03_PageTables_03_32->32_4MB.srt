1
00:00:00,14 --> 00:00:02,56
now let's move on to the next permutation

2
00:00:02,57 --> 00:00:05,66
Only difference here is that CR4.PSE for

3
00:00:05,66 --> 00:00:09,01
page size extensions is set to 1 and therefore,

4
00:00:09,02 --> 00:00:10,7
if the operating system wants,

5
00:00:10,89 --> 00:00:14,26
it can use 4 megabyte pages at a depth of

6
00:00:14,26 --> 00:00:17,19
1 instead of 4 kilobyte pages at a depth of

7
00:00:17,19 --> 00:00:17,55
2

8
00:00:18,04 --> 00:00:19,15
So let's see how that works

9
00:00:19,94 --> 00:00:22,34
So we start in the same sort of idea as

10
00:00:22,34 --> 00:00:24,18
before 32-bit linear address

11
00:00:24,18 --> 00:00:28,38
space 10 bit index at the beginning And the CR3

12
00:00:28,38 --> 00:00:31,17
is going to point at the base of this

13
00:00:31,17 --> 00:00:31,78
table 1,

14
00:00:31,78 --> 00:00:38,43
this page directory And the entry is then going to

15
00:00:38,43 --> 00:00:41,86
only use 10 bits instead of 20 bits,

16
00:00:41,86 --> 00:00:42,96
like it used last time

17
00:00:44,44 --> 00:00:47,23
So that means that the most significant 10 bits are

18
00:00:47,23 --> 00:00:50,22
going to be used as part of a physical address

19
00:00:50,22 --> 00:00:53,41
of the next 4 megabyte page where the bottom 22

20
00:00:53,41 --> 00:00:54,96
bits are all assumed to be 0

21
00:00:55,34 --> 00:00:58,08
That leaves 22 bits for flags and stuff,

22
00:00:58,12 --> 00:01:01,08
just like there were 12 bits available in 4 kilobyte

23
00:01:01,08 --> 00:01:01,55
paging

24
00:01:04,44 --> 00:01:05,09
So again,

25
00:01:05,09 --> 00:01:07,82
that most significant 10 bits is going to point at

26
00:01:07,82 --> 00:01:09,36
a 4 megabyte page

27
00:01:09,94 --> 00:01:10,25
All right

28
00:01:10,26 --> 00:01:11,74
So basically this entry,

29
00:01:11,74 --> 00:01:13,53
now we've got this table depth of 1

30
00:01:13,53 --> 00:01:17,36
We just skip directly from a page directory,

31
00:01:17,37 --> 00:01:19,56
which I need to update that to say page directory

32
00:01:20,14 --> 00:01:21,91
We skip directly from a page directory

33
00:01:21,91 --> 00:01:25,16
And instead of going through another 10 bit intermediate index

34
00:01:25,41 --> 00:01:28,83
we instead are going to just agglomerate all

35
00:01:28,83 --> 00:01:31,74
of that and say a 22 bit index into this

36
00:01:31,74 --> 00:01:35,76
2 to the 22 4 megabyte size giant page

37
00:01:37,94 --> 00:01:40,49
And then we immediately find the bites that we're looking

38
00:01:40,49 --> 00:01:40,76
for

39
00:01:41,14 --> 00:01:45,66
So that's a much simpler transition to find a large

40
00:01:45,66 --> 00:01:46,45
chunk of memory

41
00:01:48,24 --> 00:01:50,75
And that's what this intel diagram was basically trying to

42
00:01:50,75 --> 00:01:54,9
say you've got CR3 pointing out a page directory

43
00:01:54,9 --> 00:01:57,87
10 bits used to find a page directory entry,

44
00:01:57,88 --> 00:02:00,61
and then only 10 bits from within that page directory

45
00:02:00,61 --> 00:02:03,83
are used as the most significant bits of the large

46
00:02:03,83 --> 00:02:07,16
4 megabyte page and then 22 bits are used to

47
00:02:07,16 --> 00:02:10,56
offset for any given bite inside of that page

48
00:02:11,34 --> 00:02:16,17
This was what the 2008 Manual when I first made

49
00:02:16,17 --> 00:02:20,25
this class had and if we look at that in

50
00:02:20,25 --> 00:02:21,24
the newest manual,

51
00:02:21,25 --> 00:02:23,32
instead of seeing 10 bits here,

52
00:02:23,33 --> 00:02:25,27
you see 18 bits,

53
00:02:25,28 --> 00:02:26,86
you still see 22 bit index,

54
00:02:26,86 --> 00:02:27,96
but you see 18 bits

55
00:02:27,96 --> 00:02:30,7
And so if we add those two together previously,

56
00:02:30,7 --> 00:02:33,74
it was 22 and 10 so it was a 32-bit physical address

57
00:02:33,74 --> 00:02:37,07
space that we were accessing And now it's 18 and

58
00:02:37,07 --> 00:02:37,77
22

59
00:02:37,77 --> 00:02:41,46
So it's a 40-bit physical address space that this

60
00:02:41,46 --> 00:02:42,26
access is

