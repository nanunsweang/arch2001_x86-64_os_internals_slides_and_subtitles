1
00:00:00,080 --> 00:00:04,319
all right it's time to get really real

2
00:00:01,920 --> 00:00:06,399
and start digging into exactly how this

3
00:00:04,319 --> 00:00:08,240
page table mechanism works

4
00:00:06,399 --> 00:00:09,920
so we know that it translates a linear

5
00:00:08,240 --> 00:00:10,400
address space to a physical address

6
00:00:09,920 --> 00:00:12,639
space

7
00:00:10,400 --> 00:00:14,799
through some set of tables this right

8
00:00:12,639 --> 00:00:16,080
here is the simplest possible form this

9
00:00:14,799 --> 00:00:19,680
is a 32-bit

10
00:00:16,080 --> 00:00:22,400
4 kilobyte paging version of this

11
00:00:19,680 --> 00:00:23,359
and we know that it starts at CR3 to

12
00:00:22,400 --> 00:00:25,439
point at the

13
00:00:23,359 --> 00:00:27,199
starting point for translation now there

14
00:00:25,439 --> 00:00:29,119
are a ton of different versions of

15
00:00:27,199 --> 00:00:30,240
paging that can be used by an operating

16
00:00:29,119 --> 00:00:31,599
system and

17
00:00:30,240 --> 00:00:33,520
we're not going to make all of these

18
00:00:31,599 --> 00:00:35,440
mandatory for you to learn about

19
00:00:33,520 --> 00:00:37,440
we are going to cover the first one in

20
00:00:35,440 --> 00:00:38,000
considerable depth because that was the

21
00:00:37,440 --> 00:00:40,079
original

22
00:00:38,000 --> 00:00:43,360
and everything else after that is just

23
00:00:40,079 --> 00:00:45,760
kind of extensions on that same theme

24
00:00:43,360 --> 00:00:47,600
but this table is basically saying in

25
00:00:45,760 --> 00:00:50,320
32-bit mode we've got you know

26
00:00:47,600 --> 00:00:52,559
one set of possible paging permutations

27
00:00:50,320 --> 00:00:55,120
and in 64-bit we've got another

28
00:00:52,559 --> 00:00:56,239
things like the CR4 physical address

29
00:00:55,120 --> 00:00:58,559
extensions

30
00:00:56,239 --> 00:01:00,719
control whether or not you can access

31
00:00:58,559 --> 00:01:03,520
large areas of physical memory

32
00:01:00,719 --> 00:01:05,040
and page size extensions control whether

33
00:01:03,520 --> 00:01:06,479
or not you can have

34
00:01:05,040 --> 00:01:08,320
pages which are greater than 4

35
00:01:06,479 --> 00:01:10,080
kilobytes so you can see without page

36
00:01:08,320 --> 00:01:11,040
size extensions it's always 4

37
00:01:10,080 --> 00:01:13,040
kilobyte

38
00:01:11,040 --> 00:01:14,720
and with them it can be larger like 4

39
00:01:13,040 --> 00:01:15,680
megabytes 2 megabytes and even 1

40
00:01:14,720 --> 00:01:17,840
gigabyte

41
00:01:15,680 --> 00:01:19,520
the sizes of linear address to physical

42
00:01:17,840 --> 00:01:22,479
address translation that occur

43
00:01:19,520 --> 00:01:23,360
at different configurations started with

44
00:01:22,479 --> 00:01:27,680
32 to

45
00:01:23,360 --> 00:01:30,560
32 then we have with the addition of

46
00:01:27,680 --> 00:01:31,439
page size extensions well originally

47
00:01:30,560 --> 00:01:34,320
this could still

48
00:01:31,439 --> 00:01:36,720
only do 32 to 32 but now it actually

49
00:01:34,320 --> 00:01:37,920
supports a physical address space of 40

50
00:01:36,720 --> 00:01:41,360
bits

51
00:01:37,920 --> 00:01:42,560
and then on the physical address

52
00:01:41,360 --> 00:01:45,600
extensions

53
00:01:42,560 --> 00:01:47,680
originally it was 32 bits to 36 bits

54
00:01:45,600 --> 00:01:50,320
and now it sort of just continues to

55
00:01:47,680 --> 00:01:52,240
grow where there's some

56
00:01:50,320 --> 00:01:54,240
there's some configuration called the

57
00:01:52,240 --> 00:01:55,520
max physical address that's reported by

58
00:01:54,240 --> 00:01:57,680
cpuid

59
00:01:55,520 --> 00:02:00,240
and that tells you how large of a space

60
00:01:57,680 --> 00:02:02,640
ultimately the processor is capable of

61
00:02:00,240 --> 00:02:04,719
accessing so if 32-bit systems always

62
00:02:02,640 --> 00:02:06,479
started with a linear address of 32 and

63
00:02:04,719 --> 00:02:07,520
they can now access larger physical

64
00:02:06,479 --> 00:02:10,000
address spaces

65
00:02:07,520 --> 00:02:11,599
64-bit systems you would think would

66
00:02:10,000 --> 00:02:12,720
start with a linear address space of

67
00:02:11,599 --> 00:02:14,239
64-bits

68
00:02:12,720 --> 00:02:15,680
but they don't actually the the

69
00:02:14,239 --> 00:02:17,680
processors that supported these

70
00:02:15,680 --> 00:02:20,800
extensions started at 50

71
00:02:17,680 --> 00:02:22,400
sorry 48 bits of linear address space so

72
00:02:20,800 --> 00:02:24,239
you could never really get access to the

73
00:02:22,400 --> 00:02:26,480
full 64 bit space

74
00:02:24,239 --> 00:02:28,319
and only recently with the addition of

75
00:02:26,480 --> 00:02:32,160
this new CR4

76
00:02:28,319 --> 00:02:34,720
last linear address 57 bit addressing

77
00:02:32,160 --> 00:02:36,000
now they've upgraded it to 57 bits

78
00:02:34,720 --> 00:02:39,440
though I'm not aware of anyone

79
00:02:36,000 --> 00:02:41,120
using that yet so again we're going to

80
00:02:39,440 --> 00:02:42,400
cover we're going to cover technically

81
00:02:41,120 --> 00:02:44,239
all of these but not everything's going

82
00:02:42,400 --> 00:02:46,480
to be required like I said

83
00:02:44,239 --> 00:02:47,840
as far as I know no one's using 57 bit

84
00:02:46,480 --> 00:02:49,440
address space yet for

85
00:02:47,840 --> 00:02:51,120
normal operating systems maybe some

86
00:02:49,440 --> 00:02:53,120
super computers are

87
00:02:51,120 --> 00:02:55,280
and so as we go along here we'll start

88
00:02:53,120 --> 00:02:56,480
with the simplest form which is always

89
00:02:55,280 --> 00:02:58,159
4 kilobyte pages

90
00:02:56,480 --> 00:03:00,080
and a page depth of 2 so there's going

91
00:02:58,159 --> 00:03:01,440
to be 2 intermediate tables that get

92
00:03:00,080 --> 00:03:02,640
translated through before you get to

93
00:03:01,440 --> 00:03:04,879
your page

94
00:03:02,640 --> 00:03:06,400
and then when we start adding in these

95
00:03:04,879 --> 00:03:09,120
larger page sizes

96
00:03:06,400 --> 00:03:10,239
or larger physical address spaces we

97
00:03:09,120 --> 00:03:12,239
start getting different

98
00:03:10,239 --> 00:03:13,840
page depth so if it's 4 kilobytes

99
00:03:12,239 --> 00:03:15,519
then it's still 2 depth

100
00:03:13,840 --> 00:03:17,120
but 4 megabytes will end up having

101
00:03:15,519 --> 00:03:19,200
only a single depth and that

102
00:03:17,120 --> 00:03:20,480
you'll see is how they access the bigger

103
00:03:19,200 --> 00:03:23,440
page but

104
00:03:20,480 --> 00:03:25,120
if the first little you know lab for you

105
00:03:23,440 --> 00:03:26,959
if the point of the page table is to

106
00:03:25,120 --> 00:03:27,840
translate linear addresses to physical

107
00:03:26,959 --> 00:03:30,480
addresses

108
00:03:27,840 --> 00:03:32,400
let's get a sense of what kind of linear

109
00:03:30,480 --> 00:03:35,360
address space and physical address space

110
00:03:32,400 --> 00:03:36,239
is supported on your specific processor

111
00:03:35,360 --> 00:03:39,920
so to do that

112
00:03:36,239 --> 00:03:42,560
I want you to go modify your U_CPUID

113
00:03:39,920 --> 00:03:44,319
code and I want you to print out the max

114
00:03:42,560 --> 00:03:45,360
physical address and the max linear

115
00:03:44,319 --> 00:03:47,680
address

116
00:03:45,360 --> 00:03:50,879
so you can find the max physical address

117
00:03:47,680 --> 00:03:52,159
by passing in 8008 for eax and then the

118
00:03:50,879 --> 00:03:54,480
output eax

119
00:03:52,159 --> 00:03:56,319
bits 0 through 7 is going to be physical

120
00:03:54,480 --> 00:03:58,239
address and the outputs eax

121
00:03:56,319 --> 00:03:59,680
bits 8 through 15 is going to be the

122
00:03:58,239 --> 00:04:01,519
linear address

123
00:03:59,680 --> 00:04:02,959
so what is the physical address for your

124
00:04:01,519 --> 00:04:04,640
vm and

125
00:04:02,959 --> 00:04:06,720
what is the address for your host you

126
00:04:04,640 --> 00:04:08,480
can you know take that same binary and

127
00:04:06,720 --> 00:04:09,840
as long as you have the visual studio

128
00:04:08,480 --> 00:04:12,080
redistributable

129
00:04:09,840 --> 00:04:12,879
package installed on your host you can

130
00:04:12,080 --> 00:04:14,400
check there too

131
00:04:12,879 --> 00:04:15,840
or if you're running some non-windows

132
00:04:14,400 --> 00:04:16,639
system that might be a little more

133
00:04:15,840 --> 00:04:18,720
complicated

134
00:04:16,639 --> 00:04:20,000
I'll post some links to tools that you

135
00:04:18,720 --> 00:04:23,040
can use

136
00:04:20,000 --> 00:04:26,960
so for me personally my host macOS

137
00:04:23,040 --> 00:04:30,240
reported a max phys address of 39 bits

138
00:04:26,960 --> 00:04:33,280
so that would be about 512 gigabytes

139
00:04:30,240 --> 00:04:34,000
I only have 64 gigabytes in my computer

140
00:04:33,280 --> 00:04:36,400
so that's

141
00:04:34,000 --> 00:04:37,280
more than enough but what was

142
00:04:36,400 --> 00:04:40,400
interesting is

143
00:04:37,280 --> 00:04:42,000
inside of a Linux vm it reported a max

144
00:04:40,400 --> 00:04:44,720
physical address of 43

145
00:04:42,000 --> 00:04:46,080
bits and inside of my windows vm that

146
00:04:44,720 --> 00:04:48,560
I've been using thus far

147
00:04:46,080 --> 00:04:49,360
it reports a max physical address of 45

148
00:04:48,560 --> 00:04:51,280
bits

149
00:04:49,360 --> 00:04:53,040
so this could be different based on you

150
00:04:51,280 --> 00:04:54,080
know what virtualization system you're

151
00:04:53,040 --> 00:04:55,759
using

152
00:04:54,080 --> 00:04:57,520
and what processor you have and

153
00:04:55,759 --> 00:05:00,000
everything else so

154
00:04:57,520 --> 00:05:02,800
just go see what is reported by your

155
00:05:00,000 --> 00:05:02,800
system

