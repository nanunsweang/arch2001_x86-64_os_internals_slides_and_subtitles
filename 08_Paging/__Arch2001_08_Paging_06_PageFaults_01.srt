1
00:00:00,240 --> 00:00:04,319
previously when we were examining the

2
00:00:02,080 --> 00:00:06,560
page table entries the various things at

3
00:00:04,319 --> 00:00:08,559
level 4, 3, 2 and 1

4
00:00:06,560 --> 00:00:10,400
we left off for the most part the

5
00:00:08,559 --> 00:00:11,360
consideration of the case where the

6
00:00:10,400 --> 00:00:13,840
present bit

7
00:00:11,360 --> 00:00:14,480
is set to 0 because we said the MMU

8
00:00:13,840 --> 00:00:16,400
doesn't

9
00:00:14,480 --> 00:00:17,600
try to parse any further bits out of the

10
00:00:16,400 --> 00:00:19,520
entry in that case

11
00:00:17,600 --> 00:00:21,920
and that it would ultimately be an error

12
00:00:19,520 --> 00:00:24,400
so what really happens then if someone

13
00:00:21,920 --> 00:00:27,359
attempts to access a linear address

14
00:00:24,400 --> 00:00:29,199
the MMU does the table walk and it comes

15
00:00:27,359 --> 00:00:30,320
across an entry that has present equal

16
00:00:29,199 --> 00:00:33,200
to 0

17
00:00:30,320 --> 00:00:33,680
what happens is a page fault the page

18
00:00:33,200 --> 00:00:36,800
fault

19
00:00:33,680 --> 00:00:38,719
is interrupt 14 and upon

20
00:00:36,800 --> 00:00:40,640
receipt of a page fault the page fault

21
00:00:38,719 --> 00:00:42,000
handler then tries to determine

22
00:00:40,640 --> 00:00:44,480
whether or not this is going to be a

23
00:00:42,000 --> 00:00:45,680
recoverable fault or a non-recoverable

24
00:00:44,480 --> 00:00:49,360
fault

25
00:00:45,680 --> 00:00:51,440
and so this is specifically a type fault

26
00:00:49,360 --> 00:00:53,840
and any sort of memory reference whether

27
00:00:51,440 --> 00:00:54,640
it's instruction fetches or data reads

28
00:00:53,840 --> 00:00:56,399
or writes

29
00:00:54,640 --> 00:00:58,399
can cause a page fault when we talked

30
00:00:56,399 --> 00:00:59,039
about paging in the control registers we

31
00:00:58,399 --> 00:01:02,399
said that

32
00:00:59,039 --> 00:01:03,520
CR2 control register 2 will hold the

33
00:01:02,399 --> 00:01:05,600
linear address

34
00:01:03,520 --> 00:01:07,439
that was attempting to be translated at

35
00:01:05,600 --> 00:01:09,200
the time that a page fault occurred

36
00:01:07,439 --> 00:01:10,880
so that the page fault handler can go

37
00:01:09,200 --> 00:01:11,200
take a look at it and figure out where

38
00:01:10,880 --> 00:01:13,760
this

39
00:01:11,200 --> 00:01:15,840
was happening furthermore the page fault

40
00:01:13,760 --> 00:01:17,439
is one of the types of faults that

41
00:01:15,840 --> 00:01:19,040
pushes an error code

42
00:01:17,439 --> 00:01:21,040
and so the page fault handler will be

43
00:01:19,040 --> 00:01:22,320
responsible for interpreting that error

44
00:01:21,040 --> 00:01:25,680
code

45
00:01:22,320 --> 00:01:26,080
so this is the format of the page fault

46
00:01:25,680 --> 00:01:28,159
error

47
00:01:26,080 --> 00:01:29,840
code and there's some bits here that

48
00:01:28,159 --> 00:01:31,439
pertain to security stuff that we

49
00:01:29,840 --> 00:01:33,520
haven't really covered in this class

50
00:01:31,439 --> 00:01:34,799
so I'm just going to hide those for the

51
00:01:33,520 --> 00:01:36,640
moment

52
00:01:34,799 --> 00:01:38,000
and so what is the interpretation of the

53
00:01:36,640 --> 00:01:40,960
error code well

54
00:01:38,000 --> 00:01:43,759
if bit 0 is set to 0 then this was a

55
00:01:40,960 --> 00:01:45,759
page fault caused by a non-present page

56
00:01:43,759 --> 00:01:46,880
if instead it was 1 then there was not

57
00:01:45,759 --> 00:01:49,920
some sort of

58
00:01:46,880 --> 00:01:52,000
protection violation such as attempts to

59
00:01:49,920 --> 00:01:54,479
write to a read-only page

60
00:01:52,000 --> 00:01:56,799
attempts to execute something that's not

61
00:01:54,479 --> 00:01:59,600
executable and so forth

62
00:01:56,799 --> 00:02:00,960
the R/W is further information to tell

63
00:01:59,600 --> 00:02:02,640
you you know if this was a protection

64
00:02:00,960 --> 00:02:03,360
violation was it due to a read or a

65
00:02:02,640 --> 00:02:04,880
write

66
00:02:03,360 --> 00:02:06,719
U/S is telling you whether it was

67
00:02:04,880 --> 00:02:07,600
something to do with you know user-mode

68
00:02:06,719 --> 00:02:10,640
or

69
00:02:07,600 --> 00:02:11,120
supervisor-mode causing the fault the reserve

70
00:02:10,640 --> 00:02:13,440
bit

71
00:02:11,120 --> 00:02:15,360
now we don't care about and then the I/D

72
00:02:13,440 --> 00:02:16,000
bit indicates whether or not this was an

73
00:02:15,360 --> 00:02:18,720
instruction

74
00:02:16,000 --> 00:02:20,959
fetch or a data fetch so as was sort of

75
00:02:18,720 --> 00:02:22,000
indicated by that bit 0 there's two

76
00:02:20,959 --> 00:02:24,080
primary causes

77
00:02:22,000 --> 00:02:25,520
of page faults there's straight up

78
00:02:24,080 --> 00:02:27,360
non-present pages

79
00:02:25,520 --> 00:02:29,840
which could be due to invalid mappings

80
00:02:27,360 --> 00:02:32,239
and then there's permissions issues

81
00:02:29,840 --> 00:02:34,720
so let's talk about some of the cases of

82
00:02:32,239 --> 00:02:36,720
recoverable page faults

83
00:02:34,720 --> 00:02:38,879
the first is that if a page is swapped

84
00:02:36,720 --> 00:02:40,080
out as in the operating system has

85
00:02:38,879 --> 00:02:42,319
decided to

86
00:02:40,080 --> 00:02:43,519
remove some memory from RAM stick it

87
00:02:42,319 --> 00:02:46,400
onto the hard drive

88
00:02:43,519 --> 00:02:47,599
for use later on if that process tries

89
00:02:46,400 --> 00:02:49,519
to access it

90
00:02:47,599 --> 00:02:51,680
so swapping it out to disk would be one

91
00:02:49,519 --> 00:02:53,280
reason now you should be aware that

92
00:02:51,680 --> 00:02:54,959
every operating system has some

93
00:02:53,280 --> 00:02:56,000
different sort of format and some of

94
00:02:54,959 --> 00:02:59,280
them reuse

95
00:02:56,000 --> 00:03:02,959
the unused and ignored bits in the

96
00:02:59,280 --> 00:03:03,680
page page table entry to try to help it

97
00:03:02,959 --> 00:03:07,200
look up where this

98
00:03:03,680 --> 00:03:09,680
information is there's also

99
00:03:07,200 --> 00:03:11,760
automatic stack growth so this could be

100
00:03:09,680 --> 00:03:12,959
as you're continuing to push things onto

101
00:03:11,760 --> 00:03:14,400
the stack and as

102
00:03:12,959 --> 00:03:16,400
it grows towards lower and lower

103
00:03:14,400 --> 00:03:16,879
addresses eventually you're going to

104
00:03:16,400 --> 00:03:19,200
cross

105
00:03:16,879 --> 00:03:22,159
some page boundary you're going to you

106
00:03:19,200 --> 00:03:25,200
know push information into the top bytes

107
00:03:22,159 --> 00:03:27,040
of one lower page when that happens

108
00:03:25,200 --> 00:03:28,959
the page fault handler should just go

109
00:03:27,040 --> 00:03:29,519
ahead and allocate some new physical

110
00:03:28,959 --> 00:03:31,680
address

111
00:03:29,519 --> 00:03:33,680
range for that particular linear address

112
00:03:31,680 --> 00:03:34,400
that now corresponds to the bottom of

113
00:03:33,680 --> 00:03:35,840
the stack

114
00:03:34,400 --> 00:03:38,319
and it should just let the system

115
00:03:35,840 --> 00:03:40,239
continue

116
00:03:38,319 --> 00:03:42,400
then there are also attempts to write to

117
00:03:40,239 --> 00:03:45,040
read-only memory these are recoverable

118
00:03:42,400 --> 00:03:46,319
if that memory is intended to be what's

119
00:03:45,040 --> 00:03:48,239
called copy-on-write

120
00:03:46,319 --> 00:03:50,480
so if an operating system is trying to

121
00:03:48,239 --> 00:03:51,760
optimize RAM usage by using this notion

122
00:03:50,480 --> 00:03:54,000
of copy-on-write

123
00:03:51,760 --> 00:03:55,760
wherein it tries to share memory amongst

124
00:03:54,000 --> 00:03:58,159
the different processes

125
00:03:55,760 --> 00:03:59,439
then it can say okay now I can see that

126
00:03:58,159 --> 00:04:01,439
someone is trying to write to this

127
00:03:59,439 --> 00:04:05,120
process now I will split off

128
00:04:01,439 --> 00:04:07,519
and copy the written dirty changed

129
00:04:05,120 --> 00:04:08,720
page so that this other process that's

130
00:04:07,519 --> 00:04:09,680
trying to do the write will see

131
00:04:08,720 --> 00:04:11,519
something different

132
00:04:09,680 --> 00:04:13,280
than the original one which you know has

133
00:04:11,519 --> 00:04:14,879
not actually written to this page

134
00:04:13,280 --> 00:04:17,120
so that's another case in which it could

135
00:04:14,879 --> 00:04:19,040
be a recoverable page fault

136
00:04:17,120 --> 00:04:20,720
some of the unrecoverable page faults

137
00:04:19,040 --> 00:04:23,040
are just when they're straight up no

138
00:04:20,720 --> 00:04:23,840
valid linear-to-physical translation

139
00:04:23,040 --> 00:04:26,880
this could be

140
00:04:23,840 --> 00:04:27,440
because some process has generated an

141
00:04:26,880 --> 00:04:29,600
address

142
00:04:27,440 --> 00:04:30,720
just you know out of thin air or out of

143
00:04:29,600 --> 00:04:32,880
an error

144
00:04:30,720 --> 00:04:33,759
and it attempts to access memory at a

145
00:04:32,880 --> 00:04:36,000
given address

146
00:04:33,759 --> 00:04:36,880
but that address was never legitimately

147
00:04:36,000 --> 00:04:38,639
mapped into memory

148
00:04:36,880 --> 00:04:41,040
this is again why you know an operating

149
00:04:38,639 --> 00:04:41,680
system should initialize page tables to

150
00:04:41,040 --> 00:04:43,919
0

151
00:04:41,680 --> 00:04:46,000
so that if someone tries to access a

152
00:04:43,919 --> 00:04:46,560
random address that's not expected to be

153
00:04:46,000 --> 00:04:48,720
there

154
00:04:46,560 --> 00:04:50,160
it will lead to the MMU hitting a

155
00:04:48,720 --> 00:04:52,240
present bit of 0

156
00:04:50,160 --> 00:04:54,160
and causing a page fault and then you

157
00:04:52,240 --> 00:04:55,520
know if it does not correspond to one of

158
00:04:54,160 --> 00:04:57,040
the recoverable cases

159
00:04:55,520 --> 00:05:00,160
the operating system can just you know

160
00:04:57,040 --> 00:05:02,479
kill the process or do something else

161
00:05:00,160 --> 00:05:04,160
another recover unrecoverable page fault

162
00:05:02,479 --> 00:05:05,440
is an attempt to write to read-only

163
00:05:04,160 --> 00:05:07,840
memory if that is not

164
00:05:05,440 --> 00:05:09,600
intended to be copy-on-write so this is

165
00:05:07,840 --> 00:05:10,080
again you know some chunk of memory that

166
00:05:09,600 --> 00:05:11,759
is not

167
00:05:10,080 --> 00:05:13,919
supposed to be shared between two

168
00:05:11,759 --> 00:05:15,520
processes if you try to write it

169
00:05:13,919 --> 00:05:17,360
then the operating system could say no

170
00:05:15,520 --> 00:05:19,120
this is not copy-on-write you're doing

171
00:05:17,360 --> 00:05:21,120
something wrong

172
00:05:19,120 --> 00:05:22,479
any user code attempting to access

173
00:05:21,120 --> 00:05:24,800
supervisor memory well

174
00:05:22,479 --> 00:05:27,120
the operating system the whole notion of

175
00:05:24,800 --> 00:05:29,120
ring 0 ring 3 separation

176
00:05:27,120 --> 00:05:30,560
is that it's trying to stop exactly that

177
00:05:29,120 --> 00:05:32,479
sort of behavior so

178
00:05:30,560 --> 00:05:34,000
that's another grounds for termination

179
00:05:32,479 --> 00:05:35,759
of a process

180
00:05:34,000 --> 00:05:38,639
and then also the sort of permissions

181
00:05:35,759 --> 00:05:40,800
errors having to do with SMAP SMEP or XD

182
00:05:38,639 --> 00:05:43,360
things where if the particular operating

183
00:05:40,800 --> 00:05:45,440
system firmware hypervisor system

184
00:05:43,360 --> 00:05:47,199
if they're using these security bits for

185
00:05:45,440 --> 00:05:49,520
their intended purpose

186
00:05:47,199 --> 00:05:51,120
then attempts to you know execute where

187
00:05:49,520 --> 00:05:52,960
you're not supposed to execute access

188
00:05:51,120 --> 00:05:55,440
where you're not supposed to access

189
00:05:52,960 --> 00:05:57,199
should lead to page fault violations and

190
00:05:55,440 --> 00:06:00,080
ultimate you know termination of the

191
00:05:57,199 --> 00:06:00,080
offending process

