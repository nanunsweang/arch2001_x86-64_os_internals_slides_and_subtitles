1
00:00:00,080 --> 00:00:03,919
but uh this is what we're here for we

2
00:00:01,760 --> 00:00:06,720
want to learn about how 64-bit

3
00:00:03,919 --> 00:00:08,880
paging works so first of all physical

4
00:00:06,720 --> 00:00:10,559
address extensions is mandatory if

5
00:00:08,880 --> 00:00:11,759
you're going to have 64-bit paging that

6
00:00:10,559 --> 00:00:15,200
was actually shown

7
00:00:11,759 --> 00:00:17,279
on our black manta finite state machine

8
00:00:15,200 --> 00:00:18,640
diagram before about transitioning to

9
00:00:17,279 --> 00:00:20,560
long mode

10
00:00:18,640 --> 00:00:22,560
and then really it comes down to you

11
00:00:20,560 --> 00:00:23,039
know are you using page size extensions

12
00:00:22,560 --> 00:00:25,439
if not

13
00:00:23,039 --> 00:00:27,519
4 kilobytes only if so then you can

14
00:00:25,439 --> 00:00:27,840
actually get 4 kilobyte 2 megabyte

15
00:00:27,519 --> 00:00:30,000
not

16
00:00:27,840 --> 00:00:31,519
4 megabyte like it was up here and

17
00:00:30,000 --> 00:00:34,000
1 gigabyte

18
00:00:31,519 --> 00:00:34,559
and then there's the last for 57-bit

19
00:00:34,000 --> 00:00:37,840
addressing

20
00:00:34,559 --> 00:00:39,120
so let's look at this one now for this

21
00:00:37,840 --> 00:00:40,640
I'm not going to give you the nice

22
00:00:39,120 --> 00:00:42,960
animated version you can go

23
00:00:40,640 --> 00:00:44,879
re-watch the 32-bit version if you want

24
00:00:42,960 --> 00:00:46,719
to see that but this is just the

25
00:00:44,879 --> 00:00:48,320
exact same principles that we saw with

26
00:00:46,719 --> 00:00:51,600
the 32-bit version

27
00:00:48,320 --> 00:00:53,680
extended to having a depth of 4 here

28
00:00:51,600 --> 00:00:54,079
instead of a depth of 2 so originally

29
00:00:53,680 --> 00:00:56,079
you know

30
00:00:54,079 --> 00:00:58,079
put your hand over the screen right here

31
00:00:56,079 --> 00:00:59,520
and you'll see 32-bit paging right

32
00:00:58,079 --> 00:01:01,440
you'll see that you've got a page

33
00:00:59,520 --> 00:01:04,159
directory and a page table

34
00:01:01,440 --> 00:01:05,760
not quite 32-bit paging but because

35
00:01:04,159 --> 00:01:08,240
those would have needed to be 10 bits

36
00:01:05,760 --> 00:01:10,320
instead of 9 bits but close enough

37
00:01:08,240 --> 00:01:12,960
all right so miscellaneous comment just

38
00:01:10,320 --> 00:01:14,479
that it's showing 40 bits here

39
00:01:12,960 --> 00:01:16,560
this should really be you know something

40
00:01:14,479 --> 00:01:19,600
like MAXPHYADDR-12

41
00:01:16,560 --> 00:01:20,640
on a 52-bit system on a 52 bit max you

42
00:01:19,600 --> 00:01:22,560
know

43
00:01:20,640 --> 00:01:24,960
physical address space yes it's 40. so

44
00:01:22,560 --> 00:01:27,680
that's kind of what this is assuming but

45
00:01:24,960 --> 00:01:29,360
you know as I said my computer only has

46
00:01:27,680 --> 00:01:31,920
a 39-bit

47
00:01:29,360 --> 00:01:33,040
MAXPHYADDR so you know it would be

48
00:01:31,920 --> 00:01:34,479
different on my system it would be

49
00:01:33,040 --> 00:01:36,240
different on your system but

50
00:01:34,479 --> 00:01:38,479
I didn't want to update all these and

51
00:01:36,240 --> 00:01:40,240
quite frankly I started updating them

52
00:01:38,479 --> 00:01:41,600
and then I ran into exactly the problem

53
00:01:40,240 --> 00:01:43,600
which is probably why intel didn't

54
00:01:41,600 --> 00:01:47,040
update them which is it's hard to put

55
00:01:43,600 --> 00:01:49,520
MAXPHYADDR-12 here for instance

56
00:01:47,040 --> 00:01:50,560
and it gets very messy fast so anyways

57
00:01:49,520 --> 00:01:53,119
just to say that those

58
00:01:50,560 --> 00:01:54,720
40s are not real 40s they're MAXPHYADDR

59
00:01:53,119 --> 00:01:57,680
based things

60
00:01:54,720 --> 00:01:58,479
so you start at something called the

61
00:01:57,680 --> 00:02:01,920
page map

62
00:01:58,479 --> 00:02:03,840
level 4 so when intel expanded

63
00:02:01,920 --> 00:02:06,320
the to the 36-bit address for that

64
00:02:03,840 --> 00:02:08,560
physical address extensions on 32-bit

65
00:02:06,320 --> 00:02:10,720
uh they they gave it a weird name page

66
00:02:08,560 --> 00:02:12,080
directory pointer table

67
00:02:10,720 --> 00:02:13,280
so that had a weird name and eventually

68
00:02:12,080 --> 00:02:14,959
they said you know no more with the

69
00:02:13,280 --> 00:02:16,000
weird names let's just start numbering

70
00:02:14,959 --> 00:02:19,040
them so

71
00:02:16,000 --> 00:02:20,720
page map level four so your CR3

72
00:02:19,040 --> 00:02:22,480
has a physical address that points at a

73
00:02:20,720 --> 00:02:25,440
page map level 4

74
00:02:22,480 --> 00:02:27,840
and there's going to be a 9-bit index

75
00:02:25,440 --> 00:02:30,480
from the top of your 48-bit

76
00:02:27,840 --> 00:02:32,720
linear address space take the top 9

77
00:02:30,480 --> 00:02:33,360
bits and that's used as an index into

78
00:02:32,720 --> 00:02:36,879
your

79
00:02:33,360 --> 00:02:37,200
page map level 4 entry page map level

80
00:02:36,879 --> 00:02:40,160
4

81
00:02:37,200 --> 00:02:42,080
entry will then point at a page

82
00:02:40,160 --> 00:02:43,680
directory pointer table that's the sort

83
00:02:42,080 --> 00:02:45,440
of weird name I talked about

84
00:02:43,680 --> 00:02:47,040
it's going to grab 40 bits out of that

85
00:02:45,440 --> 00:02:48,319
find the physical use

86
00:02:47,040 --> 00:02:50,560
you know the bottom 12 bits will be

87
00:02:48,319 --> 00:02:52,959
assumed to be 0 and that will point

88
00:02:50,560 --> 00:02:56,160
it at a page directory pointer table

89
00:02:52,959 --> 00:02:57,519
from which 9 bits are again plucked

90
00:02:56,160 --> 00:03:00,239
from the linear address

91
00:02:57,519 --> 00:03:00,800
to index into the page directory pointer

92
00:03:00,239 --> 00:03:04,800
table

93
00:03:00,800 --> 00:03:08,239
and find the PDPTE page directory

94
00:03:04,800 --> 00:03:11,360
pointer table entry 9 bits

95
00:03:08,239 --> 00:03:14,959
index into that table PDPTE

96
00:03:11,360 --> 00:03:17,280
all right and from there it's then the

97
00:03:14,959 --> 00:03:20,239
exact same sort of page directories

98
00:03:17,280 --> 00:03:22,080
so 9 bits instead of 10 bits is used to

99
00:03:20,239 --> 00:03:24,959
index into the page directory

100
00:03:22,080 --> 00:03:27,280
and find the page directory entry and 9

101
00:03:24,959 --> 00:03:29,920
bits instead of 10 bits is used to

102
00:03:27,280 --> 00:03:31,040
index into the page table and finally

103
00:03:29,920 --> 00:03:35,040
you're left with

104
00:03:31,040 --> 00:03:38,560
40 bits of 40 bits of the entry

105
00:03:35,040 --> 00:03:39,519
are used to base to find the base of the

106
00:03:38,560 --> 00:03:42,400
page

107
00:03:39,519 --> 00:03:44,319
and the 12 bits of the offset the last

108
00:03:42,400 --> 00:03:44,799
12 bits in your linear address is used

109
00:03:44,319 --> 00:03:47,519
to

110
00:03:44,799 --> 00:03:48,400
index into the specific byte that you

111
00:03:47,519 --> 00:03:50,799
want to access

112
00:03:48,400 --> 00:03:52,720
inside of that page whereas in 32-bit

113
00:03:50,799 --> 00:03:53,599
paging each of these entries was a 4-

114
00:03:52,720 --> 00:03:55,680
byte value

115
00:03:53,599 --> 00:03:57,760
now in 64-bit paging each of these

116
00:03:55,680 --> 00:03:58,879
entries is an 8-byte value

117
00:03:57,760 --> 00:04:00,720
because of course you wouldn't be able

118
00:03:58,879 --> 00:04:02,239
to hold 40 bits there would you if it

119
00:04:00,720 --> 00:04:04,640
was still 32 bits

120
00:04:02,239 --> 00:04:06,640
now just like previously in 32-bit we

121
00:04:04,640 --> 00:04:09,200
saw that there was a version that had

122
00:04:06,640 --> 00:04:09,680
a 4 megabyte page where you basically

123
00:04:09,200 --> 00:04:12,159
just

124
00:04:09,680 --> 00:04:14,319
took out one level of the page tables

125
00:04:12,159 --> 00:04:17,280
and the page table specifically

126
00:04:14,319 --> 00:04:19,120
and you used all those bits as an index

127
00:04:17,280 --> 00:04:21,840
into a larger thing

128
00:04:19,120 --> 00:04:22,639
well previously it was 10 and 12 bits

129
00:04:21,840 --> 00:04:24,880
now it's

130
00:04:22,639 --> 00:04:26,080
9 and 12 bits which means it's going to

131
00:04:24,880 --> 00:04:29,040
be a 21-bit

132
00:04:26,080 --> 00:04:30,960
offset which means it's 2 to the 21 big

133
00:04:29,040 --> 00:04:33,199
which means it's a 2 megabyte page

134
00:04:30,960 --> 00:04:35,040
instead of a 4 megabyte page

135
00:04:33,199 --> 00:04:36,560
other than that it's all basically

136
00:04:35,040 --> 00:04:38,800
exactly the same CR3

137
00:04:36,560 --> 00:04:40,960
pointing at a page map level 4 page map

138
00:04:38,800 --> 00:04:42,160
level 4 pointing at a page directory

139
00:04:40,960 --> 00:04:44,800
pointer table

140
00:04:42,160 --> 00:04:46,320
pointing at a page directory and then

141
00:04:44,800 --> 00:04:49,440
this page directory entry

142
00:04:46,320 --> 00:04:53,360
points at a large 2 megabyte physical

143
00:04:49,440 --> 00:04:56,240
page instead of a page table

144
00:04:53,360 --> 00:04:57,280
but even bigger than that the humongous

145
00:04:56,240 --> 00:05:00,000
page

146
00:04:57,280 --> 00:05:01,840
you can do the same thing in 64-bit and

147
00:05:00,000 --> 00:05:04,639
accumulate in another 9 bits

148
00:05:01,840 --> 00:05:05,919
and you get a 30 30-bit offset which

149
00:05:04,639 --> 00:05:10,160
means you've got a

150
00:05:05,919 --> 00:05:13,039
2 to 30 byte page a 1 gigabyte page so

151
00:05:10,160 --> 00:05:14,800
CR3 to page map level 4 page map

152
00:05:13,039 --> 00:05:15,520
level four to page directory pointer

153
00:05:14,800 --> 00:05:17,120
table

154
00:05:15,520 --> 00:05:19,360
and then that table can say you know

155
00:05:17,120 --> 00:05:22,960
what I don't point at a page directory

156
00:05:19,360 --> 00:05:25,840
I point at a giant 1 gigabyte

157
00:05:22,960 --> 00:05:25,840
page

158
00:05:26,639 --> 00:05:30,560
okay so that's it for this section what

159
00:05:29,520 --> 00:05:32,479
did we learn about

160
00:05:30,560 --> 00:05:34,320
well we learned about the slicing and

161
00:05:32,479 --> 00:05:36,720
dicing of the linear address

162
00:05:34,320 --> 00:05:37,680
it gets turned into some number of bits

163
00:05:36,720 --> 00:05:39,759
is some sort of

164
00:05:37,680 --> 00:05:41,199
index into table 1 table 2 table

165
00:05:39,759 --> 00:05:43,600
3 table 4

166
00:05:41,199 --> 00:05:44,240
there's all these uh all these different

167
00:05:43,600 --> 00:05:47,280
bit

168
00:05:44,240 --> 00:05:49,280
indices into these various tables in the

169
00:05:47,280 --> 00:05:51,759
next section we're going to learn

170
00:05:49,280 --> 00:05:54,720
what's in each of those entries in each

171
00:05:51,759 --> 00:05:54,720
of those tables

