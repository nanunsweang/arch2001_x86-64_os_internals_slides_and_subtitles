1
00:00:00,160 --> 00:00:03,520
so let's go take a look at page fault

2
00:00:02,879 --> 00:00:05,920
handler

3
00:00:03,520 --> 00:00:07,680
in windows and specifically what we want

4
00:00:05,920 --> 00:00:09,120
to see is you know what does the

5
00:00:07,680 --> 00:00:11,759
stack look like at the time that the

6
00:00:09,120 --> 00:00:14,920
page fault occurred what does the CR2

7
00:00:11,759 --> 00:00:16,080
look like at the time the page fault

8
00:00:14,920 --> 00:00:18,560
occurred

9
00:00:16,080 --> 00:00:20,240
and I'm going to step back to have this

10
00:00:18,560 --> 00:00:22,480
here so that we can you know interpret

11
00:00:20,240 --> 00:00:24,160
what the stack looks like

12
00:00:22,480 --> 00:00:26,080
so we're going to break into the

13
00:00:24,160 --> 00:00:29,359
debugger

14
00:00:26,080 --> 00:00:33,760
we're going to do bang idt 14

15
00:00:29,359 --> 00:00:35,200
being idt 0n14 for decimal 14

16
00:00:33,760 --> 00:00:37,040
because we want to see what the page

17
00:00:35,200 --> 00:00:38,239
fault handler is so

18
00:00:37,040 --> 00:00:41,040
that's the name of the page fault

19
00:00:38,239 --> 00:00:42,640
handler ntKiPageFault

20
00:00:41,040 --> 00:00:44,239
and so we're going to set a hardware

21
00:00:42,640 --> 00:00:47,280
breakpoint on this

22
00:00:44,239 --> 00:00:50,320
in order to stop break on access

23
00:00:47,280 --> 00:00:52,239
execute size of 1 and

24
00:00:50,320 --> 00:00:53,600
that particular address so we want to

25
00:00:52,239 --> 00:00:56,320
basically see

26
00:00:53,600 --> 00:00:58,239
once someone hits the page fault handler

27
00:00:56,320 --> 00:00:59,359
what does the stack look at that exact

28
00:00:58,239 --> 00:01:02,000
instant

29
00:00:59,359 --> 00:01:03,760
so let's just go ahead and hit go and at

30
00:01:02,000 --> 00:01:05,840
some point someone somewhere on the

31
00:01:03,760 --> 00:01:08,240
system is going to page fault

32
00:01:05,840 --> 00:01:10,080
and so here we are now a page fault has

33
00:01:08,240 --> 00:01:12,799
occurred

34
00:01:10,080 --> 00:01:13,200
so let's go ahead and look at rsp okay

35
00:01:12,799 --> 00:01:16,240
here's

36
00:01:13,200 --> 00:01:18,799
rsp I'm gonna try to adjust this to make

37
00:01:16,240 --> 00:01:21,680
it show up as a single column

38
00:01:18,799 --> 00:01:24,840
all right that's rsp let's scroll down

39
00:01:21,680 --> 00:01:27,280
to see our CR2

40
00:01:24,840 --> 00:01:29,360
to CR CR2 all right CR2

41
00:01:27,280 --> 00:01:30,640
that is the particular linear address

42
00:01:29,360 --> 00:01:33,439
which was trying to be

43
00:01:30,640 --> 00:01:34,640
accessed at the time that this occurred

44
00:01:33,439 --> 00:01:38,000
let's go ahead and do

45
00:01:34,640 --> 00:01:40,560
pte on CR2 and

46
00:01:38,000 --> 00:01:41,439
see what it thinks about that particular

47
00:01:40,560 --> 00:01:43,439
address

48
00:01:41,439 --> 00:01:44,560
okay well it seems to have a valid

49
00:01:43,439 --> 00:01:46,240
translation

50
00:01:44,560 --> 00:01:48,320
so how do we interpret this particular

51
00:01:46,240 --> 00:01:51,360
page fault the first entry

52
00:01:48,320 --> 00:01:53,360
is the error code

53
00:01:51,360 --> 00:01:54,799
so we would interpret that like this it

54
00:01:53,360 --> 00:01:57,759
is 3

55
00:01:54,799 --> 00:01:59,360
so it is 1 for the read/write so it

56
00:01:57,759 --> 00:02:01,840
was an attempt to write

57
00:01:59,360 --> 00:02:03,119
and 1 for it was caused by a page

58
00:02:01,840 --> 00:02:04,960
level violation

59
00:02:03,119 --> 00:02:06,799
usually when I was doing this before I

60
00:02:04,960 --> 00:02:08,080
would tend to see 0 here which would

61
00:02:06,799 --> 00:02:10,239
be you know kind of

62
00:02:08,080 --> 00:02:11,840
non-intuitive you would be confused of

63
00:02:10,239 --> 00:02:13,920
like you know oh is that actually

64
00:02:11,840 --> 00:02:14,959
you know the error code it just seems to

65
00:02:13,920 --> 00:02:17,280
be 0

66
00:02:14,959 --> 00:02:19,040
but that was because now we just you

67
00:02:17,280 --> 00:02:19,840
know got lucky or unlucky depending on

68
00:02:19,040 --> 00:02:21,599
your take

69
00:02:19,840 --> 00:02:22,879
about you know what kind of page fault

70
00:02:21,599 --> 00:02:24,879
just occurred

71
00:02:22,879 --> 00:02:26,640
so this is a fault caused by page

72
00:02:24,879 --> 00:02:28,800
protection and it was a fault caused by

73
00:02:26,640 --> 00:02:32,560
a write

74
00:02:28,800 --> 00:02:35,680
then we would expect to see rip cs

75
00:02:32,560 --> 00:02:37,680
rflags rsp and ss on the stack

76
00:02:35,680 --> 00:02:39,519
so this would be the rip that would be

77
00:02:37,680 --> 00:02:40,239
the thing that actually caused the page

78
00:02:39,519 --> 00:02:42,239
fault

79
00:02:40,239 --> 00:02:45,760
let's see you know in which process

80
00:02:42,239 --> 00:02:49,519
context we are in right now

81
00:02:45,760 --> 00:02:51,599
give us a sense so tiworker.exe

82
00:02:49,519 --> 00:02:53,440
if we look at this particular thing I

83
00:02:51,599 --> 00:02:54,640
don't know that I will have symbols for

84
00:02:53,440 --> 00:02:56,959
that but you know hey

85
00:02:54,640 --> 00:02:58,640
let's try it okay don't know what that

86
00:02:56,959 --> 00:03:00,879
is don't have the symbols for it

87
00:02:58,640 --> 00:03:01,680
but that is the you know assem this is

88
00:03:00,879 --> 00:03:03,840
the

89
00:03:01,680 --> 00:03:05,920
uh this is the assembly instruction that

90
00:03:03,840 --> 00:03:06,480
caused the actual fault so remember that

91
00:03:05,920 --> 00:03:09,040
fault

92
00:03:06,480 --> 00:03:10,560
hitler's fault rip points at the

93
00:03:09,040 --> 00:03:12,400
assembly instruction that actually

94
00:03:10,560 --> 00:03:13,760
caused the default when it's a fault as

95
00:03:12,400 --> 00:03:16,159
opposed to a trap

96
00:03:13,760 --> 00:03:17,200
so this saved rip means that this

97
00:03:16,159 --> 00:03:18,879
particular address

98
00:03:17,200 --> 00:03:20,480
must have been the thing that actually

99
00:03:18,879 --> 00:03:22,720
caused the fault so

100
00:03:20,480 --> 00:03:25,040
r14 we could go back and check that we

101
00:03:22,720 --> 00:03:28,159
would expect that you know r14

102
00:03:25,040 --> 00:03:28,480
plus 8 is probably going to calculate

103
00:03:28,159 --> 00:03:30,879
out

104
00:03:28,480 --> 00:03:33,480
to this address so let's take a look now

105
00:03:30,879 --> 00:03:36,239
the bottom is 8 so we would expect

106
00:03:33,480 --> 00:03:38,640
fe20949 blah blah blah

107
00:03:36,239 --> 00:03:40,720
3c000 so let's go look

108
00:03:38,640 --> 00:03:43,920
at r14

109
00:03:40,720 --> 00:03:47,840
all right r14 indeed blah blah blah

110
00:03:43,920 --> 00:03:50,879
c 3c00 and the plus 8

111
00:03:47,840 --> 00:03:53,360
means that this is actually

112
00:03:50,879 --> 00:03:55,360
the address that was calculated through

113
00:03:53,360 --> 00:03:57,200
this particular memory access

114
00:03:55,360 --> 00:03:58,720
and that's when a page fault occurred

115
00:03:57,200 --> 00:04:00,640
because some sort of

116
00:03:58,720 --> 00:04:02,080
attempt to access memory that it

117
00:04:00,640 --> 00:04:03,680
shouldn't have

118
00:04:02,080 --> 00:04:05,760
attempt to write to memory that it

119
00:04:03,680 --> 00:04:07,280
shouldn't have so let's go ahead and go

120
00:04:05,760 --> 00:04:15,840
back and look at that pte

121
00:04:07,280 --> 00:04:15,840
entry again for that particular address

122
00:04:16,079 --> 00:04:20,400
and we can see that actually this

123
00:04:18,160 --> 00:04:22,400
address that the last page table

124
00:04:20,400 --> 00:04:25,680
indicates that particular page

125
00:04:22,400 --> 00:04:27,600
is readable only it is non-writable so

126
00:04:25,680 --> 00:04:28,800
this process whatever it was doing it

127
00:04:27,600 --> 00:04:31,040
tried to write to some

128
00:04:28,800 --> 00:04:32,639
page and that page is read only and so

129
00:04:31,040 --> 00:04:36,000
that caused a page fault

130
00:04:32,639 --> 00:04:38,560
so rip of the faulting instruction

131
00:04:36,000 --> 00:04:40,720
cs of the faulting instruction rflags

132
00:04:38,560 --> 00:04:43,520
of the faulting instruction

133
00:04:40,720 --> 00:04:45,759
rsp of the faulting instruction ss of the

134
00:04:43,520 --> 00:04:48,560
faulting instruction

135
00:04:45,759 --> 00:04:49,680
so that was a conveniently more useful

136
00:04:48,560 --> 00:04:51,440
than normal

137
00:04:49,680 --> 00:04:53,440
particular page fault that I just got

138
00:04:51,440 --> 00:04:54,160
lucky you should do this yourself and

139
00:04:53,440 --> 00:04:56,400
you should again

140
00:04:54,160 --> 00:04:58,160
go look at the error codes look at the

141
00:04:56,400 --> 00:04:59,919
values put onto the stack

142
00:04:58,160 --> 00:05:01,360
and see whether it makes sense given

143
00:04:59,919 --> 00:05:04,720
what we've just said about

144
00:05:01,360 --> 00:05:07,680
CR2 stack entries and error codes

145
00:05:04,720 --> 00:05:07,680
according to this

