1
00:00:00,160 --> 00:00:06,799
okay so let's dig into the simplest form

2
00:00:03,280 --> 00:00:09,360
the 32 to 32 4 kilobyte page

3
00:00:06,799 --> 00:00:10,000
so it all begins with a 32-bit linear

4
00:00:09,360 --> 00:00:13,120
address

5
00:00:10,000 --> 00:00:16,320
in 32-bit mode then you have your CR3

6
00:00:13,120 --> 00:00:18,400
which is also 32 bits on a 32-bit system

7
00:00:16,320 --> 00:00:19,920
and that gives you a base address of

8
00:00:18,400 --> 00:00:21,199
what for now I'm just going to call

9
00:00:19,920 --> 00:00:24,400
table 1.

10
00:00:21,199 --> 00:00:26,480
table 1 has 2 to the 10 4-byte entries

11
00:00:24,400 --> 00:00:29,359
inside of it

12
00:00:26,480 --> 00:00:30,400
and so if you use the top 10 bits of the

13
00:00:29,359 --> 00:00:33,360
linear address

14
00:00:30,400 --> 00:00:35,360
as a index into this table it will

15
00:00:33,360 --> 00:00:38,480
select one of those 2 to the 10

16
00:00:35,360 --> 00:00:40,239
entries inside of this table so there's

17
00:00:38,480 --> 00:00:43,360
just some entry

18
00:00:40,239 --> 00:00:44,000
and if the top 10 bits was 0 then you

19
00:00:43,360 --> 00:00:46,800
would be pointing

20
00:00:44,000 --> 00:00:47,440
at entry number 0 if the top 10 bits

21
00:00:46,800 --> 00:00:50,239
was

22
00:00:47,440 --> 00:00:53,680
2ff then you'd pointing at the highest

23
00:00:50,239 --> 00:00:53,680
entry inside of this table

24
00:00:56,960 --> 00:01:00,719
so let's just go ahead and say it's

25
00:00:58,480 --> 00:01:02,800
generically a 10 bit index that

26
00:01:00,719 --> 00:01:04,239
gets you an entry somewhere inside of

27
00:01:02,800 --> 00:01:06,640
this table

28
00:01:04,239 --> 00:01:09,040
each entry in this 32-bit mode is 4

29
00:01:06,640 --> 00:01:09,040
bytes

30
00:01:09,360 --> 00:01:13,439
so from within this entry although we'll

31
00:01:11,840 --> 00:01:15,600
use the entire next section to talk

32
00:01:13,439 --> 00:01:18,000
about the details of the entry

33
00:01:15,600 --> 00:01:19,520
for now let's just say that the entry

34
00:01:18,000 --> 00:01:21,439
has 20 bits

35
00:01:19,520 --> 00:01:22,799
and we're going to use those 20 bits to

36
00:01:21,439 --> 00:01:26,320
calculate the address

37
00:01:22,799 --> 00:01:28,799
of the next table here and specifically

38
00:01:26,320 --> 00:01:30,400
it's going to be the upper 20 bits are

39
00:01:28,799 --> 00:01:32,320
going to save the next entry

40
00:01:30,400 --> 00:01:33,759
and the bottom 12 bits are going to be

41
00:01:32,320 --> 00:01:37,200
assumed to be 0

42
00:01:33,759 --> 00:01:41,280
so the table must be on a 12-bit

43
00:01:37,200 --> 00:01:43,200
aligned address so this entry we said it

44
00:01:41,280 --> 00:01:45,520
every entry is 4 bytes

45
00:01:43,200 --> 00:01:46,399
so the 20 most significant bits of this

46
00:01:45,520 --> 00:01:48,159
entry

47
00:01:46,399 --> 00:01:50,720
are going to be used for the table

48
00:01:48,159 --> 00:01:52,960
address so the address of the next table

49
00:01:50,720 --> 00:01:54,720
and it's just treated them MMU just

50
00:01:52,960 --> 00:01:56,799
reads this value and it assumes the

51
00:01:54,720 --> 00:01:58,399
bottom 12 bits are all 0 so it

52
00:01:56,799 --> 00:02:02,560
expects that it ends in

53
00:01:58,399 --> 00:02:04,079
0x000 therefore because the MMU

54
00:02:02,560 --> 00:02:06,159
is able to just assume that

55
00:02:04,079 --> 00:02:08,959
the bottom bit 12 bits of the address

56
00:02:06,159 --> 00:02:09,599
are 0 it can actually reuse these 12

57
00:02:08,959 --> 00:02:11,680
bits

58
00:02:09,599 --> 00:02:14,480
for various flags and things that we'll

59
00:02:11,680 --> 00:02:14,480
learn about later

60
00:02:14,720 --> 00:02:18,400
so as I already mentioned basically the

61
00:02:16,720 --> 00:02:19,920
fact that this is all 0 means that

62
00:02:18,400 --> 00:02:22,560
the address will end in

63
00:02:19,920 --> 00:02:23,599
0x000 and that is always going

64
00:02:22,560 --> 00:02:25,760
to be a multiple

65
00:02:23,599 --> 00:02:26,879
of 0x1000 so 0, 0x1000

66
00:02:25,760 --> 00:02:29,280
0x2000

67
00:02:26,879 --> 00:02:31,040
so with frequently and operating systems

68
00:02:29,280 --> 00:02:32,160
frequently refer to something as being

69
00:02:31,040 --> 00:02:34,239
page aligned

70
00:02:32,160 --> 00:02:37,200
if it is aligned on this 4 kilobyte

71
00:02:34,239 --> 00:02:40,400
bind boundary because 2 to the 12 is

72
00:02:37,200 --> 00:02:42,319
4 kilobytes so basically every single

73
00:02:40,400 --> 00:02:43,680
place that this table could land has to

74
00:02:42,319 --> 00:02:48,160
be some offset that's

75
00:02:43,680 --> 00:02:48,160
page aligned ends with 000.

76
00:02:48,239 --> 00:02:51,280
all right so like I said we'll dig into

77
00:02:49,840 --> 00:02:53,440
the entries more later

78
00:02:51,280 --> 00:02:55,120
for now we just say all right those 20

79
00:02:53,440 --> 00:02:58,239
bits are going to point us at

80
00:02:55,120 --> 00:03:01,120
this table 2. table 2 is going to be

81
00:02:58,239 --> 00:03:02,319
2 to the 10 4 byte entries so we are

82
00:03:01,120 --> 00:03:04,640
again going to grab

83
00:03:02,319 --> 00:03:05,920
10 bits out of the linear address these

84
00:03:04,640 --> 00:03:08,400
middle 10 bits

85
00:03:05,920 --> 00:03:09,040
and we're going to use those as an index

86
00:03:08,400 --> 00:03:11,760
into

87
00:03:09,040 --> 00:03:13,599
table 2 to find some other entry that

88
00:03:11,760 --> 00:03:16,239
again is going to be some sort of 4 byte

89
00:03:13,599 --> 00:03:16,239
data structure

90
00:03:16,319 --> 00:03:20,080
once again it's going to use 20 bits and

91
00:03:18,480 --> 00:03:22,879
assume the bottom 12 bits

92
00:03:20,080 --> 00:03:23,599
are zeros and that will give us the

93
00:03:22,879 --> 00:03:25,519
address

94
00:03:23,599 --> 00:03:28,080
of the page that we're ultimately

95
00:03:25,519 --> 00:03:28,080
looking for

96
00:03:30,159 --> 00:03:34,319
so finally we find ourselves at the 4

97
00:03:33,040 --> 00:03:37,159
kilobyte page

98
00:03:34,319 --> 00:03:39,840
at some address that is page aligned

99
00:03:37,159 --> 00:03:43,120
0x000 as its last address

100
00:03:39,840 --> 00:03:45,519
as its last bits in its address and

101
00:03:43,120 --> 00:03:47,599
this is going to be a 4 kilobyte page

102
00:03:45,519 --> 00:03:49,760
so 2 to the 12 bytes

103
00:03:47,599 --> 00:03:51,920
and so that perfectly aligns with the

104
00:03:49,760 --> 00:03:53,360
fact that we've got 12 bits left here in

105
00:03:51,920 --> 00:03:55,519
the 32-bit address

106
00:03:53,360 --> 00:03:57,439
that can be used as an offset into the

107
00:03:55,519 --> 00:03:58,640
page to find the bytes that you're

108
00:03:57,439 --> 00:04:00,879
looking for

109
00:03:58,640 --> 00:04:02,959
so you could be doing a 1 byte access

110
00:04:00,879 --> 00:04:06,159
you could be doing a 4 byte access

111
00:04:02,959 --> 00:04:08,159
but basically this this linear address

112
00:04:06,159 --> 00:04:11,120
is broken up like this

113
00:04:08,159 --> 00:04:12,080
10 bits to find you the next table 10

114
00:04:11,120 --> 00:04:14,080
bits to find

115
00:04:12,080 --> 00:04:15,840
sorry 10 bits to find you the index of

116
00:04:14,080 --> 00:04:18,479
the next table and then finally

117
00:04:15,840 --> 00:04:19,919
12 bits once you've found the page in

118
00:04:18,479 --> 00:04:21,199
order to find the bytes you're looking

119
00:04:19,919 --> 00:04:23,360
for

120
00:04:21,199 --> 00:04:24,960
so for instance if the least significant

121
00:04:23,360 --> 00:04:26,400
12 bits were 0

122
00:04:24,960 --> 00:04:28,080
and you would be pointing at you know

123
00:04:26,400 --> 00:04:30,639
the zeroth byte of

124
00:04:28,080 --> 00:04:31,360
the page and let's just assume that you

125
00:04:30,639 --> 00:04:33,280
know right now

126
00:04:31,360 --> 00:04:35,120
the processor was running an assembly

127
00:04:33,280 --> 00:04:37,440
instruction I was doing like a 1 byte

128
00:04:35,120 --> 00:04:39,280
fetch or something like that

129
00:04:37,440 --> 00:04:41,120
and if the least significant bits were

130
00:04:39,280 --> 00:04:44,160
0 were 1 then it would be

131
00:04:41,120 --> 00:04:46,800
accessing or pointing at address

132
00:04:44,160 --> 00:04:47,360
1 of the page it was 2 would be address

133
00:04:46,800 --> 00:04:51,199
2

134
00:04:47,360 --> 00:04:53,520
and so forth similarly if it was

135
00:04:51,199 --> 00:04:55,360
ffc and if the processor happened to be

136
00:04:53,520 --> 00:04:58,160
doing a 4 byte fetch

137
00:04:55,360 --> 00:04:59,360
then it would see ffc and it would fetch

138
00:04:58,160 --> 00:05:02,639
four bytes and it would get

139
00:04:59,360 --> 00:05:02,639
dollar dollar bills y'all

140
00:05:02,800 --> 00:05:06,000
so let's just treat that as a generic

141
00:05:04,960 --> 00:05:09,199
12-bit at

142
00:05:06,000 --> 00:05:10,479
page offset to find the bytes and now

143
00:05:09,199 --> 00:05:13,039
let's update this

144
00:05:10,479 --> 00:05:14,479
table and let's update this diagram to

145
00:05:13,039 --> 00:05:16,320
be more accurate to what these things

146
00:05:14,479 --> 00:05:16,960
are actually called so this is not table

147
00:05:16,320 --> 00:05:19,919
1

148
00:05:16,960 --> 00:05:20,800
this is called a page directory and

149
00:05:19,919 --> 00:05:23,360
consequently

150
00:05:20,800 --> 00:05:24,960
this entry is called a page directory

151
00:05:23,360 --> 00:05:28,080
entry

152
00:05:24,960 --> 00:05:30,400
table 2 is called a page table

153
00:05:28,080 --> 00:05:32,960
and its entry is called a page table

154
00:05:30,400 --> 00:05:35,280
entry now in this class I'm generically

155
00:05:32,960 --> 00:05:37,680
going to say the term page tables

156
00:05:35,280 --> 00:05:39,120
plural to refer to all of these sort of

157
00:05:37,680 --> 00:05:41,520
data structures that

158
00:05:39,120 --> 00:05:43,120
you know as we get from depth of 2 all

159
00:05:41,520 --> 00:05:44,800
the way up to depth of 5 I'm just

160
00:05:43,120 --> 00:05:46,320
going to say page tables to mean all

161
00:05:44,800 --> 00:05:48,880
these various data structures

162
00:05:46,320 --> 00:05:50,240
on our way to a page but strictly

163
00:05:48,880 --> 00:05:52,000
speaking only the last

164
00:05:50,240 --> 00:05:54,720
thing the last data structure is called

165
00:05:52,000 --> 00:05:54,720
a page table

166
00:05:54,960 --> 00:05:58,479
alright and all of that animation was

167
00:05:56,960 --> 00:06:01,360
just to try to make it

168
00:05:58,479 --> 00:06:02,240
nicer for you to understand this single

169
00:06:01,360 --> 00:06:04,080
picture

170
00:06:02,240 --> 00:06:05,600
out of the intel manual so this is the

171
00:06:04,080 --> 00:06:07,199
picture intel would give you and then

172
00:06:05,600 --> 00:06:08,400
you know read all the text to understand

173
00:06:07,199 --> 00:06:10,720
what's going on but

174
00:06:08,400 --> 00:06:12,319
that can be quite difficult so what it's

175
00:06:10,720 --> 00:06:14,560
saying is what I just said

176
00:06:12,319 --> 00:06:16,080
take a linear address break it up this

177
00:06:14,560 --> 00:06:18,880
is saying 10 bits

178
00:06:16,080 --> 00:06:20,639
is your offset to find the PDE the page

179
00:06:18,880 --> 00:06:22,880
directory entry

180
00:06:20,639 --> 00:06:24,240
CR3 having pointed at the physical

181
00:06:22,880 --> 00:06:27,680
address of the

182
00:06:24,240 --> 00:06:27,680
base of the page directory

183
00:06:28,080 --> 00:06:31,680
then 10 bits from the page from the

184
00:06:30,639 --> 00:06:34,800
middle 10 bits

185
00:06:31,680 --> 00:06:36,880
are used to find the page table entry

186
00:06:34,800 --> 00:06:38,160
and then finally the last 12 bits are

187
00:06:36,880 --> 00:06:41,280
used to offset

188
00:06:38,160 --> 00:06:42,800
into the final page you can also see

189
00:06:41,280 --> 00:06:44,400
that it's 20 bits

190
00:06:42,800 --> 00:06:46,080
out of the page directory entry which

191
00:06:44,400 --> 00:06:48,720
was used to find the page table

192
00:06:46,080 --> 00:06:50,720
and 20 bits out of the page table entry

193
00:06:48,720 --> 00:06:52,400
which was used to find the page

194
00:06:50,720 --> 00:06:54,000
and so ultimately that gives you you

195
00:06:52,400 --> 00:06:56,240
know 32 bits

196
00:06:54,000 --> 00:06:57,680
of physical address space that you can

197
00:06:56,240 --> 00:06:59,840
access into

198
00:06:57,680 --> 00:07:03,360
20 bits to find a page and 12 bits

199
00:06:59,840 --> 00:07:03,360
within the pages

