1
00:00:00,080 --> 00:00:03,840
before we get too deep into the

2
00:00:01,760 --> 00:00:06,319
specifics of how the page tables work we

3
00:00:03,840 --> 00:00:08,000
need to also see the interactions with

4
00:00:06,319 --> 00:00:10,719
the control registers

5
00:00:08,000 --> 00:00:12,400
the intel control registers there's 5
6
00:00:10,719 --> 00:00:14,639
in particular that are going to be

7
00:00:12,400 --> 00:00:16,480
specifically relevant to paging and

8
00:00:14,639 --> 00:00:17,840
some other tangential features that are

9
00:00:16,480 --> 00:00:20,720
interesting to us

10
00:00:17,840 --> 00:00:22,880
also as an aside it's been approximately

11
00:00:20,720 --> 00:00:23,840
a decade that I've had my intermediate

12
00:00:22,880 --> 00:00:26,000
x86

13
00:00:23,840 --> 00:00:28,000
32-bit version of this class out there

14
00:00:26,000 --> 00:00:30,080
on OpenSecurityTraining1

15
00:00:28,000 --> 00:00:33,280
and apparently no one has yet taken me

16
00:00:30,080 --> 00:00:35,840
up on the idea of naming their band

17
00:00:33,280 --> 00:00:37,680
someone and the control registers so

18
00:00:35,840 --> 00:00:38,399
this is still available this is still

19
00:00:37,680 --> 00:00:39,920
out there

20
00:00:38,399 --> 00:00:41,120
I'm not going to start a band but if

21
00:00:39,920 --> 00:00:42,079
you're the kind of person who would

22
00:00:41,120 --> 00:00:44,719
start a band

23
00:00:42,079 --> 00:00:45,520
I recommend someone and the control

24
00:00:44,719 --> 00:00:47,280
registers

25
00:00:45,520 --> 00:00:48,719
if you don't do it I'm pretty sure the

26
00:00:47,280 --> 00:00:50,320
robots are going to do it when they take

27
00:00:48,719 --> 00:00:52,480
over

28
00:00:50,320 --> 00:00:54,079
all right so we've already seen control

29
00:00:52,480 --> 00:00:56,160
register 4 briefly

30
00:00:54,079 --> 00:00:58,000
in the context of well there's a bit to

31
00:00:56,160 --> 00:01:00,320
disable rdtsc

32
00:00:58,000 --> 00:01:03,039
style reading of the timestamp counter

33
00:01:00,320 --> 00:01:05,040
there was the UMIP which was user mode

34
00:01:03,039 --> 00:01:06,159
instruction prevention to stop some of

35
00:01:05,040 --> 00:01:08,320
those information

36
00:01:06,159 --> 00:01:10,560
leaky sort of assembly instructions like

37
00:01:08,320 --> 00:01:13,600
sgdt sldt

38
00:01:10,560 --> 00:01:14,560
there was FSGSBASE to have the

39
00:01:13,600 --> 00:01:16,640
enablement of

40
00:01:14,560 --> 00:01:19,119
new assembly instructions to read and

41
00:01:16,640 --> 00:01:20,640
write fsbase and gsbase

42
00:01:19,119 --> 00:01:22,640
but now let's see all of the different

43
00:01:20,640 --> 00:01:24,960
bits that are relevant for paging that

44
00:01:22,640 --> 00:01:28,000
we're going to cover here in this class

45
00:01:24,960 --> 00:01:28,640
okay that's a lot more stuff and as

46
00:01:28,000 --> 00:01:31,600
aside

47
00:01:28,640 --> 00:01:33,360
since we've got it here and I've got you

48
00:01:31,600 --> 00:01:33,680
let's go ahead and see a quick preview

49
00:01:33,360 --> 00:01:35,119
of

50
00:01:33,680 --> 00:01:37,439
the ones that will show up in future

51
00:01:35,119 --> 00:01:38,960
classes architecture 3001 for

52
00:01:37,439 --> 00:01:42,960
virtualization stuff

53
00:01:38,960 --> 00:01:43,920
vm extensions enable architecture 4001

54
00:01:42,960 --> 00:01:45,759
well this pr

55
00:01:43,920 --> 00:01:47,360
this uh protected mode enable which

56
00:01:45,759 --> 00:01:49,759
we're going to cover here in a second

57
00:01:47,360 --> 00:01:50,560
that's also relevant to the bios classes

58
00:01:49,759 --> 00:01:53,280
because

59
00:01:50,560 --> 00:01:55,119
the bios type places also are

60
00:01:53,280 --> 00:01:58,000
responsible for moving from real mode

61
00:01:55,119 --> 00:01:58,000
into protected mode

62
00:01:58,320 --> 00:02:01,680
we're also going to see these things in

63
00:02:01,200 --> 00:02:03,600
future

64
00:02:01,680 --> 00:02:04,799
exploits class I don't know which ones

65
00:02:03,600 --> 00:02:07,520
SMEP SMAP and

66
00:02:04,799 --> 00:02:09,599
CET although we'll briefly touch on SMEP

67
00:02:07,520 --> 00:02:11,440
and SMAP here in a little bit

68
00:02:09,599 --> 00:02:13,120
and then again I don't know which class

69
00:02:11,440 --> 00:02:16,080
it'll be in but some future

70
00:02:13,120 --> 00:02:18,319
trusted computing class will cover the

71
00:02:16,080 --> 00:02:19,120
safer mode extensions that was the intel

72
00:02:18,319 --> 00:02:22,000
txt

73
00:02:19,120 --> 00:02:23,840
technology to basically the precursor of

74
00:02:22,000 --> 00:02:26,239
sgx as a way to provide a sort of

75
00:02:23,840 --> 00:02:29,520
isolated execution environment

76
00:02:26,239 --> 00:02:31,920
okay CR0 bit 0 protection

77
00:02:29,520 --> 00:02:35,440
enabled this is how the system gets from

78
00:02:31,920 --> 00:02:38,480
real mode to protected mode

79
00:02:35,440 --> 00:02:39,519
WP write protect this stops ring 0

80
00:02:38,480 --> 00:02:41,920
from writing to

81
00:02:39,519 --> 00:02:43,440
read-only pages so if this is not set

82
00:02:41,920 --> 00:02:44,640
then ring 0 could just scribble

83
00:02:43,440 --> 00:02:46,000
anywhere at once but

84
00:02:44,640 --> 00:02:47,360
if this is set then it means the

85
00:02:46,000 --> 00:02:48,800
operating system's trying to restrict

86
00:02:47,360 --> 00:02:50,400
itself

87
00:02:48,800 --> 00:02:52,560
and sometimes this is used for a

88
00:02:50,400 --> 00:02:53,440
technology called copy on write that has

89
00:02:52,560 --> 00:02:56,239
to do with

90
00:02:53,440 --> 00:02:58,720
operating systems trying to optimize uh

91
00:02:56,239 --> 00:03:00,720
how much memory they use by just

92
00:02:58,720 --> 00:03:01,840
changing out contents of memory on

93
00:03:00,720 --> 00:03:03,760
demand when

94
00:03:01,840 --> 00:03:06,000
when memory is shared between different

95
00:03:03,760 --> 00:03:09,519
processes

96
00:03:06,000 --> 00:03:09,920
PG is paging enabled and that cannot be

97
00:03:09,519 --> 00:03:12,560
set

98
00:03:09,920 --> 00:03:13,599
until protection is enabled so we said

99
00:03:12,560 --> 00:03:15,200
at the very beginning

100
00:03:13,599 --> 00:03:17,360
when we're talking about processor modes

101
00:03:15,200 --> 00:03:18,000
real mode was the earliest mode it had

102
00:03:17,360 --> 00:03:19,840
no support

103
00:03:18,000 --> 00:03:21,920
for paging in virtual memory it had no

104
00:03:19,840 --> 00:03:23,760
support for protection rings

105
00:03:21,920 --> 00:03:26,799
so first you got to move into protected

106
00:03:23,760 --> 00:03:30,080
mode before you can enable paging

107
00:03:26,799 --> 00:03:33,200
okay CR4 PSE

108
00:03:30,080 --> 00:03:34,879
page size extensions this is going to

109
00:03:33,200 --> 00:03:36,879
allow the system to use

110
00:03:34,879 --> 00:03:38,640
pages that are greater than 4 kilobytes

111
00:03:36,879 --> 00:03:40,159
you can think of 4 kilobytes like it's

112
00:03:38,640 --> 00:03:43,040
the default page size

113
00:03:40,159 --> 00:03:46,400
and if PSE is set then the page tables

114
00:03:43,040 --> 00:03:49,519
can start using things bigger than 4kb

115
00:03:46,400 --> 00:03:51,200
PAE physical address extensions allows

116
00:03:49,519 --> 00:03:51,760
the hardware to access physical

117
00:03:51,200 --> 00:03:54,720
addresses

118
00:03:51,760 --> 00:03:56,879
greater than 32 bits this was originally

119
00:03:54,720 --> 00:03:57,760
appearing before there was full 64-bit

120
00:03:56,879 --> 00:04:00,799
support

121
00:03:57,760 --> 00:04:04,239
and we'll cover that a little bit later

122
00:04:00,799 --> 00:04:05,200
PGE this is page global enable and it's

123
00:04:04,239 --> 00:04:06,720
just a thing to

124
00:04:05,200 --> 00:04:08,560
there's a feature that we'll see later

125
00:04:06,720 --> 00:04:09,439
on when we get into tables called global

126
00:04:08,560 --> 00:04:12,080
pages

127
00:04:09,439 --> 00:04:12,879
and this enablement of that feature will

128
00:04:12,080 --> 00:04:15,840
allow for

129
00:04:12,879 --> 00:04:18,959
more efficient caching of translations

130
00:04:15,840 --> 00:04:22,479
of virtual to physical addresses

131
00:04:18,959 --> 00:04:26,080
and LA57

132
00:04:22,479 --> 00:04:28,960
last is what I read it as but 57 bit

133
00:04:26,080 --> 00:04:31,440
linear addresses so linear address 57

134
00:04:28,960 --> 00:04:32,400
this is a very new feature that intel

135
00:04:31,440 --> 00:04:35,199
has just

136
00:04:32,400 --> 00:04:35,759
uh expanded the size of the the

137
00:04:35,199 --> 00:04:37,600
effective

138
00:04:35,759 --> 00:04:39,440
usable size of the virtual memory space

139
00:04:37,600 --> 00:04:40,960
even though we say that it's 64 bits as

140
00:04:39,440 --> 00:04:43,840
we'll see later on it's not really

141
00:04:40,960 --> 00:04:45,199
64 bits on today's hardware it's usually

142
00:04:43,840 --> 00:04:48,479
48 bits and

143
00:04:45,199 --> 00:04:51,600
this last LA linear address 57

144
00:04:48,479 --> 00:04:54,320
will newly give us 57-bit linear address

145
00:04:51,600 --> 00:04:56,880
space on new hardware

146
00:04:54,320 --> 00:04:59,600
all right now having seen CR0 and CR4

147
00:04:56,880 --> 00:05:01,759
bits we actually now kind of know it all

148
00:04:59,600 --> 00:05:02,800
in terms of how we get from real mode to

149
00:05:01,759 --> 00:05:06,080
long mode

150
00:05:02,800 --> 00:05:07,600
you start in real mode and CR0 protected

151
00:05:06,080 --> 00:05:10,240
enable is going to get you into

152
00:05:07,600 --> 00:05:13,120
protected mode the e for the

153
00:05:10,240 --> 00:05:14,960
the ia32_efer the model specific

154
00:05:13,120 --> 00:05:15,919
register long mode enable 1 we saw

155
00:05:14,960 --> 00:05:17,360
that before

156
00:05:15,919 --> 00:05:19,440
but now we just saw the extra

157
00:05:17,360 --> 00:05:22,880
requirements which are CR4

158
00:05:19,440 --> 00:05:25,120
physical address extensions 1 and CR0

159
00:05:22,880 --> 00:05:26,720
paging 1 and that'll get us into

160
00:05:25,120 --> 00:05:30,080
compatibility mode

161
00:05:26,720 --> 00:05:31,759
and then if the code segment that's you

162
00:05:30,080 --> 00:05:35,039
know if the cached value

163
00:05:31,759 --> 00:05:36,160
of the code segment long bit is set to

164
00:05:35,039 --> 00:05:38,560
1

165
00:05:36,160 --> 00:05:41,680
because it got cached from the GDT then

166
00:05:38,560 --> 00:05:43,520
you will be running in 64-bit mode

167
00:05:41,680 --> 00:05:45,520
just as a minor aside I want to point

168
00:05:43,520 --> 00:05:48,080
out that you know you don't have to

169
00:05:45,520 --> 00:05:49,680
be forced up in compatibility mode you

170
00:05:48,080 --> 00:05:51,440
could have it be the case that

171
00:05:49,680 --> 00:05:52,720
you know long mode enable is not

172
00:05:51,440 --> 00:05:54,560
actually

173
00:05:52,720 --> 00:05:56,880
set and then you would basically be

174
00:05:54,560 --> 00:05:58,160
transitioning when you turn on paging

175
00:05:56,880 --> 00:06:00,160
whether you have physical address

176
00:05:58,160 --> 00:06:02,000
extensions or you don't you would just

177
00:06:00,160 --> 00:06:03,919
be here in protected mode you'd just be

178
00:06:02,000 --> 00:06:06,400
in 32-bit mode paging

179
00:06:03,919 --> 00:06:08,080
sort of pure 32-bit mode and similarly

180
00:06:06,400 --> 00:06:09,840
you could disable paging and you'd

181
00:06:08,080 --> 00:06:12,000
still be in protected mode just without

182
00:06:09,840 --> 00:06:15,840
paging

183
00:06:12,000 --> 00:06:17,840
okay CR4 or sorry CR3 control register

184
00:06:15,840 --> 00:06:19,360
this is going to start at the base of

185
00:06:17,840 --> 00:06:20,560
the page tables that are going to be

186
00:06:19,360 --> 00:06:22,400
walked so

187
00:06:20,560 --> 00:06:24,080
once we get into the details here in the

188
00:06:22,400 --> 00:06:26,160
next section you'll see that

189
00:06:24,080 --> 00:06:27,280
this is just a physical address that is

190
00:06:26,160 --> 00:06:29,680
going to point at

191
00:06:27,280 --> 00:06:31,680
some location for the MMU to walk the

192
00:06:29,680 --> 00:06:34,720
page tables

193
00:06:31,680 --> 00:06:36,319
and then CR2 is used when the MMU is

194
00:06:34,720 --> 00:06:37,759
trying to walk the page tables but

195
00:06:36,319 --> 00:06:38,880
something goes wrong there's some sort

196
00:06:37,759 --> 00:06:40,639
of error the

197
00:06:38,880 --> 00:06:42,400
table says something's invalid or

198
00:06:40,639 --> 00:06:44,000
there's a permission error or some other

199
00:06:42,400 --> 00:06:46,400
reason that we'll talk about later

200
00:06:44,000 --> 00:06:47,440
something goes wrong CR2 holds the

201
00:06:46,400 --> 00:06:49,440
linear address

202
00:06:47,440 --> 00:06:52,319
that was trying to be translated by the

203
00:06:49,440 --> 00:06:54,720
MMU when it encountered the error

204
00:06:52,319 --> 00:06:55,599
and that type of error is called a page

205
00:06:54,720 --> 00:06:58,720
fault so

206
00:06:55,599 --> 00:07:00,960
interrupt 14 is thrown and the interrupt

207
00:06:58,720 --> 00:07:02,000
handler for INT14 is going to ultimately

208
00:07:00,960 --> 00:07:04,160
consult this to

209
00:07:02,000 --> 00:07:05,360
try to figure out what went wrong so

210
00:07:04,160 --> 00:07:08,160
interrupt 14

211
00:07:05,360 --> 00:07:09,759
page fault we'll talk more about that in

212
00:07:08,160 --> 00:07:11,680
its own little section

213
00:07:09,759 --> 00:07:13,360
okay so how do we access those control

214
00:07:11,680 --> 00:07:14,960
registers if we're an operating system

215
00:07:13,360 --> 00:07:16,000
and we want to set or clear some of

216
00:07:14,960 --> 00:07:18,000
those bits

217
00:07:16,000 --> 00:07:19,840
we do it with just a mov assembly

218
00:07:18,000 --> 00:07:21,680
instruction it's just a different

219
00:07:19,840 --> 00:07:22,319
opcode than the normal movs you're used

220
00:07:21,680 --> 00:07:24,639
to

221
00:07:22,319 --> 00:07:25,520
it doesn't support the r/mX form it only

222
00:07:24,639 --> 00:07:29,120
supports

223
00:07:25,520 --> 00:07:32,240
a register 64 into the control register

224
00:07:29,120 --> 00:07:36,080
so this would be move r64 so rax

225
00:07:32,240 --> 00:07:38,319
into CR0 CR1 CR2 whatever

226
00:07:36,080 --> 00:07:40,240
and it has a form where you move from

227
00:07:38,319 --> 00:07:44,400
the control register back out to a

228
00:07:40,240 --> 00:07:44,400
64-bit general purpose register

