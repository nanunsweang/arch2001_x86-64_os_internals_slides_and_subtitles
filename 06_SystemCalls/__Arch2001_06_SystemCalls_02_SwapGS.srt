1
00:00:00,14 --> 00:00:02,01
So one thing you should have seen in the previous

2
00:00:02,01 --> 00:00:05,18
section was the use of an assembly instruction called swapgs

3
00:00:05,18 --> 00:00:08,66
at the entry point of the syscall target.

4
00:00:09,24 --> 00:00:11,11
So what was the reason for that?

5
00:00:11,19 --> 00:00:11,72
Well,

6
00:00:11,73 --> 00:00:14,32
if we look at the manual page for the swapgs

7
00:00:14,32 --> 00:00:16,03
assembly command,

8
00:00:16,04 --> 00:00:18,67
you can see this line when using syscall to implement

9
00:00:18,67 --> 00:00:19,38
system calls

10
00:00:19,38 --> 00:00:21,86
There is no kernel stack at the OS entry point

11
00:00:22,24 --> 00:00:22,55
neither

12
00:00:22,55 --> 00:00:25,14
is there a straightforward method to obtain a pointer to

13
00:00:25,14 --> 00:00:27,75
the kernel structures from which the kernel stack pointer could

14
00:00:27,75 --> 00:00:28,15
be read.

15
00:00:28,64 --> 00:00:28,89
Thus,

16
00:00:28,89 --> 00:00:32,15
the kernel cannot save general purpose registers or reference memory

17
00:00:32,74 --> 00:00:35,87
So the swapgs is designed to help deal with

18
00:00:35,87 --> 00:00:40,71
this problem of there being no designed way for the

19
00:00:40,71 --> 00:00:43,56
stack to be changed for the syscall assembly instructions.

20
00:00:44,04 --> 00:00:47,85
So swapgs swaps the GS base register and what

21
00:00:47,85 --> 00:00:49,33
does it swap it between?

22
00:00:49,39 --> 00:00:52,55
It swaps it between the IA32_GS_BASE,

23
00:00:52,55 --> 00:00:58,48
which is the portion of the hidden hidden portion of

24
00:00:58,48 --> 00:01:00,04
the segment register for

25
00:01:00,04 --> 00:01:00,32
gs

26
00:01:00,32 --> 00:01:04,07
that is mapped to this MSR and a new MSR

27
00:01:04,22 --> 00:01:08,25
IA32_KERNEL_GS_BASE which is used to store the

28
00:01:08,26 --> 00:01:10,45
base address for when you're in kernelspace.

29
00:01:10,94 --> 00:01:13,98
So basically when you're in a system call or possibly

30
00:01:13,98 --> 00:01:15,72
for things like interrupt handlers,

31
00:01:15,73 --> 00:01:18,31
it's desirable to just swap back and forth,

32
00:01:18,32 --> 00:01:21,4
flip these two values of what is effectively your user

33
00:01:21,4 --> 00:01:22,58
space base address,

34
00:01:22,58 --> 00:01:25,44
pointing out some userspace data structure and your kernel

35
00:01:25,44 --> 00:01:26,15
based address.

36
00:01:26,74 --> 00:01:28,12
So swapgs

37
00:01:28,12 --> 00:01:30,63
flips and flops between these two things.

38
00:01:30,64 --> 00:01:33,78
So back to this picture from before, we saw that

39
00:01:33,79 --> 00:01:37,66
the normal segment registers pointed a giant space

40
00:01:37,66 --> 00:01:41,48
But we saw that FS and GS are still treated

41
00:01:41,48 --> 00:01:44,56
like the original way segmentation work and can point at

42
00:01:44,56 --> 00:01:47,05
some arbitrary starting linear address.

43
00:01:47,34 --> 00:01:51,31
We saw that the hidden portion was mapped to the

44
00:01:51,32 --> 00:01:53,55
MSRs, fs base and Gs base,

45
00:01:53,94 --> 00:01:57,45
And now we're introducing this MSR kernel gs base.

46
00:01:57,84 --> 00:02:01,65
So when the system is running along in userspace

47
00:02:01,65 --> 00:02:05,49
the OS may or may not choose to put some

48
00:02:05,49 --> 00:02:07,56
data structure reference from GS base

49
00:02:07,56 --> 00:02:10,45
And we'll see that Windows does actually decide to put

50
00:02:10,45 --> 00:02:11,55
some data structure there.

51
00:02:12,74 --> 00:02:16,61
Then a system call occurs and it says change places

52
00:02:16,62 --> 00:02:18,34
and CS,

53
00:02:18,35 --> 00:02:19,08
Sorry

54
00:02:19,08 --> 00:02:23,15
And the gs from the kernel and userspace is swapped

55
00:02:23,64 --> 00:02:26,26
And that means that this userspace data structure all

56
00:02:26,26 --> 00:02:29,03
of a sudden now starts pointing at some kernel data

57
00:02:29,03 --> 00:02:30,62
structure inside the kernel.

58
00:02:30,64 --> 00:02:31,4
Likewise,

59
00:02:31,4 --> 00:02:34,07
when you're exiting out of kernel space,

60
00:02:34,26 --> 00:02:37,08
the kernel is going to again tell you to change

61
00:02:37,08 --> 00:02:40,68
places and the swapgs will flip back around to

62
00:02:40,68 --> 00:02:42,15
a userspace data structure

63
00:02:42,94 --> 00:02:43,22
So,

64
00:02:43,22 --> 00:02:46,32
swapgs is one of these assembly instructions which happens

65
00:02:46,32 --> 00:02:48,25
to be balanced in and of itself

66
00:02:48,25 --> 00:02:51,88
The expectation is that the kernel call swapgs on

67
00:02:51,88 --> 00:02:54,39
the way in and call swapgs on the way

68
00:02:54,39 --> 00:02:57,35
out in order to flip flop data structures.

69
00:02:58,74 --> 00:03:00,97
So what data structures in particular?

70
00:03:00,98 --> 00:03:03,84
Well on Windows for a long time,

71
00:03:03,95 --> 00:03:06,61
the 32-bit systems have this data structure called the

72
00:03:06,61 --> 00:03:07,9
thread environment block,

73
00:03:07,91 --> 00:03:10,22
but you'll also see it referred to as the thread

74
00:03:10,22 --> 00:03:11,16
information block

75
00:03:12,04 --> 00:03:12,67
In reality,

76
00:03:12,67 --> 00:03:16,22
the thread information block is just the first element of

77
00:03:16,22 --> 00:03:17,72
the thread environment block

78
00:03:17,73 --> 00:03:20,57
But thread information block was the only thing that Microsoft

79
00:03:20,57 --> 00:03:25,89
had officially documented so whatever but this thread environment block

80
00:03:25,89 --> 00:03:27,51
was used in userspace

81
00:03:27,51 --> 00:03:30,18
So if you saw references to FS frequently you would

82
00:03:30,18 --> 00:03:34,39
see that in malware analysis because malware would go know

83
00:03:34,39 --> 00:03:36,67
that this data structure is there and would go look

84
00:03:36,67 --> 00:03:37,86
up information from there

85
00:03:38,24 --> 00:03:41,2
And then in kernelspace there was a kernel processor

86
00:03:41,2 --> 00:03:42,25
control region,

87
00:03:42,26 --> 00:03:45,7
which would be used to hold a bunch of data

88
00:03:45,7 --> 00:03:48,16
structure information that the kernel wanted access to

89
00:03:49,14 --> 00:03:52,95
Now on 64-bit Windows has swapped to be using

90
00:03:52,95 --> 00:03:53,32
the gs

91
00:03:53,32 --> 00:03:55,17
instead of the fs

92
00:03:55,18 --> 00:03:57,97
And that of course makes things easier because they can

93
00:03:57,97 --> 00:04:02,03
just use the swapgs now to flip flop between

94
00:04:02,03 --> 00:04:03,64
these two data structures and kernel

95
00:04:03,65 --> 00:04:05,18
And furthermore,

96
00:04:05,23 --> 00:04:08,78
because the 30 the 64-bit system can still be

97
00:04:08,78 --> 00:04:11,91
running 32-bit code When they're using 32-bit code

98
00:04:11,91 --> 00:04:15,22
it will still be using these old locations for these

99
00:04:15,22 --> 00:04:16,0
data structures

100
00:04:16,0 --> 00:04:18,81
So that's why they're not using any specific,

101
00:04:18,81 --> 00:04:22,17
they're not reusing the FS for 64-bit specific stuff

102
00:04:22,18 --> 00:04:24,56
They're just treating it as still the 32-bit things

103
00:04:25,14 --> 00:04:26,93
And we have actually seen when we were dumping the

104
00:04:26,93 --> 00:04:29,16
GDT for or in the optional material,

105
00:04:29,16 --> 00:04:30,15
if you happen to watch it,

106
00:04:30,54 --> 00:04:32,24
Dumping the 32-bit GDT,

107
00:04:32,24 --> 00:04:35,14
you saw that the GS had invalid values so it

108
00:04:35,14 --> 00:04:36,45
was not being used on 32-bit

109
00:04:37,14 --> 00:04:39,06
So there's a little bit of symmetry going on here

110
00:04:39,06 --> 00:04:43,09
with FS and GS in 64-bit systems for Linux

111
00:04:43,09 --> 00:04:44,7
I know that a lot less well,

112
00:04:44,7 --> 00:04:47,36
but from the citations that I've been able to find

113
00:04:47,37 --> 00:04:48,85
on 32-bit systems,

114
00:04:48,85 --> 00:04:51,55
gs was used for thread local storage,

115
00:04:51,55 --> 00:04:55,11
very similar to how FS was used for thread information

116
00:04:55,12 --> 00:04:58,68
on Windows and I couldn't find any good references about

117
00:04:58,68 --> 00:04:58,99
these

118
00:04:58,99 --> 00:05:01,71
So if anyone has any good references for how those

119
00:05:01,71 --> 00:05:02,21
are used,

120
00:05:02,22 --> 00:05:03,15
please send in my way

121
00:05:03,94 --> 00:05:05,89
And then on the 64-bit systems,

122
00:05:05,89 --> 00:05:08,71
just like Windows sort of inverted what they used as

123
00:05:08,71 --> 00:05:13,56
gs moved to fs and the gs is currently

124
00:05:13,56 --> 00:05:17,36
listed as having no specific common use and that applications

125
00:05:17,36 --> 00:05:18,56
can use it for whatever they want

126
00:05:19,04 --> 00:05:20,6
Whereas in the kernelspace,

127
00:05:20,6 --> 00:05:23,62
probably in response to the swap gs functionality

128
00:05:23,84 --> 00:05:26,74
The gs now points at what are called per-CPU

129
00:05:26,74 --> 00:05:30,47
variables and it's really data structure that the kernel can

130
00:05:30,47 --> 00:05:32,57
use in order to look up things like the stack

131
00:05:32,57 --> 00:05:33,16
location

132
00:05:34,14 --> 00:05:36,51
So as I always say cites or it didn't happen

133
00:05:36,51 --> 00:05:39,32
I threw in some citations of why I'm saying any

134
00:05:39,32 --> 00:05:41,44
given data structure is used in any given way

135
00:05:41,45 --> 00:05:43,24
You can check those out at your leisure

136
00:05:43,47 --> 00:05:46,66
There is one other extra little bit of change that

137
00:05:46,66 --> 00:05:49,56
came along with this use of swapgs as a

138
00:05:49,56 --> 00:05:50,73
more explicit way

139
00:05:50,74 --> 00:05:53,16
to swap data structures in userspace and kernelspace

140
00:05:53,74 --> 00:05:57,22
So if cpuid of input of 7 in the

141
00:05:57,22 --> 00:06:00,65
eax and 0 in ecx returns in ebx

142
00:06:00,65 --> 00:06:04,52
with the fs gs base bid 0 set to 1

143
00:06:04,53 --> 00:06:08,44
and CR4 has the FSGSBASE set to

144
00:06:08,44 --> 00:06:08,86
1

145
00:06:09,04 --> 00:06:11,86
It means that the processor supports the following four new

146
00:06:11,86 --> 00:06:12,86
assembly instructions

147
00:06:12,86 --> 00:06:13,86
RDFSBASE,

148
00:06:13,94 --> 00:06:14,4
write

149
00:06:14,41 --> 00:06:16,42
FS RDGSBASE,

150
00:06:16,42 --> 00:06:17,37
WRFSBASE

151
00:06:17,37 --> 00:06:17,65


152
00:06:17,66 --> 00:06:18,46
WRGSBASE

153
00:06:18,47 --> 00:06:21,42
So these are instructions which as you can see are

154
00:06:21,42 --> 00:06:23,21
now all userspace things

155
00:06:23,22 --> 00:06:25,8
So all of a sudden you can start writing and

156
00:06:25,8 --> 00:06:28,94
reading to reading from and writing to the FS and

157
00:06:28,94 --> 00:06:31,84
GS base MSRs without having to use the

158
00:06:31,84 --> 00:06:32,72
rdmsr and

159
00:06:32,72 --> 00:06:33,14
wrmsr,

160
00:06:33,14 --> 00:06:35,75
which were ring 0 only assembly instructions

161
00:06:36,04 --> 00:06:39,05
This gives operating system makers is a little more flexibility

162
00:06:39,05 --> 00:06:52,4
in terms of what they do and usually that is

163
00:06:52,4 --> 00:06:55,42
where we find the FSGSBASE bit and that

164
00:06:55,42 --> 00:06:57,68
has to be set to 1 in order for those

165
00:06:57,68 --> 00:06:58,96
assembly instructions to work

166
00:07:00,24 --> 00:07:00,61
Now,

167
00:07:00,61 --> 00:07:01,62
miscellaneous thing

168
00:07:01,62 --> 00:07:04,28
Just kind of found it humorous as I'm googling around

169
00:07:04,28 --> 00:07:08,11
looking for references to what Linux uses for the FS

170
00:07:08,11 --> 00:07:09,37
and GS registers

171
00:07:09,38 --> 00:07:11,89
I found that this Linux article,

172
00:07:11,89 --> 00:07:16,36
Linux news article about the end of the FS GS

173
00:07:16,36 --> 00:07:17,16
base saga

174
00:07:17,44 --> 00:07:20,18
And I find it humorous because it kind of reads

175
00:07:20,18 --> 00:07:24,87
like a school newspaper talking about the drama amongst the

176
00:07:24,88 --> 00:07:27,42
various high schoolers and this is the kind of drama

177
00:07:27,42 --> 00:07:30,37
which is frequently found in operating system development

178
00:07:30,38 --> 00:07:31,64
And it's talking about how,

179
00:07:31,64 --> 00:07:32,18
you know,

180
00:07:32,18 --> 00:07:35,15
intel made the FS GS and then they were slow

181
00:07:35,15 --> 00:07:36,06
to get the,

182
00:07:36,34 --> 00:07:36,62
you know,

183
00:07:36,62 --> 00:07:39,39
patches to the Linux kernel that added support and someone

184
00:07:39,39 --> 00:07:41,61
else picked it up and then someone else didn't like

185
00:07:41,61 --> 00:07:42,85
the changes that they made

186
00:07:43,24 --> 00:07:46,21
So it's just the typical kind of West drama

187
00:07:46,21 --> 00:07:51,51
But it reads as somewhat humorous to me primarily because

188
00:07:51,52 --> 00:07:55,3
I think about the idea of internet historians someday digging

189
00:07:55,3 --> 00:07:59,8
up the archive and seeing preserved in Internet Stone

190
00:07:59,8 --> 00:08:03,46
This details of all the drama that went on anyways

191
00:08:03,46 --> 00:08:04,39
Check it out or don't,

192
00:08:04,4 --> 00:08:06,18
but it just gives you a little bit of context

193
00:08:06,18 --> 00:08:09,75
about how FSGSBASE started being used on Linux

