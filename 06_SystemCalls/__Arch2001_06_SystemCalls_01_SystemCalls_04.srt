1
00:00:08,000 --> 00:00:10,080
Here

2
00:00:08,400 --> 00:00:11,920
are some visualizations of those

3
00:00:10,080 --> 00:00:12,240
registers that I was just rattling off

4
00:00:11,920 --> 00:00:15,679
the

5
00:00:12,240 --> 00:00:17,600
fmask is used again for bit masking off

6
00:00:15,679 --> 00:00:18,240
things from the RFLAGS or EFLAGS

7
00:00:17,600 --> 00:00:21,119
register

8
00:00:18,240 --> 00:00:22,320
on transition in. The lstar just gets

9
00:00:21,119 --> 00:00:24,800
the rip

10
00:00:22,320 --> 00:00:25,920
so just jump to whatever that says to

11
00:00:24,800 --> 00:00:29,119
jump to

12
00:00:25,920 --> 00:00:31,359
and then the syscall cs and ss

13
00:00:29,119 --> 00:00:32,800
for the transition into the kernel so

14
00:00:31,359 --> 00:00:35,360
the syscall

15
00:00:32,800 --> 00:00:36,239
comes from these bits plus some math and

16
00:00:35,360 --> 00:00:38,480
the sysret

17
00:00:36,239 --> 00:00:40,640
for transitioning back down into user

18
00:00:38,480 --> 00:00:42,320
space comes from these bits plus the

19
00:00:40,640 --> 00:00:44,800
math

20
00:00:42,320 --> 00:00:46,079
but that math is not exactly intuitive

21
00:00:44,800 --> 00:00:47,680
so I wanted to just

22
00:00:46,079 --> 00:00:49,039
animate it out for you so that you could

23
00:00:47,680 --> 00:00:50,160
visualize what's going on a little

24
00:00:49,039 --> 00:00:53,760
better

25
00:00:50,160 --> 00:00:57,120
so if you had the ia32_star msr

26
00:00:53,760 --> 00:01:00,480
bits 47 to 32 are what are used

27
00:00:57,120 --> 00:01:03,039
for setting the cs the code segment

28
00:01:00,480 --> 00:01:04,720
on transition into kernelspace so if

29
00:01:03,039 --> 00:01:08,320
that happened to be just 8

30
00:01:04,720 --> 00:01:10,320
then the cs would get exactly 8

31
00:01:08,320 --> 00:01:12,000
8 would be interpreted as a segment

32
00:01:10,320 --> 00:01:15,119
selector like we've seen before

33
00:01:12,000 --> 00:01:18,240
of 1000. So 1 means

34
00:01:15,119 --> 00:01:22,799
index 1 in the GDT, 0 - GDT

35
00:01:18,240 --> 00:01:25,680
00 privilege level ring 0

36
00:01:22,799 --> 00:01:26,080
all right so ss was defined as cs

37
00:01:25,680 --> 00:01:30,400
plus

38
00:01:26,080 --> 00:01:32,799
8 so if cs was 8 then ss is 16

39
00:01:30,400 --> 00:01:33,759
and that again is just interpreted as

40
00:01:32,799 --> 00:01:37,280
per the segment

41
00:01:33,759 --> 00:01:37,920
selector description so this turns into

42
00:01:37,280 --> 00:01:41,360
a 2

43
00:01:37,920 --> 00:01:41,920
here and that is indexing into GDT entry

44
00:01:41,360 --> 00:01:43,680
2

45
00:01:41,920 --> 00:01:46,479
on the other side of things going back

46
00:01:43,680 --> 00:01:51,040
from kernelspace into userspace

47
00:01:46,479 --> 00:01:53,439
the cs register comes from bits 63 to 48

48
00:01:51,040 --> 00:01:54,159
and it is those bits plus 16 so there's

49
00:01:53,439 --> 00:01:58,159
an automatic

50
00:01:54,159 --> 00:01:59,920
16 added so 0x10 added to that

51
00:01:58,159 --> 00:02:01,520
again interpreted according to a segment

52
00:01:59,920 --> 00:02:04,640
selector that would be index

53
00:02:01,520 --> 00:02:07,200
4 up here and then

54
00:02:04,640 --> 00:02:09,599
the interesting little bit here is that

55
00:02:07,200 --> 00:02:11,039
well what if this was set to 0 you

56
00:02:09,599 --> 00:02:12,640
know what if the kernel accidentally

57
00:02:11,039 --> 00:02:15,440
misconfigured this and

58
00:02:12,640 --> 00:02:17,680
you know had it so that the RPL was set

59
00:02:15,440 --> 00:02:19,520
to 0 and so ultimately it would be

60
00:02:17,680 --> 00:02:21,280
you know treated like kernel when you

61
00:02:19,520 --> 00:02:22,080
get back into what's supposed to be user

62
00:02:21,280 --> 00:02:23,920
space

63
00:02:22,080 --> 00:02:25,920
so if we dig into the nitty-gritty

64
00:02:23,920 --> 00:02:27,840
details of the sysret

65
00:02:25,920 --> 00:02:29,840
instruction we can see that you know we

66
00:02:27,840 --> 00:02:31,680
go down here and it says if operand

67
00:02:29,840 --> 00:02:34,480
size is 64 bits

68
00:02:31,680 --> 00:02:35,200
then the cs selector is those bits plus

69
00:02:34,480 --> 00:02:37,599
16.

70
00:02:35,200 --> 00:02:39,760
great but then this is the interesting

71
00:02:37,599 --> 00:02:41,040
bit so cs selector is equal to cs

72
00:02:39,760 --> 00:02:43,599
selector or

73
00:02:41,040 --> 00:02:45,840
three so that's good basically the

74
00:02:43,599 --> 00:02:47,840
assembly instruction behind the scenes

75
00:02:45,840 --> 00:02:49,920
the pseudocode that handles it or really

76
00:02:47,840 --> 00:02:52,879
the microcode that handles it

77
00:02:49,920 --> 00:02:54,720
is making sure that always as we're

78
00:02:52,879 --> 00:02:58,000
returning back to userspace

79
00:02:54,720 --> 00:02:59,360
it is forcing us into ring 3

80
00:02:58,000 --> 00:03:01,200
and again that's just another one of

81
00:02:59,360 --> 00:03:02,959
these consequences of

82
00:03:01,200 --> 00:03:04,879
you know rings 1 and 2 never really

83
00:03:02,959 --> 00:03:06,000
being used they just decided to say you

84
00:03:04,879 --> 00:03:07,040
know what screw it we're not going to

85
00:03:06,000 --> 00:03:09,840
make this generic for

86
00:03:07,040 --> 00:03:11,200
every single privilege ring transition

87
00:03:09,840 --> 00:03:13,599
we're just going to assume people are

88
00:03:11,200 --> 00:03:15,440
using ring 0 and ring 3

89
00:03:13,599 --> 00:03:17,440
and likewise a bunch of hard-coded

90
00:03:15,440 --> 00:03:20,319
values get set into the

91
00:03:17,440 --> 00:03:21,200
hidden portion the cache portion of the

92
00:03:20,319 --> 00:03:23,920
cs

93
00:03:21,200 --> 00:03:26,720
segment register including the DPL being

94
00:03:23,920 --> 00:03:29,840
set to a hard-coded 3

95
00:03:26,720 --> 00:03:32,959
so that means in reality this is not

96
00:03:29,840 --> 00:03:34,319
10 to 20 this is 10 to 23. it's being

97
00:03:32,959 --> 00:03:37,920
ORed with 3

98
00:03:34,319 --> 00:03:40,959
and so this is set to 3

99
00:03:37,920 --> 00:03:42,159
all right and then ss was defined as the

100
00:03:40,959 --> 00:03:44,959
same bits but plus

101
00:03:42,159 --> 00:03:46,080
8 instead of plus 16 and if we

102
00:03:44,959 --> 00:03:47,760
look at the pseudocode

103
00:03:46,080 --> 00:03:49,200
we're also going to see that that has an

104
00:03:47,760 --> 00:03:52,400
or 3 in it

105
00:03:49,200 --> 00:03:54,720
so this is going to encode index 3

106
00:03:52,400 --> 00:03:56,879
so what we end up with is a GDT that

107
00:03:54,720 --> 00:03:58,000
looks like ring 0 code here ring 0

108
00:03:56,879 --> 00:04:00,080
stack there

109
00:03:58,000 --> 00:04:01,519
ring 3 stack here ring 3 code

110
00:04:00,080 --> 00:04:03,200
there now this was a completely

111
00:04:01,519 --> 00:04:04,319
arbitrary example I just picked some

112
00:04:03,200 --> 00:04:06,080
random values

113
00:04:04,319 --> 00:04:07,840
we could have picked a value here of

114
00:04:06,080 --> 00:04:10,159
8 and that would have led to

115
00:04:07,840 --> 00:04:10,879
code here and it would have shared the

116
00:04:10,159 --> 00:04:13,200
ring 0

117
00:04:10,879 --> 00:04:14,000
stack with ring 3 as well we could

118
00:04:13,200 --> 00:04:16,000
have pushed it even

119
00:04:14,000 --> 00:04:17,919
further up because if you go look at the

120
00:04:16,000 --> 00:04:18,799
nitty-gritty details of these assembly

121
00:04:17,919 --> 00:04:21,120
instructions

122
00:04:18,799 --> 00:04:23,120
you will see that the 32-bit mode

123
00:04:21,120 --> 00:04:24,720
version so this is a plus 8 and this is

124
00:04:23,120 --> 00:04:26,479
a plus 16. well they're actually in

125
00:04:24,720 --> 00:04:28,960
32-bit mode it would be a plus

126
00:04:26,479 --> 00:04:29,919
0 so then the code segment if I

127
00:04:28,960 --> 00:04:32,639
recall correctly

128
00:04:29,919 --> 00:04:34,080
in 32-bit mode is plus 0 so they

129
00:04:32,639 --> 00:04:35,680
would essentially have you know 3

130
00:04:34,080 --> 00:04:38,479
contiguous things some for

131
00:04:35,680 --> 00:04:39,840
64-bit some for 32-bit but that's

132
00:04:38,479 --> 00:04:41,199
neither here nor there so

133
00:04:39,840 --> 00:04:42,880
point is this was an arbitrary thing

134
00:04:41,199 --> 00:04:43,680
just to try and help you understand

135
00:04:42,880 --> 00:04:45,680
these

136
00:04:43,680 --> 00:04:47,840
you know things that from a single slide

137
00:04:45,680 --> 00:04:50,479
are perhaps not intuitive that

138
00:04:47,840 --> 00:04:51,840
this math this plus 8s plus 16s the

139
00:04:50,479 --> 00:04:54,560
only point of it really

140
00:04:51,840 --> 00:04:56,160
is to say okay well we want the code and

141
00:04:54,560 --> 00:04:58,560
stack to be next to each other the

142
00:04:56,160 --> 00:04:59,280
segment descriptors and dear operating

143
00:04:58,560 --> 00:05:01,199
system thou

144
00:04:59,280 --> 00:05:02,720
shalt create these segment

145
00:05:01,199 --> 00:05:05,120
descriptors next to each other in the

146
00:05:02,720 --> 00:05:07,360
GDT

147
00:05:05,120 --> 00:05:08,720
all right well what about arguments if

148
00:05:07,360 --> 00:05:10,800
you're making a system call

149
00:05:08,720 --> 00:05:11,919
from userspace to kernelspace surely

150
00:05:10,800 --> 00:05:12,880
you're going to have to give some

151
00:05:11,919 --> 00:05:14,960
arguments

152
00:05:12,880 --> 00:05:16,880
and yes you certainly do have to give

153
00:05:14,960 --> 00:05:18,960
arguments but that's not defined at an

154
00:05:16,880 --> 00:05:20,720
x86 level that's up to each operating

155
00:05:18,960 --> 00:05:22,800
system to define itself

156
00:05:20,720 --> 00:05:24,720
and consequently I'm going to go ahead

157
00:05:22,800 --> 00:05:26,960
and leave that for the future operating

158
00:05:24,720 --> 00:05:29,280
system specific architecture classes

159
00:05:26,960 --> 00:05:31,120
where we you know reiterate all this

160
00:05:29,280 --> 00:05:32,639
kind of stuff you learned in 2001

161
00:05:31,120 --> 00:05:34,560
but then you know we really drill down

162
00:05:32,639 --> 00:05:34,960
on okay what does operating system a b

163
00:05:34,560 --> 00:05:38,479
and c

164
00:05:34,960 --> 00:05:40,479
do with it so stay tuned for system call

165
00:05:38,479 --> 00:05:43,199
arguments covered in the future OS

166
00:05:40,479 --> 00:05:43,199
specific classes

