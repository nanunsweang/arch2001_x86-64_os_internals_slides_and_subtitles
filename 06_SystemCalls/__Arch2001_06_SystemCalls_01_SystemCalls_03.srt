1
00:00:08,240 --> 00:00:12,480
so syscall how does it work

2
00:00:10,639 --> 00:00:14,320
well first thing that happens when you

3
00:00:12,480 --> 00:00:16,720
issue the syscall assembly instruction

4
00:00:14,320 --> 00:00:18,000
is that it saves the rip of the address

5
00:00:16,720 --> 00:00:19,199
after the syscall

6
00:00:18,000 --> 00:00:21,680
which is basically the place you're

7
00:00:19,199 --> 00:00:23,519
going to return to into rcx

8
00:00:21,680 --> 00:00:25,119
so it's not pushing on the stack saves

9
00:00:23,519 --> 00:00:26,320
it into this register

10
00:00:25,119 --> 00:00:28,480
and the kernel is going to be

11
00:00:26,320 --> 00:00:29,439
responsible for you know using that to

12
00:00:28,480 --> 00:00:32,640
get back to the

13
00:00:29,439 --> 00:00:35,200
rip it then changes the rip

14
00:00:32,640 --> 00:00:36,079
for kernelspace into a value that is

15
00:00:35,200 --> 00:00:38,640
stored in this

16
00:00:36,079 --> 00:00:40,559
msr now again remember that msrs can

17
00:00:38,640 --> 00:00:42,320
only be written by ring 0 so

18
00:00:40,559 --> 00:00:43,840
no danger there of the attacker just

19
00:00:42,320 --> 00:00:45,600
pointing this somewhere else unless they

20
00:00:43,840 --> 00:00:49,520
already have ring 0

21
00:00:45,600 --> 00:00:52,399
so that msr holds the kernel's rip value

22
00:00:49,520 --> 00:00:53,760
then it saves off rflags into the r11

23
00:00:52,399 --> 00:00:55,760
register

24
00:00:53,760 --> 00:00:57,680
and then an interesting little bit of

25
00:00:55,760 --> 00:01:01,039
configuration happens here

26
00:00:57,680 --> 00:01:02,879
the rflags register will be masked

27
00:01:01,039 --> 00:01:06,479
against this ia32

28
00:01:02,879 --> 00:01:09,840
fmask msr and so any bit that is set to

29
00:01:06,479 --> 00:01:12,000
1 in this msr will be cleared

30
00:01:09,840 --> 00:01:13,680
in the rflags at the time that it gets

31
00:01:12,000 --> 00:01:15,439
into kernelspace

32
00:01:13,680 --> 00:01:17,119
so it's not just anding it's not just

33
00:01:15,439 --> 00:01:20,400
oring it's more like

34
00:01:17,119 --> 00:01:22,400
you take ia32 fmask invert it so every

35
00:01:20,400 --> 00:01:23,520
1 becomes a 0 every 0 becomes

36
00:01:22,400 --> 00:01:25,680
1

37
00:01:23,520 --> 00:01:26,720
and then you and the rflags with the

38
00:01:25,680 --> 00:01:28,159
inverted value

39
00:01:26,720 --> 00:01:29,759
so that it essentially clears all the

40
00:01:28,159 --> 00:01:31,520
bits that were said to be cleared in the

41
00:01:29,759 --> 00:01:34,320
fmask

42
00:01:31,520 --> 00:01:34,880
furthermore it's going to load the cs

43
00:01:34,320 --> 00:01:38,479
value

44
00:01:34,880 --> 00:01:42,399
for kernelspace with a value from ia32

45
00:01:38,479 --> 00:01:45,439
star bits 47 to 32

46
00:01:42,399 --> 00:01:48,799
and then ss is going to just be cs

47
00:01:45,439 --> 00:01:50,399
plus 8 so if these bits basically

48
00:01:48,799 --> 00:01:52,240
they're you know cs it's a segment

49
00:01:50,399 --> 00:01:55,680
selector type value

50
00:01:52,240 --> 00:01:56,000
so if these bits were 0 then ss would

51
00:01:55,680 --> 00:01:58,079
be

52
00:01:56,000 --> 00:01:59,439
8 if these bits were 8 then ss

53
00:01:58,079 --> 00:02:01,439
would be 16

54
00:01:59,439 --> 00:02:03,520
and regardless of what the value is it

55
00:02:01,439 --> 00:02:05,200
would still always be interpreted as a

56
00:02:03,520 --> 00:02:08,000
segment selector as we've seen in the

57
00:02:05,200 --> 00:02:10,160
past and you know all of this goop

58
00:02:08,000 --> 00:02:11,280
is why it's actually easier to just read

59
00:02:10,160 --> 00:02:12,879
the manuals

60
00:02:11,280 --> 00:02:14,319
operation pseudocode which we'll

61
00:02:12,879 --> 00:02:15,840
actually see in a little bit here

62
00:02:14,319 --> 00:02:17,200
to really understand what's going on

63
00:02:15,840 --> 00:02:21,040
because these type of assembly

64
00:02:17,200 --> 00:02:22,959
instructions are extremely complicated

65
00:02:21,040 --> 00:02:24,560
now an interesting thing is that

66
00:02:22,959 --> 00:02:27,840
normally you would expect that

67
00:02:24,560 --> 00:02:30,959
you know rip and cs would combine

68
00:02:27,840 --> 00:02:33,200
with rsp and ss in order to say you know

69
00:02:30,959 --> 00:02:35,280
where's my new code where's my new stack

70
00:02:33,200 --> 00:02:37,680
and so while it does define a stack

71
00:02:35,280 --> 00:02:40,640
segment here it doesn't actually

72
00:02:37,680 --> 00:02:42,160
define a way that rsp is saved so it's

73
00:02:40,640 --> 00:02:44,400
ultimately up to either

74
00:02:42,160 --> 00:02:46,640
userspace side before it gets into

75
00:02:44,400 --> 00:02:47,360
kernel or kernel side after it gets into

76
00:02:46,640 --> 00:02:49,760
kernel

77
00:02:47,360 --> 00:02:50,720
to save the rsp and make sure that it

78
00:02:49,760 --> 00:02:52,560
gets restored

79
00:02:50,720 --> 00:02:54,319
again if the kernel saves it kernel

80
00:02:52,560 --> 00:02:57,440
should restore it when userspace saves

81
00:02:54,319 --> 00:03:00,080
it userspace should restore it

82
00:02:57,440 --> 00:03:01,280
all right now that's paired with the

83
00:03:00,080 --> 00:03:04,000
sysret system

84
00:03:01,280 --> 00:03:05,440
return system call return so what does

85
00:03:04,000 --> 00:03:07,120
that do well it has to do kind of the

86
00:03:05,440 --> 00:03:09,200
inverse of the syscall of course

87
00:03:07,120 --> 00:03:10,480
so the rip is going to be restored from

88
00:03:09,200 --> 00:03:12,239
rcx so

89
00:03:10,480 --> 00:03:13,840
you know the the kernel side needs to

90
00:03:12,239 --> 00:03:16,000
make sure that it if it

91
00:03:13,840 --> 00:03:17,440
if it used rcx for something else

92
00:03:16,000 --> 00:03:19,440
anytime in kernelspace

93
00:03:17,440 --> 00:03:21,440
it better put it back to the rip of

94
00:03:19,440 --> 00:03:22,800
after after the syscall so that it can

95
00:03:21,440 --> 00:03:24,560
get back there

96
00:03:22,800 --> 00:03:26,959
the rflags is going to be restored from

97
00:03:24,560 --> 00:03:28,400
r11 so all that bit masking and stuff

98
00:03:26,959 --> 00:03:30,560
that happened doesn't matter because the

99
00:03:28,400 --> 00:03:32,319
original value got stored to r11 so it

100
00:03:30,560 --> 00:03:35,519
can just put it back

101
00:03:32,319 --> 00:03:38,640
the cs gets set to a value in

102
00:03:35,519 --> 00:03:41,920
this ia32 star register bits

103
00:03:38,640 --> 00:03:43,360
63 to 48 so those are not the same bits

104
00:03:41,920 --> 00:03:46,879
the previous one was

105
00:03:43,360 --> 00:03:49,040
47 to 32 so this is 63 to 48

106
00:03:46,879 --> 00:03:50,480
and then it takes that value and it just

107
00:03:49,040 --> 00:03:51,760
adds 16 to it so

108
00:03:50,480 --> 00:03:53,760
again it's going to just kind of

109
00:03:51,760 --> 00:03:56,720
increment it up so that it's effectively

110
00:03:53,760 --> 00:03:58,319
moving the index within the GDT

111
00:03:56,720 --> 00:04:00,159
and I'll show a sort of visualization of

112
00:03:58,319 --> 00:04:03,439
that in a second

113
00:04:00,159 --> 00:04:06,720
ss on the other hand gets i32 star

114
00:04:03,439 --> 00:04:09,280
bits 63 through 48 so same bits but

115
00:04:06,720 --> 00:04:10,000
8 is added instead of 16 so

116
00:04:09,280 --> 00:04:14,080
basically

117
00:04:10,000 --> 00:04:17,519
the ss is going to be below the

118
00:04:14,080 --> 00:04:19,040
code segment and again whichever side of

119
00:04:17,519 --> 00:04:21,120
kernel/user space

120
00:04:19,040 --> 00:04:23,040
saved off the rsp is responsible for now

121
00:04:21,120 --> 00:04:25,440
restoring it

122
00:04:23,040 --> 00:04:26,800
so syscall and sysret are another thing

123
00:04:25,440 --> 00:04:29,840
that is perfectly balanced

124
00:04:26,800 --> 00:04:29,840
as all things should be

