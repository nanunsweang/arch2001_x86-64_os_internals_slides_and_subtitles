1
00:00:08,080 --> 00:00:11,759
all right now I've got a little exercise

2
00:00:09,519 --> 00:00:13,920
for you I want you to go into WinDbg

3
00:00:11,759 --> 00:00:16,720
and use your rdmsr command in

4
00:00:13,920 --> 00:00:19,520
concert with these various msrs that we

5
00:00:16,720 --> 00:00:21,199
just learned about in syscall and sysret

6
00:00:19,520 --> 00:00:22,720
and answer these sort of questions it

7
00:00:21,199 --> 00:00:24,240
won't be these exact questions you

8
00:00:22,720 --> 00:00:26,240
should again see the website

9
00:00:24,240 --> 00:00:27,359
not this slide for what you need to

10
00:00:26,240 --> 00:00:29,840
answer but

11
00:00:27,359 --> 00:00:31,279
it's things of the general notion of you

12
00:00:29,840 --> 00:00:33,440
know what is the name of the function

13
00:00:31,279 --> 00:00:34,719
where the syscall lands you in kernel

14
00:00:33,440 --> 00:00:37,280
space on windows

15
00:00:34,719 --> 00:00:37,840
what's the first assembly instruction

16
00:00:37,280 --> 00:00:41,440
for

17
00:00:37,840 --> 00:00:43,600
all of those bits in the you know msr

18
00:00:41,440 --> 00:00:45,360
what is the ring 0 cs segment selector

19
00:00:43,600 --> 00:00:47,280
what is the ring 0 ss

20
00:00:45,360 --> 00:00:49,280
and so forth and once you've got each of

21
00:00:47,280 --> 00:00:50,960
these values what do those look like

22
00:00:49,280 --> 00:00:53,120
relative to the GDT

23
00:00:50,960 --> 00:00:54,320
values that you saw dumped before is

24
00:00:53,120 --> 00:00:57,840
everything accounted for

25
00:00:54,320 --> 00:01:00,480
what's missing what's not used and also

26
00:00:57,840 --> 00:01:02,160
having done all of these msr rdmsr

27
00:01:00,480 --> 00:01:04,080
commands in WinDbg you will have

28
00:01:02,160 --> 00:01:07,760
looked at 0xC00000081

29
00:01:04,080 --> 00:01:10,080
82 and 84. well what's 83

30
00:01:07,760 --> 00:01:12,159
you can of course look at the manual but

31
00:01:10,080 --> 00:01:14,080
maybe just look at in WinDbg and maybe

32
00:01:12,159 --> 00:01:15,840
the kernel symbols will tell you what it

33
00:01:14,080 --> 00:01:18,000
probably stands for

34
00:01:15,840 --> 00:01:19,040
finally what information is masked out

35
00:01:18,000 --> 00:01:22,000
by the fmask

36
00:01:19,040 --> 00:01:24,159
like what specific bits has this

37
00:01:22,000 --> 00:01:25,520
operating system decided that it doesn't

38
00:01:24,159 --> 00:01:26,960
want getting set

39
00:01:25,520 --> 00:01:30,400
and which of those have we learned about

40
00:01:26,960 --> 00:01:30,400
which of them have we not

