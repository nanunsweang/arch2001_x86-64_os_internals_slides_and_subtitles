1
00:00:08,000 --> 00:00:10,639
now

2
00:00:08,400 --> 00:00:11,440
in terms of the compatibility for syscall

3
00:00:10,639 --> 00:00:13,920
and sysenter

4
00:00:11,440 --> 00:00:14,920
unfortunately the compatibility matrix

5
00:00:13,920 --> 00:00:18,720
is a little bit

6
00:00:14,920 --> 00:00:19,439
messy so AMD systems that were 32-bit

7
00:00:18,720 --> 00:00:22,080
supported

8
00:00:19,439 --> 00:00:23,760
intel's sysenter and sysexit and of

9
00:00:22,080 --> 00:00:24,480
course intel supported their own 

10
00:00:23,760 --> 00:00:28,080
sysenter and

11
00:00:24,480 --> 00:00:30,160
sysexit but

12
00:00:28,080 --> 00:00:32,960
when AMD went off and invented the

13
00:00:30,160 --> 00:00:35,520
syscall mechanism for 64-bit

14
00:00:32,960 --> 00:00:36,399
they put it in their 32-bit capabilities

15
00:00:35,520 --> 00:00:38,480
but intel

16
00:00:36,399 --> 00:00:39,520
never bothered to put it in their 32-bit

17
00:00:38,480 --> 00:00:41,360
capabilities

18
00:00:39,520 --> 00:00:44,640
so if you want to run on an intel

19
00:00:41,360 --> 00:00:46,320
processor in a 32-bit operating system

20
00:00:44,640 --> 00:00:48,559
that operating system better not be

21
00:00:46,320 --> 00:00:50,800
using the syscall assembly instruction

22
00:00:48,559 --> 00:00:54,160
otherwise it's not going to work

23
00:00:50,800 --> 00:00:56,960
likewise AMD having invented 64-bit

24
00:00:54,160 --> 00:00:59,280
uh you know syscall they of course

25
00:00:56,960 --> 00:01:03,039
support it in 64-bit mode

26
00:00:59,280 --> 00:01:05,519
but they don't support the 64-bit

27
00:01:03,039 --> 00:01:07,280
extended version of sysenter which

28
00:01:05,519 --> 00:01:08,159
intel made later basically they made

29
00:01:07,280 --> 00:01:10,000
their own

30
00:01:08,159 --> 00:01:11,680
they didn't just extend this one and so

31
00:01:10,000 --> 00:01:12,640
they don't support that whereas intel

32
00:01:11,680 --> 00:01:15,200
does

33
00:01:12,640 --> 00:01:16,320
so basically what we have is a situation

34
00:01:15,200 --> 00:01:19,680
like this

35
00:01:16,320 --> 00:01:22,720
got a connect4 and 32-bit

36
00:01:19,680 --> 00:01:24,840
it is preferred to use sysenter and for

37
00:01:22,720 --> 00:01:28,000
64-bit it is preferred to use

38
00:01:24,840 --> 00:01:29,759
syscall so

39
00:01:28,000 --> 00:01:31,600
how do you know like so obviously an

40
00:01:29,759 --> 00:01:32,479
operating system has to know what it can

41
00:01:31,600 --> 00:01:34,640
use in a

42
00:01:32,479 --> 00:01:36,320
given hardware should it fall back to

43
00:01:34,640 --> 00:01:36,799
interrupts should it use the new much

44
00:01:36,320 --> 00:01:39,840
faster

45
00:01:36,799 --> 00:01:42,640
syscall sysenter type things well that

46
00:01:39,840 --> 00:01:45,360
again becomes one of those things where

47
00:01:42,640 --> 00:01:46,000
you know we have to look up some msr and

48
00:01:45,360 --> 00:01:48,799
msr

49
00:01:46,000 --> 00:01:50,799
says to go look in cpuid this happens to

50
00:01:48,799 --> 00:01:53,840
be the msr we had already seen the

51
00:01:50,799 --> 00:01:55,920
extended feature enable

52
00:01:53,840 --> 00:01:58,560
model specific register that we had seen

53
00:01:55,920 --> 00:02:01,360
for the long mode enable bit before

54
00:01:58,560 --> 00:02:02,320
so bit 0 of this is syscall so if

55
00:02:01,360 --> 00:02:04,399
this is set to 1

56
00:02:02,320 --> 00:02:05,840
it enables the syscall assembly

57
00:02:04,399 --> 00:02:07,680
instructions

58
00:02:05,840 --> 00:02:09,520
and so going back and checking what this

59
00:02:07,680 --> 00:02:12,480
said it said you know

60
00:02:09,520 --> 00:02:14,480
bit 11 is so this is actually kind of

61
00:02:12,480 --> 00:02:14,959
counterintuitive it says you know here

62
00:02:14,480 --> 00:02:18,000
if

63
00:02:14,959 --> 00:02:19,680
bit 20 or bit 29 then you know we can

64
00:02:18,000 --> 00:02:22,400
use this stuff but actually

65
00:02:19,680 --> 00:02:24,000
you know those had to do with bit 20 I

66
00:02:22,400 --> 00:02:26,400
don't remember but it was like the long

67
00:02:24,000 --> 00:02:29,200
mode enable bit 29 I think was the

68
00:02:26,400 --> 00:02:31,280
nx enable and actually the syscall is

69
00:02:29,200 --> 00:02:35,200
not dependent on either of those two

70
00:02:31,280 --> 00:02:37,440
it's dependent upon bit 11 the syscall

71
00:02:35,200 --> 00:02:38,319
capability of the processor and here in

72
00:02:37,440 --> 00:02:41,280
the cpuid

73
00:02:38,319 --> 00:02:43,360
output you can see this mention of intel

74
00:02:41,280 --> 00:02:45,040
processor support syscall and sysret only

75
00:02:43,360 --> 00:02:47,879
in 64-bit mode so they're basically

76
00:02:45,040 --> 00:02:50,879
saying we don't do syscall in 32-bit

77
00:02:47,879 --> 00:02:50,879
mode

