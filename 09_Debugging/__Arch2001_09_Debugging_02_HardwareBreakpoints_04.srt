1
00:00:00,640 --> 00:00:04,960
as a reminder of the sort of taxonomy

2
00:00:03,360 --> 00:00:06,560
of conditions that exist somewhere on

3
00:00:04,960 --> 00:00:07,440
the system that require the processor's

4
00:00:06,560 --> 00:00:09,120
attention

5
00:00:07,440 --> 00:00:11,040
we had interrupts from hardware and

6
00:00:09,120 --> 00:00:13,440
instructions and exceptions

7
00:00:11,040 --> 00:00:15,280
which were the fault which the rip

8
00:00:13,440 --> 00:00:17,680
points at the faulting instruction

9
00:00:15,280 --> 00:00:19,119
and the trap where the rip points at the

10
00:00:17,680 --> 00:00:20,560
instruction after the trapping

11
00:00:19,119 --> 00:00:22,160
instruction

12
00:00:20,560 --> 00:00:24,240
so let's talk about what happens when a

13
00:00:22,160 --> 00:00:27,359
hardware breakpoint fires

14
00:00:24,240 --> 00:00:29,599
it actually fires the idt entry one

15
00:00:27,359 --> 00:00:31,199
which the manual calls a debug exception

16
00:00:29,599 --> 00:00:33,920
so you can see that

17
00:00:31,199 --> 00:00:36,239
one this is from hardware and two it's

18
00:00:33,920 --> 00:00:38,239
called an exception so this notion of

19
00:00:36,239 --> 00:00:38,800
hardware interrupts versus exceptions

20
00:00:38,239 --> 00:00:42,160
are not

21
00:00:38,800 --> 00:00:44,320
mutually exclusive so what happens

22
00:00:42,160 --> 00:00:46,399
is that whenever it's an execute

23
00:00:44,320 --> 00:00:48,800
breakpoint or a general detect

24
00:00:46,399 --> 00:00:51,039
breakpoint meaning there was that gde

25
00:00:48,800 --> 00:00:52,640
bit which is when someone does a move

26
00:00:51,039 --> 00:00:53,840
instruction trying to write to the debug

27
00:00:52,640 --> 00:00:56,879
registers

28
00:00:53,840 --> 00:00:57,600
it is going to be treated as a fault so

29
00:00:56,879 --> 00:00:59,840
in that case

30
00:00:57,600 --> 00:01:01,359
basically break on execute a general

31
00:00:59,840 --> 00:01:04,159
detect it's a fault

32
00:01:01,359 --> 00:01:05,760
halt hitler's fault pointing at bobo

33
00:01:04,159 --> 00:01:08,799
saying this is all your fault

34
00:01:05,760 --> 00:01:12,080
rip points at the faulting instruction

35
00:01:08,799 --> 00:01:12,799
then also on

36
00:01:12,080 --> 00:01:14,880
break on write

37
00:01:12,799 --> 00:01:16,240
break on read write and break on io

38
00:01:14,880 --> 00:01:19,360
hardware break points

39
00:01:16,240 --> 00:01:21,360
it's a trap so basically there's two

40
00:01:19,360 --> 00:01:24,479
different types of behavior that the

41
00:01:21,360 --> 00:01:26,799
debugger has to account for so

42
00:01:24,479 --> 00:01:28,000
practically speaking that means if it is

43
00:01:26,799 --> 00:01:30,799
a break on write

44
00:01:28,000 --> 00:01:33,040
or a break on you know read write the

45
00:01:30,799 --> 00:01:35,520
data is actually going to be overwritten

46
00:01:33,040 --> 00:01:37,920
before the exception is generated so any

47
00:01:35,520 --> 00:01:38,479
sort of debug handler that would want to

48
00:01:37,920 --> 00:01:40,400
actually

49
00:01:38,479 --> 00:01:42,479
determine what the before and after

50
00:01:40,400 --> 00:01:44,560
values were would actually have to

51
00:01:42,479 --> 00:01:46,000
read the value at the time of the

52
00:01:44,560 --> 00:01:48,079
initial breakpoint setup

53
00:01:46,000 --> 00:01:50,560
on the other hand an instruction

54
00:01:48,079 --> 00:01:52,159
breakpoint is actually detected before

55
00:01:50,560 --> 00:01:53,600
the instruction executes now this is

56
00:01:52,159 --> 00:01:54,000
different from the software breakpoint

57
00:01:53,600 --> 00:01:56,240
we saw

58
00:01:54,000 --> 00:01:58,799
before we said the software breakpoint

59
00:01:56,240 --> 00:02:01,439
from before was actually a trap

60
00:01:58,799 --> 00:02:02,799
and so basically previously it would

61
00:02:01,439 --> 00:02:05,920
trap after these

62
00:02:02,799 --> 00:02:08,239
hex cc the int 3 assembly instruction

63
00:02:05,920 --> 00:02:09,039
but it always knew it could back up by a

64
00:02:08,239 --> 00:02:11,039
single

65
00:02:09,039 --> 00:02:12,640
single byte in order to get back to what

66
00:02:11,039 --> 00:02:14,239
the original instruction would be

67
00:02:12,640 --> 00:02:16,160
in order to overwrite to the original

68
00:02:14,239 --> 00:02:17,520
instruction now in this case with the

69
00:02:16,160 --> 00:02:20,000
hardware breakpoint

70
00:02:17,520 --> 00:02:22,480
because it's actually breaking before

71
00:02:20,000 --> 00:02:24,239
the instruction occurs you know

72
00:02:22,480 --> 00:02:25,599
a debugger that would want to you know

73
00:02:24,239 --> 00:02:27,280
step over the instruction or something

74
00:02:25,599 --> 00:02:28,480
like that it would have to go figure out

75
00:02:27,280 --> 00:02:30,480
you know we'd have to essentially

76
00:02:28,480 --> 00:02:33,360
disassemble the assembly instruction

77
00:02:30,480 --> 00:02:34,720
so that is sort of undesirable so

78
00:02:33,360 --> 00:02:36,560
practically speaking

79
00:02:34,720 --> 00:02:39,360
what's going to occur is that the

80
00:02:36,560 --> 00:02:42,800
debugger is going to use the resume flag

81
00:02:39,360 --> 00:02:44,800
so in order to resume execution without

82
00:02:42,800 --> 00:02:46,400
re-firing the hardware breakpoint

83
00:02:44,800 --> 00:02:47,920
just continuously hitting the same

84
00:02:46,400 --> 00:02:50,800
assembly instruction

85
00:02:47,920 --> 00:02:52,720
it will use resume flag so resume flag

86
00:02:50,800 --> 00:02:53,360
was another one of these special system

87
00:02:52,720 --> 00:02:57,200
flags

88
00:02:53,360 --> 00:03:00,959
in the r e r flags or e flags register

89
00:02:57,200 --> 00:03:03,360
bit 16. so the behavior of the resume

90
00:03:00,959 --> 00:03:05,280
flag is that the processor ignores

91
00:03:03,360 --> 00:03:06,959
instruction breakpoints for the duration

92
00:03:05,280 --> 00:03:08,879
of the next instruction

93
00:03:06,959 --> 00:03:10,800
the processor then automatically clears

94
00:03:08,879 --> 00:03:12,879
this flag after the instruction

95
00:03:10,800 --> 00:03:14,959
returned to has become has been

96
00:03:12,879 --> 00:03:16,800
successfully executed

97
00:03:14,959 --> 00:03:18,159
so if you break on execute with a

98
00:03:16,800 --> 00:03:20,879
hardware breakpoint

99
00:03:18,159 --> 00:03:22,560
and then the rip is pointing at the

100
00:03:20,879 --> 00:03:23,599
assembly instruction that's going to

101
00:03:22,560 --> 00:03:27,200
execute but which

102
00:03:23,599 --> 00:03:29,760
has not yet executed the debugger

103
00:03:27,200 --> 00:03:31,360
must set the resume flag and then it can

104
00:03:29,760 --> 00:03:33,120
resume and then the resume flag will

105
00:03:31,360 --> 00:03:35,360
make it so that it doesn't just re-break

106
00:03:33,120 --> 00:03:37,920
on the exact same assembly instruction

107
00:03:35,360 --> 00:03:38,720
in order to actually set the resume flag

108
00:03:37,920 --> 00:03:41,360
the

109
00:03:38,720 --> 00:03:43,840
debugger has to manipulate the stored r

110
00:03:41,360 --> 00:03:44,879
flags that's on the stack as a result of

111
00:03:43,840 --> 00:03:48,000
the interrupt

112
00:03:44,879 --> 00:03:48,879
and then the iret q the return interrupt

113
00:03:48,000 --> 00:03:51,200
return

114
00:03:48,879 --> 00:03:51,920
is going to actually set the r flags

115
00:03:51,200 --> 00:03:53,920
because

116
00:03:51,920 --> 00:03:55,680
we talked at the very very beginning if

117
00:03:53,920 --> 00:03:57,920
you recall way back for cpu id

118
00:03:55,680 --> 00:03:58,879
we said cpu id needs to be able to set

119
00:03:57,920 --> 00:04:01,200
and clear the

120
00:03:58,879 --> 00:04:03,439
or the process software needs to be able

121
00:04:01,200 --> 00:04:05,280
to set and clear the id flag in the

122
00:04:03,439 --> 00:04:07,040
our flags register in order to confirm

123
00:04:05,280 --> 00:04:11,519
that cpu id is actually usable

124
00:04:07,040 --> 00:04:13,200
we talked about push f q and pop fq

125
00:04:11,519 --> 00:04:15,599
and those were the quad word forms

126
00:04:13,200 --> 00:04:18,560
pushing and popping the r flags register

127
00:04:15,599 --> 00:04:19,359
but it turns out that actually the rf

128
00:04:18,560 --> 00:04:22,800
flag

129
00:04:19,359 --> 00:04:24,160
is not valid to be set by the pop fq it

130
00:04:22,800 --> 00:04:26,960
can only be set by this

131
00:04:24,160 --> 00:04:27,440
iret queue so if you look in the manual

132
00:04:26,960 --> 00:04:30,240
for

133
00:04:27,440 --> 00:04:32,160
pop f you'll see this table which

134
00:04:30,240 --> 00:04:35,199
essentially says

135
00:04:32,160 --> 00:04:36,800
the resume flag is never ever set on any

136
00:04:35,199 --> 00:04:38,639
sort of conditions here

137
00:04:36,800 --> 00:04:40,479
having to do with the pop f these other

138
00:04:38,639 --> 00:04:43,759
ones yes they can potentially be set

139
00:04:40,479 --> 00:04:43,759
but resume flag is not

