1
00:00:00,080 --> 00:00:03,840
so let's do a simple little lab here I

2
00:00:02,879 --> 00:00:06,399
want you to

3
00:00:03,840 --> 00:00:08,240
go ahead and set a break-on-execute and

4
00:00:06,399 --> 00:00:09,760
a break-on-read write hardware

5
00:00:08,240 --> 00:00:11,519
breakpoint with WinDbg

6
00:00:09,760 --> 00:00:13,759
you can just go ahead and pick your own

7
00:00:11,519 --> 00:00:14,400
locations and I'll pick my own and show

8
00:00:13,759 --> 00:00:17,199
you the video

9
00:00:14,400 --> 00:00:18,640
video after the fact go ahead and list

10
00:00:17,199 --> 00:00:19,439
your break points after you've set them

11
00:00:18,640 --> 00:00:21,920
with ba

12
00:00:19,439 --> 00:00:23,199
just to confirm they're set now while

13
00:00:21,920 --> 00:00:24,240
you're sitting there in the debugger you

14
00:00:23,199 --> 00:00:26,480
will not see

15
00:00:24,240 --> 00:00:27,439
the debug registers actually update

16
00:00:26,480 --> 00:00:29,279
immediately

17
00:00:27,439 --> 00:00:30,960
so you need to run the debugger and then

18
00:00:29,279 --> 00:00:33,120
immediately stop it

19
00:00:30,960 --> 00:00:34,880
and then at that point you will see the

20
00:00:33,120 --> 00:00:36,719
debug register is updated

21
00:00:34,880 --> 00:00:38,160
then I want you to go back and look at

22
00:00:36,719 --> 00:00:41,600
debug register 7

23
00:00:38,160 --> 00:00:44,000
and 8 sorry 6 and 7 and

24
00:00:41,600 --> 00:00:45,440
also your debug registers you know 0,

25
00:00:44,000 --> 00:00:46,879
1, 2 and 3

26
00:00:45,440 --> 00:00:48,640
you don't know exactly which ones will

27
00:00:46,879 --> 00:00:49,440
be filled in it kind of depends on you

28
00:00:48,640 --> 00:00:51,920
know whether or not

29
00:00:49,440 --> 00:00:53,120
there were other debug breakpoints set

30
00:00:51,920 --> 00:00:54,800
but realistically

31
00:00:53,120 --> 00:00:56,559
you probably are not going to have any

32
00:00:54,800 --> 00:00:58,000
other one set so it'll probably be debug

33
00:00:56,559 --> 00:01:00,399
register 0 and 1

34
00:00:58,000 --> 00:01:01,280
which get these two breakpoints so go

35
00:01:00,399 --> 00:01:03,760
ahead and do that

36
00:01:01,280 --> 00:01:07,760
and go interpret the bits according to

37
00:01:03,760 --> 00:01:07,760
these register fields

