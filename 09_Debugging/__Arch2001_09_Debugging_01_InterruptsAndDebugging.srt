1
00:00:00,240 --> 00:00:03,679
now I'd like to talk about the

2
00:00:01,599 --> 00:00:04,880
relationship between interrupts which we

3
00:00:03,679 --> 00:00:07,200
learned about a while ago

4
00:00:04,880 --> 00:00:08,240
and debugging we'll start with the

5
00:00:07,200 --> 00:00:10,639
simplest form

6
00:00:08,240 --> 00:00:11,840
which is the interrupt 3 the breakpoint

7
00:00:10,639 --> 00:00:14,719
exception

8
00:00:11,840 --> 00:00:16,720
this also has its own separate interrupt

9
00:00:14,719 --> 00:00:19,920
assembly instruction INT3

10
00:00:16,720 --> 00:00:23,279
which has a 1 byte opcode form

11
00:00:19,920 --> 00:00:25,119
0xCC whereas the normal interrupt is 0xCD

12
00:00:23,279 --> 00:00:26,320
followed by some number for the

13
00:00:25,119 --> 00:00:29,039
interrupt number

14
00:00:26,320 --> 00:00:30,880
so INT3 is actually what debuggers are

15
00:00:29,039 --> 00:00:32,880
using when they say they're setting a

16
00:00:30,880 --> 00:00:35,280
software breakpoint which is generally

17
00:00:32,880 --> 00:00:37,920
the default form of breakpoints

18
00:00:35,280 --> 00:00:39,040
so here we go the breakpoint exception

19
00:00:37,920 --> 00:00:41,840
or interrupt

20
00:00:39,040 --> 00:00:43,440
it is of type trap and you can see here

21
00:00:41,840 --> 00:00:46,480
it has the INT3

22
00:00:43,440 --> 00:00:48,399
not INT space 3. so the first thing

23
00:00:46,480 --> 00:00:50,320
that happens when some debugger

24
00:00:48,399 --> 00:00:52,000
sets a software breakpoint like visual

25
00:00:50,320 --> 00:00:53,039
studio if you're just clicking off to

26
00:00:52,000 --> 00:00:54,800
the side

27
00:00:53,039 --> 00:00:56,399
is that it's going to overwrite the

28
00:00:54,800 --> 00:00:58,399
first byte of the

29
00:00:56,399 --> 00:00:59,520
assembly instruction that corresponds to

30
00:00:58,399 --> 00:01:00,719
the line where you're trying to set the

31
00:00:59,520 --> 00:01:02,000
breakpoint or the address where you're

32
00:01:00,719 --> 00:01:03,600
trying to set the breakpoint

33
00:01:02,000 --> 00:01:05,040
it's going to overwrite the first byte

34
00:01:03,600 --> 00:01:06,960
with 0xCC

35
00:01:05,040 --> 00:01:09,360
so that when the processor would

36
00:01:06,960 --> 00:01:10,320
normally execute whatever assembly

37
00:01:09,360 --> 00:01:13,200
instruction is there

38
00:01:10,320 --> 00:01:14,880
instead it will execute the INT3

39
00:01:13,200 --> 00:01:16,720
assembly instruction

40
00:01:14,880 --> 00:01:18,560
it's going to have to update some sort

41
00:01:16,720 --> 00:01:20,640
of metadata list that the debugger

42
00:01:18,560 --> 00:01:23,280
itself keeps track of to figure out

43
00:01:20,640 --> 00:01:24,640
what bytes it's actually overwritten in

44
00:01:23,280 --> 00:01:27,040
order to know later on

45
00:01:24,640 --> 00:01:29,040
what byte it needs to put back in order

46
00:01:27,040 --> 00:01:30,560
to execute the original assembly

47
00:01:29,040 --> 00:01:34,000
instruction

48
00:01:30,560 --> 00:01:35,840
now once that INT3 is actually hit

49
00:01:34,000 --> 00:01:37,680
the debugger is going to look up the

50
00:01:35,840 --> 00:01:37,920
location it's going to see okay there's

51
00:01:37,680 --> 00:01:40,240
a

52
00:01:37,920 --> 00:01:42,720
INT3 at this specific address and then

53
00:01:40,240 --> 00:01:44,560
it has to basically replace that byte

54
00:01:42,720 --> 00:01:46,320
execute the assembly instruction and

55
00:01:44,560 --> 00:01:49,439
then assuming that the

56
00:01:46,320 --> 00:01:51,280
person hasn't actually toggled off the

57
00:01:49,439 --> 00:01:52,720
breakpoint assuming they didn't like

58
00:01:51,280 --> 00:01:54,399
undo the breakpoint

59
00:01:52,720 --> 00:01:55,920
yet they're gonna you know write to the

60
00:01:54,399 --> 00:01:57,600
breakpoint back there

61
00:01:55,920 --> 00:01:59,520
and then they're just going to proceed

62
00:01:57,600 --> 00:02:01,520
and let the program run as normal

63
00:01:59,520 --> 00:02:02,719
so we're going to do a quick lab that's

64
00:02:01,520 --> 00:02:05,200
going to actually

65
00:02:02,719 --> 00:02:07,280
try to show us and expose the duplicity

66
00:02:05,200 --> 00:02:09,840
of debuggers like visual studio

67
00:02:07,280 --> 00:02:10,720
in that this particular application

68
00:02:09,840 --> 00:02:13,680
ProofPudding

69
00:02:10,720 --> 00:02:14,959
reads its own assembly bytes and prints

70
00:02:13,680 --> 00:02:17,440
them out to screen

71
00:02:14,959 --> 00:02:19,200
so we will see specifically the debugger

72
00:02:17,440 --> 00:02:19,440
trying to hide from us the fact that it

73
00:02:19,200 --> 00:02:23,120
is

74
00:02:19,440 --> 00:02:25,680
inserted a 0xCC so let's go ahead and take

75
00:02:23,120 --> 00:02:25,680
a look at this

76
00:02:25,840 --> 00:02:30,239
ProofPudding is just a simple thing

77
00:02:27,840 --> 00:02:33,040
that calls an assembly function

78
00:02:30,239 --> 00:02:35,040
and what that assembly function does is

79
00:02:33,040 --> 00:02:37,840
it takes the address of some label

80
00:02:35,040 --> 00:02:40,000
bla so it takes the address of bla and

81
00:02:37,840 --> 00:02:41,440
it moves it to rax so basically it's

82
00:02:40,000 --> 00:02:44,160
just returning

83
00:02:41,440 --> 00:02:46,000
the address of bla which in so doing is

84
00:02:44,160 --> 00:02:46,400
essentially returning the address of

85
00:02:46,000 --> 00:02:49,680
this

86
00:02:46,400 --> 00:02:52,959
nop instruction then up in the C

87
00:02:49,680 --> 00:02:56,080
code it's going to say that the

88
00:02:52,959 --> 00:02:57,519
data pointed to by this address of blah

89
00:02:56,080 --> 00:02:58,959
that's coming back from within this

90
00:02:57,519 --> 00:03:00,480
assembly function

91
00:02:58,959 --> 00:03:02,000
is the following data and it actually

92
00:03:00,480 --> 00:03:04,319
dereferences it

93
00:03:02,000 --> 00:03:05,440
as a byte pointer so it's essentially

94
00:03:04,319 --> 00:03:07,360
going to

95
00:03:05,440 --> 00:03:09,200
treat the address of the nop as a byte

96
00:03:07,360 --> 00:03:11,040
pointer it's going to dereference it and

97
00:03:09,200 --> 00:03:11,840
so it's going to give us 1 byte at

98
00:03:11,040 --> 00:03:14,400
that address

99
00:03:11,840 --> 00:03:16,080
which should be 0x90 because that's

100
00:03:14,400 --> 00:03:17,440
the value of a nop assembly

101
00:03:16,080 --> 00:03:19,360
instruction

102
00:03:17,440 --> 00:03:21,040
so let's go ahead and set a breakpoint

103
00:03:19,360 --> 00:03:22,480
before we actually walk into the code

104
00:03:21,040 --> 00:03:23,760
and let's set a breakpoint at the end

105
00:03:22,480 --> 00:03:25,280
after the prints

106
00:03:23,760 --> 00:03:28,239
make sure you set it as a startup

107
00:03:25,280 --> 00:03:30,560
project make sure it's set to debugging

108
00:03:28,239 --> 00:03:31,440
and then we go we're going to step into

109
00:03:30,560 --> 00:03:32,799
it

110
00:03:31,440 --> 00:03:34,480
you can see it's going to get the

111
00:03:32,799 --> 00:03:36,239
address of bla

112
00:03:34,480 --> 00:03:37,760
and then the address of bla should just

113
00:03:36,239 --> 00:03:41,120
be this nop

114
00:03:37,760 --> 00:03:42,080
and so if we just continue on so we see

115
00:03:41,120 --> 00:03:44,799
printed out

116
00:03:42,080 --> 00:03:45,200
that at this address the address of bla

117
00:03:44,799 --> 00:03:48,560


118
00:03:45,200 --> 00:03:50,959
1A1A the byte is 0x90. so that's

119
00:03:48,560 --> 00:03:54,879
exactly what we would expect to see

120
00:03:50,959 --> 00:03:57,120
because that is a 0x90 at that address

121
00:03:54,879 --> 00:03:58,799
but now let's set a breakpoint right

122
00:03:57,120 --> 00:04:00,159
here let's set a breakpoint here and

123
00:03:58,799 --> 00:04:01,120
what I'm telling you about software

124
00:04:00,159 --> 00:04:02,640
breakpoints

125
00:04:01,120 --> 00:04:04,720
is that the debugger is going to

126
00:04:02,640 --> 00:04:07,519
actually overwrite that 0x90

127
00:04:04,720 --> 00:04:08,400
with a 0xCC and that's what's going to

128
00:04:07,519 --> 00:04:10,159
cause the

129
00:04:08,400 --> 00:04:12,000
breakpoint exception of fire and the

130
00:04:10,159 --> 00:04:14,080
debugger to be notified

131
00:04:12,000 --> 00:04:15,120
and ultimately it will you know stop in

132
00:04:14,080 --> 00:04:17,919
the debugger

133
00:04:15,120 --> 00:04:19,199
so let's go ahead and run the debugger

134
00:04:17,919 --> 00:04:22,240
continue on

135
00:04:19,199 --> 00:04:22,720
okay now it has hit this breakpoint and

136
00:04:22,240 --> 00:04:24,639
so

137
00:04:22,720 --> 00:04:27,360
we want to see you know whether or not

138
00:04:24,639 --> 00:04:28,960
this is 0x90 or whether this is 0xCC

139
00:04:27,360 --> 00:04:30,639
so we're broken right here right now so

140
00:04:28,960 --> 00:04:34,800
we could just put in

141
00:04:30,639 --> 00:04:37,840
rip and that is the address 1A1A again

142
00:04:34,800 --> 00:04:41,600
and you can see that it says 1A1A is

143
00:04:37,840 --> 00:04:43,360
90. and also rax is the value

144
00:04:41,600 --> 00:04:45,120
that's being returned for the address of

145
00:04:43,360 --> 00:04:48,240
bla so we could put in

146
00:04:45,120 --> 00:04:53,360
rax and again it's 1A1A and it says

147
00:04:48,240 --> 00:04:53,360
it's 0x90. but when we continue

148
00:04:53,440 --> 00:04:57,199
what we see is that it prints out at

149
00:04:56,080 --> 00:05:01,360
1a1a

150
00:04:57,199 --> 00:05:03,759
is 0xCC so basically the debugger lied

151
00:05:01,360 --> 00:05:05,680
to us the debugger said there was a

152
00:05:03,759 --> 00:05:07,919
0x90 there in that memory address

153
00:05:05,680 --> 00:05:11,120
but in reality there was a 0xCC from

154
00:05:07,919 --> 00:05:11,120
the debugger itself

