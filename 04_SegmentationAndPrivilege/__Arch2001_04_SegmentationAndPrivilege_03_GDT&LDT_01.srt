1
00:00:00,160 --> 00:00:04,960
so let's learn more about these GDT and

2
00:00:02,639 --> 00:00:06,480
LDT tables that we learned about that we

3
00:00:04,960 --> 00:00:07,520
saw references to in the previous

4
00:00:06,480 --> 00:00:09,760
section

5
00:00:07,520 --> 00:00:11,599
so the segment selector is what we saw

6
00:00:09,760 --> 00:00:13,440
and we saw there was a table indicator

7
00:00:11,599 --> 00:00:15,519
bit in there where if it was 0 it was

8
00:00:13,440 --> 00:00:17,279
pointing at the GDT and if it was 1 it

9
00:00:15,519 --> 00:00:19,600
was pointing at the LDT

10
00:00:17,279 --> 00:00:21,439
so we're going to dig more into that so

11
00:00:19,600 --> 00:00:23,279
while this table indicator says which of

12
00:00:21,439 --> 00:00:25,840
these tables it's pointing at

13
00:00:23,279 --> 00:00:28,400
how we find the actual tables themselves

14
00:00:25,840 --> 00:00:31,039
are with these special purpose registers

15
00:00:28,400 --> 00:00:33,120
there's a GDT register which points at a

16
00:00:31,039 --> 00:00:34,480
memory location that says here's the

17
00:00:33,120 --> 00:00:36,960
memory for the GDT

18
00:00:34,480 --> 00:00:38,719
and there's an LDT register that points

19
00:00:36,960 --> 00:00:40,239
at a memory location that says here's

20
00:00:38,719 --> 00:00:42,239
the LDT

21
00:00:40,239 --> 00:00:43,840
furthermore this LDT register even

22
00:00:42,239 --> 00:00:44,879
though it was shown as if it had all

23
00:00:43,840 --> 00:00:47,200
these fields

24
00:00:44,879 --> 00:00:48,320
in practice it sort of behaves like a

25
00:00:47,200 --> 00:00:50,960
segment register

26
00:00:48,320 --> 00:00:52,800
and that there is only a 16-bit area

27
00:00:50,960 --> 00:00:55,039
that's actually visible to us

28
00:00:52,800 --> 00:00:57,360
and that can be set and then there is a

29
00:00:55,039 --> 00:00:58,000
hidden portion that acts as a cache of

30
00:00:57,360 --> 00:01:00,000
information

31
00:00:58,000 --> 00:01:02,160
coming from the other table and then the

32
00:01:00,000 --> 00:01:03,440
entries in these tables are called

33
00:01:02,160 --> 00:01:05,199
segment descriptors

34
00:01:03,440 --> 00:01:06,880
but we're not going to learn about them

35
00:01:05,199 --> 00:01:08,960
until the next section they're basically

36
00:01:06,880 --> 00:01:11,760
data structures describing the segments

37
00:01:08,960 --> 00:01:13,439
that are in these tables

38
00:01:11,760 --> 00:01:14,880
let's look at that first register the

39
00:01:13,439 --> 00:01:17,840
global descriptor table

40
00:01:14,880 --> 00:01:18,240
register so this register is 10 bytes

41
00:01:17,840 --> 00:01:21,280
long

42
00:01:18,240 --> 00:01:22,799
it has 2 bytes for the table limit

43
00:01:21,280 --> 00:01:26,159
that says the size

44
00:01:22,799 --> 00:01:27,360
and 8 bytes for a 64-bit linear base

45
00:01:26,159 --> 00:01:29,840
address

46
00:01:27,360 --> 00:01:31,439
so that is a linear address and we said

47
00:01:29,840 --> 00:01:33,600
for now we're pretending paging doesn't

48
00:01:31,439 --> 00:01:35,600
exist so that effectively is a physical

49
00:01:33,600 --> 00:01:37,680
address as far as we're concerned

50
00:01:35,600 --> 00:01:39,439
so 64-bit address of the start of the

51
00:01:37,680 --> 00:01:42,560
table and a 2-byte

52
00:01:39,439 --> 00:01:45,200
address size of the end of the table

53
00:01:42,560 --> 00:01:46,399
now this table limit field is actually a

54
00:01:45,200 --> 00:01:49,119
size in bytes

55
00:01:46,399 --> 00:01:49,840
and it is a size that specifies the last

56
00:01:49,119 --> 00:01:52,799
valid

57
00:01:49,840 --> 00:01:55,520
or included byte in the table so if the

58
00:01:52,799 --> 00:01:57,680
overall table was 0x1000 big

59
00:01:55,520 --> 00:01:59,360
this wouldn't say 0x1000 it would say

60
00:01:57,680 --> 00:02:02,399
0xfff

61
00:01:59,360 --> 00:02:05,520
to basically say the last byte is

62
00:02:02,399 --> 00:02:07,360
this base address plus fff

63
00:02:05,520 --> 00:02:08,560
that's the last byte that you can index

64
00:02:07,360 --> 00:02:11,599
into

65
00:02:08,560 --> 00:02:14,640
now setting this register is done

66
00:02:11,599 --> 00:02:17,520
via a special instruction lgdt

67
00:02:14,640 --> 00:02:19,360
load the GDT register and you can see

68
00:02:17,520 --> 00:02:21,360
this is a privileged instruction which

69
00:02:19,360 --> 00:02:24,080
can only be done by the kernel

70
00:02:21,360 --> 00:02:25,120
and reading or storing out the contents

71
00:02:24,080 --> 00:02:28,560
of that register

72
00:02:25,120 --> 00:02:31,440
can be done via the sgdt store GDT

73
00:02:28,560 --> 00:02:33,920
to memory instruction and that actually

74
00:02:31,440 --> 00:02:35,840
is not a privileged instruction

75
00:02:33,920 --> 00:02:37,200
now there's really no good reason why

76
00:02:35,840 --> 00:02:38,959
someone other than the kernel would

77
00:02:37,200 --> 00:02:40,239
need to actually read out the contents

78
00:02:38,959 --> 00:02:42,160
of this register

79
00:02:40,239 --> 00:02:43,280
but there's an interesting dichotomy

80
00:02:42,160 --> 00:02:45,519
that we'll

81
00:02:43,280 --> 00:02:47,440
see exploited for interesting purposes

82
00:02:45,519 --> 00:02:49,040
later on given the fact that this

83
00:02:47,440 --> 00:02:50,480
you know one of these instructions for

84
00:02:49,040 --> 00:02:54,800
writing is privileged but one of these

85
00:02:50,480 --> 00:02:54,800
instructions for reading is not

