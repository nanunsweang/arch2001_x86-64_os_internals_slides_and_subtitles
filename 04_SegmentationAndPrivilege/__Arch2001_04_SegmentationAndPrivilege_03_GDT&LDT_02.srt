1
00:00:00,960 --> 00:00:04,640
so let's go ahead and just go into the

2
00:00:02,960 --> 00:00:08,000
debugger and look at

3
00:00:04,640 --> 00:00:09,519
the GDT register now it turns out that

4
00:00:08,000 --> 00:00:12,160
there's a WinDbg-ism

5
00:00:09,519 --> 00:00:13,519
in that although the register itself is

6
00:00:12,160 --> 00:00:15,599
10 bytes big

7
00:00:13,519 --> 00:00:16,800
uh WinDbg actually sort of breaks up

8
00:00:15,599 --> 00:00:19,840
your view into it

9
00:00:16,800 --> 00:00:22,880
to what they call the gdtr is

10
00:00:19,840 --> 00:00:23,519
actually only the upper 64-bit base

11
00:00:22,880 --> 00:00:25,039
address

12
00:00:23,519 --> 00:00:26,960
and what they call this sort of

13
00:00:25,039 --> 00:00:29,679
pseudo-register gdtl

14
00:00:26,960 --> 00:00:30,960
is the lower limit address so let's go

15
00:00:29,679 --> 00:00:33,200
ahead and look at that let's figure out

16
00:00:30,960 --> 00:00:36,000
what the base address is of the GDT

17
00:00:33,200 --> 00:00:38,239
and what the size of the GDT is based on

18
00:00:36,000 --> 00:00:41,840
this register

19
00:00:38,239 --> 00:00:44,399
so I'm going to go into my debugger

20
00:00:41,840 --> 00:00:45,200
and I break into the kernel loop gotta

21
00:00:44,399 --> 00:00:48,960
wake up my

22
00:00:45,200 --> 00:00:51,440
vm as well so wake up my vm

23
00:00:48,960 --> 00:00:52,879
break into the debugger and if you

24
00:00:51,440 --> 00:00:53,680
scroll all the way down in your

25
00:00:52,879 --> 00:00:55,840
registers

26
00:00:53,680 --> 00:00:57,600
you could customize it and put the GDT

27
00:00:55,840 --> 00:00:58,239
at the beginning but if you just go all

28
00:00:57,600 --> 00:01:00,239
the way down

29
00:00:58,239 --> 00:01:01,600
you'll see that the GDT register is

30
00:01:00,239 --> 00:01:04,000
actually at the bottom

31
00:01:01,600 --> 00:01:05,119
so if we scroll all the way down we see

32
00:01:04,000 --> 00:01:07,760
gdtr

33
00:01:05,119 --> 00:01:08,479
is some address that looks like a kernel

34
00:01:07,760 --> 00:01:10,080
address

35
00:01:08,479 --> 00:01:11,360
we haven't covered canonical addresses

36
00:01:10,080 --> 00:01:12,000
yet but it looks like kernel address to

37
00:01:11,360 --> 00:01:14,080
me and

38
00:01:12,000 --> 00:01:15,040
you'll see later why you can kind of

39
00:01:14,080 --> 00:01:17,040
immediately

40
00:01:15,040 --> 00:01:19,280
determine that that's a kernel address

41
00:01:17,040 --> 00:01:20,560
so 64-bit address that is the base

42
00:01:19,280 --> 00:01:23,280
address that is where

43
00:01:20,560 --> 00:01:24,799
in memory we can find the GDT table and

44
00:01:23,280 --> 00:01:28,320
if we wanted we could

45
00:01:24,799 --> 00:01:30,000
do a dd gdtr and we would just be

46
00:01:28,320 --> 00:01:31,759
looking at some bytes in this table we

47
00:01:30,000 --> 00:01:34,560
don't know how to interpret them yet but

48
00:01:31,759 --> 00:01:35,840
there are some data structures in there

49
00:01:34,560 --> 00:01:39,200
and the gdtl

50
00:01:35,840 --> 00:01:42,399
is 57. so I said that the the limit

51
00:01:39,200 --> 00:01:44,799
is the size in bytes of the lat and it

52
00:01:42,399 --> 00:01:47,439
includes the last byte of the thing so

53
00:01:44,799 --> 00:01:48,159
if it's 57 that means it's a total of 58

54
00:01:47,439 --> 00:01:50,960
bytes just

55
00:01:48,159 --> 00:01:52,320
indexes 0 through 57. so whatever this

56
00:01:50,960 --> 00:01:54,560
data structure is

57
00:01:52,320 --> 00:01:56,479
it should be considered to be done after

58
00:01:54,560 --> 00:02:01,280
58 bytes

59
00:01:56,479 --> 00:02:01,280
alright so that is the GDT register

