1
00:00:00,640 --> 00:00:04,560
So we've got two labs for that we've got

2
00:00:03,520 --> 00:00:06,879
the user space

3
00:00:04,560 --> 00:00:08,559
segment registers which is just going to

4
00:00:06,879 --> 00:00:10,320
pull the values out in user space it

5
00:00:08,559 --> 00:00:12,160
just uses a mov instruction

6
00:00:10,320 --> 00:00:14,000
it's going to go ahead and put them into

7
00:00:12,160 --> 00:00:15,360
memory to be read

8
00:00:14,000 --> 00:00:18,240
and then we've got a kernel version of

9
00:00:15,360 --> 00:00:21,760
the same so let's go ahead and

10
00:00:18,240 --> 00:00:21,760
do those two labs right now.

11
00:00:22,080 --> 00:00:27,680
All right so in your code you have user

12
00:00:25,199 --> 00:00:30,240
segment registers you can see there's a

13
00:00:27,680 --> 00:00:33,440
variety of functions defined read cs

14
00:00:30,240 --> 00:00:33,680
ds etc these are passing in a pointer to

15
00:00:33,440 --> 00:00:35,840
a

16
00:00:33,680 --> 00:00:36,719
short that is going to be the location

17
00:00:35,840 --> 00:00:39,840
where the

18
00:00:36,719 --> 00:00:40,559
value of these registers is written back

19
00:00:39,840 --> 00:00:42,480
to

20
00:00:40,559 --> 00:00:43,760
if we look at the assembly it's just you

21
00:00:42,480 --> 00:00:46,320
know super trivial

22
00:00:43,760 --> 00:00:48,079
information saying you know take the cs

23
00:00:46,320 --> 00:00:50,719
register and move it to

24
00:00:48,079 --> 00:00:52,320
you know the first argument rcx whatever

25
00:00:50,719 --> 00:00:52,960
that points to in memory just move it

26
00:00:52,320 --> 00:00:55,440
there

27
00:00:52,960 --> 00:00:57,840
and so that's going to be pointing to

28
00:00:55,440 --> 00:00:59,600
the the parameters that are passed so

29
00:00:57,840 --> 00:01:00,480
just pass an address of some local

30
00:00:59,600 --> 00:01:03,120
variable

31
00:01:00,480 --> 00:01:04,960
and then cs gets put into there same

32
00:01:03,120 --> 00:01:07,280
thing for ss and so forth

33
00:01:04,960 --> 00:01:08,159
so what you need to do is set a

34
00:01:07,280 --> 00:01:10,240
breakpoint

35
00:01:08,159 --> 00:01:11,200
at the end here make sure that this is

36
00:01:10,240 --> 00:01:13,200
set for the

37
00:01:11,200 --> 00:01:16,000
startup project and go ahead and start

38
00:01:13,200 --> 00:01:16,000
it in your debugger

39
00:01:17,280 --> 00:01:20,560
and once we do that we can see it just

40
00:01:19,840 --> 00:01:24,640
prints out

41
00:01:20,560 --> 00:01:26,400
cs is 0x33, ss is 0x2b

42
00:01:24,640 --> 00:01:28,799
so for each of these we could parse them

43
00:01:26,400 --> 00:01:30,640
then there are 16 these values are 16-

44
00:01:28,799 --> 00:01:33,119
bit you know data structures

45
00:01:30,640 --> 00:01:35,520
so what are the values well RPL the

46
00:01:33,119 --> 00:01:37,040
least significant 2 bits is 3 here

47
00:01:35,520 --> 00:01:38,720
3, 3, 3, 3 looks like it's

48
00:01:37,040 --> 00:01:40,640
3's all the way down

49
00:01:38,720 --> 00:01:43,200
table indicator is 0's all the way

50
00:01:40,640 --> 00:01:45,600
down so everybody is pointing at the GDT

51
00:01:43,200 --> 00:01:46,320
and the indices seem to be different so

52
00:01:45,600 --> 00:01:49,759
6, 5,

53
00:01:46,320 --> 00:01:51,520
5, 5, 0xa, 5. All right great we're

54
00:01:49,759 --> 00:01:53,680
just going to throw that into our slides

55
00:01:51,520 --> 00:01:56,159
and you know take a look at it later.

56
00:01:53,680 --> 00:01:57,840
Now let's see what things look like from

57
00:01:56,159 --> 00:02:00,320
kernel space

58
00:01:57,840 --> 00:02:03,040
so to do that you would right click on

59
00:02:00,320 --> 00:02:06,320
your K segment registers

60
00:02:03,040 --> 00:02:08,959
build it wait till you find the

61
00:02:06,320 --> 00:02:10,720
output path of where it's storing your

62
00:02:08,959 --> 00:02:14,239
kernel driver

63
00:02:10,720 --> 00:02:14,239
copy the output path

64
00:02:15,200 --> 00:02:23,360
go ahead and put that in to explore

65
00:02:19,520 --> 00:02:24,400
copy the kernel driver and move over to

66
00:02:23,360 --> 00:02:27,599
your

67
00:02:24,400 --> 00:02:31,360
debuggable system paste

68
00:02:27,599 --> 00:02:32,480
your kernel driver stuff in and go ahead

69
00:02:31,360 --> 00:02:36,400
and run the commands

70
00:02:32,480 --> 00:02:39,920
to install it so I go ahead and do that

71
00:02:36,400 --> 00:02:42,480
it says install this driver anyways

72
00:02:39,920 --> 00:02:43,280
it takes a minute to actually run the

73
00:02:42,480 --> 00:02:44,879
driver

74
00:02:43,280 --> 00:02:47,440
and then we'll go back and look at it in

75
00:02:44,879 --> 00:02:50,160
the kernel debugger to see what it says

76
00:02:47,440 --> 00:02:51,680
all right here's what it says same kind

77
00:02:50,160 --> 00:02:52,720
of code so it just says all right

78
00:02:51,680 --> 00:02:56,000
segment register

79
00:02:52,720 --> 00:02:59,440
is 10 18 2b etc

80
00:02:56,000 --> 00:03:01,920
RPL is 0, 0, 3, 3, 3, 3

81
00:02:59,440 --> 00:03:02,879
GDT all the way down and different

82
00:03:01,920 --> 00:03:06,159
indices 2,

83
00:03:02,879 --> 00:03:07,200
3, 5, 5, 0xa, 5 again I'm just going

84
00:03:06,159 --> 00:03:10,159
to go through that bit

85
00:03:07,200 --> 00:03:12,080
into a table in the slides but you

86
00:03:10,159 --> 00:03:12,480
should probably pause now and go ahead

87
00:03:12,080 --> 00:03:14,480
and

88
00:03:12,480 --> 00:03:19,840
do this yourself and see that you can

89
00:03:14,480 --> 00:03:19,840
get the same output as me

90
00:03:22,239 --> 00:03:28,560
okay so what did I get in my tables I

91
00:03:25,519 --> 00:03:30,239
have for user space segments I have RPL 

92
00:03:28,560 --> 00:03:32,319
of 3 all the way down the line like

93
00:03:30,239 --> 00:03:35,040
I said GDT all the way along the line

94
00:03:32,319 --> 00:03:36,879
but over here in kernel I have RPL  of

95
00:03:35,040 --> 00:03:40,000
0 in two places

96
00:03:36,879 --> 00:03:42,480
and 3 is everywhere else so

97
00:03:40,000 --> 00:03:44,239
we have things not the same right here

98
00:03:42,480 --> 00:03:46,560
so for the cs and ss

99
00:03:44,239 --> 00:03:47,680
we seem to have different values both

100
00:03:46,560 --> 00:03:49,680
for the indices

101
00:03:47,680 --> 00:03:51,920
this is 2 and 3 this is 6 and

102
00:03:49,680 --> 00:03:55,519
5 and for the RPL's

103
00:03:51,920 --> 00:03:57,519
so 3's and 0's so that seems to be

104
00:03:55,519 --> 00:04:00,080
correlated with the fact that this is in

105
00:03:57,519 --> 00:04:02,080
kernel and this is in user space

106
00:04:00,080 --> 00:04:03,760
and then for the rest of this everything

107
00:04:02,080 --> 00:04:06,080
seems to be the same in user space

108
00:04:03,760 --> 00:04:08,640
versus kernel space

109
00:04:06,080 --> 00:04:10,400
you know it's basically all 3's so

110
00:04:08,640 --> 00:04:11,439
it's all as if it was you know user

111
00:04:10,400 --> 00:04:12,959
spacey stuff

112
00:04:11,439 --> 00:04:14,560
and there could be a variety of reasons

113
00:04:12,959 --> 00:04:16,239
for that which you know

114
00:04:14,560 --> 00:04:19,120
we could speculate on or we can just

115
00:04:16,239 --> 00:04:22,479
wait till we explain it

116
00:04:19,120 --> 00:04:24,960
all right so some inferences from this

117
00:04:22,479 --> 00:04:26,479
is that Windows maintains you know

118
00:04:24,960 --> 00:04:28,479
different cs and ss

119
00:04:26,479 --> 00:04:29,520
segment selectors for user space and

120
00:04:28,479 --> 00:04:31,280
kernel space

121
00:04:29,520 --> 00:04:33,360
right so you had different indices

122
00:04:31,280 --> 00:04:36,320
between user space and kernel space

123
00:04:33,360 --> 00:04:36,880
2 and 3 versus 6 and 5 and so

124
00:04:36,320 --> 00:04:40,240
that's one

125
00:04:36,880 --> 00:04:43,680
clear difference. Windows x86-64

126
00:04:40,240 --> 00:04:45,520
doesn't seem to change the ds, es, fs, or gs

127
00:04:43,680 --> 00:04:47,440
segment selectors when moving between

128
00:04:45,520 --> 00:04:50,800
user space and kernel space

129
00:04:47,440 --> 00:04:51,680
and I uh highlight segment selector here

130
00:04:50,800 --> 00:04:54,240
because that's

131
00:04:51,680 --> 00:04:56,320
not all that potentially could change

132
00:04:54,240 --> 00:04:57,600
the definition of segments

133
00:04:56,320 --> 00:04:59,680
but it doesn't seem to change those

134
00:04:57,600 --> 00:05:01,039
segment selectors between user space and

135
00:04:59,680 --> 00:05:02,880
kernel space

136
00:05:01,039 --> 00:05:04,560
and the most important inference from

137
00:05:02,880 --> 00:05:07,280
that table was that the RPL 

138
00:05:04,560 --> 00:05:08,080
field of the cs and ss segments seems to

139
00:05:07,280 --> 00:05:12,720
correlate with

140
00:05:08,080 --> 00:05:15,440
the ring for kernel and user space

141
00:05:12,720 --> 00:05:17,199
all right so that is our first little

142
00:05:15,440 --> 00:05:19,520
bit of information that we needed

143
00:05:17,199 --> 00:05:20,720
for finding information about how the

144
00:05:19,520 --> 00:05:24,080
privilege rings

145
00:05:20,720 --> 00:05:26,240
work so one last uh

146
00:05:24,080 --> 00:05:27,680
reiteration of segmentation and the

147
00:05:26,240 --> 00:05:30,720
translation of

148
00:05:27,680 --> 00:05:32,720
linear logical to linear addresses

149
00:05:30,720 --> 00:05:34,240
you've got logical addresses which are

150
00:05:32,720 --> 00:05:35,600
16-bit segment selector

151
00:05:34,240 --> 00:05:37,199
which is probably going to be in one of

152
00:05:35,600 --> 00:05:39,039
those you can either be in you know a

153
00:05:37,199 --> 00:05:40,960
far pointer or it can be

154
00:05:39,039 --> 00:05:42,720
implicitly being used by the segment

155
00:05:40,960 --> 00:05:46,240
selectors like cs, ss

156
00:05:42,720 --> 00:05:48,160
etc you've got a offset the typical

157
00:05:46,240 --> 00:05:50,560
64-bit value you expect

158
00:05:48,160 --> 00:05:52,000
you've got some tables the GDT and LDT

159
00:05:50,560 --> 00:05:55,039
which we'll learn more about in the next

160
00:05:52,000 --> 00:05:56,240
section and those have data about the

161
00:05:55,039 --> 00:05:58,240
particular segments

162
00:05:56,240 --> 00:05:59,280
which is used to find a base address and

163
00:05:58,240 --> 00:06:01,199
add to the offset

164
00:05:59,280 --> 00:06:03,680
to ultimately translate through to a

165
00:06:01,199 --> 00:06:07,039
linear address

166
00:06:03,680 --> 00:06:09,199
okay so let's go ahead and update our

167
00:06:07,039 --> 00:06:10,400
map of like all this stuff that I claim

168
00:06:09,199 --> 00:06:11,440
you're gonna learn by the end of the

169
00:06:10,400 --> 00:06:13,759
class

170
00:06:11,440 --> 00:06:14,880
well first thing rflags you already

171
00:06:13,759 --> 00:06:17,919
knew that so

172
00:06:14,880 --> 00:06:19,280
what's new segment selectors segment

173
00:06:17,919 --> 00:06:20,880
selectors is what's new

174
00:06:19,280 --> 00:06:22,400
and we're gonna you know just keep

175
00:06:20,880 --> 00:06:23,840
chipping away at this picture

176
00:06:22,400 --> 00:06:27,120
and we're just gonna keep seeing more

177
00:06:23,840 --> 00:06:27,120
and more stuff from here

