1
00:00:00,160 --> 00:00:03,120
so let's go ahead and look at the LDT

2
00:00:02,480 --> 00:00:06,560
register

3
00:00:03,120 --> 00:00:08,639
and see what it uses as the GDT index we

4
00:00:06,560 --> 00:00:09,440
said LDT register is a 16-bit segment

5
00:00:08,639 --> 00:00:11,120
selector

6
00:00:09,440 --> 00:00:12,719
we covered segment selectors in the

7
00:00:11,120 --> 00:00:13,519
previous section we know they have an

8
00:00:12,719 --> 00:00:16,080
rpl

9
00:00:13,519 --> 00:00:18,880
table indicator and a index so let's see

10
00:00:16,080 --> 00:00:18,880
what that index is

11
00:00:20,720 --> 00:00:24,880
all right I break in my debugger

12
00:00:26,560 --> 00:00:30,400
and if we scroll all the way down we

13
00:00:28,320 --> 00:00:32,960
find the LDT register

14
00:00:30,400 --> 00:00:34,320
and it seems to be set to 0 so you

15
00:00:32,960 --> 00:00:37,520
know what is

16
00:00:34,320 --> 00:00:40,960
0 well 0 is actually an

17
00:00:37,520 --> 00:00:42,960
invalid entry in the GDT

18
00:00:40,960 --> 00:00:44,800
so it says right here that first clip

19
00:00:42,960 --> 00:00:46,399
descriptor in the GDT is not used and

20
00:00:44,800 --> 00:00:47,840
this is treated as invalid

21
00:00:46,399 --> 00:00:50,079
so that means for all intents and

22
00:00:47,840 --> 00:00:50,879
purposes right now on this windows

23
00:00:50,079 --> 00:00:53,760
version

24
00:00:50,879 --> 00:00:55,440
the LDT is not being used at all and

25
00:00:53,760 --> 00:00:56,719
we'll talk a little bit later we'll put

26
00:00:55,440 --> 00:00:58,239
a reference to something where you can

27
00:00:56,719 --> 00:01:00,320
see a little bit more about how

28
00:00:58,239 --> 00:01:03,840
the LDT is or isn't used in different

29
00:01:00,320 --> 00:01:03,840
versions of windows over time

