1
00:00:00,960 --> 00:00:04,640
all right so here is this big picture of

2
00:00:03,520 --> 00:00:06,240
what we're going to learn in the class

3
00:00:04,640 --> 00:00:07,359
we saw rflags we saw segment

4
00:00:06,240 --> 00:00:10,000
selectors

5
00:00:07,359 --> 00:00:11,840
and now we just saw the notion of the

6
00:00:10,000 --> 00:00:13,599
global descriptor table we didn't cover

7
00:00:11,840 --> 00:00:16,160
what's inside of it yet

8
00:00:13,599 --> 00:00:18,320
and we also saw the notion of the GDT

9
00:00:16,160 --> 00:00:18,800
register points at the global descriptor

10
00:00:18,320 --> 00:00:20,320
table

11
00:00:18,800 --> 00:00:22,960
and so the next section will talk about

12
00:00:20,320 --> 00:00:24,880
the things that are inside of it

13
00:00:22,960 --> 00:00:26,320
we also saw the notion of the local

14
00:00:24,880 --> 00:00:28,800
descriptor table

15
00:00:26,320 --> 00:00:30,320
and the local descriptor table register

16
00:00:28,800 --> 00:00:31,199
which points at the local descriptor

17
00:00:30,320 --> 00:00:33,680
table

18
00:00:31,199 --> 00:00:34,800
and we saw that there's notionally going

19
00:00:33,680 --> 00:00:38,079
to be some entry

20
00:00:34,800 --> 00:00:39,520
the ldtr has some index that it's

21
00:00:38,079 --> 00:00:42,640
selecting from the GDT

22
00:00:39,520 --> 00:00:43,040
which describes the LDT itself and sort

23
00:00:42,640 --> 00:00:46,239
of

24
00:00:43,040 --> 00:00:48,079
you know points back at that to this LDT

25
00:00:46,239 --> 00:00:50,160
itself that probably should be pointing

26
00:00:48,079 --> 00:00:52,879
right here at the base of the ldt but

27
00:00:50,160 --> 00:00:52,879
whatever

