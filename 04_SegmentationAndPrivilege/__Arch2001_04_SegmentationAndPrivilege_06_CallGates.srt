1
00:00:00,160 --> 00:00:04,160
So in the previous video I left you

2
00:00:01,680 --> 00:00:05,359
hanging with the mystery van having made

3
00:00:04,160 --> 00:00:07,919
an appearance

4
00:00:05,359 --> 00:00:08,400
and I said that there's no way to just

5
00:00:07,919 --> 00:00:11,519
set

6
00:00:08,400 --> 00:00:13,679
the cs the code segment register

7
00:00:11,519 --> 00:00:14,799
you can't do a mov instruction and you

8
00:00:13,679 --> 00:00:17,840
can't just do

9
00:00:14,799 --> 00:00:20,000
a pop instruction and so how can you

10
00:00:17,840 --> 00:00:23,039
practically speaking transition from

11
00:00:20,000 --> 00:00:25,279
ring 3 to ring 0 well call gates

12
00:00:23,039 --> 00:00:26,960
are one of the ways that you can do that

13
00:00:25,279 --> 00:00:28,800
so basically you can think of a call

14
00:00:26,960 --> 00:00:31,840
gate like Sonic's running along

15
00:00:28,800 --> 00:00:32,880
at his normal privilege level CPL 3 he

16
00:00:31,840 --> 00:00:36,079
hits a call gate

17
00:00:32,880 --> 00:00:38,000
and boing, he's up to CPL 0

18
00:00:36,079 --> 00:00:39,680
but it's obviously more complicated than

19
00:00:38,000 --> 00:00:42,079
that so how does it work

20
00:00:39,680 --> 00:00:43,520
well we've got a call gate descriptor

21
00:00:42,079 --> 00:00:47,200
and this is one of the

22
00:00:43,520 --> 00:00:47,920
16-byte system segment descriptor types

23
00:00:47,200 --> 00:00:51,120
of things

24
00:00:47,920 --> 00:00:53,199
that fit into the GDT or LDT

25
00:00:51,120 --> 00:00:54,559
but the interesting thing about the call

26
00:00:53,199 --> 00:00:57,280
gate descriptor is that

27
00:00:54,559 --> 00:00:58,160
it has a segment selector inside of it

28
00:00:57,280 --> 00:01:02,559
and a segment

29
00:00:58,160 --> 00:01:02,559
offset that allows for a 64-bit value

30
00:01:03,120 --> 00:01:06,320
so unlike some of the segment

31
00:01:04,720 --> 00:01:09,520
descriptors that we've seen

32
00:01:06,320 --> 00:01:11,760
such as the ones for code and data

33
00:01:09,520 --> 00:01:12,720
that have some sort of segment base and

34
00:01:11,760 --> 00:01:14,799
segment limit

35
00:01:12,720 --> 00:01:15,920
this instead seems to have something

36
00:01:14,799 --> 00:01:18,400
akin to a

37
00:01:15,920 --> 00:01:19,520
logical address or a far pointer there's

38
00:01:18,400 --> 00:01:23,040
a segment selector

39
00:01:19,520 --> 00:01:26,240
and a offset into that segment well this

40
00:01:23,040 --> 00:01:28,000
is one more down of the system segment

41
00:01:26,240 --> 00:01:30,720
descriptors that we've

42
00:01:28,000 --> 00:01:33,119
we want to cover all of these as we go

43
00:01:30,720 --> 00:01:36,240
so how does one call a call gate

44
00:01:33,119 --> 00:01:39,360
well you issue a call instruction

45
00:01:36,240 --> 00:01:42,240
with a far pointer that has a

46
00:01:39,360 --> 00:01:44,079
segment selector that points at a call

47
00:01:42,240 --> 00:01:46,399
gate segment descriptor

48
00:01:44,079 --> 00:01:47,119
all right it's also clear it's not as if

49
00:01:46,399 --> 00:01:49,040
I you know

50
00:01:47,119 --> 00:01:50,159
have just thrown out a ton of jargon at

51
00:01:49,040 --> 00:01:51,680
you

52
00:01:50,159 --> 00:01:54,560
so let's see some different ways we can

53
00:01:51,680 --> 00:01:57,759
visualize that intel has this picture

54
00:01:54,560 --> 00:02:00,079
which says you have a call of a

55
00:01:57,759 --> 00:02:01,920
far pointer to a call gate and this far

56
00:02:00,079 --> 00:02:02,719
pointer is a segment selector and an

57
00:02:01,920 --> 00:02:05,920
offset

58
00:02:02,719 --> 00:02:08,560
but this offset is not actually used by

59
00:02:05,920 --> 00:02:09,200
the processor so I call this a faux far

60
00:02:08,560 --> 00:02:12,239
pointer

61
00:02:09,200 --> 00:02:13,520
it is a fake far pointer so you have to

62
00:02:12,239 --> 00:02:14,480
put an offset but it's not actually

63
00:02:13,520 --> 00:02:17,280
going to be used

64
00:02:14,480 --> 00:02:19,200
instead the call instruction is mostly

65
00:02:17,280 --> 00:02:20,319
just using this segment selector behind

66
00:02:19,200 --> 00:02:23,360
the scenes

67
00:02:20,319 --> 00:02:25,440
it is selecting a segment descriptor in

68
00:02:23,360 --> 00:02:27,040
the descriptor table could be the GDT

69
00:02:25,440 --> 00:02:29,760
could be the LDT

70
00:02:27,040 --> 00:02:31,920
selects this call gate descriptor the

71
00:02:29,760 --> 00:02:34,480
call gate descriptor is itself

72
00:02:31,920 --> 00:02:35,599
basically a far pointer because it has a

73
00:02:34,480 --> 00:02:38,080
segment selector

74
00:02:35,599 --> 00:02:39,840
and an offset so this far pointer is

75
00:02:38,080 --> 00:02:42,080
then further selecting

76
00:02:39,840 --> 00:02:43,200
a different thing from within the GDT or

77
00:02:42,080 --> 00:02:45,360
LDT

78
00:02:43,200 --> 00:02:46,959
so this would point at a code segment

79
00:02:45,360 --> 00:02:48,000
descriptor and this would have you know

80
00:02:46,959 --> 00:02:50,000
a base and a limit

81
00:02:48,000 --> 00:02:51,040
but in 64-bit world we don't care about

82
00:02:50,000 --> 00:02:53,120
either of those

83
00:02:51,040 --> 00:02:55,360
and so you would basically take this far

84
00:02:53,120 --> 00:02:56,400
pointer and you would use the base from

85
00:02:55,360 --> 00:02:58,480
whatever it selects

86
00:02:56,400 --> 00:02:59,680
and you'd use the offset that's baked

87
00:02:58,480 --> 00:03:01,120
into the GDT

88
00:02:59,680 --> 00:03:02,640
and you wouldn't care about the offset

89
00:03:01,120 --> 00:03:03,440
that the person in ring 3 happened

90
00:03:02,640 --> 00:03:05,280
to use

91
00:03:03,440 --> 00:03:06,800
and you just add to the offset to the

92
00:03:05,280 --> 00:03:07,760
base and that's how you would find the

93
00:03:06,800 --> 00:03:10,959
actual address

94
00:03:07,760 --> 00:03:11,920
where the code to be run is found so

95
00:03:10,959 --> 00:03:16,159
let's look at that

96
00:03:11,920 --> 00:03:17,440
again in my customized animatey style

97
00:03:16,159 --> 00:03:19,920
diagrams

98
00:03:17,440 --> 00:03:21,519
so we've got our faux far pointer so

99
00:03:19,920 --> 00:03:23,280
we've got a segment selector

100
00:03:21,519 --> 00:03:24,879
and an offset that doesn't really matter

101
00:03:23,280 --> 00:03:26,879
so it could be anything we'll just set

102
00:03:24,879 --> 00:03:29,760
it to zeros

103
00:03:26,879 --> 00:03:31,120
segment selector required but not used

104
00:03:29,760 --> 00:03:33,120
great

105
00:03:31,120 --> 00:03:34,720
so what does this segment selector

106
00:03:33,120 --> 00:03:38,159
actually encode

107
00:03:34,720 --> 00:03:39,920
well 8 is 1 0 0 0

108
00:03:38,159 --> 00:03:41,519
so it's saying you know I request the

109
00:03:39,920 --> 00:03:44,159
privilege level of 0

110
00:03:41,519 --> 00:03:46,400
I have a table indicator of 0 so I

111
00:03:44,159 --> 00:03:48,080
want to select from the GDT

112
00:03:46,400 --> 00:03:49,920
and then let's just say that it happens

113
00:03:48,080 --> 00:03:51,760
to say index 1 because that's what this

114
00:03:49,920 --> 00:03:54,000
uses here

115
00:03:51,760 --> 00:03:55,599
so index 1 in this call would be

116
00:03:54,000 --> 00:03:58,879
expected to point at

117
00:03:55,599 --> 00:04:00,560
a call gate segment descriptor so that

118
00:03:58,879 --> 00:04:02,400
was this 16

119
00:04:00,560 --> 00:04:04,480
byte data structure that we saw just a

120
00:04:02,400 --> 00:04:06,239
second ago and which has a

121
00:04:04,480 --> 00:04:07,599
basically a far pointer inside of it a

122
00:04:06,239 --> 00:04:10,879
segment selector and a

123
00:04:07,599 --> 00:04:12,720
64-bit offset so that would have some

124
00:04:10,879 --> 00:04:13,439
values in there let's just make up some

125
00:04:12,720 --> 00:04:15,280
values

126
00:04:13,439 --> 00:04:18,079
let's say that the segment selector

127
00:04:15,280 --> 00:04:21,519
itself had a

128
00:04:18,079 --> 00:04:23,840
RPL of 0 a table indicator of 0

129
00:04:21,519 --> 00:04:24,720
and a index of 3 so this is going to

130
00:04:23,840 --> 00:04:26,880
grab the

131
00:04:24,720 --> 00:04:28,479
third entry from the GDT and we'll just

132
00:04:26,880 --> 00:04:32,400
say the 64-bit address

133
00:04:28,479 --> 00:04:36,000
is fff12340000

134
00:04:32,400 --> 00:04:36,960
it's got a descriptor privilege level in

135
00:04:36,000 --> 00:04:40,240
the call gate

136
00:04:36,960 --> 00:04:42,160
of 3 this indicates that code ring

137
00:04:40,240 --> 00:04:43,759
3 code can call through here if this

138
00:04:42,160 --> 00:04:44,720
was 0 then you couldn't call through

139
00:04:43,759 --> 00:04:46,320
this call gate

140
00:04:44,720 --> 00:04:49,120
and it's got a present bit of one just

141
00:04:46,320 --> 00:04:50,720
to indicate it's actually here

142
00:04:49,120 --> 00:04:52,560
all right well that's the call gate

143
00:04:50,720 --> 00:04:53,280
segment descriptor which for all intents

144
00:04:52,560 --> 00:04:56,080
and purposes

145
00:04:53,280 --> 00:04:56,960
is just its own far pointer and that

146
00:04:56,080 --> 00:04:59,840
selects

147
00:04:56,960 --> 00:05:00,720
index 3 out of the GDT which would be

148
00:04:59,840 --> 00:05:04,080
some sort of

149
00:05:00,720 --> 00:05:07,120
call call some sort of code

150
00:05:04,080 --> 00:05:08,560
segment but in 64-bit the

151
00:05:07,120 --> 00:05:10,000
code segment base and limit aren't

152
00:05:08,560 --> 00:05:10,479
really cared about so it's just going to

153
00:05:10,000 --> 00:05:12,479
be

154
00:05:10,479 --> 00:05:13,759
treated as 0 and it's going to say

155
00:05:12,479 --> 00:05:17,080
that base of 0

156
00:05:13,759 --> 00:05:19,759
plus this offset from the call gate of

157
00:05:17,080 --> 00:05:21,520
ffffff1234 and zeros

158
00:05:19,759 --> 00:05:23,360
would mean that ultimately it's just

159
00:05:21,520 --> 00:05:26,479
going to be ffff1234

160
00:05:23,360 --> 00:05:28,639
zeros so the new code

161
00:05:26,479 --> 00:05:30,320
segment register is going to be the

162
00:05:28,639 --> 00:05:30,880
value that was plucked from the call

163
00:05:30,320 --> 00:05:33,039
gate

164
00:05:30,880 --> 00:05:34,160
so this is how we actually change the

165
00:05:33,039 --> 00:05:36,479
code segment

166
00:05:34,160 --> 00:05:37,919
register by calling through a call gate

167
00:05:36,479 --> 00:05:39,680
and we'll learn some more ways in the

168
00:05:37,919 --> 00:05:41,520
future

169
00:05:39,680 --> 00:05:42,800
so the net result of all that is that

170
00:05:41,520 --> 00:05:45,280
you called a call

171
00:05:42,800 --> 00:05:46,880
gate and you landed in some kernel

172
00:05:45,280 --> 00:05:48,880
address

173
00:05:46,880 --> 00:05:50,240
so what does that look like exactly like

174
00:05:48,880 --> 00:05:53,600
this CPL 3

175
00:05:50,240 --> 00:05:56,479
running along calls a call gate and boom

176
00:05:53,600 --> 00:05:57,919
up to some code running in kernel space

177
00:05:56,479 --> 00:06:00,080
at some particular address

178
00:05:57,919 --> 00:06:01,039
that the kernel set up by filling in a

179
00:06:00,080 --> 00:06:04,720
call gate

180
00:06:01,039 --> 00:06:07,039
into the into the GDT or LDT

181
00:06:04,720 --> 00:06:08,319
so that got you into kernel space how do

182
00:06:07,039 --> 00:06:11,199
you get back

183
00:06:08,319 --> 00:06:11,520
well if a normal call instruction pushes

184
00:06:11,199 --> 00:06:14,560
an

185
00:06:11,520 --> 00:06:17,520
rip then a far call through

186
00:06:14,560 --> 00:06:19,680
a inter privilege call gate is going to

187
00:06:17,520 --> 00:06:22,800
push the ss:rsp

188
00:06:19,680 --> 00:06:24,560
and the cs:rip and so between these two

189
00:06:22,800 --> 00:06:26,319
things that gives you enough information

190
00:06:24,560 --> 00:06:26,880
to resume back to where you were coming

191
00:06:26,319 --> 00:06:28,400
from

192
00:06:26,880 --> 00:06:30,160
and you do that via the return

193
00:06:28,400 --> 00:06:31,840
instruction so just like there's a

194
00:06:30,160 --> 00:06:34,400
special form of the call instruction

195
00:06:31,840 --> 00:06:37,840
that knows about this sort of far call

196
00:06:34,400 --> 00:06:41,120
through a inter privilege call gate

197
00:06:37,840 --> 00:06:43,600
segment descriptor there is a far return

198
00:06:41,120 --> 00:06:44,400
that knows to pop this stuff off of the

199
00:06:43,600 --> 00:06:47,840
stack

200
00:06:44,400 --> 00:06:48,479
pop an rip an old cs an old rsp and old

201
00:06:47,840 --> 00:06:50,800
ss

202
00:06:48,479 --> 00:06:52,240
to get you back down to ring 3

203
00:06:50,800 --> 00:06:55,039
because the cs would be

204
00:06:52,240 --> 00:06:56,400
pointing at your ring 3 code segment

205
00:06:55,039 --> 00:06:57,039
this would be pointing at your ring

206
00:06:56,400 --> 00:06:59,199
3

207
00:06:57,039 --> 00:07:00,160
stack segment and it'll get you back

208
00:06:59,199 --> 00:07:03,360
down to

209
00:07:00,160 --> 00:07:06,400
ring 3 so

210
00:07:03,360 --> 00:07:08,080
all of that said this was just a example

211
00:07:06,400 --> 00:07:10,000
of one way that you could

212
00:07:08,080 --> 00:07:12,240
get from ring 3 to ring 0 but

213
00:07:10,000 --> 00:07:13,840
nobody actually uses this anymore

214
00:07:12,240 --> 00:07:15,599
used it a really really long time ago

215
00:07:13,840 --> 00:07:18,000
like Windows 95

216
00:07:15,599 --> 00:07:20,160
type long ago but this is not what they

217
00:07:18,000 --> 00:07:21,440
use I just wanted to cover it so that we

218
00:07:20,160 --> 00:07:22,880
could you know see

219
00:07:21,440 --> 00:07:24,720
you know one of the mechanisms that

220
00:07:22,880 --> 00:07:27,039
exists so we can cover all of our

221
00:07:24,720 --> 00:07:29,360
segment descriptor types but also you

222
00:07:27,039 --> 00:07:32,800
know I referenced a while back

223
00:07:29,360 --> 00:07:35,680
this alex ionescu blog post about

224
00:07:32,800 --> 00:07:38,080
how the LDT was used on Windows in that

225
00:07:35,680 --> 00:07:39,680
post he actually talks about how he used

226
00:07:38,080 --> 00:07:42,479
call gates in order to

227
00:07:39,680 --> 00:07:43,360
build a sort of attack against Windows

228
00:07:42,479 --> 00:07:44,879
kernel

229
00:07:43,360 --> 00:07:47,120
but it was one that was already closed

230
00:07:44,879 --> 00:07:48,000
at the time but to me this is a very

231
00:07:47,120 --> 00:07:50,400
good example

232
00:07:48,000 --> 00:07:51,840
of how people who really understand the

233
00:07:50,400 --> 00:07:53,520
system at a deep level

234
00:07:51,840 --> 00:07:55,199
can come up with these kind of attacks

235
00:07:53,520 --> 00:07:56,479
and they can find the corner cases that

236
00:07:55,199 --> 00:07:58,879
other people miss

237
00:07:56,479 --> 00:08:01,039
and so it's again you know part of the

238
00:07:58,879 --> 00:08:03,199
uh satisfaction that comes from really

239
00:08:01,039 --> 00:08:05,440
understanding the system at a deep level

240
00:08:03,199 --> 00:08:06,879
so now that you understand call gates

241
00:08:05,440 --> 00:08:07,520
and you understand segments and things

242
00:08:06,879 --> 00:08:09,840
like that

243
00:08:07,520 --> 00:08:12,240
you will be extremely well prepared to

244
00:08:09,840 --> 00:08:14,080
go read and understand this post

245
00:08:12,240 --> 00:08:15,599
so what's new what else did we learn in

246
00:08:14,080 --> 00:08:19,039
this section

247
00:08:15,599 --> 00:08:22,400
well we saw the call gate segment selector

248
00:08:19,039 --> 00:08:22,720
can be used to point at some call gate in

249
00:08:22,400 --> 00:08:25,360
the

250
00:08:22,720 --> 00:08:26,080
LDT or GDT here just happens to be shown

251
00:08:25,360 --> 00:08:29,599
in the

252
00:08:26,080 --> 00:08:31,680
LDT and that call gate ultimately points

253
00:08:29,599 --> 00:08:33,599
at some sort of protected procedure some

254
00:08:31,680 --> 00:08:36,240
sort of code in the kernel

255
00:08:33,599 --> 00:08:36,800
that is intended to run and do some

256
00:08:36,240 --> 00:08:38,560
action

257
00:08:36,800 --> 00:08:41,360
after transitioning from userspace to

258
00:08:38,560 --> 00:08:41,360
kernelspace

