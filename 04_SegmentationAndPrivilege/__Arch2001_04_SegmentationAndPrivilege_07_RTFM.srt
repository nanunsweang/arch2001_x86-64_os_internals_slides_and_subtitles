1
00:00:00,399 --> 00:00:05,120
so having covered the special version of

2
00:00:02,879 --> 00:00:07,839
the call instruction that is used to

3
00:00:05,120 --> 00:00:09,840
far call to a call gate and use

4
00:00:07,839 --> 00:00:10,400
segmentation to transition privilege

5
00:00:09,840 --> 00:00:13,200
rings

6
00:00:10,400 --> 00:00:14,400
Kylo Ren demands that I go back in RTFM

7
00:00:13,200 --> 00:00:16,080
to explain the other

8
00:00:14,400 --> 00:00:18,000
conditional cases that I skipped in

9
00:00:16,080 --> 00:00:20,320
architecture 1001

10
00:00:18,000 --> 00:00:22,960
and who am I to say no to a dark

11
00:00:20,320 --> 00:00:25,519
man-child of the sith

12
00:00:22,960 --> 00:00:27,439
so back in architecture 1001 we saw you

13
00:00:25,519 --> 00:00:28,880
know there's many different forms of the

14
00:00:27,439 --> 00:00:30,800
call instruction

15
00:00:28,880 --> 00:00:32,559
and at the time we only focused on these

16
00:00:30,800 --> 00:00:35,120
forms a call near

17
00:00:32,559 --> 00:00:36,880
relative and a call near absolute

18
00:00:35,120 --> 00:00:37,920
indirect we said there's no sort of

19
00:00:36,880 --> 00:00:40,800
thing called a call

20
00:00:37,920 --> 00:00:42,559
short and so forth but what we skipped

21
00:00:40,800 --> 00:00:44,879
at the time and said we would come back

22
00:00:42,559 --> 00:00:48,079
to an architecture 2001

23
00:00:44,879 --> 00:00:49,440
are the things that call far now this

24
00:00:48,079 --> 00:00:52,399
one right here we're not going to cover

25
00:00:49,440 --> 00:00:55,840
because it's invalid in 64-bit

26
00:00:52,399 --> 00:00:58,640
but this right here call memory 16

27
00:00:55,840 --> 00:00:59,680
and then a 64-bit value well that kind

28
00:00:58,640 --> 00:01:03,039
of looks like a

29
00:00:59,680 --> 00:01:05,280
far pointer or a logical address so in

30
00:01:03,039 --> 00:01:05,840
64-bit mode if the selector points to a

31
00:01:05,280 --> 00:01:08,799
gate

32
00:01:05,840 --> 00:01:11,439
like a call gate then rip equals the

33
00:01:08,799 --> 00:01:14,640
64-bits displacement taken from the gate

34
00:01:11,439 --> 00:01:16,720
else rip is a 64-bit offset from the far

35
00:01:14,640 --> 00:01:18,799
pointer referenced in the instruction

36
00:01:16,720 --> 00:01:19,920
so this is actually referring to 2

37
00:01:18,799 --> 00:01:21,280
different forms

38
00:01:19,920 --> 00:01:23,920
this could either be calling through a

39
00:01:21,280 --> 00:01:27,360
call gate or you could just be

40
00:01:23,920 --> 00:01:28,799
jumping around between segments without

41
00:01:27,360 --> 00:01:29,280
actually transitioning through a call

42
00:01:28,799 --> 00:01:31,680
gate

43
00:01:29,280 --> 00:01:32,880
sort of depends so we already covered

44
00:01:31,680 --> 00:01:34,880
the call gate example

45
00:01:32,880 --> 00:01:37,759
and if this is not the call gate example

46
00:01:34,880 --> 00:01:40,479
then it is basically an inter-segment

47
00:01:37,759 --> 00:01:41,759
transition but it is not one that has a

48
00:01:40,479 --> 00:01:43,360
privilege transition

49
00:01:41,759 --> 00:01:44,960
the only way you can do those privileged

50
00:01:43,360 --> 00:01:45,920
transitions is if you jump through the

51
00:01:44,960 --> 00:01:47,520
call gate

52
00:01:45,920 --> 00:01:49,439
so you could bounce from if you were

53
00:01:47,520 --> 00:01:50,640
using a multi-segment model which as we

54
00:01:49,439 --> 00:01:52,479
said no one does

55
00:01:50,640 --> 00:01:54,159
you could bounce between one segment to

56
00:01:52,479 --> 00:01:56,640
the next

57
00:01:54,159 --> 00:01:57,600
and again the far pointer versus near

58
00:01:56,640 --> 00:01:59,759
pointer

59
00:01:57,600 --> 00:02:01,439
it's all just to say that far pointers

60
00:01:59,759 --> 00:02:04,079
have the segment selectors and the

61
00:02:01,439 --> 00:02:06,880
64-bit offsets

62
00:02:04,079 --> 00:02:07,600
so later on in the call instruction

63
00:02:06,880 --> 00:02:09,119
manual

64
00:02:07,600 --> 00:02:11,280
it talks about you know the different

65
00:02:09,119 --> 00:02:13,040
forms the 4 forms of the calls

66
00:02:11,280 --> 00:02:15,120
there was near call that's exactly what

67
00:02:13,040 --> 00:02:17,280
we covered in architecture 1001

68
00:02:15,120 --> 00:02:19,520
there's far call and so this would just

69
00:02:17,280 --> 00:02:21,280
be calling to a procedure located in a

70
00:02:19,520 --> 00:02:22,160
different segment than the current code

71
00:02:21,280 --> 00:02:24,480
segment

72
00:02:22,160 --> 00:02:26,000
and so this is an inter-segment call but

73
00:02:24,480 --> 00:02:28,080
you can see by the difference

74
00:02:26,000 --> 00:02:29,760
this is implicitly saying it is not a

75
00:02:28,080 --> 00:02:31,680
inter-privilege call so this is saying

76
00:02:29,760 --> 00:02:35,360
at the same privilege you can have a far

77
00:02:31,680 --> 00:02:37,360
call but if it's a inter-privilege call

78
00:02:35,360 --> 00:02:38,959
inter-privilege far call then that means

79
00:02:37,360 --> 00:02:40,879
you're jumping to some other

80
00:02:38,959 --> 00:02:43,519
different privilege level and that is

81
00:02:40,879 --> 00:02:45,280
the call gate mechanism that we just saw

82
00:02:43,519 --> 00:02:46,640
the final form that we haven't seen yet

83
00:02:45,280 --> 00:02:48,840
is called a task switch

84
00:02:46,640 --> 00:02:50,000
and we'll talk about tasks in a future

85
00:02:48,840 --> 00:02:51,840
section

86
00:02:50,000 --> 00:02:53,440
all right likewise to the call

87
00:02:51,840 --> 00:02:54,800
instruction there was the jump and we

88
00:02:53,440 --> 00:02:58,959
saw many forms of jump

89
00:02:54,800 --> 00:03:01,680
but we skipped over things so the 64-bit

90
00:02:58,959 --> 00:03:03,040
far jump looks like this again we have

91
00:03:01,680 --> 00:03:04,720
an invalid version

92
00:03:03,040 --> 00:03:06,959
and we have something that takes a far

93
00:03:04,720 --> 00:03:09,760
pointer and this is a jump far

94
00:03:06,959 --> 00:03:10,959
absolute indirect address given in this

95
00:03:09,760 --> 00:03:14,000
far pointer

96
00:03:10,959 --> 00:03:16,959
so again this is just a way to do inter

97
00:03:14,000 --> 00:03:19,120
segment jumps but they can't be inter

98
00:03:16,959 --> 00:03:23,360
segment privilege level transfer jumps

99
00:03:19,120 --> 00:03:23,360
because that's not a supported mechanism

