1
00:00:00,080 --> 00:00:04,080
so it's not as much of a problem for

2
00:00:01,760 --> 00:00:04,960
this sort of intentional online delivery

3
00:00:04,080 --> 00:00:06,879
but normally

4
00:00:04,960 --> 00:00:08,320
when this was a in-person delivered

5
00:00:06,879 --> 00:00:10,719
class right about now

6
00:00:08,320 --> 00:00:12,480
everyone's brains would be overheating

7
00:00:10,719 --> 00:00:14,160
going through all that segmentation

8
00:00:12,480 --> 00:00:16,320
information all that interrupt

9
00:00:14,160 --> 00:00:18,080
information to say nothing of the system

10
00:00:16,320 --> 00:00:19,680
call information which is a new section

11
00:00:18,080 --> 00:00:21,039
that I've added in pulled in from the

12
00:00:19,680 --> 00:00:23,279
rootkits class

13
00:00:21,039 --> 00:00:25,119
uh a typical in-person class the

14
00:00:23,279 --> 00:00:27,199
students are like overloaded with

15
00:00:25,119 --> 00:00:28,720
bitfields and everything by this point

16
00:00:27,199 --> 00:00:31,199
so I like to have this

17
00:00:28,720 --> 00:00:32,880
rdtsc intermission just to let people's

18
00:00:31,199 --> 00:00:34,239
brains cool off a little bit before we

19
00:00:32,880 --> 00:00:36,640
get into the next section

20
00:00:34,239 --> 00:00:37,680
which is paging which is another brain

21
00:00:36,640 --> 00:00:41,120
beater

22
00:00:37,680 --> 00:00:42,160
so rdtsc what is that that is the read

23
00:00:41,120 --> 00:00:43,760
time-stamp counter

24
00:00:42,160 --> 00:00:45,200
and what's the time it's time to read

25
00:00:43,760 --> 00:00:46,640
the time-stamp counter

26
00:00:45,200 --> 00:00:48,640
which I know doesn't even really work as

27
00:00:46,640 --> 00:00:50,079
a beastie boys reference but I still

28
00:00:48,640 --> 00:00:52,160
tried it anyways

29
00:00:50,079 --> 00:00:53,840
so the time-stamp counter is a 64-bit

30
00:00:52,160 --> 00:00:56,399
counter introduced into pentiums

31
00:00:53,840 --> 00:00:58,000
so you probably don't need to use cpuid

32
00:00:56,399 --> 00:01:00,239
in order to check whether the TSC

33
00:00:58,000 --> 00:01:01,199
bit is set so that it indicates this is

34
00:01:00,239 --> 00:01:03,199
supported

35
00:01:01,199 --> 00:01:05,519
but you know I made you do that for cpu

36
00:01:03,199 --> 00:01:07,439
id itself so whatever

37
00:01:05,519 --> 00:01:09,680
so the thing about the time-stamp counter

38
00:01:07,439 --> 00:01:12,080
is that when the processor is reset

39
00:01:09,680 --> 00:01:14,159
it resets to 0 and then it increments

40
00:01:12,080 --> 00:01:17,280
on every clock cycle thereafter

41
00:01:14,159 --> 00:01:18,960
so each tick of the clock the CPU

42
00:01:17,280 --> 00:01:21,200
you know clock that controls execution

43
00:01:18,960 --> 00:01:22,320
of the CPU will increment the value in

44
00:01:21,200 --> 00:01:24,400
the time-stamp counter

45
00:01:22,320 --> 00:01:26,640
now you can also read this directly out

46
00:01:24,400 --> 00:01:28,880
of the time-stamp counter msr

47
00:01:26,640 --> 00:01:30,720
which is number 10 so you know it's a

48
00:01:28,880 --> 00:01:33,520
old msr

49
00:01:30,720 --> 00:01:34,560
and there actually is a bit in control

50
00:01:33,520 --> 00:01:36,640
register 4

51
00:01:34,560 --> 00:01:38,640
bit 2 which can be set so that it

52
00:01:36,640 --> 00:01:39,119
actually restricts the time-stamp counter

53
00:01:38,640 --> 00:01:41,680
lookup

54
00:01:39,119 --> 00:01:42,799
to ring 0 only so between the msr

55
00:01:41,680 --> 00:01:44,640
reads and the

56
00:01:42,799 --> 00:01:46,560
restrictions on this you could make it

57
00:01:44,640 --> 00:01:47,360
so userspace straight up can't use this

58
00:01:46,560 --> 00:01:49,439
mechanism

59
00:01:47,360 --> 00:01:51,200
but as far as I know no one actually

60
00:01:49,439 --> 00:01:53,119
sets that perhaps some virtualization

61
00:01:51,200 --> 00:01:54,720
software might but I'm not aware of it

62
00:01:53,119 --> 00:01:56,560
so different processors increment the

63
00:01:54,720 --> 00:01:57,920
time-stamp counter differently but

64
00:01:56,560 --> 00:01:59,439
that really doesn't matter unless you're

65
00:01:57,920 --> 00:02:00,640
trying to do extremely detailed

66
00:01:59,439 --> 00:02:02,240
performance monitoring

67
00:02:00,640 --> 00:02:04,320
in which case you can go see that

68
00:02:02,240 --> 00:02:04,719
section but it's also interesting that

69
00:02:04,320 --> 00:02:06,799
the

70
00:02:04,719 --> 00:02:08,319
the virtualization extensions for

71
00:02:06,799 --> 00:02:10,720
intel's instructions set

72
00:02:08,319 --> 00:02:12,640
have special provisions specifically to

73
00:02:10,720 --> 00:02:13,440
allow virtualization software to

74
00:02:12,640 --> 00:02:16,160
intercept

75
00:02:13,440 --> 00:02:17,280
the rdtsc and lie about how long it

76
00:02:16,160 --> 00:02:21,440
takes things to run

77
00:02:17,280 --> 00:02:23,360
so here is the TSC bit in the cr4 which

78
00:02:21,440 --> 00:02:24,000
again can be set in order to make it so

79
00:02:23,360 --> 00:02:26,080
that

80
00:02:24,000 --> 00:02:27,360
time-stamp counter is disabled and not

81
00:02:26,080 --> 00:02:29,440
accessible for

82
00:02:27,360 --> 00:02:32,160
ring 3 code so what are some fun

83
00:02:29,440 --> 00:02:34,239
uses for rdtsc well one is for timing

84
00:02:32,160 --> 00:02:36,480
code for performance reasons but I

85
00:02:34,239 --> 00:02:38,640
consider that a bit of yawn the other

86
00:02:36,480 --> 00:02:41,120
interesting thing for us security folks

87
00:02:38,640 --> 00:02:43,040
is that it can be used for anti-debug

88
00:02:41,120 --> 00:02:44,319
checks where malware can actually time

89
00:02:43,040 --> 00:02:46,000
its own code

90
00:02:44,319 --> 00:02:47,440
and see whether or not it thinks it's

91
00:02:46,000 --> 00:02:50,480
being debugged and then change its

92
00:02:47,440 --> 00:02:50,480
behavior accordingly

