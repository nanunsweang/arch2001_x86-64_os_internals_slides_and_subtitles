1
00:00:00,080 --> 00:00:05,759
so let's look at a simple lab to allow

2
00:00:03,040 --> 00:00:07,520
you to time and performance profile some

3
00:00:05,759 --> 00:00:08,880
code that you can then you know go

4
00:00:07,520 --> 00:00:10,559
stick some code in there and see you

5
00:00:08,880 --> 00:00:10,960
know what the difference is in terms of

6
00:00:10,559 --> 00:00:12,799
time

7
00:00:10,960 --> 00:00:14,080
but then let's also go look at naval

8
00:00:12,799 --> 00:00:16,640
gaze in order to

9
00:00:14,080 --> 00:00:17,760
have some code which actually analyzes

10
00:00:16,640 --> 00:00:19,680
its own behavior

11
00:00:17,760 --> 00:00:21,119
and changes its behavior based on the

12
00:00:19,680 --> 00:00:23,840
timing alright so

13
00:00:21,119 --> 00:00:25,599
guestimate you should go set guestimate

14
00:00:23,840 --> 00:00:27,599
as your startup project

15
00:00:25,599 --> 00:00:28,640
and check out the source code it's

16
00:00:27,599 --> 00:00:30,800
pretty simple

17
00:00:28,640 --> 00:00:32,719
it has a single assembly function which

18
00:00:30,800 --> 00:00:34,800
you can put assembly into

19
00:00:32,719 --> 00:00:36,000
in order to you know see the differences

20
00:00:34,800 --> 00:00:37,600
in timing so you can put

21
00:00:36,000 --> 00:00:39,440
as many assembly instructions there you

22
00:00:37,600 --> 00:00:40,559
can put memory accesses versus just

23
00:00:39,440 --> 00:00:42,079
register accesses

24
00:00:40,559 --> 00:00:44,000
be aware that you know caching and

25
00:00:42,079 --> 00:00:46,399
things like that will change the timing

26
00:00:44,000 --> 00:00:48,160
but so basically just there's a compiler

27
00:00:46,399 --> 00:00:50,480
intrinsic for rdtsc

28
00:00:48,160 --> 00:00:51,520
from which we can get a 64-bit you know

29
00:00:50,480 --> 00:00:53,600
tick stamp count

30
00:00:51,520 --> 00:00:55,360
and then we can get a tick stamp count

31
00:00:53,600 --> 00:00:57,280
at the end and then we can diff them to

32
00:00:55,360 --> 00:00:59,359
see how long something takes to run

33
00:00:57,280 --> 00:01:01,039
so go ahead and set a breakpoint at the

34
00:00:59,359 --> 00:01:03,120
end of the function

35
00:01:01,039 --> 00:01:04,559
and just run it over and over and see

36
00:01:03,120 --> 00:01:07,680
how long this simple

37
00:01:04,559 --> 00:01:10,960
move assembly instruction takes so 2400

38
00:01:07,680 --> 00:01:14,200
cycles restart 2400 cycles

39
00:01:10,960 --> 00:01:17,680
restart 2900 cycles

40
00:01:14,200 --> 00:01:18,400
22c I'm actually looking for when I ran

41
00:01:17,680 --> 00:01:21,360
it before

42
00:01:18,400 --> 00:01:23,280
I got a nice anomaly which speaks to the

43
00:01:21,360 --> 00:01:24,720
fact of running for virtualization

44
00:01:23,280 --> 00:01:27,280
versus non-virtualization

45
00:01:24,720 --> 00:01:28,080
so at one point I got like a c06 or

46
00:01:27,280 --> 00:01:30,159
something like that

47
00:01:28,080 --> 00:01:32,479
so I can tell you that in general like a

48
00:01:30,159 --> 00:01:34,079
simple mov of immediate to a register

49
00:01:32,479 --> 00:01:35,439
should not take this many cycles

50
00:01:34,079 --> 00:01:37,520
and this is actually the result of

51
00:01:35,439 --> 00:01:39,680
virtualization so you can go ahead

52
00:01:37,520 --> 00:01:41,759
and run this on a bare metal host as

53
00:01:39,680 --> 00:01:42,159
well to see how many cycles it takes

54
00:01:41,759 --> 00:01:43,920
there

55
00:01:42,159 --> 00:01:45,520
and get a sense of what kind of you know

56
00:01:43,920 --> 00:01:47,520
overhead we're potentially

57
00:01:45,520 --> 00:01:49,600
incurring as a result of virtualization

58
00:01:47,520 --> 00:01:50,799
potentially messing with or intercepting

59
00:01:49,600 --> 00:01:53,439
the rdtsc

60
00:01:50,799 --> 00:01:54,000
but that is not the more interesting one

61
00:01:53,439 --> 00:01:56,399
so go

62
00:01:54,000 --> 00:01:58,000
ahead and switch over to NavalGaze

63
00:01:56,399 --> 00:01:59,759
let's look at the source code so the

64
00:01:58,000 --> 00:02:01,280
idea here is that you know you might

65
00:01:59,759 --> 00:02:02,479
have some malware that you're trying to

66
00:02:01,280 --> 00:02:03,040
analyze and it's got a bunch of

67
00:02:02,479 --> 00:02:04,320
functions

68
00:02:03,040 --> 00:02:06,000
you don't know the names of any of the

69
00:02:04,320 --> 00:02:06,640
functions so you're just kind of going

70
00:02:06,000 --> 00:02:08,000
along

71
00:02:06,640 --> 00:02:09,920
skimming through the code you should

72
00:02:08,000 --> 00:02:11,280
always skim through code to start with

73
00:02:09,920 --> 00:02:12,720
and you're running along just trying to

74
00:02:11,280 --> 00:02:14,239
get a sense of what's going on and maybe

75
00:02:12,720 --> 00:02:15,920
you say oh this function too

76
00:02:14,239 --> 00:02:17,680
that seems to be kind of interesting so

77
00:02:15,920 --> 00:02:19,120
let's set a breakpoint on that

78
00:02:17,680 --> 00:02:21,440
and you know let's go ahead and run

79
00:02:19,120 --> 00:02:23,120
there we run our debugger oops I didn't

80
00:02:21,440 --> 00:02:25,200
I don't think I said I did set it as

81
00:02:23,120 --> 00:02:27,200
startup okay so you run the debugger

82
00:02:25,200 --> 00:02:28,239
step into the function okay well it

83
00:02:27,200 --> 00:02:29,840
turns out that's not

84
00:02:28,239 --> 00:02:31,360
interesting so let's go ahead and run

85
00:02:29,840 --> 00:02:33,599
continue whatever

86
00:02:31,360 --> 00:02:36,080
and we can see that in this case the

87
00:02:33,599 --> 00:02:38,319
code prints out La de da de de, I mean as

88
00:02:36,080 --> 00:02:40,480
innocent as can be so what's the

89
00:02:38,319 --> 00:02:42,640
behavior if we don't set a breakpoint

90
00:02:40,480 --> 00:02:43,760
in this code if we just go ahead and run

91
00:02:42,640 --> 00:02:47,360
it to completion

92
00:02:43,760 --> 00:02:48,160
for that we see La de da de da, got your ass haxed

93
00:02:47,360 --> 00:02:50,480
didn't ya

94
00:02:48,160 --> 00:02:51,519
so it's basically changing the behavior

95
00:02:50,480 --> 00:02:53,519
of the code

96
00:02:51,519 --> 00:02:55,280
based on whether or not it detects that

97
00:02:53,519 --> 00:02:56,720
there's breakpoints inside of it

98
00:02:55,280 --> 00:02:58,640
and there's other ways to detect

99
00:02:56,720 --> 00:02:59,200
breakpoints as we'll see later on in the

100
00:02:58,640 --> 00:03:01,040
class

101
00:02:59,200 --> 00:03:02,959
but this one was just you know a very

102
00:03:01,040 --> 00:03:04,159
simple thing it's the idea of

103
00:03:02,959 --> 00:03:06,239
you know there's going to be so many

104
00:03:04,159 --> 00:03:08,720
different functions inside the code

105
00:03:06,239 --> 00:03:10,959
that you know some of them might contain

106
00:03:08,720 --> 00:03:14,000
rdtscs scattered throughout and

107
00:03:10,959 --> 00:03:15,840
the use of those rdtscs can be used to

108
00:03:14,000 --> 00:03:17,840
ultimately get some sort of difference

109
00:03:15,840 --> 00:03:18,400
in timing between different areas of the

110
00:03:17,840 --> 00:03:20,480
code

111
00:03:18,400 --> 00:03:21,440
and so the processor might know that you

112
00:03:20,480 --> 00:03:23,599
know there's a

113
00:03:21,440 --> 00:03:24,720
specific range of timing that they might

114
00:03:23,599 --> 00:03:26,159
have between the

115
00:03:24,720 --> 00:03:27,840
sorry the malware might know that

116
00:03:26,159 --> 00:03:29,920
there's a range of timing that they have

117
00:03:27,840 --> 00:03:31,280
between you know the slowest processors

118
00:03:29,920 --> 00:03:33,519
they expect to execute on on

119
00:03:31,280 --> 00:03:36,159
and the fastest ones and they know that

120
00:03:33,519 --> 00:03:38,640
like if you set a breakpoint even if you

121
00:03:36,159 --> 00:03:39,519
run it and continue as fast as humanly

122
00:03:38,640 --> 00:03:41,920
possible

123
00:03:39,519 --> 00:03:42,879
that's still human time scales and

124
00:03:41,920 --> 00:03:44,879
that's going to

125
00:03:42,879 --> 00:03:46,640
be detectable so let me try that one

126
00:03:44,879 --> 00:03:48,480
more time so I'm going to

127
00:03:46,640 --> 00:03:50,640
run and then I'm going to continue

128
00:03:48,480 --> 00:03:51,280
immediately so I went as fast as humanly

129
00:03:50,640 --> 00:03:52,720
possible

130
00:03:51,280 --> 00:03:54,239
but still it detects the timing

131
00:03:52,720 --> 00:03:55,599
difference because that's human time

132
00:03:54,239 --> 00:03:57,599
scales of clicking

133
00:03:55,599 --> 00:03:58,640
and it's going to alter its behavior

134
00:03:57,599 --> 00:04:00,799
accordingly so

135
00:03:58,640 --> 00:04:02,720
this was just rdtsc an interesting

136
00:04:00,799 --> 00:04:04,000
little miscellaneous instruction which

137
00:04:02,720 --> 00:04:05,760
some of the folks taking this for

138
00:04:04,000 --> 00:04:06,640
malware analysis might run into

139
00:04:05,760 --> 00:04:08,640
eventually

140
00:04:06,640 --> 00:04:10,480
in terms of malware trying to alter its

141
00:04:08,640 --> 00:04:12,480
behavior based on the timing so the

142
00:04:10,480 --> 00:04:14,799
conclusion is that a human stepping

143
00:04:12,480 --> 00:04:16,639
through code is easy for code to detect

144
00:04:14,799 --> 00:04:18,639
and hopefully you know now that you know

145
00:04:16,639 --> 00:04:19,600
the existence of that you as a malware

146
00:04:18,639 --> 00:04:21,600
analyst would

147
00:04:19,600 --> 00:04:22,960
be aware of what you can do to try to

148
00:04:21,600 --> 00:04:24,560
combat that and that's the kind of thing

149
00:04:22,960 --> 00:04:25,199
that will be covered in future malware

150
00:04:24,560 --> 00:04:27,440
classes

151
00:04:25,199 --> 00:04:28,639
and so ultimately rdtsc is just sort of

152
00:04:27,440 --> 00:04:30,240
an interesting little assembly

153
00:04:28,639 --> 00:04:32,240
instruction which in my opinion

154
00:04:30,240 --> 00:04:36,400
is less fun than a barrel of monkeys but

155
00:04:32,240 --> 00:04:36,400
more fun than a tube stock of scorpions

