1
00:00:00,04 --> 00:00:00,87
So what is port

2
00:00:00,87 --> 00:00:01,14
I/O

3
00:00:01,14 --> 00:00:02,91
According to the intel manuals?

4
00:00:03,01 --> 00:00:03,33
Well,

5
00:00:03,33 --> 00:00:06,16
they say that in addition in addition to transferring data

6
00:00:06,16 --> 00:00:07,4
to and from external memory,

7
00:00:07,4 --> 00:00:10,32
the processors are capable of transferring data to and from

8
00:00:10,33 --> 00:00:13,44
input output ports where I/O ports

9
00:00:13,44 --> 00:00:16,8
I/O ports are created in system hardware by circuitry that

10
00:00:16,8 --> 00:00:20,91
decodes control data and address pins on the processor and

11
00:00:20,91 --> 00:00:21,24
the

12
00:00:21,24 --> 00:00:24,36
I/O ports and then configured to communicate with peripheral devices

13
00:00:24,74 --> 00:00:27,52
So basically the hardware understands whether or not there's a

14
00:00:27,52 --> 00:00:29,83
particular address pin set that says,

15
00:00:29,83 --> 00:00:32,78
hey this is this a access that I'm trying to

16
00:00:32,78 --> 00:00:34,74
do right now is actually meant for port I/O

17
00:00:34,74 --> 00:00:35,36
Not RAM

18
00:00:36,24 --> 00:00:37,41
And it says that an

19
00:00:37,41 --> 00:00:39,63
I/O port can be an input port an output port

20
00:00:39,63 --> 00:00:41,13
or a bidirectional port

21
00:00:41,14 --> 00:00:42,1
And I think of the future

22
00:00:42,1 --> 00:00:44,18
I really need to update these slides to be more

23
00:00:44,18 --> 00:00:45,15
portal themed

24
00:00:45,16 --> 00:00:48,04
So there are 2 to the 16 possible 8-bit

25
00:00:48,05 --> 00:00:48,28


26
00:00:48,28 --> 00:00:51,72
I/O ports on the system with port 0 through port

27
00:00:51,73 --> 00:00:51,92
F

28
00:00:51,92 --> 00:00:52,11
F

29
00:00:52,11 --> 00:00:52,46
F

30
00:00:52,84 --> 00:00:55,7
And if you want to access sizes larger than 8

31
00:00:55,7 --> 00:00:56,67
bits at a time,

32
00:00:56,67 --> 00:01:00,09
you can you just basically the hardware needs to support

33
00:01:00,09 --> 00:01:04,58
accessing at 2 or 4 byte granularity via a port

34
00:01:04,58 --> 00:01:07,3
that then essentially takes up consecutive ports

35
00:01:07,31 --> 00:01:10,05
So if the hardware happened to be configured to that

36
00:01:10,05 --> 00:01:12,99
port 0 had a 4 byte access capability,

37
00:01:13,02 --> 00:01:17,59
it would be port 0 0, 1, 2 and 3 would all be

38
00:01:17,59 --> 00:01:19,85
being accessed at the same time when you are actually

39
00:01:19,85 --> 00:01:22,27
trying to read from port 0 and then the next

40
00:01:22,27 --> 00:01:24,16
available port would be 4

41
00:01:24,27 --> 00:01:26,51
And the manual says that when hardware is trying to

42
00:01:26,51 --> 00:01:27,89
access these large reports,

43
00:01:27,89 --> 00:01:29,04
like 32-bit ports,

44
00:01:29,31 --> 00:01:32,72
they should be aligned to addresses that are multiples 4

45
00:01:32,72 --> 00:01:33,27
for instance

46
00:01:33,27 --> 00:01:34,99
So port 0 could be 1

47
00:01:34,99 --> 00:01:37,59
32-bit port port 4 could be the next port

48
00:01:37,59 --> 00:01:38,96
8 could be the next and so forth

49
00:01:39,14 --> 00:01:41,86
Before you can access the ports using the in and

50
00:01:41,86 --> 00:01:43,67
out instructions that we're going to learn about in a

51
00:01:43,67 --> 00:01:44,15
second

52
00:01:44,34 --> 00:01:46,28
You have to have the necessary privileges

53
00:01:46,33 --> 00:01:48,48
And it turns out that there is a 2-bit

54
00:01:48,49 --> 00:01:48,75


55
00:01:48,75 --> 00:01:49,02
I/O

56
00:01:49,02 --> 00:01:52,98
Privilege level field in the rflags which you can't

57
00:01:52,98 --> 00:01:55,15
use the in and out assembly instructions

58
00:01:55,15 --> 00:01:56,14
Unless the I

59
00:01:56,14 --> 00:01:56,27
O

60
00:01:56,27 --> 00:01:56,48
P

61
00:01:56,48 --> 00:01:56,97
L

62
00:01:57,04 --> 00:01:58,78
Is such that your CPL,

63
00:01:58,78 --> 00:02:01,7
your current privilege level is less than or equal to

64
00:02:01,7 --> 00:02:01,85


65
00:02:01,85 --> 00:02:01,94
I/O

66
00:02:01,94 --> 00:02:02,55
Privilege level

67
00:02:02,84 --> 00:02:04,66
So if I/O privilege level was 0,

68
00:02:04,84 --> 00:02:06,8
your CPL must be less than or equal to 0

69
00:02:06,8 --> 00:02:09,98
So it must be 0 IOPL is 3 then you're

70
00:02:09,98 --> 00:02:13,78
good 3, 1, 2 capable of accessing it

71
00:02:13,79 --> 00:02:14,82
So this is the I

72
00:02:14,82 --> 00:02:14,93
O

73
00:02:14,93 --> 00:02:15,14
P

74
00:02:15,14 --> 00:02:15,37
L

75
00:02:15,37 --> 00:02:17,95
Field in the rflags register

76
00:02:18,34 --> 00:02:19,87
It's bits 12 and 13

77
00:02:19,88 --> 00:02:21,99
And so now it's time for a very quick little

78
00:02:21,99 --> 00:02:23,07
pause lab,

79
00:02:23,08 --> 00:02:24,38
I want you to pause the video,

80
00:02:24,39 --> 00:02:27,08
go look at your rflags register and check what

81
00:02:27,08 --> 00:02:27,4
the I

82
00:02:27,4 --> 00:02:27,52
O

83
00:02:27,52 --> 00:02:27,73
P

84
00:02:27,73 --> 00:02:28,03
L

85
00:02:28,04 --> 00:02:30,25
It's 12 and 13 is set to

86
00:02:34,84 --> 00:02:35,29
Okay

87
00:02:35,29 --> 00:02:37,47
So what you should have seen is that it is

88
00:02:37,47 --> 00:02:40,67
set to 0 because most operating systems set IOPL

89
00:02:40,67 --> 00:02:40,87


90
00:02:40,87 --> 00:02:41,03


91
00:02:41,03 --> 00:02:41,66
To 0

92
00:02:41,68 --> 00:02:44,52
If they didn't then userspace code would be able

93
00:02:44,52 --> 00:02:48,06
to do port I/O that would potentially allow them to manipulate

94
00:02:48,06 --> 00:02:50,56
some hardware that the operating system depends on

95
00:02:50,57 --> 00:02:52,6
And that would cause the operating system to have a

96
00:02:52,6 --> 00:02:53,16
bad day

97
00:02:53,24 --> 00:02:53,43
Now,

98
00:02:53,43 --> 00:02:56,37
some miscellaneous points is that actually some of the assembly

99
00:02:56,37 --> 00:02:57,75
instructions we saw before?

100
00:02:57,75 --> 00:03:00,43
We just simplified things like set the interrupt flag and

101
00:03:00,43 --> 00:03:03,62
clear the interrupt flag actually are not possible to be

102
00:03:03,62 --> 00:03:06,37
done unless your CPL is less than or equal to

103
00:03:06,37 --> 00:03:07,14
the IOPL

104
00:03:07,17 --> 00:03:11,66
So those assembly instructions are actually restricted behind IOPL privileges

105
00:03:11,67 --> 00:03:13,56
Also in case you're thinking to yourself,

106
00:03:13,56 --> 00:03:15,83
well I think I'll just use popfq to change

107
00:03:15,83 --> 00:03:18,04
the IOPL to 3 and then I can

108
00:03:18,04 --> 00:03:18,7
use port I/O

109
00:03:18,7 --> 00:03:18,87


110
00:03:18,87 --> 00:03:19,66
From ring 3

111
00:03:19,94 --> 00:03:22,09
That's not going to work because it turns out that

112
00:03:22,09 --> 00:03:25,65
popfq will not change the IOPL field unless you're

113
00:03:25,65 --> 00:03:26,68
already in ring 0

114
00:03:26,69 --> 00:03:27,11
All right

115
00:03:27,11 --> 00:03:30,78
Here's the in assembly instruction and it is something that

116
00:03:30,78 --> 00:03:33,2
is restricted most of the time to ring 0

117
00:03:33,2 --> 00:03:35,39
It's not technically it depends on what the IOPL is

118
00:03:35,39 --> 00:03:37,13
but we'll just say it's a ring 0 thing because

119
00:03:37,13 --> 00:03:38,36
that's how people tend to set it

120
00:03:39,04 --> 00:03:40,4
And there are two forms here,

121
00:03:40,59 --> 00:03:43,5
there's one form which takes an immediate 8 bit value

122
00:03:43,5 --> 00:03:44,41
for the port,

123
00:03:44,42 --> 00:03:46,94
which is going to be used for reading in from

124
00:03:46,95 --> 00:03:49,48
and it can read in a byte or 2 bytes

125
00:03:49,48 --> 00:03:50,56
or 4 bytes

126
00:03:51,04 --> 00:03:54,54
And then there's another form that uses dx to specify

127
00:03:54,54 --> 00:03:55,91
the port and again a byte

128
00:03:55,92 --> 00:03:57,46
2 bytes or 4 bytes

129
00:03:58,14 --> 00:03:59,6
Note that this is dx

130
00:03:59,6 --> 00:04:01,97
So this is the 16-bit value,

131
00:04:01,98 --> 00:04:05,16
lowest 16 bits of edx or rdx

132
00:04:05,64 --> 00:04:08,8
And so that means that this is the only form

133
00:04:08,8 --> 00:04:10,82
that can be used to access all 2 to the

134
00:04:10,82 --> 00:04:11,76
16 ports

135
00:04:12,14 --> 00:04:14,79
An immediate 8 bit port number will not allow you

136
00:04:14,79 --> 00:04:16,48
to access the upper bytes

137
00:04:16,49 --> 00:04:17,68
The upper port range

138
00:04:17,69 --> 00:04:18,19
Also,

139
00:04:18,19 --> 00:04:21,13
the manual says that when accessing the 16 or 32

140
00:04:21,13 --> 00:04:22,03
bit port I/O,

141
00:04:22,15 --> 00:04:25,34
the upper and size attribute determines the port size

142
00:04:25,35 --> 00:04:27,91
So that upper and size attribute was one of those

143
00:04:27,91 --> 00:04:32,75
things associated to help disambiguate for the disk for the

144
00:04:32,75 --> 00:04:33,49
processor

145
00:04:33,49 --> 00:04:34,91
Like if it sees ED,

146
00:04:34,92 --> 00:04:37,23
how does it know whether it's 16 bit value or

147
00:04:37,23 --> 00:04:38,52
a 32 bit value?

148
00:04:38,59 --> 00:04:40,68
That was another one of those things that comes from

149
00:04:40,68 --> 00:04:41,54
segmentation

150
00:04:41,55 --> 00:04:44,01
So it comes from segmentation and there is a default

151
00:04:44,01 --> 00:04:46,74
size associated with based on your segment,

152
00:04:46,87 --> 00:04:49,97
whether you have a 16-bit segment or 32-bit

153
00:04:49,97 --> 00:04:50,39
segment,

154
00:04:50,4 --> 00:04:52,79
but there is actually ways to override that

155
00:04:52,79 --> 00:04:57,18
So you could access Just 16 16 bits instead of 32

156
00:04:57,18 --> 00:05:03,06
bits using instruction operand size override prefixes,

157
00:05:03,44 --> 00:05:06,35
which we'll talk about later on as some optional material

158
00:05:06,57 --> 00:05:09,24
So paired with the in there is an out assembly

159
00:05:09,24 --> 00:05:13,31
instruction And it basically has the same caveats as with

160
00:05:13,31 --> 00:05:16,68
respect to dx 16-bit value,

161
00:05:16,68 --> 00:05:18,95
putting that out as the output port

162
00:05:19,34 --> 00:05:21,44
So if you wanted a 32-bit value and you

163
00:05:21,44 --> 00:05:24,47
wanted to output to a 16-bit port address,

164
00:05:24,48 --> 00:05:26,18
you need to use the dx form

165
00:05:26,31 --> 00:05:29,84
Otherwise you could use the 8 bit immediate port form

166
00:05:29,9 --> 00:05:32,21
The next question then would be how do you learn

167
00:05:32,21 --> 00:05:36,31
about which ports correspond to which hardware devices and for

168
00:05:36,31 --> 00:05:36,51
that?

169
00:05:36,51 --> 00:05:37,56
I can tell you right now

170
00:05:37,56 --> 00:05:41,76
One does not simply read the intel software development manual

171
00:05:42,44 --> 00:05:45,25
Instead you have to consult what's called the chip set

172
00:05:45,25 --> 00:05:48,91
documentation and we talk about this a lot more in

173
00:05:48,91 --> 00:05:51,27
a lot more detail in architecture 4001

174
00:05:51,27 --> 00:05:54,08
So I'm just giving you the briefest of overviews now

175
00:05:54,09 --> 00:05:55,77
But basically back in the day,

176
00:05:55,77 --> 00:05:59,05
there used to be a literal chip set of system

177
00:05:59,06 --> 00:06:00,77
of chips on your machine

178
00:06:00,78 --> 00:06:02,07
You had your CPU,

179
00:06:02,07 --> 00:06:04,14
you had what was called the memory controller hub and

180
00:06:04,14 --> 00:06:05,35
you had what was called the

181
00:06:05,35 --> 00:06:06,27
I/O controller hub

182
00:06:06,28 --> 00:06:06,84
Over time

183
00:06:06,84 --> 00:06:09,77
intel then merged the I/O and memory controller hub and

184
00:06:09,77 --> 00:06:11,66
put some of the memory controller hub stuff into the

185
00:06:11,66 --> 00:06:12,3
CPU

186
00:06:12,31 --> 00:06:15,29
And then you landed on the CPU and the platform

187
00:06:15,29 --> 00:06:16,15
controller hub

188
00:06:16,16 --> 00:06:17,68
And further as time goes along,

189
00:06:17,68 --> 00:06:20,33
things tend to move towards system on a chip where

190
00:06:20,33 --> 00:06:22,86
everything is all combined into a single processor

191
00:06:26,84 --> 00:06:29,76
So this is what you tend to see on laptops

192
00:06:29,76 --> 00:06:30,87
and embedded systems,

193
00:06:30,87 --> 00:06:34,56
but desktop system still typically have a discreet platform controller

194
00:06:34,56 --> 00:06:36,56
hub separate from the CPU

195
00:06:37,04 --> 00:06:37,9
So then the question is,

196
00:06:37,9 --> 00:06:41,01
where do you find in these things the documentation of

197
00:06:41,01 --> 00:06:41,56
port I/O?

198
00:06:41,94 --> 00:06:42,29
Well,

199
00:06:42,3 --> 00:06:43,48
back when it was the

200
00:06:43,48 --> 00:06:44,71
I/O controller hub,

201
00:06:44,72 --> 00:06:47,79
you would find what ports correspond to what hardware in

202
00:06:47,79 --> 00:06:50,43
the I/O controller hub documentation these days with a

203
00:06:50,43 --> 00:06:51,68
platform controller hub

204
00:06:51,68 --> 00:06:54,36
You use the platform controller hub documentation

205
00:06:54,37 --> 00:06:57,34
And if you have a sockish type system or

206
00:06:57,34 --> 00:06:58,22
some of the newer,

207
00:06:58,22 --> 00:06:58,53
just,

208
00:06:58,53 --> 00:06:58,72
you know,

209
00:06:58,72 --> 00:07:01,37
it could be a desktop system that just is everything

210
00:07:01,37 --> 00:07:01,97
all in one

211
00:07:01,98 --> 00:07:03,52
There will be a thing called the

212
00:07:03,52 --> 00:07:04,35
I/O data sheet

213
00:07:04,44 --> 00:07:07,45
And in those sort of documentation you will find things

214
00:07:07,45 --> 00:07:09,91
like those big tables of a bunch of port

215
00:07:09,91 --> 00:07:13,53
I/O addresses and they'll tell you roughly what it's configured

216
00:07:13,53 --> 00:07:13,79
to

217
00:07:13,79 --> 00:07:16,63
So this would say LPC super,

218
00:07:16,63 --> 00:07:17,36
I/O chip

219
00:07:17,74 --> 00:07:20,64
This says these ports are for the interrupt controller

220
00:07:20,65 --> 00:07:23,99
This port is not actually really specified in a meaningful

221
00:07:23,99 --> 00:07:24,31
way,

222
00:07:24,31 --> 00:07:26,83
but I can tell you port 60 has to do

223
00:07:26,83 --> 00:07:31,19
with the old 8042 keyboard controller from the original

224
00:07:31,19 --> 00:07:34,54
IBM Pcs and then there's sort of the Intel hardware

225
00:07:34,54 --> 00:07:37,13
like emulates that capability and newer things

226
00:07:37,14 --> 00:07:39,92
But what we want to cover next is a look

227
00:07:39,92 --> 00:07:41,07
into this one

228
00:07:41,08 --> 00:07:43,81
This is the nonmaskable interrupt controller,

229
00:07:43,81 --> 00:07:45,75
which we're going to ignore and do the real time

230
00:07:45,75 --> 00:07:46,22
controller

231
00:07:46,23 --> 00:07:49,5
So it was a bit for nonmaskable interrupts that

232
00:07:49,5 --> 00:07:52,76
got merged in with bits for the Real time clock

233
00:07:52,77 --> 00:07:53,38
controller,

234
00:07:53,38 --> 00:07:56,41
which got merged in with bits for just some arbitrary

235
00:07:56,41 --> 00:07:57,39
storage memory

236
00:07:57,47 --> 00:07:59,61
So we'll cover this one in the next section

237
00:07:59,62 --> 00:08:02,36
This has to do with ports 70 through 77

