1
00:00:00,14 --> 00:00:00,38
All right

2
00:00:00,38 --> 00:00:01,52
Let's go look at our lab,

3
00:00:01,53 --> 00:00:03,26
K_CMOS_PortIO

8
00:00:03,64 --> 00:00:05,7
And you're going to compile that and run it on

9
00:00:05,7 --> 00:00:05,85
the

10
00:00:05,85 --> 00:00:07,86
debugger and then look at the output in WinDbg

11
00:00:08,24 --> 00:00:10,57
and what you hope to see is that the time

12
00:00:10,57 --> 00:00:13,75
information matches up with the stuff that we set back

13
00:00:13,75 --> 00:00:15,85
here index 0 should be seconds,

14
00:00:15,85 --> 00:00:17,26
2 minutes etc

15
00:00:18,64 --> 00:00:21,68
So let's take a look at that in your labs

16
00:00:21,68 --> 00:00:23,13
Go to K_CMOS_PortIO

19
00:00:23,13 --> 00:00:26,7
open up Source.c And the CodeOfConsequence

20
00:00:26,7 --> 00:00:28,31
is relatively small here

21
00:00:28,49 --> 00:00:31,65
We've just got a 256 byte array because we're going

22
00:00:31,65 --> 00:00:34,65
to read both the standard and the extended CMOS bytes

23
00:00:35,04 --> 00:00:36,51
Then we got some indices

24
00:00:36,51 --> 00:00:38,25
We're going to do some for loops

25
00:00:38,51 --> 00:00:40,88
1st for loop is going to read the standard bytes

26
00:00:40,99 --> 00:00:41,95
and to do that

27
00:00:41,96 --> 00:00:46,62
It is going to write out a single byte yo

28
00:00:46,62 --> 00:00:49,62
the Index Register 70 And so it's going to write

29
00:00:49,62 --> 00:00:52,08
out byte i, i starts at 0

30
00:00:52,08 --> 00:00:55,96
So we're starting at index 0 into Index into Port

31
00:00:55,96 --> 00:00:56,46
70

32
00:00:56,94 --> 00:00:59,85
Then we are going to read in a single byte

33
00:01:00,24 --> 00:01:01,64
From Port 71

34
00:01:01,89 --> 00:01:02,9
So behind the scenes,

35
00:01:02,9 --> 00:01:04,74
this is an in assembly instruction,

36
00:01:04,74 --> 00:01:05,37
this is just,

37
00:01:05,38 --> 00:01:05,62
you know,

38
00:01:05,62 --> 00:01:09,83
the compiler intrinsic in assembly instruction with a byte size

39
00:01:09,99 --> 00:01:11,41
from Port 71,

40
00:01:11,64 --> 00:01:14,74
that's going to go back into the CMOS character

41
00:01:14,74 --> 00:01:16,82
array then to read the extended portion,

42
00:01:16,82 --> 00:01:18,74
we're going to have another index j,

43
00:01:18,74 --> 00:01:21,8
which we're going to start at 0 in to port

44
00:01:21,8 --> 00:01:24,98
72 instead of 70 because that's how we access the

45
00:01:24,98 --> 00:01:25,96
extended area

46
00:01:26,54 --> 00:01:28,62
And then we are going to read in from port

47
00:01:28,62 --> 00:01:31,88
73 instead of 71 and that's going to be put

48
00:01:31,88 --> 00:01:34,91
into CMOS that i so i just keep incrementing

49
00:01:35,34 --> 00:01:38,55
j will give us another loop Through the bottom through

50
00:01:38,55 --> 00:01:40,46
these extended 128 bytes

51
00:01:41,34 --> 00:01:43,05
So let's go ahead and compile that

52
00:01:44,44 --> 00:01:46,75
And then we're going to take the output from the

53
00:01:46,75 --> 00:01:47,58
compilation,

54
00:01:47,59 --> 00:01:49,12
copy it over to our VM and see it in

55
00:01:49,12 --> 00:01:49,56
the debugger

56
00:01:50,14 --> 00:01:51,7
So as usual,

57
00:01:51,71 --> 00:01:56,36
copy the entire path of your compiled data,

58
00:01:56,84 --> 00:01:57,95
compiled Executable

59
00:01:58,94 --> 00:02:00,26
Put it into explorer,

60
00:02:00,36 --> 00:02:02,15
grab 3 binaries

61
00:02:03,24 --> 00:02:03,99
I already did

62
00:02:03,99 --> 00:02:05,16
it, forgot to clean it up

63
00:02:05,74 --> 00:02:05,85
Well,

64
00:02:05,85 --> 00:02:10,53
here's my new binary and then you can just go

65
00:02:10,53 --> 00:02:11,28
up to this,

66
00:02:11,45 --> 00:02:12,26
it installed

67
00:02:13,04 --> 00:02:16,57
Run it and go check out my output over in

68
00:02:16,57 --> 00:02:17,08
WinDbg.

69
00:02:17,08 --> 00:02:19,06
So here is the output streaming out

70
00:02:19,94 --> 00:02:22,43
So let's go up and see what we see as

71
00:02:22,43 --> 00:02:24,16
the time when it was actually started

72
00:02:24,84 --> 00:02:26,76
We go back to index 0,

73
00:02:28,64 --> 00:02:29,75
just enough space

74
00:02:30,24 --> 00:02:31,32
So index zero,

75
00:02:31,32 --> 00:02:32,25
it said seconds

76
00:02:32,25 --> 00:02:33,23
15 minutes,

77
00:02:33,23 --> 00:02:34,97
53 hours

78
00:02:34,97 --> 00:02:35,41
11

79
00:02:35,41 --> 00:02:38,76
So 11 53 and 50 and 15 seconds

80
00:02:39,34 --> 00:02:40,97
And indeed in my VM,

81
00:02:40,97 --> 00:02:43,96
11 53 and however many seconds was

82
00:02:44,34 --> 00:02:45,69
And then this is 6,

83
00:02:45,69 --> 00:02:45,98
15,

84
00:02:45,98 --> 00:02:46,26
2021,

85
00:02:47,04 --> 00:02:49,05
So 2021,

86
00:02:49,06 --> 00:02:49,7
6, 15

87
00:02:49,71 --> 00:02:52,66
So that CMOS data exactly matches the real-time

88
00:02:52,66 --> 00:02:53,95
clock information we expect

89
00:02:54,44 --> 00:02:57,16
And then we have a bunch of other random data

90
00:02:57,16 --> 00:02:58,38
and what is it?

91
00:02:58,58 --> 00:02:59,26
I don't know

92
00:02:59,64 --> 00:03:00,66
How would you find out

93
00:03:01,04 --> 00:03:01,41
Well,

94
00:03:01,42 --> 00:03:03,81
you would have to do something like set a hardware

95
00:03:03,81 --> 00:03:05,35
breakpoint on port

96
00:03:05,35 --> 00:03:05,75
I/O

97
00:03:06,14 --> 00:03:09,95
For the ports 70 and or 72

98
00:03:09,96 --> 00:03:10,72
And then,

99
00:03:10,72 --> 00:03:11,03
you know,

100
00:03:11,03 --> 00:03:13,92
just set the debugger to basically say like,

101
00:03:13,92 --> 00:03:16,35
let's check what the and then you could say like

102
00:03:16,35 --> 00:03:18,81
let's check the particular value in particular registers

103
00:03:18,81 --> 00:03:21,45
And if it's the value for the particular index that

104
00:03:21,45 --> 00:03:22,46
you're interested in,

105
00:03:22,84 --> 00:03:24,62
then you would say,

106
00:03:24,62 --> 00:03:25,15
okay,

107
00:03:25,54 --> 00:03:27,96
now I can go look at the code in there

108
00:03:28,34 --> 00:03:31,36
operating system or whatever that's actually accessing this

109
00:03:31,94 --> 00:03:32,16
Alright,

110
00:03:32,16 --> 00:03:35,6
let's look beyond the base range and into the extended

111
00:03:35,6 --> 00:03:36,19
range,

112
00:03:36,28 --> 00:03:39,51
and we do indeed see some additional data being used

113
00:03:39,51 --> 00:03:40,54
here again

114
00:03:40,54 --> 00:03:43,35
What is the data I have no idea only way to find

115
00:03:43,35 --> 00:03:48,46
out is to basically reverse engineer something statically or dynamically

116
00:03:48,94 --> 00:03:51,99
dynamically would be using these hardware breakpoints to see what

117
00:03:51,99 --> 00:03:52,26
it is

