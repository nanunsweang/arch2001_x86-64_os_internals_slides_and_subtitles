1
00:00:00,24 --> 00:00:03,15
A little more non standard example of port IO

2
00:00:03,64 --> 00:00:07,02
So folks had reverse engineered VMware to find what

3
00:00:07,02 --> 00:00:07,58
they called

4
00:00:07,58 --> 00:00:08,63
A backdoor,

5
00:00:08,64 --> 00:00:08,84
IO

6
00:00:08,84 --> 00:00:09,43
port

7
00:00:09,44 --> 00:00:11,93
It's not exactly a backdoor in the malicious sense of

8
00:00:11,93 --> 00:00:12,29
the word

9
00:00:12,38 --> 00:00:15,91
It's just some mechanism that VMware added into the

10
00:00:15,91 --> 00:00:19,87
virtualization system so that it responds to particular commands sent

11
00:00:19,87 --> 00:00:20,83
on a given port

12
00:00:20,83 --> 00:00:21,36
IO port

13
00:00:21,64 --> 00:00:25,95
So I've chosen to use the commands for reading the

14
00:00:25,96 --> 00:00:29,76
copy paste buffer that's used when copying into and out

15
00:00:29,76 --> 00:00:31,04
of a particular VM

16
00:00:31,05 --> 00:00:34,96
And there's also some optional no operating system found dialog

17
00:00:34,96 --> 00:00:35,95
box that you can pop up

18
00:00:36,34 --> 00:00:38,88
But if you want to find more commands and fiddle

19
00:00:38,88 --> 00:00:41,5
around this link will be on the website for you

20
00:00:41,5 --> 00:00:42,66
to go read about that

21
00:00:43,34 --> 00:00:46,16
So let's go see what this code looks like

22
00:00:46,74 --> 00:00:51,08
So go down to VMware_PortIO and driver.c

23
00:00:51,08 --> 00:00:51,46


24
00:00:51,63 --> 00:00:54,86
And inside of here we've got a few assembly functions

25
00:00:54,87 --> 00:00:56,55
What do those assembly functions do?

26
00:00:57,34 --> 00:00:57,5
Well,

27
00:00:57,5 --> 00:01:00,36
here's the OS_popup which is commented out right

28
00:01:00,36 --> 00:01:04,98
now let's get down to one is a command to

29
00:01:04,98 --> 00:01:06,65
get the keyboard,

30
00:01:06,66 --> 00:01:08,9
clipboard text length

31
00:01:08,9 --> 00:01:11,65
So like how much text is in the clipboard right

32
00:01:11,65 --> 00:01:11,96
now,

33
00:01:12,44 --> 00:01:15,26
another is to get the actual clipboard text itself

34
00:01:16,04 --> 00:01:16,31
Now,

35
00:01:16,31 --> 00:01:20,35
the particular magic value that VMware wants to find

36
00:01:20,35 --> 00:01:23,32
in eax at the time of port IO is this value

37
00:01:23,32 --> 00:01:25,55
just found by people reverse engineering?

38
00:01:26,04 --> 00:01:28,33
It expects a command in

39
00:01:28,33 --> 00:01:28,86
cx

40
00:01:29,03 --> 00:01:30,7
And then the port

41
00:01:30,7 --> 00:01:32,96
IO port is 5658

42
00:01:33,74 --> 00:01:35,71
So if you set up things like this,

43
00:01:35,71 --> 00:01:37,48
so that there is a particular command in

44
00:01:37,48 --> 00:01:37,85
cx

45
00:01:38,24 --> 00:01:42,02
And then you just do a in assembly instruction from

46
00:01:42,02 --> 00:01:42,39
dX

47
00:01:42,39 --> 00:01:45,5
which is 5658 to eax

48
00:01:45,5 --> 00:01:47,45
So this is going to be the 4 byte read

49
00:01:47,45 --> 00:01:47,84
in

50
00:01:48,18 --> 00:01:50,86
This will give you the clipboard text length

51
00:01:50,87 --> 00:01:51,75
Once you have that,

52
00:01:51,75 --> 00:01:55,06
then you could dynamically allocate a buffer or just know how

53
00:01:55,06 --> 00:01:57,26
big the amount of data is to read in

54
00:01:57,34 --> 00:02:01,05
And you can instead run this getting the actual data command

55
00:02:01,44 --> 00:02:02,24
the same thing

56
00:02:02,25 --> 00:02:05,22
Magic number in the eax command in

57
00:02:05,22 --> 00:02:05,66
cx

58
00:02:05,83 --> 00:02:07,63
Same port 5658

59
00:02:07,64 --> 00:02:10,15
And again you read in 4 bytes or you could

60
00:02:10,15 --> 00:02:12,77
read 1 byte at a time and this will read

61
00:02:12,77 --> 00:02:14,16
in the actual clipboard text

62
00:02:14,64 --> 00:02:16,72
So let's go ahead and build that

63
00:02:16,72 --> 00:02:19,04
And then the key thing is that we're going to

64
00:02:19,04 --> 00:02:21,56
want to copy in some actual texts were going to

65
00:02:21,71 --> 00:02:25,25
copy over the binary data right now to copy are

66
00:02:25,37 --> 00:02:26,53
executables

67
00:02:26,6 --> 00:02:29,15
We want to make sure we get some text into

68
00:02:29,15 --> 00:02:33,41
the clipboard buffer before we actually run this thing

69
00:02:33,41 --> 00:02:35,03
Otherwise we're not going to see what we want to

70
00:02:35,03 --> 00:02:35,25
see

71
00:02:36,24 --> 00:02:38,36
So let's go ahead and put that there

72
00:02:39,74 --> 00:02:41,46
Run the the commands to install it

73
00:02:41,46 --> 00:02:47,94
Oops, there it don't install because I forgot

74
00:02:47,95 --> 00:02:49,91
to copy some text

75
00:02:49,91 --> 00:02:54,64
So let's copy some text and this time let's run

76
00:02:54,64 --> 00:03:00,72
it accept and then let's look at what we see

77
00:03:00,73 --> 00:03:01,15
in

78
00:03:02,44 --> 00:03:05,35
So it was complaining that the data wasn't a text

79
00:03:06,44 --> 00:03:09,41
Think it might have to actually be text that comes

80
00:03:09,41 --> 00:03:11,87
in from the outside

81
00:03:11,87 --> 00:03:15,25
So I'm just gonna copy this clipboard and there you

82
00:03:15,25 --> 00:03:15,4
go

83
00:03:15,4 --> 00:03:17,66
You can see that my clipboard contains clipboard

84
00:03:18,24 --> 00:03:21,34
So let's remove this reinstall

85
00:03:21,34 --> 00:03:30,84
It reinstall doesn't prom because I already did it

86
00:03:30,85 --> 00:03:31,86
And there we go

87
00:03:32,34 --> 00:03:35,35
Is this your clipboard data clipboard

88
00:03:37,74 --> 00:03:40,77
So that's just a fun little miscellaneous use of port IO

89
00:03:40,77 --> 00:03:44,06
to access some mechanism that VMware is built into

90
00:03:44,06 --> 00:03:45,36
its virtualization system

