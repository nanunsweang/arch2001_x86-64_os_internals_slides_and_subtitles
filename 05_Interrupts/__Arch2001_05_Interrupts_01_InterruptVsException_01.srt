1
00:00:00,399 --> 00:00:04,080
okay so for this new topic interrupts

2
00:00:02,320 --> 00:00:05,759
and exceptions we're going to start out

3
00:00:04,080 --> 00:00:06,560
a little bit wordy and read to you from

4
00:00:05,759 --> 00:00:08,240
the manual

5
00:00:06,560 --> 00:00:09,840
and then we'll you know reiterate

6
00:00:08,240 --> 00:00:11,920
throughout the rest of this section

7
00:00:09,840 --> 00:00:13,519
exactly what the manual is trying to say

8
00:00:11,920 --> 00:00:15,280
so interrupts and exceptions

9
00:00:13,519 --> 00:00:16,640
are events that indicate a condition

10
00:00:15,280 --> 00:00:18,800
exists somewhere in the system

11
00:00:16,640 --> 00:00:20,000
the processor or within the executing

12
00:00:18,800 --> 00:00:21,520
program or task

13
00:00:20,000 --> 00:00:24,240
that requires the attention of the

14
00:00:21,520 --> 00:00:26,000
processor when an interrupt is received

15
00:00:24,240 --> 00:00:28,000
and an exception is detected

16
00:00:26,000 --> 00:00:29,039
the currently running procedure or task

17
00:00:28,000 --> 00:00:30,880
is suspended

18
00:00:29,039 --> 00:00:32,640
while the processor executes an

19
00:00:30,880 --> 00:00:34,160
interrupt or exception handler

20
00:00:32,640 --> 00:00:36,079
when the execution of the handler is

21
00:00:34,160 --> 00:00:38,079
complete the procedure resumes execution

22
00:00:36,079 --> 00:00:39,760
of the interrupted procedural task

23
00:00:38,079 --> 00:00:41,600
and the processor receives interrupts

24
00:00:39,760 --> 00:00:43,120
from two sources external hardware

25
00:00:41,600 --> 00:00:44,559
generated interrupts and software

26
00:00:43,120 --> 00:00:46,399
generated interrupts

27
00:00:44,559 --> 00:00:48,480
okay so what is this saying we've got

28
00:00:46,399 --> 00:00:50,800
interrupts and we've got exceptions

29
00:00:48,480 --> 00:00:52,320
and what it's saying is for both of them

30
00:00:50,800 --> 00:00:54,239
they both have to do with when

31
00:00:52,320 --> 00:00:56,160
something just happened on the system

32
00:00:54,239 --> 00:00:59,280
and it needs to halt whatever it's doing

33
00:00:56,160 --> 00:01:02,239
and go off and handle whatever happened

34
00:00:59,280 --> 00:01:03,760
so I kind of break it up like this so

35
00:01:02,239 --> 00:01:05,119
we've got an event indicating a

36
00:01:03,760 --> 00:01:06,240
condition exists somewhere in the system

37
00:01:05,119 --> 00:01:07,040
that requires the attention of the

38
00:01:06,240 --> 00:01:09,040
processor

39
00:01:07,040 --> 00:01:10,400
and there's two types there's interrupts

40
00:01:09,040 --> 00:01:12,640
and there's exceptions

41
00:01:10,400 --> 00:01:13,439
and of the interrupts there are hardware

42
00:01:12,640 --> 00:01:15,200
interrupts

43
00:01:13,439 --> 00:01:16,720
and then there's going to be software

44
00:01:15,200 --> 00:01:19,119
interrupts that occur because of

45
00:01:16,720 --> 00:01:21,040
assembly instructions for instance

46
00:01:19,119 --> 00:01:22,640
then of the exceptions there's three

47
00:01:21,040 --> 00:01:24,880
types there's faults

48
00:01:22,640 --> 00:01:26,080
there's traps and there's aborts and

49
00:01:24,880 --> 00:01:28,880
we'll talk about each of those

50
00:01:26,080 --> 00:01:30,320
in a little bit so what is the

51
00:01:28,880 --> 00:01:31,840
difference between an interrupt and

52
00:01:30,320 --> 00:01:33,360
exception if they're both conditions

53
00:01:31,840 --> 00:01:34,960
that require interrupting whatever

54
00:01:33,360 --> 00:01:36,240
you're doing and jumping off somewhere

55
00:01:34,960 --> 00:01:38,640
and handling it

56
00:01:36,240 --> 00:01:40,320
well generally speaking exceptions

57
00:01:38,640 --> 00:01:42,479
typically indicate some sort of

58
00:01:40,320 --> 00:01:43,520
error condition whereas interrupts are

59
00:01:42,479 --> 00:01:45,920
more often than not

60
00:01:43,520 --> 00:01:47,680
caused by external hardware saying like

61
00:01:45,920 --> 00:01:49,439
hey someone pressed a key

62
00:01:47,680 --> 00:01:51,920
on a keyboard please you know handle

63
00:01:49,439 --> 00:01:53,520
this key now hey someone sent a packet

64
00:01:51,920 --> 00:01:55,200
to the network card please handle this

65
00:01:53,520 --> 00:01:58,240
packet now so

66
00:01:55,200 --> 00:01:59,439
you can sort of think e for exceptions e

67
00:01:58,240 --> 00:02:01,119
for errors

68
00:01:59,439 --> 00:02:02,560
exceptions are typically error

69
00:02:01,119 --> 00:02:03,360
conditions that need to be handled and

70
00:02:02,560 --> 00:02:05,680
interrupts are

71
00:02:03,360 --> 00:02:07,759
different things also we'll learn later

72
00:02:05,680 --> 00:02:09,679
about a thing called the interrupt flag

73
00:02:07,759 --> 00:02:11,440
and interrupts clear the interrupt flag

74
00:02:09,679 --> 00:02:12,800
so that you don't have recursive

75
00:02:11,440 --> 00:02:14,720
interrupts happening interrupting

76
00:02:12,800 --> 00:02:18,000
interrupting interrupting each other

77
00:02:14,720 --> 00:02:20,959
but exceptions don't but we'll cover

78
00:02:18,000 --> 00:02:23,200
interrupt flag in a little bit later so

79
00:02:20,959 --> 00:02:23,599
of these 3 categories of exceptions

80
00:02:23,200 --> 00:02:26,080
they're

81
00:02:23,599 --> 00:02:27,520
sort of unmemorable and hopefully I'll

82
00:02:26,080 --> 00:02:29,520
provide you a thing to make it a little

83
00:02:27,520 --> 00:02:32,160
bit more memorable here in a second

84
00:02:29,520 --> 00:02:33,200
but the categories were faults traps and

85
00:02:32,160 --> 00:02:35,120
aborts

86
00:02:33,200 --> 00:02:37,360
the fault was a recoverable type of

87
00:02:35,120 --> 00:02:38,480
exception and it's recoverable because

88
00:02:37,360 --> 00:02:41,680
the rip

89
00:02:38,480 --> 00:02:42,560
which points at the specific instruction

90
00:02:41,680 --> 00:02:44,640
that caused

91
00:02:42,560 --> 00:02:46,400
the exception is going to be pushed onto

92
00:02:44,640 --> 00:02:48,239
the stack so basically it's saying you

93
00:02:46,400 --> 00:02:49,519
know this thing right here is where a

94
00:02:48,239 --> 00:02:51,599
problem occurred

95
00:02:49,519 --> 00:02:53,040
go handle it and then you can come back

96
00:02:51,599 --> 00:02:55,760
later and return

97
00:02:53,040 --> 00:02:56,959
execution flow there if appropriate a

98
00:02:55,760 --> 00:02:59,280
trap on the other hand

99
00:02:56,959 --> 00:03:00,400
is also recoverable but the difference

100
00:02:59,280 --> 00:03:03,040
is that whereas

101
00:03:00,400 --> 00:03:04,000
a fault pointed the rip at the assembly

102
00:03:03,040 --> 00:03:07,200
instruction that caused

103
00:03:04,000 --> 00:03:09,360
the exception a fault points the rip at

104
00:03:07,200 --> 00:03:11,040
the assembly instruction after the

105
00:03:09,360 --> 00:03:12,480
instruction so basically

106
00:03:11,040 --> 00:03:14,159
fault is like saying this thing right

107
00:03:12,480 --> 00:03:16,000
here caused the problem and

108
00:03:14,159 --> 00:03:17,680
trap is saying yeah a problem just

109
00:03:16,000 --> 00:03:19,920
occurred but I had to continue this

110
00:03:17,680 --> 00:03:22,159
assembly instruction and I moved on

111
00:03:19,920 --> 00:03:24,319
but you know just so you know now here

112
00:03:22,159 --> 00:03:26,400
the the rip is pointing at the next

113
00:03:24,319 --> 00:03:28,879
assembly instruction

114
00:03:26,400 --> 00:03:29,840
and the third category a abort is

115
00:03:28,879 --> 00:03:32,159
unrecoverable

116
00:03:29,840 --> 00:03:34,319
and it may or may not actually save the

117
00:03:32,159 --> 00:03:36,319
rip it depends on the particular type of

118
00:03:34,319 --> 00:03:38,400
error that occurred

119
00:03:36,319 --> 00:03:39,440
so to help you try to remember this

120
00:03:38,400 --> 00:03:42,959
we're going to go with these

121
00:03:39,440 --> 00:03:44,720
mnemonic devices a fault rip points at

122
00:03:42,959 --> 00:03:47,120
the faulting instruction just like

123
00:03:44,720 --> 00:03:50,640
simpsons hitler here pointing at bobo

124
00:03:47,120 --> 00:03:52,239
saying this is all your fault

125
00:03:50,640 --> 00:03:54,480
but it wasn't bobo's fault it was

126
00:03:52,239 --> 00:03:56,239
hitler's fault so rip

127
00:03:54,480 --> 00:03:59,200
points at a faulting assembly

128
00:03:56,239 --> 00:04:02,319
instruction fault hitler hitler's fault

129
00:03:59,200 --> 00:04:05,040
it was hitler's fault and rip points at

130
00:04:02,319 --> 00:04:08,640
the faulting assembly instruction

131
00:04:05,040 --> 00:04:11,519
it's a trap so rip for a trap

132
00:04:08,640 --> 00:04:11,840
points at the assembly instruction after

133
00:04:11,519 --> 00:04:13,760
the

134
00:04:11,840 --> 00:04:15,920
trapping instructions so something

135
00:04:13,760 --> 00:04:17,919
happened and after the fact

136
00:04:15,920 --> 00:04:19,519
they tell you it's a trap but it's too

137
00:04:17,919 --> 00:04:22,560
late it's a trap

138
00:04:19,519 --> 00:04:25,680
and the trap has already been sprung

139
00:04:22,560 --> 00:04:26,479
finally aborts rip may or may not be

140
00:04:25,680 --> 00:04:29,280
recoverable

141
00:04:26,479 --> 00:04:31,360
abort abort because there's nothing you

142
00:04:29,280 --> 00:04:33,600
can do about it

143
00:04:31,360 --> 00:04:35,280
so back to the categories something

144
00:04:33,600 --> 00:04:36,240
indicating something happened that needs

145
00:04:35,280 --> 00:04:38,240
the processors

146
00:04:36,240 --> 00:04:39,600
attention interrupts from hardware and

147
00:04:38,240 --> 00:04:40,400
assembly instructions which we'll see

148
00:04:39,600 --> 00:04:43,199
more later

149
00:04:40,400 --> 00:04:44,960
exceptions fault hitler's fault points

150
00:04:43,199 --> 00:04:45,759
at the assembly instruction that is

151
00:04:44,960 --> 00:04:48,160
faulting

152
00:04:45,759 --> 00:04:49,440
trap it's a trap it's too late the

153
00:04:48,160 --> 00:04:51,680
assembly instruction has already

154
00:04:49,440 --> 00:04:52,479
occurred rip points at the next assembly

155
00:04:51,680 --> 00:04:54,720
instruction

156
00:04:52,479 --> 00:04:55,600
and abort abort there's nothing you can

157
00:04:54,720 --> 00:04:58,639
do about it

158
00:04:55,600 --> 00:05:00,320
the ship is going down so

159
00:04:58,639 --> 00:05:02,240
what happens when these sort of things

160
00:05:00,320 --> 00:05:04,639
interrupts and exceptions occur

161
00:05:02,240 --> 00:05:06,160
is that the processor needs to save some

162
00:05:04,639 --> 00:05:07,600
state so that it can say like we're

163
00:05:06,160 --> 00:05:10,000
running we're running we're running

164
00:05:07,600 --> 00:05:12,000
stop jump off somewhere else and come

165
00:05:10,000 --> 00:05:12,960
back later on if we can actually recover

166
00:05:12,000 --> 00:05:14,639
from it

167
00:05:12,960 --> 00:05:16,960
and so you know how is it going to do

168
00:05:14,639 --> 00:05:18,639
this well the hardware saves very little

169
00:05:16,960 --> 00:05:21,199
state just enough to resume

170
00:05:18,639 --> 00:05:22,479
execution of whatever was occurring and

171
00:05:21,199 --> 00:05:24,639
then it's actually the

172
00:05:22,479 --> 00:05:25,680
the responsibility of a exception

173
00:05:24,639 --> 00:05:28,000
handler or a

174
00:05:25,680 --> 00:05:29,440
interrupt handler in order to save any

175
00:05:28,000 --> 00:05:31,520
additional state it needs

176
00:05:29,440 --> 00:05:33,520
in order to get back to the running

177
00:05:31,520 --> 00:05:35,919
state of the system before the exception

178
00:05:33,520 --> 00:05:38,240
or interrupt occurred

179
00:05:35,919 --> 00:05:40,320
so just to show you a little bit of the

180
00:05:38,240 --> 00:05:43,039
how it used to work on 32-bit systems

181
00:05:40,320 --> 00:05:45,280
just to contrast with 64-bit

182
00:05:43,039 --> 00:05:46,720
previously when you had no privilege

183
00:05:45,280 --> 00:05:48,240
level change so there could be

184
00:05:46,720 --> 00:05:49,600
exceptions and aborts where maybe there

185
00:05:48,240 --> 00:05:52,560
would be a privilege level chain

186
00:05:49,600 --> 00:05:53,759
specifically it was interrupts is the

187
00:05:52,560 --> 00:05:54,960
most common case where you're going to

188
00:05:53,759 --> 00:05:56,639
have a privilege level change

189
00:05:54,960 --> 00:05:58,400
typically something might be occurring

190
00:05:56,639 --> 00:05:59,919
you might be running in you know user

191
00:05:58,400 --> 00:06:01,280
space and then an interrupt occurs

192
00:05:59,919 --> 00:06:02,880
because you got a network packet

193
00:06:01,280 --> 00:06:04,720
and it's going to transition into the

194
00:06:02,880 --> 00:06:06,400
kernel because the kernel

195
00:06:04,720 --> 00:06:08,160
is going to be the one responsible for

196
00:06:06,400 --> 00:06:09,440
handling you know a kernel driver that

197
00:06:08,160 --> 00:06:11,600
talks to a network

198
00:06:09,440 --> 00:06:13,199
piece of hardware so there's the no

199
00:06:11,600 --> 00:06:14,639
privilege change version and there's a

200
00:06:13,199 --> 00:06:16,240
privilege change version

201
00:06:14,639 --> 00:06:18,880
when there was no privilege change

202
00:06:16,240 --> 00:06:20,560
previously they wouldn't actually save

203
00:06:18,880 --> 00:06:22,639
the stack pointer or the

204
00:06:20,560 --> 00:06:24,160
stack segment register they would just

205
00:06:22,639 --> 00:06:27,440
save the eflags

206
00:06:24,160 --> 00:06:29,039
cs eip and error code if appropriate not

207
00:06:27,440 --> 00:06:30,000
everything has an error code some do

208
00:06:29,039 --> 00:06:31,680
some don't

209
00:06:30,000 --> 00:06:33,199
and then with a privilege level change

210
00:06:31,680 --> 00:06:34,000
so when they're going for instance from

211
00:06:33,199 --> 00:06:37,520
ring 3 to ring

212
00:06:34,000 --> 00:06:38,800
0 they would save off the stack pointer

213
00:06:37,520 --> 00:06:42,400
as well so the ss

214
00:06:38,800 --> 00:06:43,520
the esp eflag cs eip and error code if

215
00:06:42,400 --> 00:06:46,000
appropriate

216
00:06:43,520 --> 00:06:47,520
so now on 64-bit systems it's no longer

217
00:06:46,000 --> 00:06:50,080
conditional about whether or not

218
00:06:47,520 --> 00:06:51,039
you have a privilege level change they

219
00:06:50,080 --> 00:06:53,599
just always

220
00:06:51,039 --> 00:06:55,039
save the ss and the rsp the rflags the

221
00:06:53,599 --> 00:06:57,039
cs the rip

222
00:06:55,039 --> 00:06:58,319
and error code if the particular thing

223
00:06:57,039 --> 00:07:00,080
takes an error code

224
00:06:58,319 --> 00:07:01,360
and so this picture was just showing you

225
00:07:00,080 --> 00:07:03,599
know the the difference

226
00:07:01,360 --> 00:07:05,759
assuming a privilege level change back

227
00:07:03,599 --> 00:07:08,319
in the day you know 4-byte registers

228
00:07:05,759 --> 00:07:09,520
and now 8-byte registers so this is

229
00:07:08,319 --> 00:07:11,599
what you would expect to see

230
00:07:09,520 --> 00:07:13,360
after some sort of interrupt or exception

231
00:07:11,599 --> 00:07:14,560
occurred the stack would have an error

232
00:07:13,360 --> 00:07:18,319
code if appropriate

233
00:07:14,560 --> 00:07:21,039
rip, cs, rflags, etc.

234
00:07:18,319 --> 00:07:22,720
So you've seen push pop the balanced uh

235
00:07:21,039 --> 00:07:24,080
perfectly balanced set of assembly

236
00:07:22,720 --> 00:07:24,639
instructions for pushing things onto the

237
00:07:24,080 --> 00:07:27,039
stack

238
00:07:24,639 --> 00:07:28,720
popping them off you've seen call ret

239
00:07:27,039 --> 00:07:30,000
call was the thing that redirected

240
00:07:28,720 --> 00:07:32,720
control flow somewhere

241
00:07:30,000 --> 00:07:34,880
else and then ret popped the return

242
00:07:32,720 --> 00:07:38,400
address off the stack and went back

243
00:07:34,880 --> 00:07:40,400
and so now here you go int and iret

244
00:07:38,400 --> 00:07:41,520
perfectly balanced as all things should

245
00:07:40,400 --> 00:07:44,080
be

246
00:07:41,520 --> 00:07:45,280
so an interrupt would occur and the

247
00:07:44,080 --> 00:07:46,879
stack pointer would be

248
00:07:45,280 --> 00:07:48,319
you know pointing somewhere in order to

249
00:07:46,879 --> 00:07:50,639
save information

250
00:07:48,319 --> 00:07:51,680
and a consequence of an interrupt or an

251
00:07:50,639 --> 00:07:53,599
exception occurring

252
00:07:51,680 --> 00:07:56,800
is that the hardware will actually sort

253
00:07:53,599 --> 00:07:58,960
of push this information onto the stack

254
00:07:56,800 --> 00:08:00,319
and so after the interrupter exception

255
00:07:58,960 --> 00:08:01,919
this is what the stack is going to look

256
00:08:00,319 --> 00:08:04,960
like

257
00:08:01,919 --> 00:08:07,199
but then the iret interrupt to return

258
00:08:04,960 --> 00:08:09,599
assembly instruction is what's going to

259
00:08:07,199 --> 00:08:12,080
be used for a interrupt handler

260
00:08:09,599 --> 00:08:13,759
to get back out of that interrupt and to

261
00:08:12,080 --> 00:08:16,160
pop this information

262
00:08:13,759 --> 00:08:17,199
back off the stack to effectively resume

263
00:08:16,160 --> 00:08:19,039
execution

264
00:08:17,199 --> 00:08:20,400
where it was pointing when the interrupt

265
00:08:19,039 --> 00:08:22,879
occurred so

266
00:08:20,400 --> 00:08:26,479
pops it back off and resumes execution

267
00:08:22,879 --> 00:08:26,479
wherever it was

