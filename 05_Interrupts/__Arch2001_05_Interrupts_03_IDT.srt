1
00:00:00,320 --> 00:00:03,280
okay so let's start digging into how

2
00:00:02,480 --> 00:00:05,279
interrupts work

3
00:00:03,280 --> 00:00:06,960
by digging into the interrupt descriptor

4
00:00:05,279 --> 00:00:09,120
table and just like

5
00:00:06,960 --> 00:00:10,880
we dealt with the global descriptor

6
00:00:09,120 --> 00:00:12,320
table before we started looking at the

7
00:00:10,880 --> 00:00:13,759
entries in this table

8
00:00:12,320 --> 00:00:15,360
same way here we're going to look at the

9
00:00:13,759 --> 00:00:16,480
table overall before we look at the

10
00:00:15,360 --> 00:00:18,880
entries

11
00:00:16,480 --> 00:00:22,320
so how is the IDT found well it turns

12
00:00:18,880 --> 00:00:25,359
out there's an IDT register named idtr

13
00:00:22,320 --> 00:00:27,439
amazing so when an interrupt occurs

14
00:00:25,359 --> 00:00:28,560
the hardware automatically consults the

15
00:00:27,439 --> 00:00:32,160
idtr

16
00:00:28,560 --> 00:00:34,000
and from there it finds the IDT itself

17
00:00:32,160 --> 00:00:36,399
then it's going to look for an offset in

18
00:00:34,000 --> 00:00:38,559
the IDT that is going to be a particular

19
00:00:36,399 --> 00:00:40,640
index inside of it for a particular

20
00:00:38,559 --> 00:00:42,160
interrupt then it's going to push the

21
00:00:40,640 --> 00:00:45,120
save stack information

22
00:00:42,160 --> 00:00:46,559
onto the stack using the TSS using the

23
00:00:45,120 --> 00:00:49,520
stack address specified by the

24
00:00:46,559 --> 00:00:51,360
TSS and then from within the particular

25
00:00:49,520 --> 00:00:51,840
entry in the IDT it's going to pull out

26
00:00:51,360 --> 00:00:54,719
a

27
00:00:51,840 --> 00:00:55,840
cs and rip that are going to be used as

28
00:00:54,719 --> 00:01:00,239
the

29
00:00:55,840 --> 00:01:02,960
new executable state so idtr

30
00:01:00,239 --> 00:01:04,879
looks exactly like the gdtr in that it

31
00:01:02,960 --> 00:01:07,119
has a 64-bit portion

32
00:01:04,879 --> 00:01:09,680
that says here's the base of the table

33
00:01:07,119 --> 00:01:12,479
and a 16-bit portion that says here is

34
00:01:09,680 --> 00:01:16,400
the limit or the size of the table

35
00:01:12,479 --> 00:01:19,360
and just like the gdtr had a lgdt

36
00:01:16,400 --> 00:01:22,400
here we have an lidt which loads the 10

37
00:01:19,360 --> 00:01:24,960
bytes from memory into the IDT register

38
00:01:22,400 --> 00:01:26,000
and take a wild guess about whether this

39
00:01:24,960 --> 00:01:29,119
is going to be userspace

40
00:01:26,000 --> 00:01:32,320
or kernelspace and its userspace

41
00:01:29,119 --> 00:01:34,400
again so the dichotomy continues with

42
00:01:32,320 --> 00:01:37,920
privileged instruction to write to the

43
00:01:34,400 --> 00:01:40,560
IDT register and unprivileged to read it

44
00:01:37,920 --> 00:01:41,520
also again WinDbg does its own little

45
00:01:40,560 --> 00:01:44,880
WinDbgism

46
00:01:41,520 --> 00:01:47,439
for a pseudo-register the idtl

47
00:01:44,880 --> 00:01:49,920
that says that holds just this bottom

48
00:01:47,439 --> 00:01:51,280
16-bit portion of what is actually an

49
00:01:49,920 --> 00:01:54,479
80-bit register

50
00:01:51,280 --> 00:01:56,560
so just like with the GDT the idtr has a

51
00:01:54,479 --> 00:01:58,079
base that points at the start of some

52
00:01:56,560 --> 00:01:59,119
table that has a bunch of data

53
00:01:58,079 --> 00:02:01,520
structures in it

54
00:01:59,119 --> 00:02:04,079
and a limit that specifies the top end

55
00:02:01,520 --> 00:02:07,439
of that data structure

56
00:02:04,079 --> 00:02:08,039
so the IDT is an array of less than or

57
00:02:07,439 --> 00:02:11,360
equal to

58
00:02:08,039 --> 00:02:12,640
256 16-byte descriptors based on that

59
00:02:11,360 --> 00:02:15,760
limit field

60
00:02:12,640 --> 00:02:17,920
and 0 through 31 are reserved by intel

61
00:02:15,760 --> 00:02:19,120
for their own usage for architectural

62
00:02:17,920 --> 00:02:22,000
specific

63
00:02:19,120 --> 00:02:22,800
exceptions and interrupts whereas 32 and

64
00:02:22,000 --> 00:02:25,120
above are

65
00:02:22,800 --> 00:02:26,879
values that an operating system can use

66
00:02:25,120 --> 00:02:28,480
for whatever

67
00:02:26,879 --> 00:02:30,000
and so while there is going to be a

68
00:02:28,480 --> 00:02:32,080
little bit of interaction with

69
00:02:30,000 --> 00:02:33,599
segmentation in the IDT that's again

70
00:02:32,080 --> 00:02:35,360
part of the reason why we learned about

71
00:02:33,599 --> 00:02:37,360
segmentation first

72
00:02:35,360 --> 00:02:39,599
conceptually I find it helpful to just

73
00:02:37,360 --> 00:02:42,239
think of the IDT like it's a big

74
00:02:39,599 --> 00:02:44,560
table of function pointers or function

75
00:02:42,239 --> 00:02:46,879
far pointers as the case may be

76
00:02:44,560 --> 00:02:48,640
so interrupt 0 will have a function

77
00:02:46,879 --> 00:02:50,160
pointer associated with it interrupt 1

78
00:02:48,640 --> 00:02:51,840
will have a function pointer interrupt

79
00:02:50,160 --> 00:02:53,440
2 will have a function pointer

80
00:02:51,840 --> 00:02:56,000
so you just kind of think of it like

81
00:02:53,440 --> 00:02:57,599
when interrupt 0 fires it goes to the

82
00:02:56,000 --> 00:03:00,879
function pointer pointed to

83
00:02:57,599 --> 00:03:03,280
by interrupt 0 so

84
00:03:00,879 --> 00:03:04,800
again IDT register points at the base

85
00:03:03,280 --> 00:03:07,360
then there's the IDT

86
00:03:04,800 --> 00:03:10,080
which has entries inside of it and I

87
00:03:07,360 --> 00:03:12,239
said there is segmentation used here so

88
00:03:10,080 --> 00:03:14,000
inside of the IDT entries as we'll see

89
00:03:12,239 --> 00:03:17,040
in the next section there is

90
00:03:14,000 --> 00:03:18,080
a far pointer so it is having a segment

91
00:03:17,040 --> 00:03:19,760
selector inside

92
00:03:18,080 --> 00:03:21,760
of there it's going to select something

93
00:03:19,760 --> 00:03:23,599
from the GDT or LDT

94
00:03:21,760 --> 00:03:25,519
but because this is used for code

95
00:03:23,599 --> 00:03:26,959
execution because you're ultimately you

96
00:03:25,519 --> 00:03:29,440
know trying to find some

97
00:03:26,959 --> 00:03:31,200
interrupt procedure to run well that

98
00:03:29,440 --> 00:03:32,319
means that you know the actual base

99
00:03:31,200 --> 00:03:34,720
address of the segment

100
00:03:32,319 --> 00:03:36,560
is ignored because code segments are

101
00:03:34,720 --> 00:03:37,680
always just treated as if they have a

102
00:03:36,560 --> 00:03:40,000
base of 0

103
00:03:37,680 --> 00:03:42,640
so it's really going to be a base of 0 a

104
00:03:40,000 --> 00:03:46,080
limit of 2 to 64 minus 1.

105
00:03:42,640 --> 00:03:46,959
and the part that is used here from the

106
00:03:46,080 --> 00:03:49,599
GDT

107
00:03:46,959 --> 00:03:52,319
are the access control bits so if you're

108
00:03:49,599 --> 00:03:53,680
trying to call a particular interrupt

109
00:03:52,319 --> 00:03:56,319
there's going to be checks for access

110
00:03:53,680 --> 00:03:58,400
control on the interrupt gate itself

111
00:03:56,319 --> 00:04:00,480
as well as the ultimate destination

112
00:03:58,400 --> 00:04:01,120
segment where that interrupt gate points

113
00:04:00,480 --> 00:04:03,360
at

114
00:04:01,120 --> 00:04:05,360
so access control is still in effect

115
00:04:03,360 --> 00:04:08,319
base addresses being reconfigurable

116
00:04:05,360 --> 00:04:10,239
is not so just a quick skim through the

117
00:04:08,319 --> 00:04:13,439
interrupts that are actually reserved by

118
00:04:10,239 --> 00:04:14,799
intel interrupt 0 is a divide error so

119
00:04:13,439 --> 00:04:17,199
if you divide by 0

120
00:04:14,799 --> 00:04:18,160
you get interrupt 0. interrupt 1 is a

121
00:04:17,199 --> 00:04:20,639
debug

122
00:04:18,160 --> 00:04:22,800
exception which has to do with hardware

123
00:04:20,639 --> 00:04:24,720
interrupts as we'll see later on

124
00:04:22,800 --> 00:04:27,199
there's the non-maskable interrupt

125
00:04:24,720 --> 00:04:29,759
there's a breakpoint this is used for

126
00:04:27,199 --> 00:04:31,840
software breakpoints INT3 as we'll

127
00:04:29,759 --> 00:04:34,720
dig into a little more later on

128
00:04:31,840 --> 00:04:35,360
and overflow had to do with that INTO

129
00:04:34,720 --> 00:04:39,120
UD

130
00:04:35,360 --> 00:04:42,320
has to do with UD2 instruction uh

131
00:04:39,120 --> 00:04:43,759
various invalid conditions the

132
00:04:42,320 --> 00:04:47,040
general protection fault

133
00:04:43,759 --> 00:04:47,759
interrupt vector 13 is by far the most

134
00:04:47,040 --> 00:04:50,000
common

135
00:04:47,759 --> 00:04:51,520
type of problem that can occur you see

136
00:04:50,000 --> 00:04:53,280
sort of everywhere when you're reading

137
00:04:51,520 --> 00:04:54,800
the manual for assembly instructions

138
00:04:53,280 --> 00:04:56,320
you'll see you know if this goes wrong

139
00:04:54,800 --> 00:04:58,320
general protection fault if this goes

140
00:04:56,320 --> 00:05:00,080
wrong general protection fault

141
00:04:58,320 --> 00:05:01,840
and then page fault is an extremely

142
00:05:00,080 --> 00:05:03,600
common one as well but we're going to

143
00:05:01,840 --> 00:05:05,680
learn about that in the paging section

144
00:05:03,600 --> 00:05:06,960
later on

145
00:05:05,680 --> 00:05:09,199
all right and then you know other

146
00:05:06,960 --> 00:05:11,520
miscellaneous only interesting ones are

147
00:05:09,199 --> 00:05:14,080
this one used for virtualization for

148
00:05:11,520 --> 00:05:15,280
extended page tables and control

149
00:05:14,080 --> 00:05:17,520
protection exception

150
00:05:15,280 --> 00:05:19,440
fairly sure this wasn't here when I made

151
00:05:17,520 --> 00:05:20,400
the class before because this has to do

152
00:05:19,440 --> 00:05:22,560
with CET

153
00:05:20,400 --> 00:05:24,800
control flow enforcement technology the

154
00:05:22,560 --> 00:05:25,199
new security mechanism and so for all of

155
00:05:24,800 --> 00:05:27,440
these

156
00:05:25,199 --> 00:05:28,720
exceptions and interrupts there are

157
00:05:27,440 --> 00:05:30,639
actually individual

158
00:05:28,720 --> 00:05:33,199
manual pages for each of them starting

159
00:05:30,639 --> 00:05:35,120
in section 6.15 if you want to go dig

160
00:05:33,199 --> 00:05:37,199
into those

161
00:05:35,120 --> 00:05:38,320
now just a little bit about terminology

162
00:05:37,199 --> 00:05:40,800
that I have to

163
00:05:38,320 --> 00:05:42,320
mention now that we have seen references

164
00:05:40,800 --> 00:05:45,360
both to segmentation

165
00:05:42,320 --> 00:05:47,199
and to faults right so we saw in the

166
00:05:45,360 --> 00:05:49,199
in the very first interrupt section you

167
00:05:47,199 --> 00:05:51,039
know hitler rip

168
00:05:49,199 --> 00:05:52,240
pointing at the instruction which is

169
00:05:51,039 --> 00:05:55,199
faulting

170
00:05:52,240 --> 00:05:56,560
well segmentation faults are neither to

171
00:05:55,199 --> 00:05:58,800
do with segmentation

172
00:05:56,560 --> 00:05:59,840
in the intel x86 sense of the word or

173
00:05:58,800 --> 00:06:03,039
faults in the

174
00:05:59,840 --> 00:06:04,319
intel x86 sense of the word the reality

175
00:06:03,039 --> 00:06:06,160
is most segmentation

176
00:06:04,319 --> 00:06:07,440
errors actually generate a general

177
00:06:06,160 --> 00:06:09,520
protection fault

178
00:06:07,440 --> 00:06:11,280
and segmentation isn't used to protect

179
00:06:09,520 --> 00:06:13,360
memory it's actually paging

180
00:06:11,280 --> 00:06:14,479
so it's page faults that occur most of

181
00:06:13,360 --> 00:06:18,240
the time

182
00:06:14,479 --> 00:06:22,319
so segfault is actually a unix signal

183
00:06:18,240 --> 00:06:23,360
the SIGSEGV and signals are just an os

184
00:06:22,319 --> 00:06:26,080
abstraction

185
00:06:23,360 --> 00:06:26,800
so this fault is not actually an x86

186
00:06:26,080 --> 00:06:28,639
fault

187
00:06:26,800 --> 00:06:30,080
basically you would expect that unix

188
00:06:28,639 --> 00:06:32,400
systems are going to

189
00:06:30,080 --> 00:06:34,960
use these signals when they run on

190
00:06:32,400 --> 00:06:38,560
various different architectures x86

191
00:06:34,960 --> 00:06:39,120
SPARC DEC honeywell you know who knows

192
00:06:38,560 --> 00:06:44,720
whatever

193
00:06:39,120 --> 00:06:44,720
whatever things I suppose ARM as well

194
00:06:44,960 --> 00:06:49,199
alright so what did we learn in this

195
00:06:46,720 --> 00:06:51,039
section well we learned the general

196
00:06:49,199 --> 00:06:52,880
principle that there is a thing called

197
00:06:51,039 --> 00:06:54,319
the IDT but we didn't learn what's

198
00:06:52,880 --> 00:06:56,639
inside of it

199
00:06:54,319 --> 00:06:59,280
we did learn that the IDT register is

200
00:06:56,639 --> 00:07:01,199
going to point at the base of the IDT

201
00:06:59,280 --> 00:07:03,120
so let's continue on to the next section

202
00:07:01,199 --> 00:07:06,000
and figure out what is the stuff inside

203
00:07:03,120 --> 00:07:06,000
the IDT

