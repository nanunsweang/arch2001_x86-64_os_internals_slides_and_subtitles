1
00:00:00,240 --> 00:00:04,160
so what's in the IDT interrupt

2
00:00:02,240 --> 00:00:06,080
descriptors are what's in the IDT

3
00:00:04,160 --> 00:00:07,680
and there's two types of descriptors

4
00:00:06,080 --> 00:00:09,760
there are interrupt gates and

5
00:00:07,680 --> 00:00:11,360
trap gates and for all intents and

6
00:00:09,760 --> 00:00:13,200
purposes and as far as you're concerned

7
00:00:11,360 --> 00:00:13,840
for right now these two are exactly the

8
00:00:13,200 --> 00:00:15,440
same

9
00:00:13,840 --> 00:00:16,800
because the only difference between an

10
00:00:15,440 --> 00:00:18,480
interrupt gate and a trap gate is the

11
00:00:16,800 --> 00:00:21,119
way the processor handles the

12
00:00:18,480 --> 00:00:22,000
interrupt flag flag in the eflags

13
00:00:21,119 --> 00:00:24,000
register

14
00:00:22,000 --> 00:00:26,000
the interrupt flag is discussed in the

15
00:00:24,000 --> 00:00:28,240
next section so for now let's just treat

16
00:00:26,000 --> 00:00:31,920
it like they're exactly the same thing

17
00:00:28,240 --> 00:00:33,280
so this is the this is the format of the

18
00:00:31,920 --> 00:00:35,280
interrupt or trap gate

19
00:00:33,280 --> 00:00:37,600
and it should look very familiar to you

20
00:00:35,280 --> 00:00:39,200
because it looks very similar to

21
00:00:37,600 --> 00:00:41,200
other types of descriptors that we saw

22
00:00:39,200 --> 00:00:43,360
in the GDT so if we squint our

23
00:00:41,200 --> 00:00:45,600
eyes we kind of see that well it's got a

24
00:00:43,360 --> 00:00:46,320
segment selector and it's got a 64-bit

25
00:00:45,600 --> 00:00:48,320
offset

26
00:00:46,320 --> 00:00:49,520
and so that should look like a logical

27
00:00:48,320 --> 00:00:52,559
address or a far

28
00:00:49,520 --> 00:00:54,559
pointer to you beyond which

29
00:00:52,559 --> 00:00:55,760
it looks very similar to another gate

30
00:00:54,559 --> 00:00:59,280
that we've seen before

31
00:00:55,760 --> 00:01:00,719
a call gate so interrupt gate call gate

32
00:00:59,280 --> 00:01:01,600
interrupt gate call gate interrupt gate

33
00:01:00,719 --> 00:01:05,680
call gate

34
00:01:01,600 --> 00:01:08,240
I'm seeing double here four Krustys

35
00:01:05,680 --> 00:01:09,439
so the IDT relation to segmentation as

36
00:01:08,240 --> 00:01:10,000
already mentioned in the previous

37
00:01:09,439 --> 00:01:12,799
section

38
00:01:10,000 --> 00:01:13,600
is that there's effectively a logical

39
00:01:12,799 --> 00:01:16,080
address a

40
00:01:13,600 --> 00:01:17,520
far pointer inside the IDT so that

41
00:01:16,080 --> 00:01:18,720
far pointer is going to have a segment

42
00:01:17,520 --> 00:01:21,439
selector that selects

43
00:01:18,720 --> 00:01:22,240
something from the GDT but because this

44
00:01:21,439 --> 00:01:24,880
is interrupt

45
00:01:22,240 --> 00:01:26,000
is getting code execution somewhere else

46
00:01:24,880 --> 00:01:28,000
an interrupt procedure

47
00:01:26,000 --> 00:01:30,320
the base is always 0 but the access

48
00:01:28,000 --> 00:01:33,119
control stuff is still checked

49
00:01:30,320 --> 00:01:33,600
so interrupt gates and trap gates the

50
00:01:33,119 --> 00:01:35,600
type

51
00:01:33,600 --> 00:01:38,159
is 1110 for interrupt gate

52
00:01:35,600 --> 00:01:39,840
and 1111 for trap gate

53
00:01:38,159 --> 00:01:42,159
and that's actually what we're seeing in

54
00:01:39,840 --> 00:01:44,159
this table 1110 for

55
00:01:42,159 --> 00:01:46,159
interrupt gate and 1111

56
00:01:44,159 --> 00:01:48,399
for trap gate and with that we've

57
00:01:46,159 --> 00:01:49,680
actually now seen every single type of

58
00:01:48,399 --> 00:01:52,479
64-bit

59
00:01:49,680 --> 00:01:52,880
segment descriptor and gate descriptor

60
00:01:52,479 --> 00:01:56,000
so

61
00:01:52,880 --> 00:01:59,360
call gates interrupt trap gates and tss

62
00:01:56,000 --> 00:02:02,079
segment and LDT segment

63
00:01:59,360 --> 00:02:02,719
so p bit as before present bit indicates

64
00:02:02,079 --> 00:02:05,360
whether or not

65
00:02:02,719 --> 00:02:06,799
anything here is valid and if it's if

66
00:02:05,360 --> 00:02:08,399
it's 0 and someone tries to use it

67
00:02:06,799 --> 00:02:10,239
that'll cause an error

68
00:02:08,399 --> 00:02:12,000
and DPL is again the descriptor

69
00:02:10,239 --> 00:02:12,640
privilege level and this is going to be

70
00:02:12,000 --> 00:02:14,720
checked

71
00:02:12,640 --> 00:02:16,720
when a software interrupt is occurring

72
00:02:14,720 --> 00:02:18,640
not when a hardware interrupt occurs

73
00:02:16,720 --> 00:02:19,760
so if a software interrupt is called and

74
00:02:18,640 --> 00:02:22,160
it calls through this

75
00:02:19,760 --> 00:02:24,239
interrupt gate then it's going to check

76
00:02:22,160 --> 00:02:25,599
that the current privilege level is less

77
00:02:24,239 --> 00:02:28,000
than or equal to

78
00:02:25,599 --> 00:02:30,959
the descriptor privilege level so if for

79
00:02:28,000 --> 00:02:34,160
instance this DPL was set to 0

80
00:02:30,959 --> 00:02:37,280
and you were at CPL 3 then it would

81
00:02:34,160 --> 00:02:38,800
not allow you through here the IST field

82
00:02:37,280 --> 00:02:40,400
on the other hand is the only

83
00:02:38,800 --> 00:02:42,160
really meaningful difference between

84
00:02:40,400 --> 00:02:43,280
this interrupt gate and the call gates

85
00:02:42,160 --> 00:02:45,840
from previously

86
00:02:43,280 --> 00:02:47,599
the IST was that interrupt stack table

87
00:02:45,840 --> 00:02:51,200
and we had seen in the TSS

88
00:02:47,599 --> 00:02:53,120
that it would be seven possible values

89
00:02:51,200 --> 00:02:54,400
that can specify for this specific

90
00:02:53,120 --> 00:02:57,440
interrupt I want my

91
00:02:54,400 --> 00:02:59,920
rsp to be from that table

92
00:02:57,440 --> 00:03:01,599
so here's that again so now we know this

93
00:02:59,920 --> 00:03:02,159
is what an interrupt descriptor looks

94
00:03:01,599 --> 00:03:05,519
like

95
00:03:02,159 --> 00:03:07,760
so if ist is non-zero then that means

96
00:03:05,519 --> 00:03:11,840
that it's trying to say I want to use

97
00:03:07,760 --> 00:03:14,560
IST entry 1, 2 etc up to 7

98
00:03:11,840 --> 00:03:15,599
if it's 0 that's reserved and that's

99
00:03:14,560 --> 00:03:18,159
not used

100
00:03:15,599 --> 00:03:19,440
so if it's 0 then instead of using an

101
00:03:18,159 --> 00:03:21,519
IST entry

102
00:03:19,440 --> 00:03:23,599
it's basically going to use these

103
00:03:21,519 --> 00:03:25,200
traditional TSS entries for

104
00:03:23,599 --> 00:03:26,480
if you're going to ring 0 if you're

105
00:03:25,200 --> 00:03:26,959
going to ring 1 if you're going to

106
00:03:26,480 --> 00:03:29,519
ring

107
00:03:26,959 --> 00:03:29,519
2

