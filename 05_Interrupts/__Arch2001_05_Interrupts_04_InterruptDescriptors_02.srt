1
00:00:00,240 --> 00:00:03,760
So now I've got a whole bunch of

2
00:00:01,760 --> 00:00:05,520
questions for you the actual questions

3
00:00:03,760 --> 00:00:05,920
will be on the website so don't take

4
00:00:05,520 --> 00:00:07,600
these

5
00:00:05,920 --> 00:00:09,360
as the canonical truth of what I'm

6
00:00:07,600 --> 00:00:11,120
asking you here instead check the

7
00:00:09,360 --> 00:00:15,040
website for this next lab

8
00:00:11,120 --> 00:00:18,160
but basically I want you to use dq idtr

9
00:00:15,040 --> 00:00:20,320
l10 or 0x10 to

10
00:00:18,160 --> 00:00:22,160
dump out the first 8 IDT descriptors

11
00:00:20,320 --> 00:00:24,000
and I want you to basically hand parse

12
00:00:22,160 --> 00:00:25,680
them according to the previous

13
00:00:24,000 --> 00:00:27,199
interrupt descriptor format that I've

14
00:00:25,680 --> 00:00:28,000
just shown and then answer these

15
00:00:27,199 --> 00:00:30,160
questions

16
00:00:28,000 --> 00:00:31,279
things like you know why is l10 8

17
00:00:30,160 --> 00:00:33,120
descriptors

18
00:00:31,279 --> 00:00:34,960
are there any interrupt gates versus

19
00:00:33,120 --> 00:00:36,640
trap gates which entries are interrupt

20
00:00:34,960 --> 00:00:38,800
gate which are trap gates

21
00:00:36,640 --> 00:00:41,120
what is the target for far pointer for

22
00:00:38,800 --> 00:00:42,000
each entry which entries if any use the

23
00:00:41,120 --> 00:00:45,200
IST

24
00:00:42,000 --> 00:00:45,920
what is the target rsp based on each of

25
00:00:45,200 --> 00:00:48,559
these

26
00:00:45,920 --> 00:00:50,000
and which entries have a DPL of 3 so

27
00:00:48,559 --> 00:00:51,520
that userspace can actually call

28
00:00:50,000 --> 00:00:54,079
through them

29
00:00:51,520 --> 00:00:55,120
so that's part 1 of the lab and in

30
00:00:54,079 --> 00:00:57,840
part 2

31
00:00:55,120 --> 00:00:59,039
you will use the WinDbg command

32
00:00:57,840 --> 00:01:00,719
!idt 

33
00:00:59,039 --> 00:01:03,440
and that's going to give you a more

34
00:01:00,719 --> 00:01:05,760
pretty print form of the IDT

35
00:01:03,440 --> 00:01:07,439
and you're going to compare what it says

36
00:01:05,760 --> 00:01:08,560
versus what you hand parsed in the

37
00:01:07,439 --> 00:01:10,159
previous section

38
00:01:08,560 --> 00:01:12,159
and see what you got right which you

39
00:01:10,159 --> 00:01:13,680
didn't and for any ones that you didn't

40
00:01:12,159 --> 00:01:15,360
get right then you need to you know

41
00:01:13,680 --> 00:01:17,920
re-examine and try to figure out what

42
00:01:15,360 --> 00:01:17,920
you did wrong

