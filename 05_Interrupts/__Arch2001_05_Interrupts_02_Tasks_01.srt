1
00:00:00,080 --> 00:00:03,199
in the previous section we saw that when

2
00:00:01,920 --> 00:00:05,440
an interrupt occurs

3
00:00:03,199 --> 00:00:06,560
there will be some save state pushed

4
00:00:05,440 --> 00:00:08,639
onto the stack

5
00:00:06,560 --> 00:00:10,639
and then when an interrupt returns that

6
00:00:08,639 --> 00:00:12,480
save state is popped back off so that

7
00:00:10,639 --> 00:00:13,519
the processor can get back to whatever

8
00:00:12,480 --> 00:00:15,599
it was doing

9
00:00:13,519 --> 00:00:16,880
to understand how the stack works in

10
00:00:15,599 --> 00:00:18,720
particular for this

11
00:00:16,880 --> 00:00:20,880
save state we need to learn a little bit

12
00:00:18,720 --> 00:00:22,880
about something called tasks

13
00:00:20,880 --> 00:00:24,320
so scattered throughout the intel

14
00:00:22,880 --> 00:00:26,880
documentation you will see

15
00:00:24,320 --> 00:00:28,720
references to things called tasks and

16
00:00:26,880 --> 00:00:31,279
that was a hardware mechanism

17
00:00:28,720 --> 00:00:33,200
designed to support multitasking

18
00:00:31,279 --> 00:00:35,440
allowing the processor to stay

19
00:00:33,200 --> 00:00:36,960
save information and context switch

20
00:00:35,440 --> 00:00:39,680
between you know different

21
00:00:36,960 --> 00:00:41,680
processes or different tasks but like

22
00:00:39,680 --> 00:00:43,120
many things with the original design

23
00:00:41,680 --> 00:00:45,440
like segmentation

24
00:00:43,120 --> 00:00:47,039
tasks weren't really being used for its

25
00:00:45,440 --> 00:00:50,320
intended design purpose

26
00:00:47,039 --> 00:00:53,360
so during the x86-64 extensions

27
00:00:50,320 --> 00:00:54,399
uh it was largely removed from the

28
00:00:53,360 --> 00:00:56,719
architecture

29
00:00:54,399 --> 00:00:58,800
but there is one element that is still

30
00:00:56,719 --> 00:01:01,120
used and that is the task state

31
00:00:58,800 --> 00:01:03,199
segment and this actually must be used

32
00:01:01,120 --> 00:01:05,519
by an operating system because

33
00:01:03,199 --> 00:01:07,040
it is still architecturally baked in

34
00:01:05,519 --> 00:01:10,840
that it is consulted

35
00:01:07,040 --> 00:01:12,720
upon uh stack switching that occurs for

36
00:01:10,840 --> 00:01:15,520
interrupts

37
00:01:12,720 --> 00:01:16,080
so the information about tasks start

38
00:01:15,520 --> 00:01:18,799
with a

39
00:01:16,080 --> 00:01:21,520
task register and this looks very

40
00:01:18,799 --> 00:01:24,799
similar if not identical to the

41
00:01:21,520 --> 00:01:27,520
ldtr the local descriptor table register

42
00:01:24,799 --> 00:01:29,200
because it has a 16-bit segment selector

43
00:01:27,520 --> 00:01:31,520
and for all intents and purposes that's

44
00:01:29,200 --> 00:01:33,439
the only thing you can actually access

45
00:01:31,520 --> 00:01:34,960
under normal circumstances and then

46
00:01:33,439 --> 00:01:37,040
there's the hidden portion to the

47
00:01:34,960 --> 00:01:37,520
register which gets filled in from some

48
00:01:37,040 --> 00:01:40,400
table

49
00:01:37,520 --> 00:01:42,240
the GDT which we now know about and this

50
00:01:40,400 --> 00:01:42,640
is going to have a base and a limit of

51
00:01:42,240 --> 00:01:45,920
the

52
00:01:42,640 --> 00:01:49,600
task state segment just filled in

53
00:01:45,920 --> 00:01:50,240
from the table so to set this register

54
00:01:49,600 --> 00:01:53,759
there is a

55
00:01:50,240 --> 00:01:56,320
instruction LTR load task register which

56
00:01:53,759 --> 00:01:57,119
installs the 16 bit segment selector and

57
00:01:56,320 --> 00:01:59,360
STR

58
00:01:57,119 --> 00:02:01,119
which will allow you to write out the

59
00:01:59,360 --> 00:02:02,960
value of the task register

60
00:02:01,119 --> 00:02:05,200
and once again we see this sort of

61
00:02:02,960 --> 00:02:06,560
dichotomy of a thing that can only write

62
00:02:05,200 --> 00:02:08,560
to it is privileged

63
00:02:06,560 --> 00:02:10,879
and the thing that can read from it is

64
00:02:08,560 --> 00:02:13,280
not privileged

65
00:02:10,879 --> 00:02:15,120
so first things first from this picture

66
00:02:13,280 --> 00:02:17,280
that is in the intel manuals we have to

67
00:02:15,120 --> 00:02:18,000
say that when we're talking about 64-bit

68
00:02:17,280 --> 00:02:20,160
systems

69
00:02:18,000 --> 00:02:22,640
there's no such thing anymore as a task

70
00:02:20,160 --> 00:02:24,800
gate this is just not a thing

71
00:02:22,640 --> 00:02:26,800
so there is the task register and the

72
00:02:24,800 --> 00:02:30,080
task register points at some

73
00:02:26,800 --> 00:02:31,200
TSS descriptor which can only be found

74
00:02:30,080 --> 00:02:33,360
in the GDT

75
00:02:31,200 --> 00:02:35,040
according to the LTR so even though this

76
00:02:33,360 --> 00:02:36,400
is just a segment selector and you would

77
00:02:35,040 --> 00:02:38,560
think that you know

78
00:02:36,400 --> 00:02:40,879
it could have a 0 or 1 for the

79
00:02:38,560 --> 00:02:43,040
table indicator to say GDT or LDT

80
00:02:40,879 --> 00:02:46,560
no the instruction that actually loads

81
00:02:43,040 --> 00:02:49,120
it says this must be in the GDT

82
00:02:46,560 --> 00:02:50,879
so task gates like I just said reserved

83
00:02:49,120 --> 00:02:53,920
in 64-bit mode so

84
00:02:50,879 --> 00:02:56,080
no more task gates all right looking at

85
00:02:53,920 --> 00:02:58,319
this another way task register selector

86
00:02:56,080 --> 00:03:00,319
the hidden portion so the selector

87
00:02:58,319 --> 00:03:03,360
selects from the GDT

88
00:03:00,319 --> 00:03:06,080
it points at a TSS descriptor when

89
00:03:03,360 --> 00:03:08,239
this register is loaded up the table

90
00:03:06,080 --> 00:03:10,640
from memory will be consulted it will

91
00:03:08,239 --> 00:03:11,920
fill in this cached portion so that it

92
00:03:10,640 --> 00:03:13,519
doesn't have to look it up from the

93
00:03:11,920 --> 00:03:15,280
table any further when it's trying to

94
00:03:13,519 --> 00:03:16,800
access the TSS

95
00:03:15,280 --> 00:03:18,319
and so the base address will point at

96
00:03:16,800 --> 00:03:20,000
the bottom of this data structure and

97
00:03:18,319 --> 00:03:22,080
the segment limits at the top

98
00:03:20,000 --> 00:03:24,640
so first of all what does a TSS

99
00:03:22,080 --> 00:03:27,360
descriptor look like

100
00:03:24,640 --> 00:03:29,120
well it looks exactly like the LDT

101
00:03:27,360 --> 00:03:30,159
descriptors that we saw a little while

102
00:03:29,120 --> 00:03:32,879
ago

103
00:03:30,159 --> 00:03:33,920
so it has base addresses which are

104
00:03:32,879 --> 00:03:36,480
spread out to

105
00:03:33,920 --> 00:03:38,480
support a 64-bit base address so

106
00:03:36,480 --> 00:03:39,599
specifying a linear address somewhere in

107
00:03:38,480 --> 00:03:41,760
the memory space

108
00:03:39,599 --> 00:03:44,159
and a segment limit a 20-bit segment

109
00:03:41,760 --> 00:03:44,959
limit to say you know what is the limit

110
00:03:44,159 --> 00:03:47,680
here but

111
00:03:44,959 --> 00:03:50,640
again in 64-bit systems limits aren't

112
00:03:47,680 --> 00:03:50,640
really cared about

113
00:03:50,799 --> 00:03:54,799
all right so now we've actually seen a

114
00:03:52,799 --> 00:03:58,720
couple more things from our

115
00:03:54,799 --> 00:04:01,200
system segment descriptor table so TSS

116
00:03:58,720 --> 00:04:02,319
64-bit TSS for available and busy you

117
00:04:01,200 --> 00:04:04,239
don't really have to care about the

118
00:04:02,319 --> 00:04:06,480
available and busy it just is a hardware

119
00:04:04,239 --> 00:04:07,439
mechanism that sets a bit to say whether

120
00:04:06,480 --> 00:04:09,920
or not

121
00:04:07,439 --> 00:04:10,720
there is an access currently ongoing to

122
00:04:09,920 --> 00:04:14,159
the

123
00:04:10,720 --> 00:04:16,079
TSS all right so then what does the tss

124
00:04:14,159 --> 00:04:18,239
itself look like

125
00:04:16,079 --> 00:04:19,680
well on 64-bit systems it looks like

126
00:04:18,239 --> 00:04:23,440
this

127
00:04:19,680 --> 00:04:24,240
we've got first and foremost a 64-bit

128
00:04:23,440 --> 00:04:27,759
value

129
00:04:24,240 --> 00:04:30,080
at the bottom called rsp0 and when

130
00:04:27,759 --> 00:04:31,280
an interrupt is occurring and changing

131
00:04:30,080 --> 00:04:33,840
state into ring

132
00:04:31,280 --> 00:04:34,800
0 this is what's going to be used as

133
00:04:33,840 --> 00:04:36,720
the rsp

134
00:04:34,800 --> 00:04:38,800
that's going to be used to decide where

135
00:04:36,720 --> 00:04:40,639
to throw stuff on the stack

136
00:04:38,800 --> 00:04:42,000
so the kernel needs to set this up this

137
00:04:40,639 --> 00:04:43,840
obviously needs to be in some

138
00:04:42,000 --> 00:04:45,199
kernel space location so that people

139
00:04:43,840 --> 00:04:48,240
can't tamper with it

140
00:04:45,199 --> 00:04:51,120
and this is where state will be saved

141
00:04:48,240 --> 00:04:51,680
if the processor was changing to ring

142
00:04:51,120 --> 00:04:53,919
1

143
00:04:51,680 --> 00:04:56,160
instead of ring 0 then this re this

144
00:04:53,919 --> 00:04:58,240
value would be used instead

145
00:04:56,160 --> 00:05:00,160
and similarly if it was changing to ring

146
00:04:58,240 --> 00:05:02,880
2 this value would be used instead

147
00:05:00,160 --> 00:05:05,520
so these are the rsp values that are

148
00:05:02,880 --> 00:05:07,600
used on transition to those rings

149
00:05:05,520 --> 00:05:09,600
now there's also this new element that

150
00:05:07,600 --> 00:05:12,479
is new for the x86-64

151
00:05:09,600 --> 00:05:13,280
extensions called the interrupt stack

152
00:05:12,479 --> 00:05:16,000
table

153
00:05:13,280 --> 00:05:17,280
and it's basically a list of seven

154
00:05:16,000 --> 00:05:19,440
possible values

155
00:05:17,280 --> 00:05:20,960
that are used for different interrupts

156
00:05:19,440 --> 00:05:22,320
so different interrupts when we look at

157
00:05:20,960 --> 00:05:23,440
the interrupt descriptor table in the

158
00:05:22,320 --> 00:05:25,039
next section

159
00:05:23,440 --> 00:05:26,639
they can say you know what for my

160
00:05:25,039 --> 00:05:28,160
interrupt I wanted to use the stack from

161
00:05:26,639 --> 00:05:30,000
here and another one can say I wanted to

162
00:05:28,160 --> 00:05:32,160
use the stack from here

163
00:05:30,000 --> 00:05:34,000
so this is a new mechanism to allow a

164
00:05:32,160 --> 00:05:36,479
little more precision on a per

165
00:05:34,000 --> 00:05:38,479
interrupt basis than this which would be

166
00:05:36,479 --> 00:05:41,360
used for everyone transitioning to a

167
00:05:38,479 --> 00:05:41,360
given ring level

168
00:05:41,520 --> 00:05:44,800
now just for you know for curiosity's

169
00:05:44,240 --> 00:05:47,360
sake

170
00:05:44,800 --> 00:05:48,560
to show what the old 32-bit and 16-bit

171
00:05:47,360 --> 00:05:51,440
values look like

172
00:05:48,560 --> 00:05:53,280
of TSS this is basically just to show

173
00:05:51,440 --> 00:05:56,319
that previously it really was

174
00:05:53,280 --> 00:05:58,880
more like the name states a task state

175
00:05:56,319 --> 00:06:01,680
segment task state segment so it was

176
00:05:58,880 --> 00:06:03,759
intending to save state about the tasks

177
00:06:01,680 --> 00:06:05,360
including general purpose registers so

178
00:06:03,759 --> 00:06:07,759
general purpose registers

179
00:06:05,360 --> 00:06:09,199
segment registers and then it still had

180
00:06:07,759 --> 00:06:11,840
these notions of

181
00:06:09,199 --> 00:06:13,280
stack information so stack segment

182
00:06:11,840 --> 00:06:15,039
registers and so forth

183
00:06:13,280 --> 00:06:16,960
and that goes all the way back to the

184
00:06:15,039 --> 00:06:20,000
16-bit format from the original

185
00:06:16,960 --> 00:06:22,160
8086s so like we saw

186
00:06:20,000 --> 00:06:23,759
in the previous video some sort of

187
00:06:22,160 --> 00:06:26,800
interrupt happens

188
00:06:23,759 --> 00:06:29,680
then stack information gets pushed

189
00:06:26,800 --> 00:06:30,880
then an iret occurs stack information

190
00:06:29,680 --> 00:06:32,960
gets popped

191
00:06:30,880 --> 00:06:35,919
and so where does the TSS play into that

192
00:06:32,960 --> 00:06:37,919
the TSS holds a pointer to the stack

193
00:06:35,919 --> 00:06:39,600
which is where the stack is going to be

194
00:06:37,919 --> 00:06:40,560
considered to start before that

195
00:06:39,600 --> 00:06:43,360
information

196
00:06:40,560 --> 00:06:44,720
gets pushed on to the stack so TSS helps

197
00:06:43,360 --> 00:06:46,479
the hardware know

198
00:06:44,720 --> 00:06:48,000
where to actually push information

199
00:06:46,479 --> 00:06:50,880
that's why TSS is still

200
00:06:48,000 --> 00:06:50,880
required

