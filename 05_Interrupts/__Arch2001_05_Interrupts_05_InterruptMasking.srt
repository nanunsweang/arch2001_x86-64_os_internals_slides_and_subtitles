1
00:00:00,320 --> 00:00:04,319
all right so let's talk about interrupt

2
00:00:01,760 --> 00:00:07,200
masking it's sometimes useful

3
00:00:04,319 --> 00:00:08,960
to basically disable the interrupts most

4
00:00:07,200 --> 00:00:10,400
often you want to do this when you're

5
00:00:08,960 --> 00:00:11,759
already in the middle of an interrupt

6
00:00:10,400 --> 00:00:13,679
there's this notion of

7
00:00:11,759 --> 00:00:15,200
you know reentrancy and whether or not

8
00:00:13,679 --> 00:00:15,759
you can interrupt while inside of an

9
00:00:15,200 --> 00:00:17,039
interrupt while

10
00:00:15,759 --> 00:00:19,039
inside of an interrupt and

11
00:00:17,039 --> 00:00:20,080
generally speaking most systems are not

12
00:00:19,039 --> 00:00:22,640
designed to

13
00:00:20,080 --> 00:00:23,519
support this unlimited nesting of

14
00:00:22,640 --> 00:00:25,920
interrupts

15
00:00:23,519 --> 00:00:27,279
so if you go into an interrupt and you

16
00:00:25,920 --> 00:00:29,039
want to block subsequent interrupts

17
00:00:27,279 --> 00:00:29,519
until you're done with the most critical

18
00:00:29,039 --> 00:00:31,760
section

19
00:00:29,519 --> 00:00:32,640
of that interrupt handling that can be

20
00:00:31,760 --> 00:00:34,960
done by

21
00:00:32,640 --> 00:00:36,719
clearing the interrupt flag now it turns

22
00:00:34,960 --> 00:00:38,879
out that the interrupt flag is

23
00:00:36,719 --> 00:00:40,079
actually so first of all it's a flag in

24
00:00:38,879 --> 00:00:42,079
the RFLAGS register

25
00:00:40,079 --> 00:00:43,840
and it's automatically cleared whenever

26
00:00:42,079 --> 00:00:44,640
an interrupt occurs through an interrupt

27
00:00:43,840 --> 00:00:46,879
gate

28
00:00:44,640 --> 00:00:48,640
but it is not cleared if you go through a

29
00:00:46,879 --> 00:00:50,399
trap gate and as we said in the previous

30
00:00:48,640 --> 00:00:51,120
section this is literally the only

31
00:00:50,399 --> 00:00:53,199
difference

32
00:00:51,120 --> 00:00:54,239
descriptor style like they're all the

33
00:00:53,199 --> 00:00:56,559
exact same

34
00:00:54,239 --> 00:00:57,440
fields and stuff like that it basically

35
00:00:56,559 --> 00:00:59,199
comes down to

36
00:00:57,440 --> 00:01:01,039
if you set something up to vector

37
00:00:59,199 --> 00:01:02,719
through an interrupt gate that means you

38
00:01:01,039 --> 00:01:04,879
want the interrupt flag cleared and if

39
00:01:02,719 --> 00:01:06,000
you set it up to go through a trap gate

40
00:01:04,879 --> 00:01:08,560
that means you don't want it

41
00:01:06,000 --> 00:01:10,240
automatically cleared now even if it

42
00:01:08,560 --> 00:01:11,760
wasn't automatically cleared

43
00:01:10,240 --> 00:01:14,720
you could still in your interrupt

44
00:01:11,760 --> 00:01:17,040
handler manually clear it by using the

45
00:01:14,720 --> 00:01:17,759
clear interrupt flag instruction but

46
00:01:17,040 --> 00:01:19,600
that would be

47
00:01:17,759 --> 00:01:21,680
somewhat pointless right why use a trap

48
00:01:19,600 --> 00:01:22,880
gate just to go on to manually clear it

49
00:01:21,680 --> 00:01:23,680
when you could just use an interrupt

50
00:01:22,880 --> 00:01:26,479
gate

51
00:01:23,680 --> 00:01:28,479
but the options do exist particularly

52
00:01:26,479 --> 00:01:30,240
for you know situations in which

53
00:01:28,479 --> 00:01:31,759
you know perhaps some kernel code wants

54
00:01:30,240 --> 00:01:32,320
to make really really sure that it

55
00:01:31,759 --> 00:01:34,640
doesn't get

56
00:01:32,320 --> 00:01:36,079
interrupted by some hardware interrupts

57
00:01:34,640 --> 00:01:36,880
uh they can go ahead and clear the

58
00:01:36,079 --> 00:01:39,759
interrupt flag

59
00:01:36,880 --> 00:01:42,000
and unclear it once they're ready to

60
00:01:39,759 --> 00:01:44,799
accept interrupts again

61
00:01:42,000 --> 00:01:46,560
so there's the interrupt flag bit 9

62
00:01:44,799 --> 00:01:49,840
in the RFLAGS register

63
00:01:46,560 --> 00:01:51,680
and it's the interrupt enable flag

64
00:01:49,840 --> 00:01:52,960
so there's a couple exceptions to the

65
00:01:51,680 --> 00:01:55,840
interrupt masking

66
00:01:52,960 --> 00:01:57,759
and that is that the interrupt flag does not

67
00:01:55,840 --> 00:01:59,680
mask so if this is clear it does not

68
00:01:57,759 --> 00:02:00,640
mask the explicit invocation of an

69
00:01:59,680 --> 00:02:02,399
interrupt with

70
00:02:00,640 --> 00:02:04,520
those software instructions we saw

71
00:02:02,399 --> 00:02:07,119
previously the INT n

72
00:02:04,520 --> 00:02:09,599
INT1, 3, O, UD2

73
00:02:07,119 --> 00:02:11,200
also the interrupt flag does not mask a

74
00:02:09,599 --> 00:02:11,520
non-maskable interrupt that's the whole

75
00:02:11,200 --> 00:02:13,200
point

76
00:02:11,520 --> 00:02:15,120
the non-maskable interrupt is a high

77
00:02:13,200 --> 00:02:18,400
priority thing that no one can stop and

78
00:02:15,120 --> 00:02:18,400
it's just coming through

