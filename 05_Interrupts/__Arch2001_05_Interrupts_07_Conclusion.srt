1
00:00:00,080 --> 00:00:03,679
so what can we conclude from this

2
00:00:01,760 --> 00:00:05,759
section well we've learned a bunch about

3
00:00:03,679 --> 00:00:08,400
interrupts we've picked up some stray

4
00:00:05,759 --> 00:00:09,599
assembly instructions along the way the

5
00:00:08,400 --> 00:00:11,519
software interrupt

6
00:00:09,599 --> 00:00:13,840
even though a normal interrupt typically

7
00:00:11,519 --> 00:00:15,839
comes from hardware trying to say like

8
00:00:13,840 --> 00:00:18,080
hey I need you to do something right now

9
00:00:15,839 --> 00:00:21,199
but there's also a software interrupt to

10
00:00:18,080 --> 00:00:22,960
manually invoke an IDT handler there's

11
00:00:21,199 --> 00:00:24,720
the interrupt return which pops

12
00:00:22,960 --> 00:00:26,320
information back off the stack to get

13
00:00:24,720 --> 00:00:27,840
back to wherever and whatever the

14
00:00:26,320 --> 00:00:28,960
processor was doing before it was

15
00:00:27,840 --> 00:00:31,359
interrupted

16
00:00:28,960 --> 00:00:32,559
there's the INT1 and INT3 INTO and

17
00:00:31,359 --> 00:00:35,200
UD2

18
00:00:32,559 --> 00:00:37,520
various versions of software interrupts

19
00:00:35,200 --> 00:00:39,280
that have special versions of encoding

20
00:00:37,520 --> 00:00:41,440
or special behavior

21
00:00:39,280 --> 00:00:43,200
and then there's the store task register

22
00:00:41,440 --> 00:00:45,200
and load task register

23
00:00:43,200 --> 00:00:46,559
for setting up the information about the

24
00:00:45,200 --> 00:00:49,120
task state segment

25
00:00:46,559 --> 00:00:50,879
used for finding the stack lets the

26
00:00:49,120 --> 00:00:51,440
hardware find the stack when interrupt

27
00:00:50,879 --> 00:00:54,480
occurs

28
00:00:51,440 --> 00:00:57,120
the store and load IDT which

29
00:00:54,480 --> 00:00:57,760
loads up the idtr or stores it out to

30
00:00:57,120 --> 00:00:59,359
memory

31
00:00:57,760 --> 00:01:01,359
which we also saw as a little

32
00:00:59,359 --> 00:01:03,280
miscellaneous thing that the

33
00:01:01,359 --> 00:01:04,799
information disclosed by that is

34
00:01:03,280 --> 00:01:06,159
potentially beneficial to

35
00:01:04,799 --> 00:01:08,000
someone wanting to know whether or not

36
00:01:06,159 --> 00:01:09,360
they're in a virtualization system a la

37
00:01:08,000 --> 00:01:11,520
the red pill

38
00:01:09,360 --> 00:01:12,720
and then set and clear the interrupt

39
00:01:11,520 --> 00:01:14,720
flag used for

40
00:01:12,720 --> 00:01:17,040
masking interrupts if you don't want to

41
00:01:14,720 --> 00:01:18,560
be interrupted at a given point in time

42
00:01:17,040 --> 00:01:21,119
but I think this is the more interesting

43
00:01:18,560 --> 00:01:22,400
point this is our giant you know table

44
00:01:21,119 --> 00:01:24,000
of everything we're going to learn and

45
00:01:22,400 --> 00:01:25,840
that's this is what it looked like after

46
00:01:24,000 --> 00:01:26,720
our previous you know segmentation

47
00:01:25,840 --> 00:01:29,040
section

48
00:01:26,720 --> 00:01:30,320
and this is what it looks like now after

49
00:01:29,040 --> 00:01:32,240
the interrupt section

50
00:01:30,320 --> 00:01:33,439
so between segmentation and interrupts

51
00:01:32,240 --> 00:01:36,000
we really covered

52
00:01:33,439 --> 00:01:36,799
basically all of this stuff and what's

53
00:01:36,000 --> 00:01:39,040
left to do

54
00:01:36,799 --> 00:01:40,960
is to learn about paging and virtual

55
00:01:39,040 --> 00:01:44,000
memory management down here

56
00:01:40,960 --> 00:01:44,000
so let's move on

