1
00:00:00,080 --> 00:00:03,600
all right now for a quick diversion with

2
00:00:02,000 --> 00:00:05,200
a little bit of fun stuff that you can

3
00:00:03,600 --> 00:00:07,359
do with some of the

4
00:00:05,200 --> 00:00:09,200
segmentation and interrupt information

5
00:00:07,359 --> 00:00:12,160
that you've just recently learned

6
00:00:09,200 --> 00:00:14,080
so back in 2004 Joanna Rutkowska posted

7
00:00:12,160 --> 00:00:16,080
an article called red pill or how to

8
00:00:14,080 --> 00:00:18,800
detect a vmm using almost

9
00:00:16,080 --> 00:00:19,279
one cpu instruction and what she was

10
00:00:18,800 --> 00:00:21,680
doing

11
00:00:19,279 --> 00:00:22,560
was she found a recognition that the

12
00:00:21,680 --> 00:00:24,560
sidt

13
00:00:22,560 --> 00:00:25,840
instruction which stores the interrupt

14
00:00:24,560 --> 00:00:28,400
descriptor table

15
00:00:25,840 --> 00:00:30,400
could be used to profile what the

16
00:00:28,400 --> 00:00:32,880
location was of the IDT

17
00:00:30,400 --> 00:00:33,760
in memory and what she had found was

18
00:00:32,880 --> 00:00:35,840
that in

19
00:00:33,760 --> 00:00:37,920
places virtualization systems like

20
00:00:35,840 --> 00:00:40,800
VMware or VirtualPC

21
00:00:37,920 --> 00:00:42,320
it had a particular reliable address

22
00:00:40,800 --> 00:00:44,000
inside of VMs

23
00:00:42,320 --> 00:00:46,079
that was not the same address that it

24
00:00:44,000 --> 00:00:48,480
would tend to have outside of VMs when

25
00:00:46,079 --> 00:00:50,800
run on a native host

26
00:00:48,480 --> 00:00:52,399
and so you know the original red pill

27
00:00:50,800 --> 00:00:52,960
looked something like this just a little

28
00:00:52,399 --> 00:00:55,840
bit of C

29
00:00:52,960 --> 00:00:56,239
code just to translate this for you this

30
00:00:55,840 --> 00:00:57,600
m

31
00:00:56,239 --> 00:00:59,680
was the place where she was going to

32
00:00:57,600 --> 00:01:02,640
store the idtr

33
00:00:59,680 --> 00:01:04,479
it was 4 bytes because a 32-bit base

34
00:01:02,640 --> 00:01:07,760
address and a 2-byte

35
00:01:04,479 --> 00:01:08,320
uh limit and then rpill this array was

36
00:01:07,760 --> 00:01:10,320
basically

37
00:01:08,320 --> 00:01:11,680
encoding the assembly instruction for

38
00:01:10,320 --> 00:01:14,240
the sidt

39
00:01:11,680 --> 00:01:15,280
these zeros right here we're going to be

40
00:01:14,240 --> 00:01:18,000
overwritten

41
00:01:15,280 --> 00:01:19,759
so she takes the m the key address of m

42
00:01:18,000 --> 00:01:21,200
and overwrites it into the assembly

43
00:01:19,759 --> 00:01:23,040
instruction because the

44
00:01:21,200 --> 00:01:24,960
memory location is actually built into

45
00:01:23,040 --> 00:01:27,200
the sidt instruction

46
00:01:24,960 --> 00:01:28,880
so she writes that into the rpill array

47
00:01:27,200 --> 00:01:30,720
she invokes rpill as if it's a

48
00:01:28,880 --> 00:01:32,799
function pointer so effectively just

49
00:01:30,720 --> 00:01:35,520
calling this sidt instruction

50
00:01:32,799 --> 00:01:36,000
and then she checks the you know output

51
00:01:35,520 --> 00:01:37,439
of it

52
00:01:36,000 --> 00:01:39,200
saying you know is the most significant

53
00:01:37,439 --> 00:01:43,040
byte 0xd0 then

54
00:01:39,200 --> 00:01:44,880
you know I think that I'm inside of a vm

55
00:01:43,040 --> 00:01:47,360
so let's look at the slightly more

56
00:01:44,880 --> 00:01:50,720
verbose version of that

57
00:01:47,360 --> 00:01:51,840
updated for 64-bit so if you go into

58
00:01:50,720 --> 00:01:55,280
your vm

59
00:01:51,840 --> 00:01:57,759
for U_VerboseRedPill

60
00:01:55,280 --> 00:01:59,759
then you'll see that I have a 10 byte

61
00:01:57,759 --> 00:02:02,479
storage array instead

62
00:01:59,759 --> 00:02:05,920
and that's going to store the sidt zero

63
00:02:02,479 --> 00:02:05,920
it out and then just use the

64
00:02:06,000 --> 00:02:10,239
compiler intrinsic for sidt because

65
00:02:08,239 --> 00:02:12,800
microsoft does actually provide a

66
00:02:10,239 --> 00:02:14,239
sidt assembly instruction intrinsic so

67
00:02:12,800 --> 00:02:14,959
I don't actually need to use raw

68
00:02:14,239 --> 00:02:17,440
assembly

69
00:02:14,959 --> 00:02:19,120
and that's just going to store the sidt

70
00:02:17,440 --> 00:02:22,160
or the IDT register

71
00:02:19,120 --> 00:02:23,599
into storage and so then really all you

72
00:02:22,160 --> 00:02:25,680
do from there is you just check the most

73
00:02:23,599 --> 00:02:27,040
significant bit uh byte and you say like

74
00:02:25,680 --> 00:02:29,440
okay well I know that

75
00:02:27,040 --> 00:02:32,000
on you know my VMware system it looks

76
00:02:29,440 --> 00:02:33,599
like it can be e2, e3 or e4

77
00:02:32,000 --> 00:02:35,280
now by the time that you actually take

78
00:02:33,599 --> 00:02:37,440
this class I will have gone off and

79
00:02:35,280 --> 00:02:39,200
profiled this on you know VMware Player

80
00:02:37,440 --> 00:02:41,760
and physical hardware etc

81
00:02:39,200 --> 00:02:44,319
because this all needs to be updated but

82
00:02:41,760 --> 00:02:46,480
if I run it on my system right now

83
00:02:44,319 --> 00:02:48,400
what I will see is when I get here and

84
00:02:46,480 --> 00:02:49,040
I've run the sidt assembly instruction

85
00:02:48,400 --> 00:02:51,760
already

86
00:02:49,040 --> 00:02:52,560
looks like I have an e3 so if I continue

87
00:02:51,760 --> 00:02:55,120
on

88
00:02:52,560 --> 00:02:57,120
then ta-da my code magically knows that

89
00:02:55,120 --> 00:02:59,040
it's inside of VMware

90
00:02:57,120 --> 00:03:00,959
but really all it's doing is just you

91
00:02:59,040 --> 00:03:01,760
know checking that there's some fixed

92
00:03:00,959 --> 00:03:04,080
hard-coded

93
00:03:01,760 --> 00:03:05,760
signature that I choose to say like this

94
00:03:04,080 --> 00:03:08,239
is the most significant byte that

95
00:03:05,760 --> 00:03:09,599
or the most significant portion of the

96
00:03:08,239 --> 00:03:11,920
address value of the

97
00:03:09,599 --> 00:03:13,440
64-bit base address it's not actually

98
00:03:11,920 --> 00:03:15,680
the most significant most significant

99
00:03:13,440 --> 00:03:18,159
part because that'll always just be f's

100
00:03:15,680 --> 00:03:18,959
so the fundamental thing that makes red

101
00:03:18,159 --> 00:03:21,920
pill work

102
00:03:18,959 --> 00:03:24,480
is the fact that while lidt loading

103
00:03:21,920 --> 00:03:25,120
up the IDT register is privileged sidt

104
00:03:24,480 --> 00:03:26,640
is not

105
00:03:25,120 --> 00:03:28,720
and we've actually seen that multiple

106
00:03:26,640 --> 00:03:32,080
times now through this class we saw GDT

107
00:03:28,720 --> 00:03:35,760
we saw LDT we saw TR and we saw

108
00:03:32,080 --> 00:03:38,480
IDT all of these system table registers

109
00:03:35,760 --> 00:03:38,879
they all had a privileged component to

110
00:03:38,480 --> 00:03:41,599
write

111
00:03:38,879 --> 00:03:42,000
and a non-privileged component to read

112
00:03:41,599 --> 00:03:44,319
so

113
00:03:42,000 --> 00:03:46,239
the implication of red pill back at the

114
00:03:44,319 --> 00:03:48,640
time circa 2004

115
00:03:46,239 --> 00:03:49,920
was that essentially well now malware

116
00:03:48,640 --> 00:03:51,840
can actually know

117
00:03:49,920 --> 00:03:54,080
that it's inside of a vm and so maybe

118
00:03:51,840 --> 00:03:55,840
it's going to think hey I'm inside of a

119
00:03:54,080 --> 00:03:57,280
malware analyst sandbox so I'm just

120
00:03:55,840 --> 00:04:00,959
going to not run

121
00:03:57,280 --> 00:04:02,560
well you know fast forward 15 plus years

122
00:04:00,959 --> 00:04:04,879
and you know virtualization based

123
00:04:02,560 --> 00:04:07,360
security is a thing on desktops

124
00:04:04,879 --> 00:04:09,040
and um you know just in general you know

125
00:04:07,360 --> 00:04:09,840
you have cloud-based infrastructure and

126
00:04:09,040 --> 00:04:12,319
so forth

127
00:04:09,840 --> 00:04:14,239
so I wouldn't say it's necessarily the

128
00:04:12,319 --> 00:04:16,639
case anymore that

129
00:04:14,239 --> 00:04:18,079
malware is disincentivized to run in a

130
00:04:16,639 --> 00:04:20,320
virtualized environment

131
00:04:18,079 --> 00:04:22,000
but still it depends on you know who the

132
00:04:20,320 --> 00:04:24,320
attackers are targeting maybe they want

133
00:04:22,000 --> 00:04:26,560
to run maybe they don't

134
00:04:24,320 --> 00:04:28,560
so inside of the manual there was this

135
00:04:26,560 --> 00:04:31,040
table that I had found at some point

136
00:04:28,560 --> 00:04:32,800
and it said you know these instructions

137
00:04:31,040 --> 00:04:33,919
are they useful to the application and

138
00:04:32,800 --> 00:04:35,840
there's a whole bunch of no

139
00:04:33,919 --> 00:04:37,600
none of these instructions are actually

140
00:04:35,840 --> 00:04:38,880
useful to people in ring 3

141
00:04:37,600 --> 00:04:41,680
and are they protected from the

142
00:04:38,880 --> 00:04:43,600
application and the answer is no for a

143
00:04:41,680 --> 00:04:44,880
bunch of them the reads or no's and the

144
00:04:43,600 --> 00:04:46,720
writes are yes

145
00:04:44,880 --> 00:04:48,240
so you know I kind of found this to be

146
00:04:46,720 --> 00:04:50,720
ironic

147
00:04:48,240 --> 00:04:52,320
but as I was updating these slides I

148
00:04:50,720 --> 00:04:54,320
went to grab a new screenshot

149
00:04:52,320 --> 00:04:56,560
higher quality screenshot and this is

150
00:04:54,320 --> 00:04:58,880
what I saw in the manual instead

151
00:04:56,560 --> 00:04:59,840
I saw that this thing like sidt which

152
00:04:58,880 --> 00:05:02,560
previously was

153
00:04:59,840 --> 00:05:03,520
unprotected now says it's protected from

154
00:05:02,560 --> 00:05:06,880
the application

155
00:05:03,520 --> 00:05:10,560
if CR4.UMIP equals 1

156
00:05:06,880 --> 00:05:12,000
so what is UMIP UMIP is user mode

157
00:05:10,560 --> 00:05:15,280
instruction prevention

158
00:05:12,000 --> 00:05:19,759
so we go off and we look at cpuid eax

159
00:05:15,280 --> 00:05:23,199
7 ecx 0 and we get an output in ecx

160
00:05:19,759 --> 00:05:25,680
of if bit 2 is set that means that this

161
00:05:23,199 --> 00:05:27,360
hardware supports UMIP

162
00:05:25,680 --> 00:05:28,800
then furthermore if the hardware

163
00:05:27,360 --> 00:05:32,400
supports UMIP

164
00:05:28,800 --> 00:05:33,840
Cr4 bit 11 has this user mode

165
00:05:32,400 --> 00:05:36,320
instruction prevention bit

166
00:05:33,840 --> 00:05:38,320
which when set means that the following

167
00:05:36,320 --> 00:05:39,520
instructions cannot be executed in CPL

168
00:05:38,320 --> 00:05:42,080
greater than 0 meaning

169
00:05:39,520 --> 00:05:44,240
anyone except kernel can't run sgdt

170
00:05:42,080 --> 00:05:45,520
can't store the GDT can't store the IDT

171
00:05:44,240 --> 00:05:47,039
store the LDT

172
00:05:45,520 --> 00:05:48,960
don't know what that is it's actually

173
00:05:47,039 --> 00:05:51,919
just storing part of a control register

174
00:05:48,960 --> 00:05:53,759
or store the task register and so UMIP

175
00:05:51,919 --> 00:05:54,639
is a rather recent thing which has just

176
00:05:53,759 --> 00:05:57,360
been added on

177
00:05:54,639 --> 00:05:59,039
you know later more recent processors so

178
00:05:57,360 --> 00:06:01,360
hasn't existed for most of history

179
00:05:59,039 --> 00:06:03,520
exists now but it doesn't mean it's

180
00:06:01,360 --> 00:06:08,160
necessarily used

181
00:06:03,520 --> 00:06:10,880
so UMIP bit 4 oh sorry bit 1 and cr4

182
00:06:08,160 --> 00:06:11,600
and you know you can now go check inside

183
00:06:10,880 --> 00:06:13,520
of your vm

184
00:06:11,600 --> 00:06:14,960
see whether or not UMIP is set inside of

185
00:06:13,520 --> 00:06:16,560
your vm and

186
00:06:14,960 --> 00:06:18,160
go ahead and test it also on your

187
00:06:16,560 --> 00:06:19,280
physical machine because sometimes

188
00:06:18,160 --> 00:06:20,560
things like this

189
00:06:19,280 --> 00:06:21,759
might actually be supported by the

190
00:06:20,560 --> 00:06:23,600
hardware but not supported by the

191
00:06:21,759 --> 00:06:24,960
virtualization system so

192
00:06:23,600 --> 00:06:26,720
maybe they clear it even if the

193
00:06:24,960 --> 00:06:29,680
operating system supports it so

194
00:06:26,720 --> 00:06:30,880
I want you to pause now and go on and

195
00:06:29,680 --> 00:06:33,840
you know do this lab

196
00:06:30,880 --> 00:06:34,560
quick go check bit 11 inside of your vm

197
00:06:33,840 --> 00:06:36,479
it's okay

198
00:06:34,560 --> 00:06:37,919
I'll wait for you you should pause and

199
00:06:36,479 --> 00:06:41,039
do this right

200
00:06:37,919 --> 00:06:41,440
now because otherwise I'm going to spoil

201
00:06:41,039 --> 00:06:44,880
this

202
00:06:41,440 --> 00:06:46,240
in the next part all right and the

203
00:06:44,880 --> 00:06:48,479
spoiler is

204
00:06:46,240 --> 00:06:50,400
windows doesn't actually use UMIP so it

205
00:06:48,479 --> 00:06:52,479
turns out that they had a virtualization

206
00:06:50,400 --> 00:06:53,199
based security mechanism that predated

207
00:06:52,479 --> 00:06:55,360
umip

208
00:06:53,199 --> 00:06:56,400
where they used it to block things like

209
00:06:55,360 --> 00:06:58,960
sgdt and

210
00:06:56,400 --> 00:07:01,280
sidt so they already had a mechanism

211
00:06:58,960 --> 00:07:03,039
they're continuing to use that

212
00:07:01,280 --> 00:07:04,560
and so that's that's where their eggs

213
00:07:03,039 --> 00:07:05,680
are and that's the basket their eggs are

214
00:07:04,560 --> 00:07:09,039
in

215
00:07:05,680 --> 00:07:11,360
so then uh inspired by things like nope

216
00:07:09,039 --> 00:07:13,520
uh red pill there were subsequent things

217
00:07:11,360 --> 00:07:15,680
like Nopill or ScoopyNG

218
00:07:13,520 --> 00:07:16,960
these would use other things like

219
00:07:15,680 --> 00:07:20,240
profiling the

220
00:07:16,960 --> 00:07:21,840
ldtr rather than the idtr

221
00:07:20,240 --> 00:07:23,680
and so you know this was done because

222
00:07:21,840 --> 00:07:25,759
you know red pill signatures could be

223
00:07:23,680 --> 00:07:26,880
unreliable but this is less relevant

224
00:07:25,759 --> 00:07:29,840
these days given that

225
00:07:26,880 --> 00:07:30,240
windows is not using the LDT and Scoopy

226
00:07:29,840 --> 00:07:32,240
NG

227
00:07:30,240 --> 00:07:34,160
is basically just including a variety of

228
00:07:32,240 --> 00:07:37,199
different checks including things

229
00:07:34,160 --> 00:07:38,639
such as checking for special mechanisms

230
00:07:37,199 --> 00:07:39,520
that are virtualization software

231
00:07:38,639 --> 00:07:41,039
specific

232
00:07:39,520 --> 00:07:42,960
in order to detect whether or not

233
00:07:41,039 --> 00:07:45,039
software is running inside of a vm

234
00:07:42,960 --> 00:07:46,400
this one in particular VMware port io

235
00:07:45,039 --> 00:07:49,599
we'll learn about a little more

236
00:07:46,400 --> 00:07:49,599
later in the class

