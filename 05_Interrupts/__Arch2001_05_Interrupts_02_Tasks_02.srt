1
00:00:00,000 --> 00:00:03,840
all right let's do a quick little lab to

2
00:00:01,839 --> 00:00:06,879
look at what the TSS

3
00:00:03,840 --> 00:00:09,360
looks like on our VMs so first you're

4
00:00:06,879 --> 00:00:11,200
going to use the ms_gdt command

5
00:00:09,360 --> 00:00:12,480
like you used in the previous section

6
00:00:11,200 --> 00:00:14,960
about segmentation

7
00:00:12,480 --> 00:00:17,199
and from there you will find a TSS

8
00:00:14,960 --> 00:00:19,760
64-bit TSS doesn't matter which one you

9
00:00:17,199 --> 00:00:22,080
pick if you have a multi-processor vm

10
00:00:19,760 --> 00:00:23,519
then you're going to use the dt display

11
00:00:22,080 --> 00:00:25,760
type command and you're going to

12
00:00:23,519 --> 00:00:27,519
recursively display all the types

13
00:00:25,760 --> 00:00:29,439
and you're going to cast that address

14
00:00:27,519 --> 00:00:32,480
that you got from the GDT

15
00:00:29,439 --> 00:00:33,480
into a microsoft kernel data structure

16
00:00:32,480 --> 00:00:36,559
the kernel

17
00:00:33,480 --> 00:00:37,680
TSS64 and that will show you the various

18
00:00:36,559 --> 00:00:39,920
contents

19
00:00:37,680 --> 00:00:41,520
that look like this so this the data

20
00:00:39,920 --> 00:00:43,360
structure from microsoft is going to be

21
00:00:41,520 --> 00:00:46,320
telling us about this

22
00:00:43,360 --> 00:00:49,200
so let's go ahead and see that after all

23
00:00:46,320 --> 00:00:49,200
my animations

24
00:00:49,680 --> 00:00:52,079
all right

25
00:00:53,120 --> 00:01:02,320
so go ahead and break in in the kernel

26
00:00:58,320 --> 00:01:05,760
run the ms_gdt command

27
00:01:02,320 --> 00:01:07,840
find an address from a 64-bit

28
00:01:05,760 --> 00:01:09,439
task state segment you can see that it

29
00:01:07,840 --> 00:01:10,080
happens to be using the type of busy

30
00:01:09,439 --> 00:01:13,920
here

31
00:01:10,080 --> 00:01:16,080
not available and then we are going to

32
00:01:13,920 --> 00:01:18,400
copy this command which you should get

33
00:01:16,080 --> 00:01:22,960
from the website

34
00:01:18,400 --> 00:01:24,720
and cast the information to that

35
00:01:22,960 --> 00:01:26,240
all right and there you go so it's

36
00:01:24,720 --> 00:01:26,960
basically saying you know reserved is

37
00:01:26,240 --> 00:01:30,159
0

38
00:01:26,960 --> 00:01:34,079
rsp0 is currently set to

39
00:01:30,159 --> 00:01:35,439
some actual real value pause my vm here

40
00:01:34,079 --> 00:01:36,720
again because I don't like my fan

41
00:01:35,439 --> 00:01:39,119
spinning up

42
00:01:36,720 --> 00:01:40,000
so rsp0 is actually filled in rsp1

43
00:01:39,119 --> 00:01:41,920
and 2 are not

44
00:01:40,000 --> 00:01:43,520
actually filled in so that just shows

45
00:01:41,920 --> 00:01:44,960
you that you know microsoft is not

46
00:01:43,520 --> 00:01:47,360
expecting to use

47
00:01:44,960 --> 00:01:48,560
ring 1 or ring 2 the IST the

48
00:01:47,360 --> 00:01:51,840
interrupt stack table

49
00:01:48,560 --> 00:01:55,040
is has particular individual

50
00:01:51,840 --> 00:01:56,079
values for entries 1, 2, 3 and

51
00:01:55,040 --> 00:01:57,759
4

52
00:01:56,079 --> 00:01:59,360
0 is actually reserved so we can't

53
00:01:57,759 --> 00:02:01,040
use that and then

54
00:01:59,360 --> 00:02:02,399
the other values are not actually used

55
00:02:01,040 --> 00:02:04,479
so there's at least 4

56
00:02:02,399 --> 00:02:06,560
interrupts that we would expect later on

57
00:02:04,479 --> 00:02:07,119
which are going to have some specific

58
00:02:06,560 --> 00:02:08,800
stack

59
00:02:07,119 --> 00:02:11,440
that they want to use that is not equal

60
00:02:08,800 --> 00:02:13,760
to the normal just going into ring 0

61
00:02:11,440 --> 00:02:13,760
stack

62
00:02:17,680 --> 00:02:20,720
okay so what did we learn in this

63
00:02:19,840 --> 00:02:22,879
section well

64
00:02:20,720 --> 00:02:25,280
first of all we learned about the task

65
00:02:22,879 --> 00:02:27,520
register which is also shown here

66
00:02:25,280 --> 00:02:28,400
it's just a segment selector a 16-bit

67
00:02:27,520 --> 00:02:31,840
value

68
00:02:28,400 --> 00:02:34,400
which selects from the GDT

69
00:02:31,840 --> 00:02:36,959
a TSS descriptor which we saw looks the

70
00:02:34,400 --> 00:02:40,000
exact same as the LDT descriptor

71
00:02:36,959 --> 00:02:42,640
and that specifies a base and limit for

72
00:02:40,000 --> 00:02:45,599
the task state segment which we just saw

73
00:02:42,640 --> 00:02:46,560
holds some locations where the stack

74
00:02:45,599 --> 00:02:49,760
should be

75
00:02:46,560 --> 00:02:52,160
used when you have an interrupt

76
00:02:49,760 --> 00:02:53,599
also we saw in the TSS there's a thing

77
00:02:52,160 --> 00:02:56,400
called the interrupt stack table

78
00:02:53,599 --> 00:02:58,400
the IST and so within there that can

79
00:02:56,400 --> 00:03:00,080
also be used for some specific

80
00:02:58,400 --> 00:03:03,200
interrupts which we'll learn about in

81
00:03:00,080 --> 00:03:03,200
the next section

