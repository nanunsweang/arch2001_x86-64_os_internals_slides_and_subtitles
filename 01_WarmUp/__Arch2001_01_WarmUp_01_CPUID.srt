1
00:00:00,160 --> 00:00:03,199
okay let's start the class with a little

2
00:00:01,839 --> 00:00:04,960
bit of a warm up

3
00:00:03,199 --> 00:00:06,560
first thing we're going to do is learn a

4
00:00:04,960 --> 00:00:09,519
new assembly instruction

5
00:00:06,560 --> 00:00:11,200
cpuid which stands for cpu feature

6
00:00:09,519 --> 00:00:13,599
identification

7
00:00:11,200 --> 00:00:14,960
so as Intel continues to change its

8
00:00:13,599 --> 00:00:16,880
hardware over time

9
00:00:14,960 --> 00:00:18,720
they need a way for software to find out

10
00:00:16,880 --> 00:00:19,600
whether or not particular features are

11
00:00:18,720 --> 00:00:21,359
available

12
00:00:19,600 --> 00:00:23,359
so whether or not it supports hardware

13
00:00:21,359 --> 00:00:24,400
virtualization hyper threading thermal

14
00:00:23,359 --> 00:00:26,880
monitors

15
00:00:24,400 --> 00:00:27,760
or a variety of new security features

16
00:00:26,880 --> 00:00:30,080
cpuid

17
00:00:27,760 --> 00:00:31,439
is a mechanism in order to find out

18
00:00:30,080 --> 00:00:35,360
whether or not the cpuid

19
00:00:31,439 --> 00:00:38,079
cpu supports these functions so cpuid

20
00:00:35,360 --> 00:00:40,879
doesn't exactly have operands per se

21
00:00:38,079 --> 00:00:41,840
to the instruction it is just cpuid

22
00:00:40,879 --> 00:00:44,879
instruction

23
00:00:41,840 --> 00:00:46,559
but it takes a sort of input in a value

24
00:00:44,879 --> 00:00:49,200
that is placed into eax

25
00:00:46,559 --> 00:00:50,320
it is eax even on 64-bit systems it's

26
00:00:49,200 --> 00:00:53,039
not rax

27
00:00:50,320 --> 00:00:53,680
it's still using the eax register and

28
00:00:53,039 --> 00:00:56,320
also

29
00:00:53,680 --> 00:00:58,160
sometimes some particular things that

30
00:00:56,320 --> 00:01:00,079
need to be looked up we'll also put a

31
00:00:58,160 --> 00:01:02,480
value in ecx

32
00:01:00,079 --> 00:01:04,239
so basically software puts values into

33
00:01:02,480 --> 00:01:06,720
eax and ecx

34
00:01:04,239 --> 00:01:08,799
and then after the cpuid instruction is

35
00:01:06,720 --> 00:01:12,479
executed the outputs are stored in

36
00:01:08,799 --> 00:01:16,080
e a b c d x so

37
00:01:12,479 --> 00:01:18,880
eax, ebx, ecx, edx

38
00:01:16,080 --> 00:01:20,560
so the first thing is technically cpuid

39
00:01:18,880 --> 00:01:22,479
didn't exist on the first systems

40
00:01:20,560 --> 00:01:23,600
so you would have to actually look up

41
00:01:22,479 --> 00:01:25,759
whether or not the

42
00:01:23,600 --> 00:01:27,840
hardware even supports cpuid well of

43
00:01:25,759 --> 00:01:30,000
course everyone at this point should be

44
00:01:27,840 --> 00:01:31,439
running something newer than a 486

45
00:01:30,000 --> 00:01:32,479
otherwise I don't know exactly how you

46
00:01:31,439 --> 00:01:35,200
think you're going to do a

47
00:01:32,479 --> 00:01:37,119
64-bit class but just to kind of cover

48
00:01:35,200 --> 00:01:39,360
how this worked and to hit another one

49
00:01:37,119 --> 00:01:42,240
of those bits in the eflags register

50
00:01:39,360 --> 00:01:43,600
we find out that whether or not the CPU

51
00:01:42,240 --> 00:01:47,360
supports the cpuid

52
00:01:43,600 --> 00:01:48,640
instruction is conveyed via the id flag

53
00:01:47,360 --> 00:01:51,600
which is bit 21

54
00:01:48,640 --> 00:01:53,200
in the eflags register and how software

55
00:01:51,600 --> 00:01:55,040
figures out whether or not

56
00:01:53,200 --> 00:01:56,399
cpuid is supported is they don't just look

57
00:01:55,040 --> 00:01:58,399
is it 1 is it 0

58
00:01:56,399 --> 00:02:00,399
they have to be able to set and clear

59
00:01:58,399 --> 00:02:01,920
the flag and that will indicate support

60
00:02:00,399 --> 00:02:04,560
for cpuid

61
00:02:01,920 --> 00:02:06,000
so here is the first of the new flags

62
00:02:04,560 --> 00:02:10,399
that I promised to show you

63
00:02:06,000 --> 00:02:13,200
the id flag bit 21 in eflags register

64
00:02:10,399 --> 00:02:14,800
and so how would we change the eflags

65
00:02:13,200 --> 00:02:16,239
register how would we set and clear the

66
00:02:14,800 --> 00:02:18,160
eflags register

67
00:02:16,239 --> 00:02:20,720
well to do that we need some more

68
00:02:18,160 --> 00:02:24,000
instructions specifically the push

69
00:02:20,720 --> 00:02:24,879
f for flag as in eflags or flags as it

70
00:02:24,000 --> 00:02:27,520
originally was

71
00:02:24,879 --> 00:02:28,640
pushfq quad word size that's the

72
00:02:27,520 --> 00:02:31,840
rflags register

73
00:02:28,640 --> 00:02:33,040
and popfq so basically remember that

74
00:02:31,840 --> 00:02:35,680
qword form of

75
00:02:33,040 --> 00:02:37,760
eflags is rflags it's really just got

76
00:02:35,680 --> 00:02:39,360
a bunch of it's got 32 extra bits that

77
00:02:37,760 --> 00:02:41,120
don't do anything they're all zero

78
00:02:39,360 --> 00:02:42,560
so all the real bits are in the bottom

79
00:02:41,120 --> 00:02:44,319
32 bits

80
00:02:42,560 --> 00:02:45,760
and so sometimes I mention this just

81
00:02:44,319 --> 00:02:47,920
because some things like

82
00:02:45,760 --> 00:02:49,200
you know visual studio for instance will

83
00:02:47,920 --> 00:02:51,920
actually say

84
00:02:49,200 --> 00:02:53,599
eflags instead of rflags so sometimes

85
00:02:51,920 --> 00:02:56,959
I'll say one or the other but

86
00:02:53,599 --> 00:02:59,440
all the relevant bits are in eflags. So

87
00:02:56,959 --> 00:03:00,800
pushfq it's going to push the rflags

88
00:02:59,440 --> 00:03:02,480
register onto the stack

89
00:03:00,800 --> 00:03:04,640
and so the idea would be push it onto

90
00:03:02,480 --> 00:03:05,840
the stack and then manipulate the value

91
00:03:04,640 --> 00:03:07,920
that's on the stack

92
00:03:05,840 --> 00:03:10,400
and then pop it back into the register

93
00:03:07,920 --> 00:03:12,560
using the popfq

94
00:03:10,400 --> 00:03:13,760
and so the thing is when you're setting

95
00:03:12,560 --> 00:03:15,280
flags this way

96
00:03:13,760 --> 00:03:17,200
there are actually going to be some

97
00:03:15,280 --> 00:03:19,040
flags in the rflags register

98
00:03:17,200 --> 00:03:21,280
which will not actually be set unless

99
00:03:19,040 --> 00:03:23,360
you're running in kernel mode

100
00:03:21,280 --> 00:03:25,200
so just so you know not everything that

101
00:03:23,360 --> 00:03:27,040
you might find in the rflags register

102
00:03:25,200 --> 00:03:29,360
can actually be set by everyone in user

103
00:03:27,040 --> 00:03:29,360
space

104
00:03:29,599 --> 00:03:33,519
now let's look at some examples of cpuid

105
00:03:32,239 --> 00:03:35,920
inputs and outputs

106
00:03:33,519 --> 00:03:38,159
so in the Intel manual this column

107
00:03:35,920 --> 00:03:39,920
represents the values that would be put

108
00:03:38,159 --> 00:03:43,200
into the eax value

109
00:03:39,920 --> 00:03:45,120
eax register so if you put 0 into eax

110
00:03:43,200 --> 00:03:47,760
and then you executed cpuid

111
00:03:45,120 --> 00:03:49,680
what you would get is the output eax

112
00:03:47,760 --> 00:03:51,200
would have the maximum input value

113
00:03:49,680 --> 00:03:53,200
basically telling you like what is the

114
00:03:51,200 --> 00:03:54,239
maximum value that could exist in this

115
00:03:53,200 --> 00:03:58,080
column

116
00:03:54,239 --> 00:03:59,120
and then ebx edx ecx would contain the

117
00:03:58,080 --> 00:04:02,959
string

118
00:03:59,120 --> 00:04:03,599
genuine that's a capital 'I' not a

119
00:04:02,959 --> 00:04:07,280
lowercase

120
00:04:03,599 --> 00:04:09,360
'l'. 'i'-intel so genuine intel

121
00:04:07,280 --> 00:04:11,360
and you can actually see Wikipedia if

122
00:04:09,360 --> 00:04:13,200
you want for cpuid if you want to see

123
00:04:11,360 --> 00:04:14,720
other vendors. AMD has a different string

124
00:04:13,200 --> 00:04:16,160
that would appear and so that's how you

125
00:04:14,720 --> 00:04:18,959
can tell if this was a

126
00:04:16,160 --> 00:04:19,359
intel system versus AMD and a bunch of

127
00:04:18,959 --> 00:04:21,359
other

128
00:04:19,359 --> 00:04:23,280
different vendors if they supported x86

129
00:04:21,359 --> 00:04:25,440
will provide some sort of different

130
00:04:23,280 --> 00:04:27,040
value here to say no we're not an Intel

131
00:04:25,440 --> 00:04:30,320
processor where something else

132
00:04:27,040 --> 00:04:31,840
that's implementing x86. We'll actually

133
00:04:30,320 --> 00:04:35,280
see this one a little bit later

134
00:04:31,840 --> 00:04:38,720
input 2 when we start covering the

135
00:04:35,280 --> 00:04:39,040
cache and TLB information but here's one

136
00:04:38,720 --> 00:04:41,120
that

137
00:04:39,040 --> 00:04:42,800
is a little more interesting to us here

138
00:04:41,120 --> 00:04:45,919
at open security training

139
00:04:42,800 --> 00:04:49,280
some security bits so if the input is

140
00:04:45,919 --> 00:04:50,639
7 and the ecx is 0

141
00:04:49,280 --> 00:04:53,520
because there's going to be a 7 and

142
00:04:50,639 --> 00:04:55,040
something that's non-zero so 7 and 0

143
00:04:53,520 --> 00:04:56,639
bit 2 would for instance tell you

144
00:04:55,040 --> 00:04:59,120
whether or not supports Intel

145
00:04:56,639 --> 00:04:59,759
software guard extensions which is its

146
00:04:59,120 --> 00:05:02,080
way

147
00:04:59,759 --> 00:05:04,160
Intel security technology for providing

148
00:05:02,080 --> 00:05:07,280
a sort of isolated enclave of

149
00:05:04,160 --> 00:05:09,520
execution. Bit 7 here

150
00:05:07,280 --> 00:05:10,960
in the ebx register would tell us

151
00:05:09,520 --> 00:05:13,280
whether or not it supports

152
00:05:10,960 --> 00:05:14,000
supervisory mode execution prevention

153
00:05:13,280 --> 00:05:16,479
SMEP

154
00:05:14,000 --> 00:05:18,800
that's a security mechanism that the

155
00:05:16,479 --> 00:05:19,600
kernel can use in order to basically say

156
00:05:18,800 --> 00:05:22,479
dear kernel

157
00:05:19,600 --> 00:05:24,479
dear myself don't let me execute stuff

158
00:05:22,479 --> 00:05:27,360
in user space code

159
00:05:24,479 --> 00:05:28,800
areas so that can be used to help

160
00:05:27,360 --> 00:05:29,919
mitigate against certain types of

161
00:05:28,800 --> 00:05:32,000
exploits where

162
00:05:29,919 --> 00:05:34,880
an attacker forces the kernel to jump to

163
00:05:32,000 --> 00:05:37,120
code that they control in user space

164
00:05:34,880 --> 00:05:39,120
and there's SMEP and there's SMAP and if

165
00:05:37,120 --> 00:05:41,520
SMEP is execution prevention

166
00:05:39,120 --> 00:05:43,280
SMAP is access prevention and this

167
00:05:41,520 --> 00:05:44,240
basically just says you know stop the

168
00:05:43,280 --> 00:05:46,960
kernel from

169
00:05:44,240 --> 00:05:48,960
you know accessing data that is in user

170
00:05:46,960 --> 00:05:50,160
space because maybe the attacker is

171
00:05:48,960 --> 00:05:52,479
you know again trying to trick the

172
00:05:50,160 --> 00:05:54,320
kernel into reading data they control

173
00:05:52,479 --> 00:05:56,880
so we'll cover SMEP and SMAP a little

174
00:05:54,320 --> 00:06:00,240
bit later on but really that's a topic

175
00:05:56,880 --> 00:06:02,960
for later exploits classes

176
00:06:00,240 --> 00:06:03,840
all right so let's go ahead and see a

177
00:06:02,960 --> 00:06:06,560
basic

178
00:06:03,840 --> 00:06:08,319
lab that uses the cpuid instruction and

179
00:06:06,560 --> 00:06:10,319
this is basically going to be a

180
00:06:08,319 --> 00:06:13,280
template or framework to let you fiddle

181
00:06:10,319 --> 00:06:15,120
around and later on you know read and

182
00:06:13,280 --> 00:06:17,440
see particular things that are supported

183
00:06:15,120 --> 00:06:20,080
on your hardware

184
00:06:17,440 --> 00:06:21,360
so you should have already set up your

185
00:06:20,080 --> 00:06:24,319
code in your class but

186
00:06:21,360 --> 00:06:25,600
just to show it in case you didn't again

187
00:06:24,319 --> 00:06:28,000
you have to go to the

188
00:06:25,600 --> 00:06:29,759
downloads page that url is on the set

189
00:06:28,000 --> 00:06:33,039
this up before you start the class

190
00:06:29,759 --> 00:06:36,240
supposed to download the zip file

191
00:06:33,039 --> 00:06:39,840
and open it expand it

192
00:06:36,240 --> 00:06:41,600
extract it all extract

193
00:06:39,840 --> 00:06:42,960
and then I requested that you put it

194
00:06:41,600 --> 00:06:45,120
onto your desktop

195
00:06:42,960 --> 00:06:49,199
just so that you know we all have a

196
00:06:45,120 --> 00:06:51,280
common location

197
00:06:49,199 --> 00:06:53,440
and I've got some older version but this

198
00:06:51,280 --> 00:06:56,880
is the version I did right now

199
00:06:53,440 --> 00:06:58,240
and then you open up the solution file

200
00:06:56,880 --> 00:07:00,160
architecture 2001

201
00:06:58,240 --> 00:07:01,680
os internals you're going to get a

202
00:07:00,160 --> 00:07:04,400
prompt that says you know hey

203
00:07:01,680 --> 00:07:06,080
this is untrustworthy and you say like

204
00:07:04,400 --> 00:07:06,800
don't ask me that I know I just

205
00:07:06,080 --> 00:07:09,680
downloaded

206
00:07:06,800 --> 00:07:09,680
and hit ok

207
00:07:09,840 --> 00:07:13,440
so U_cpuid is what we're going to want to

208
00:07:12,000 --> 00:07:16,240
look at. I

209
00:07:13,440 --> 00:07:16,880
renamed the various projects to have K

210
00:07:16,240 --> 00:07:18,560
if they're going to

211
00:07:16,880 --> 00:07:21,520
execute in kernel space and U if

212
00:07:18,560 --> 00:07:23,919
they're meant to execute in user space

213
00:07:21,520 --> 00:07:25,280
so if we look at the source code for

214
00:07:23,919 --> 00:07:28,800
U_cpuid

215
00:07:25,280 --> 00:07:31,199
there's a thing called is_cpuid_supported

216
00:07:28,800 --> 00:07:33,120
which is found in our assembly code here

217
00:07:31,199 --> 00:07:36,880
and that has to do with the thing about

218
00:07:33,120 --> 00:07:39,039
pushf and popfq to modify the bits to

219
00:07:36,880 --> 00:07:40,000
confirm that we can really use the cpuid

220
00:07:39,039 --> 00:07:42,400
instruction again

221
00:07:40,000 --> 00:07:44,319
everyone should have cpuid capable

222
00:07:42,400 --> 00:07:46,000
systems at this point but if you want to

223
00:07:44,319 --> 00:07:47,680
single step through that you can see

224
00:07:46,000 --> 00:07:49,520
what's going on with the pushing of the

225
00:07:47,680 --> 00:07:51,919
flags onto the stack and modifying the

226
00:07:49,520 --> 00:07:51,919
flags

227
00:07:52,560 --> 00:07:57,440
and so basically this code is going to

228
00:07:55,360 --> 00:07:59,919
check if cpuid is supported we expect

229
00:07:57,440 --> 00:08:02,879
everyone to have cpuid supported

230
00:07:59,919 --> 00:08:04,400
and then it'll go down to call cpuid

231
00:08:02,879 --> 00:08:06,560
which is another little assembly

232
00:08:04,400 --> 00:08:08,639
function that we have here

233
00:08:06,560 --> 00:08:10,160
and so all this does is it takes a

234
00:08:08,639 --> 00:08:12,879
variety of it takes some

235
00:08:10,160 --> 00:08:14,000
input arguments which are the ecx and

236
00:08:12,879 --> 00:08:16,639
the eax

237
00:08:14,000 --> 00:08:17,199
which are supposed to be set and then it

238
00:08:16,639 --> 00:08:19,440
takes

239
00:08:17,199 --> 00:08:22,080
some output arguments which are the eax

240
00:08:19,440 --> 00:08:25,039
out to ebx out ecx edx out

241
00:08:22,080 --> 00:08:27,680
and it's basically passing by reference

242
00:08:25,039 --> 00:08:30,639
pointers to places where to store those

243
00:08:27,680 --> 00:08:32,719
it's going to set your value into the

244
00:08:30,639 --> 00:08:33,599
rax register really the lower bits being

245
00:08:32,719 --> 00:08:36,080
the eax

246
00:08:33,599 --> 00:08:36,800
and ecx then it's going to just call the

247
00:08:36,080 --> 00:08:39,120
cpuid

248
00:08:36,800 --> 00:08:40,560
assembly instruction afterwards it's

249
00:08:39,120 --> 00:08:44,720
going to store back out

250
00:08:40,560 --> 00:08:46,959
those values from eax ebx ecx edx

251
00:08:44,720 --> 00:08:48,080
and consequently those will be placed

252
00:08:46,959 --> 00:08:51,920
into the

253
00:08:48,080 --> 00:08:55,680
C variables of eax_out ebx_out ecx_out

254
00:08:51,920 --> 00:08:58,880
edx_out so we said that if eax

255
00:08:55,680 --> 00:09:02,000
was 0 then the eax

256
00:08:58,880 --> 00:09:02,720
out is going to be the maximum input

257
00:09:02,000 --> 00:09:05,600
value for

258
00:09:02,720 --> 00:09:07,200
cpuid information so that tells you you

259
00:09:05,600 --> 00:09:08,000
know this is the maximum thing you can

260
00:09:07,200 --> 00:09:10,160
actually ask about

261
00:09:08,000 --> 00:09:11,440
in cpuid on this particular processor

262
00:09:10,160 --> 00:09:12,959
because of course that'll

263
00:09:11,440 --> 00:09:14,880
keep increasing over time as new

264
00:09:12,959 --> 00:09:18,480
features get added

265
00:09:14,880 --> 00:09:20,640
and then we're taking this ebx edx ecx

266
00:09:18,480 --> 00:09:22,880
and concatenating them in sort of the

267
00:09:20,640 --> 00:09:24,640
right order sticking a null terminator

268
00:09:22,880 --> 00:09:26,880
there and this should be our

269
00:09:24,640 --> 00:09:28,320
genuine Intel string because that's what

270
00:09:26,880 --> 00:09:32,080
the manual says

271
00:09:28,320 --> 00:09:33,360
will be conveyed out in ebx edx ecx

272
00:09:32,080 --> 00:09:35,279
and then there's some other things we

273
00:09:33,360 --> 00:09:38,399
could do we could do that thing eax

274
00:09:35,279 --> 00:09:41,360
in of 7 and the ecx of 0

275
00:09:38,399 --> 00:09:42,080
call cpuid and that should then give us

276
00:09:41,360 --> 00:09:44,480
you know

277
00:09:42,080 --> 00:09:45,279
some bits like in ebx that we can check

278
00:09:44,480 --> 00:09:48,640
so

279
00:09:45,279 --> 00:09:50,880
bit 2 bit 7 bit 20 will tell us about

280
00:09:48,640 --> 00:09:52,800
things like is SGX supported is SMEP

281
00:09:50,880 --> 00:09:54,399
supported is SMAP supported

282
00:09:52,800 --> 00:09:56,240
and there'll be some further things that

283
00:09:54,399 --> 00:09:58,160
we'll cover later on in the class and

284
00:09:56,240 --> 00:09:59,760
you can of course just edit this to your

285
00:09:58,160 --> 00:10:01,040
heart's content to check out anything

286
00:09:59,760 --> 00:10:03,440
you want to

287
00:10:01,040 --> 00:10:04,640
so what I want you to do is go ahead and

288
00:10:03,440 --> 00:10:08,000
set a breakpoint

289
00:10:04,640 --> 00:10:09,360
on the last line of this code

290
00:10:08,000 --> 00:10:10,959
make sure so this is the reason why I

291
00:10:09,360 --> 00:10:12,399
wanted to show you this is because it's

292
00:10:10,959 --> 00:10:14,079
a little counter-intuitive

293
00:10:12,399 --> 00:10:15,519
first time you open up the solution you

294
00:10:14,079 --> 00:10:18,160
got to make sure you're in

295
00:10:15,519 --> 00:10:19,200
64-bit compilation not which was the

296
00:10:18,160 --> 00:10:20,880
default

297
00:10:19,200 --> 00:10:24,000
and then you can go ahead and just run

298
00:10:20,880 --> 00:10:24,000
the thing in a debugger

299
00:10:24,880 --> 00:10:27,920
once you do that what you will see is

300
00:10:27,519 --> 00:10:30,959
the

301
00:10:27,920 --> 00:10:32,079
output basically saying the maximum

302
00:10:30,959 --> 00:10:34,959
input value for

303
00:10:32,079 --> 00:10:36,000
cpuid is 22 on this particular

304
00:10:34,959 --> 00:10:37,839
processor that it's

305
00:10:36,000 --> 00:10:39,600
executing on right now the identity

306
00:10:37,839 --> 00:10:42,240
string is genuine intel

307
00:10:39,600 --> 00:10:43,360
and based on the bits it does not

308
00:10:42,240 --> 00:10:45,040
support SGX

309
00:10:43,360 --> 00:10:47,680
inside this processor inside this

310
00:10:45,040 --> 00:10:49,440
virtualization now sometimes

311
00:10:47,680 --> 00:10:52,160
your virtualization environment might

312
00:10:49,440 --> 00:10:53,040
actually lie about cpuid information so

313
00:10:52,160 --> 00:10:54,880
that's kind of

314
00:10:53,040 --> 00:10:56,560
part of the game with virtualization if

315
00:10:54,880 --> 00:10:57,519
you're running something in a vm like I

316
00:10:56,560 --> 00:11:00,000
am right now

317
00:10:57,519 --> 00:11:02,480
the hypervisor can add its will choose

318
00:11:00,000 --> 00:11:03,120
to you know intercept things like cpuid

319
00:11:02,480 --> 00:11:05,200
and

320
00:11:03,120 --> 00:11:07,279
lie about the register values that come

321
00:11:05,200 --> 00:11:08,720
back so you know we don't really know

322
00:11:07,279 --> 00:11:10,399
whether or not this processor supports

323
00:11:08,720 --> 00:11:11,920
SGX or not we don't really know whether

324
00:11:10,399 --> 00:11:13,839
it supports SMEP or SMAP

325
00:11:11,920 --> 00:11:15,279
could be the virtualization lying to us

326
00:11:13,839 --> 00:11:15,680
you'd have to actually run this kind of

327
00:11:15,279 --> 00:11:17,680
thing

328
00:11:15,680 --> 00:11:19,920
outside of your virtual machine on the

329
00:11:17,680 --> 00:11:21,040
raw hardware to find out what's really

330
00:11:19,920 --> 00:11:22,959
true

331
00:11:21,040 --> 00:11:24,560
but anyways for now tells us this

332
00:11:22,959 --> 00:11:26,240
doesn't support SGX and I know that's

333
00:11:24,560 --> 00:11:28,320
actually true on my processor

334
00:11:26,240 --> 00:11:29,680
and it says it does support SMEP and it

335
00:11:28,320 --> 00:11:32,399
does support SMAP

336
00:11:29,680 --> 00:11:34,079
so that's the basics of cpuid like I

337
00:11:32,399 --> 00:11:36,240
said you can go ahead and

338
00:11:34,079 --> 00:11:37,600
you know go look at the Intel manual for

339
00:11:36,240 --> 00:11:39,120
cpuid and

340
00:11:37,600 --> 00:11:40,959
fiddle with this to your heart's content

341
00:11:39,120 --> 00:11:42,160
you know set different values in get

342
00:11:40,959 --> 00:11:43,760
different values out

343
00:11:42,160 --> 00:11:47,120
and see what's supported on your

344
00:11:43,760 --> 00:11:47,120
hardware and what isn't

