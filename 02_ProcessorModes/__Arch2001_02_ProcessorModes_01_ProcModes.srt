1
00:00:00,399 --> 00:00:03,600
all right so I want to talk to you about

2
00:00:01,920 --> 00:00:05,279
the different ways which an intel

3
00:00:03,600 --> 00:00:07,040
processor can execute

4
00:00:05,279 --> 00:00:08,880
and to do that intel has provided us

5
00:00:07,040 --> 00:00:09,679
with this extremely helpful and not at

6
00:00:08,880 --> 00:00:13,040
all messy

7
00:00:09,679 --> 00:00:16,400
finite state machine so all roads

8
00:00:13,040 --> 00:00:19,600
of resets lead to real mode

9
00:00:16,400 --> 00:00:21,199
so let's go ahead and start there

10
00:00:19,600 --> 00:00:22,800
so in the beginning there was real mode

11
00:00:21,199 --> 00:00:23,840
and it was the suck from a security

12
00:00:22,800 --> 00:00:26,640
perspective

13
00:00:23,840 --> 00:00:27,920
it is the original execution mode of the

14
00:00:26,640 --> 00:00:31,359
intel 8086

15
00:00:27,920 --> 00:00:34,399
processor and that means that basically

16
00:00:31,359 --> 00:00:35,280
old software things like dos run in real

17
00:00:34,399 --> 00:00:36,719
mode

18
00:00:35,280 --> 00:00:38,480
but of course because it's the very

19
00:00:36,719 --> 00:00:40,559
first mode of execution

20
00:00:38,480 --> 00:00:41,920
it does it's not very featureful right

21
00:00:40,559 --> 00:00:43,520
it doesn't have virtual memory which

22
00:00:41,920 --> 00:00:46,000
we'll learn all about in this class

23
00:00:43,520 --> 00:00:48,000
has no privilege levels so there's no

24
00:00:46,000 --> 00:00:49,120
security separation everything's all

25
00:00:48,000 --> 00:00:51,760
just running in

26
00:00:49,120 --> 00:00:52,640
one big executable space and that's why

27
00:00:51,760 --> 00:00:55,760
you know older

28
00:00:52,640 --> 00:00:56,559
operating systems were very unreliable

29
00:00:55,760 --> 00:00:58,320
because

30
00:00:56,559 --> 00:01:00,000
different processes could scribble all

31
00:00:58,320 --> 00:01:01,280
over everybody's memory and crash the

32
00:01:00,000 --> 00:01:03,440
overall system

33
00:01:01,280 --> 00:01:04,640
and it is the original sort of 16-bit

34
00:01:03,440 --> 00:01:06,479
execution mode

35
00:01:04,640 --> 00:01:09,119
now like that finite state machine

36
00:01:06,479 --> 00:01:11,040
showed if you reset your processor it is

37
00:01:09,119 --> 00:01:12,880
going to go back into real mode

38
00:01:11,040 --> 00:01:14,799
and so everything always starts in real

39
00:01:12,880 --> 00:01:16,640
mode every time you power on and power

40
00:01:14,799 --> 00:01:18,560
off the processor

41
00:01:16,640 --> 00:01:20,400
and so that's why it's usually the job

42
00:01:18,560 --> 00:01:22,400
of things like the BIOS

43
00:01:20,400 --> 00:01:23,840
to get you out of real mode as soon as

44
00:01:22,400 --> 00:01:25,200
humanly possible and get you into the

45
00:01:23,840 --> 00:01:27,360
more

46
00:01:25,200 --> 00:01:28,640
functional feature full protected mode

47
00:01:27,360 --> 00:01:29,119
so that's why we're not really going to

48
00:01:28,640 --> 00:01:30,960
cover

49
00:01:29,119 --> 00:01:33,280
real mode that much in this class we're

50
00:01:30,960 --> 00:01:36,320
going to cover it in architecture 4001

51
00:01:33,280 --> 00:01:38,880
which is more of the BIOS class

52
00:01:36,320 --> 00:01:41,759
all right so if we started in real mode

53
00:01:38,880 --> 00:01:43,680
and we got a system management interrupt

54
00:01:41,759 --> 00:01:45,920
we would find ourselves in system

55
00:01:43,680 --> 00:01:47,520
management mode

56
00:01:45,920 --> 00:01:49,520
so system management mode is an

57
00:01:47,520 --> 00:01:52,159
interesting execution mode

58
00:01:49,520 --> 00:01:53,600
it's basically a isolated mode off to

59
00:01:52,159 --> 00:01:55,280
the side it's invoked by

60
00:01:53,600 --> 00:01:57,680
that system management interrupt like

61
00:01:55,280 --> 00:01:58,880
mentioned but it has the capability for

62
00:01:57,680 --> 00:02:01,439
self-protection

63
00:01:58,880 --> 00:02:03,600
so essentially the code that executes in

64
00:02:01,439 --> 00:02:03,920
this special system management ram and

65
00:02:03,600 --> 00:02:06,079
this

66
00:02:03,920 --> 00:02:08,000
special system management mode it can

67
00:02:06,079 --> 00:02:09,599
protect itself against even the most

68
00:02:08,000 --> 00:02:11,440
powerful and privileged operating

69
00:02:09,599 --> 00:02:13,040
systems or hypervisors

70
00:02:11,440 --> 00:02:14,800
and so the original point of it was to

71
00:02:13,040 --> 00:02:16,560
have something that couldn't

72
00:02:14,800 --> 00:02:18,400
be messed up that would be responsible

73
00:02:16,560 --> 00:02:19,520
for things like power management

74
00:02:18,400 --> 00:02:21,680
something that

75
00:02:19,520 --> 00:02:23,599
needs to be done transparently in the

76
00:02:21,680 --> 00:02:25,280
background doesn't necessarily need any

77
00:02:23,599 --> 00:02:26,959
operating system specific things

78
00:02:25,280 --> 00:02:28,400
because it's more of a hardware specific

79
00:02:26,959 --> 00:02:30,400
type thing

80
00:02:28,400 --> 00:02:32,239
and also you know intel had recommended

81
00:02:30,400 --> 00:02:34,800
it for use via for

82
00:02:32,239 --> 00:02:35,440
security functionality and so for a long

83
00:02:34,800 --> 00:02:37,200
time

84
00:02:35,440 --> 00:02:39,920
uh you know BIOS vendors and things like

85
00:02:37,200 --> 00:02:43,440
that would put uh security critical code

86
00:02:39,920 --> 00:02:44,959
into system management mode so if you're

87
00:02:43,440 --> 00:02:47,440
familiar with other

88
00:02:44,959 --> 00:02:48,879
uh processors things like arm systems

89
00:02:47,440 --> 00:02:51,680
that have trust zone

90
00:02:48,879 --> 00:02:52,319
trust zone to me feels analogous to smm

91
00:02:51,680 --> 00:02:54,720
because

92
00:02:52,319 --> 00:02:56,480
it's basically where you may have all

93
00:02:54,720 --> 00:02:58,239
the security critical code

94
00:02:56,480 --> 00:02:59,760
in you know some special isolated area

95
00:02:58,239 --> 00:03:01,840
of memory but it's really just a

96
00:02:59,760 --> 00:03:04,000
different processor execution mode

97
00:03:01,840 --> 00:03:05,840
which isolates it it's not actual true

98
00:03:04,000 --> 00:03:07,599
hardware separation they're all

99
00:03:05,840 --> 00:03:09,440
all code just running on the same

100
00:03:07,599 --> 00:03:12,400
processor and you're hoping that your

101
00:03:09,440 --> 00:03:12,879
security mechanisms keep it isolated and

102
00:03:12,400 --> 00:03:14,560
so

103
00:03:12,879 --> 00:03:16,560
SMM is kind of interesting from that

104
00:03:14,560 --> 00:03:18,879
perspective but again

105
00:03:16,560 --> 00:03:20,319
this is a thing that is more relevant to

106
00:03:18,879 --> 00:03:21,920
stuff like BIOS's

107
00:03:20,319 --> 00:03:23,440
so we're going to be covering that in

108
00:03:21,920 --> 00:03:27,519
architecture 4001

109
00:03:23,440 --> 00:03:30,159
instead of this class so from real mode

110
00:03:27,519 --> 00:03:31,120
if you set some particular bit you would

111
00:03:30,159 --> 00:03:34,480
find yourself

112
00:03:31,120 --> 00:03:37,440
in protected mode. Now protected mode

113
00:03:34,480 --> 00:03:38,000
is the main thing that most operating

114
00:03:37,440 --> 00:03:40,480
systems

115
00:03:38,000 --> 00:03:41,840
actually execute in and there's this

116
00:03:40,480 --> 00:03:44,159
quote from the manual

117
00:03:41,840 --> 00:03:46,000
that I put in here just to you know call

118
00:03:44,159 --> 00:03:47,760
out the fact that it's saying

119
00:03:46,000 --> 00:03:49,200
there's a special way of executing in

120
00:03:47,760 --> 00:03:51,599
protected mode where

121
00:03:49,200 --> 00:03:53,040
you kind of pretend like you're in 8086

122
00:03:51,599 --> 00:03:53,760
mode you pretend like you're in real

123
00:03:53,040 --> 00:03:56,640
mode

124
00:03:53,760 --> 00:03:58,560
and that's called virtual-8086 mode this

125
00:03:56,640 --> 00:03:59,519
is sort of a backwards compatibility

126
00:03:58,560 --> 00:04:01,200
mechanism

127
00:03:59,519 --> 00:04:03,360
and the only reason I pull it out is

128
00:04:01,200 --> 00:04:06,000
just because it says virtual-8086 mode

129
00:04:03,360 --> 00:04:09,040
is actually a protected mode attribute

130
00:04:06,000 --> 00:04:11,599
that can be enabled for any task it is

131
00:04:09,040 --> 00:04:13,439
not actually a processor mode so even

132
00:04:11,599 --> 00:04:15,200
though intel has it on its finite state

133
00:04:13,439 --> 00:04:17,040
machine they say right in their manual

134
00:04:15,200 --> 00:04:17,759
that it's not actually a real processor

135
00:04:17,040 --> 00:04:20,000
mode

136
00:04:17,759 --> 00:04:21,919
so we're mostly not gonna well we're

137
00:04:20,000 --> 00:04:23,040
entirely not going to care about virtual

138
00:04:21,919 --> 00:04:25,520
8086 mode

139
00:04:23,040 --> 00:04:26,160
you can just think of it like when

140
00:04:25,520 --> 00:04:28,400
windows

141
00:04:26,160 --> 00:04:30,800
first started having you know 32-bit

142
00:04:28,400 --> 00:04:33,040
execution and they wanted to run

143
00:04:30,800 --> 00:04:34,400
you know 16-bit dos programs because

144
00:04:33,040 --> 00:04:35,840
backwards compatibility is the most

145
00:04:34,400 --> 00:04:38,400
important thing

146
00:04:35,840 --> 00:04:40,320
virtual-8086 mode is a way that they

147
00:04:38,400 --> 00:04:43,360
achieved that

148
00:04:40,320 --> 00:04:44,880
now protected mode overall is important

149
00:04:43,360 --> 00:04:46,479
because it's the place that starts

150
00:04:44,880 --> 00:04:47,520
adding the nice functionality like

151
00:04:46,479 --> 00:04:50,080
virtual memory

152
00:04:47,520 --> 00:04:50,639
and privilege rings the capability to

153
00:04:50,080 --> 00:04:53,840
actually

154
00:04:50,639 --> 00:04:55,520
isolate the kernel from user space

155
00:04:53,840 --> 00:04:57,120
so like I said all modern operating

156
00:04:55,520 --> 00:04:58,639
systems are going to be executing in

157
00:04:57,120 --> 00:05:01,600
protected mode

158
00:04:58,639 --> 00:05:02,479
so from protected mode you could set

159
00:05:01,600 --> 00:05:05,520
some bits

160
00:05:02,479 --> 00:05:06,479
and you would find yourself in ia32e

161
00:05:05,520 --> 00:05:09,520
mode or

162
00:05:06,479 --> 00:05:11,440
long mode so this is the actual

163
00:05:09,520 --> 00:05:12,639
64-bit mode this is the thing we said

164
00:05:11,440 --> 00:05:14,880
has many names

165
00:05:12,639 --> 00:05:17,919
but AMD when they originally created the

166
00:05:14,880 --> 00:05:19,360
x86-64 extensions called it long mode

167
00:05:17,919 --> 00:05:20,960
so that's why on the finite state

168
00:05:19,360 --> 00:05:25,440
machine you'll see LME

169
00:05:20,960 --> 00:05:28,240
long mode enable for the AMD long mode

170
00:05:25,440 --> 00:05:30,960
so intel calls it things in their manual

171
00:05:28,240 --> 00:05:32,639
like ia32e or intel 64. but like we said

172
00:05:30,960 --> 00:05:36,840
in the architecture 1001

173
00:05:32,639 --> 00:05:38,240
class we're just going to call it x86

174
00:05:36,840 --> 00:05:40,560
64. so

175
00:05:38,240 --> 00:05:42,400
from this picture uh we said that you

176
00:05:40,560 --> 00:05:43,280
know real mode and system management

177
00:05:42,400 --> 00:05:46,240
mode are mostly

178
00:05:43,280 --> 00:05:48,639
left for architecture 4001. We said that

179
00:05:46,240 --> 00:05:50,080
virtual-8086 mode is not even real mode

180
00:05:48,639 --> 00:05:52,560
according to intel's own

181
00:05:50,080 --> 00:05:54,000
manuals so if we go ahead and you know

182
00:05:52,560 --> 00:05:57,039
simplify this down

183
00:05:54,000 --> 00:05:59,199
what we end up with is this so you

184
00:05:57,039 --> 00:06:01,039
always start in real mode and you try to

185
00:05:59,199 --> 00:06:01,840
get to protected mode as soon as humanly

186
00:06:01,039 --> 00:06:04,240
possible

187
00:06:01,840 --> 00:06:05,120
and then from there a 64-bit capable

188
00:06:04,240 --> 00:06:07,360
operating system

189
00:06:05,120 --> 00:06:08,160
is going to move its way into 64-bit

190
00:06:07,360 --> 00:06:10,160
mode

191
00:06:08,160 --> 00:06:12,319
and here's our missing little reset

192
00:06:10,160 --> 00:06:14,720
because you will always land back in

193
00:06:12,319 --> 00:06:16,800
real mode whenever you reset.

194
00:06:14,720 --> 00:06:18,400
Now if you like finite state machines

195
00:06:16,800 --> 00:06:18,960
here's another finite state machine for

196
00:06:18,400 --> 00:06:21,759
you

197
00:06:18,960 --> 00:06:22,800
it is the AMD version of the exact same

198
00:06:21,759 --> 00:06:24,880
state machine

199
00:06:22,800 --> 00:06:26,479
and quite frankly I like it better I

200
00:06:24,880 --> 00:06:28,240
feel like it's cleaner I feel like it's

201
00:06:26,479 --> 00:06:28,960
cooler because you know it looks like an

202
00:06:28,240 --> 00:06:31,360
alien

203
00:06:28,960 --> 00:06:33,600
it specifically looks like the aliens

204
00:06:31,360 --> 00:06:35,759
from batteries not included

205
00:06:33,600 --> 00:06:37,120
but my references are old and I should

206
00:06:35,759 --> 00:06:40,400
feel old so

207
00:06:37,120 --> 00:06:42,880
updated just for this class 2018's

208
00:06:40,400 --> 00:06:44,240
black manta so from now on when you see

209
00:06:42,880 --> 00:06:46,080
the AMD

210
00:06:44,240 --> 00:06:47,280
finite state machine covering processor

211
00:06:46,080 --> 00:06:50,479
execution modes

212
00:06:47,280 --> 00:06:52,319
you will see nothing but black manta so

213
00:06:50,479 --> 00:06:52,800
giving this the sort of simplification

214
00:06:52,319 --> 00:06:55,039
that we

215
00:06:52,800 --> 00:06:57,199
gave the other finite state machine

216
00:06:55,039 --> 00:06:59,840
after reset you always land yourself

217
00:06:57,199 --> 00:07:01,680
in real mode setting some sort of bit

218
00:06:59,840 --> 00:07:03,639
which here is a little more specific

219
00:07:01,680 --> 00:07:05,599
that's why I like this picture it says

220
00:07:03,639 --> 00:07:09,280
cr0pe not just pe

221
00:07:05,599 --> 00:07:11,759
one that gets you into protected mode

222
00:07:09,280 --> 00:07:12,560
set some more bits and you're going to

223
00:07:11,759 --> 00:07:15,840
get yourself

224
00:07:12,560 --> 00:07:17,440
into compatibility mode and from there

225
00:07:15,840 --> 00:07:18,720
set some more bits and get yourself into

226
00:07:17,440 --> 00:07:20,240
64-bit mode

227
00:07:18,720 --> 00:07:21,520
so the thing I like about this graph

228
00:07:20,240 --> 00:07:22,319
besides the fact that it's much more

229
00:07:21,520 --> 00:07:24,479
specific

230
00:07:22,319 --> 00:07:26,160
about uh the particular bits that need

231
00:07:24,479 --> 00:07:27,919
to be set in order to transition your

232
00:07:26,160 --> 00:07:29,280
way through the finite state machine

233
00:07:27,919 --> 00:07:31,520
is that it makes it a little more clear

234
00:07:29,280 --> 00:07:34,000
that long mode kind of does have

235
00:07:31,520 --> 00:07:35,520
uh 2 ways of operating there's the

236
00:07:34,000 --> 00:07:36,960
true 64-bit mode

237
00:07:35,520 --> 00:07:39,199
which is what you're going to get when

238
00:07:36,960 --> 00:07:40,639
you're executing a 64-bit application in

239
00:07:39,199 --> 00:07:42,240
a 64-bit kernel

240
00:07:40,639 --> 00:07:43,440
and then there's compatibility mode

241
00:07:42,240 --> 00:07:44,080
which is sort of the backwards

242
00:07:43,440 --> 00:07:47,599
compatible

243
00:07:44,080 --> 00:07:49,520
way of getting 32-bit execution

244
00:07:47,599 --> 00:07:51,360
uh out of the system so you might have a

245
00:07:49,520 --> 00:07:53,360
64-bit kernel but it could

246
00:07:51,360 --> 00:07:54,720
execute your 32-bit program in

247
00:07:53,360 --> 00:07:56,639
compatibility mode

248
00:07:54,720 --> 00:07:58,639
and compatibility mode basically looks

249
00:07:56,639 --> 00:08:02,479
exactly like protected mode

250
00:07:58,639 --> 00:08:05,520
in 32-bit executing processors

251
00:08:02,479 --> 00:08:06,319
so that's it for our processor execution

252
00:08:05,520 --> 00:08:08,080
modes

253
00:08:06,319 --> 00:08:09,599
now we're going to spend a whole bunch

254
00:08:08,080 --> 00:08:11,440
of time figuring out you know what

255
00:08:09,599 --> 00:08:13,199
exactly are all these bits and what do

256
00:08:11,440 --> 00:08:13,759
they mean and how do we get to 64-bit

257
00:08:13,199 --> 00:08:15,360
mode

258
00:08:13,759 --> 00:08:17,919
for an operating system that wants to do

259
00:08:15,360 --> 00:08:17,919
that

